package com.kaamkaaj.kaamkaaj;

/**
 * Created by workcellsolutions on 03/08/16.
 */
public class AppConstants {
  //  public static final String API_URL = "http://139.59.30.24/staging/";
 //   public static final String API_URL = "http://13.127.181.61/kkapi/";
   // public static final String API_URL = "http://52.66.199.106:1337/";
    public static final String API_URL = "http://52.66.199.106:1337/";

    public static final String JOHN_DOE_RANDOM_ID = "john_doe_id";
    public static final String FAILED_ID_MSG = "no user";
    public static final String FONTS_LATO_HEAVY_TTF = "fonts/Roboto-Bold.ttf";
    public static final String FONTS_LATO_MEDIUM_TTF = "fonts/Roboto-Medium.ttf";
    public static final String FONTS_LATO_REGULAR_TTF = "fonts/Roboto-Regular.ttf";

    public static final String LOGOUT_MSG = "invalid_token";
    public static final int MIN_SALARY = 0;
    public static final int MAX_SALARY = 50000;
    public static final int MIN_RANGE = 20;
    public static final int MAX_RANGE = 100;
    public static final String NETWORK_ERROR = "network-error";
    public static final String FROM_CLASS = "from_class";
    public static final String FILTER_SKILLS = "FILTER_SKILLS";
    public static final String FILTER_LOCATION = "LOCA";
    public static final String FILTER_MIN_SALARY = "ENKGGWEG";
    public static final String FILTER_MAX_SALARY = "REGR";
    public static final String FILTER_MIN_EXP = "IJWFAAWOF";
    public static final String FILTER_MAX_EXP = "WEFHWEFJN";
    public static final String FILTER_EDUCATION = "SGSEG";
    public static final String SAVE_FOR_LATER_LIST = "fdmksgkgrggr";

    // SMS provider identification
    // It should match with your SMS gateway origin
    // You can use  MSGIND, TESTER and ALERTS as sender ID
    // If you want custom sender Id, approve MSG91 to get one

    // special character to prefix the otp. Make sure this character appears only once in the sms
    public static final String OTP_DELIMITER = "";

    //Refer : https://developers.google.com/cloud-messaging/gcm#senderid
    //public static final String ANDROID_PROJECT_SENDER_ID = "100689030746";
    public static final String ANDROID_PROJECT_SENDER_ID = "270853949402";

    public static final String USER_TYPE = "user_type";
    public static final String USER_TYPE_MAIN = "user_type_main";
    public static final String USER_JOBSEEKER = "job_seeker";
    public static final String USER_EMPLOYER = "employer";
    public static final String EMPLOYER_TYPE_CORPORATE = "corporate";
    public static final String EMPLOYER_TYPE_INDI = "indi";

    public static final String PREF = "com.kaamkaaj.kaamkaaj";
    public static final String APPLIED_JOBS = "applied_jobs";
    public static final String PHONE_NUMBER = "phone-number";
    public static final String API_TOKEN = "api_token";
    public static final String API_TOKEN_EMPLOYEE = "api_token_employee";
  public static final String NEW_TOKEN_EMPLOYEE = "new_token_employee";
    public static final String EMPLOYER_EMAIL = "employer_email";
    public static final String EMPLOYER_NAME = "employer_name";
    public static final String EMPLOYER_MOBILE = "employer_mobile";
    public static final String USERlOGIN_TYPE = "userlogin_type";
    public static final String FROM_STRING = "from-string";
    //App State
    public static final String STATE = "state";
    public static final String CAN = "can";
    public static final String CAN_POSITION = "can-position";
    public static final int STATE_INITIAL = 0;
    public static final int STATE_ANONYMOUS = 1;
    public static final int STATE_REGISTERED = 2;
    public static final String USER_STATUS = "user_status";
    public static final String APPLIED_JOBS_LIST = "applied_jobs_list";

    public static final String VIEW_TYPE_PROGRESSBAR = "view-type-progressbar";
    public static final String VIEW_TYPE_NORMAL = "view-type-normal";

    public static final String PROMO_STATE = "promo-state";

    public static final String UPDATE_CRITICAL = "Critical";
    public static final String UPDATE_NON_CRITICAL = "Non Critical";
    public static final String UPDATE_MESSAGE_CRITICAL = "Please update app to continue";
    public static final String UPDATE_MESSAGE_NON_CRITICAL = "Please update to newer version";
    public static final String UPDATE_TITLE_CRITICAL = "Critical Update";
    public static final String UPDATE_TITLE_NON_CRITICAL = "New Update";
    public static final String KEY_LATEST_VERSION_AVAILABLE = "key-latest-version-available";
    public static final String KEY_LATEST_VERSION_CODE = "key-latest-version-code";
    public static final String KEY_SKIPPED_VERSION_AVAILABLE = "key-skipped-version-available";
    public static final String KEY_LATEST_VERSION_TYPE = "key-latest-version-type";

    public static final String AGREEMENT_TYPE_POLICY = "policy";
    public static final String AGREEMENT_TYPE_TERMS = "terms";
    public static final String AGREEMENT_TYPE_ABOUT_US = "about-us";
    public static final String KEY_ABOUT_US_CATEGORY = "key-aboutus-category";


    public static final String SEARCH_FRAGMENT = "search-fragment";
    public static final String KEY_DASHBOARD_BANNER_LIST = "key-dashboard-banner-list";
    public static final String KEY_TRANSACTION_TYPE = "key-transaction-type";
    public static final String KEY_TRANSACTION = "key-transaction";

    public static final String KEY_CLIENT = "key-client";
    public static final String KEY_CLIENT_DATA = "key-client-data";
    public static final String KEY_CLIENT_ID = "key-client-id";
    public static final String KEY_CLIENT_REGISTERED = "Registered";
    public static final String KEY_CLIENT_VERIFY = "Verified";
    public static final String KEY_CLIENT_APPLY = "Applied";
    public static final String KEY_CLIENT_ALLOT = "Allot";
    public static final String KEY_CART = "key-cart";


    public static final String KEY_EARNINGS = "key-earnings";
    public static final String KEY_LUMPSUM_PORTFOLIO = "key-lumpsum-portfolio";
    public static final String KEY_SYSTEMATIC_PORTFOLIO = "key-systematic-portfolio";
    public static final String KEY_GOAL_DATA = "key-goal-data";
    public static final String USER_NAME = "user_name";
    public static final String ANONYMOUS_USER_NAME = "John Doe";
    public static final String KEY_USER_MOBILE = "key-user-mobile";
    public static final String KEY_USER_EMAIL_ID = "key-user-email-id";
    public static final String KEY_USER_STATUS = "key-user-status";

    public static final String COLLECTION_DEFAULT_NAME = "default";


    ///// CDSM related




}
