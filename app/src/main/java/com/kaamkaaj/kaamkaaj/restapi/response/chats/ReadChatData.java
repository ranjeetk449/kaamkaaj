package com.kaamkaaj.kaamkaaj.restapi.response.chats;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class ReadChatData {
    public ArrayList<ReadChatList> getChats() {
        return chats;
    }

    public void setChats(ArrayList<ReadChatList> chats) {
        this.chats = chats;
    }

    ArrayList<ReadChatList> chats;
}
