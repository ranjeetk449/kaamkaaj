package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class MyJobListData {

    public ArrayList<MyJobList> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<MyJobList> jobs) {
        this.jobs = jobs;
    }

    ArrayList<MyJobList> jobs = null;

}
