package com.kaamkaaj.kaamkaaj.restapi.response.login;

/**
 * Created by akshaysingh on 4/26/18.
 */

public class FetchProfileData {
    String status;
    UserProfile userProfile;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
