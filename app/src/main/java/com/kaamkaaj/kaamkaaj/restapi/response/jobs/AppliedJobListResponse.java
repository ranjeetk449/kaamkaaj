package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/7/18.
 */

public class AppliedJobListResponse extends BaseResponse{
    public AppliedJobListData getData() {
        return data;
    }

    public void setData(AppliedJobListData data) {
        this.data = data;
    }

    AppliedJobListData data;

}
