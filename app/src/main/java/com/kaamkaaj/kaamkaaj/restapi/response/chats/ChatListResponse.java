package com.kaamkaaj.kaamkaaj.restapi.response.chats;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class ChatListResponse extends BaseResponse {
    public ChatListData getData() {
        return data;
    }

    public void setData(ChatListData data) {
        this.data = data;
    }

    ChatListData data;
}
