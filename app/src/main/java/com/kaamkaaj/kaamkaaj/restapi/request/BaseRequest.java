package com.kaamkaaj.kaamkaaj.restapi.request;

/**
 * Created by Bhanushali on 12/21/2016.
 */

public class BaseRequest {
    public String token;

    public String randomId;
    public String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
