package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 5/18/18.
 */

public class EmployerProfileData {
    public String status;
    public EmployerUserProfile userProfile;
}
