package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class MyJobListResponse extends BaseResponse {


    public MyJobListData getData() {
        return data;
    }

    public void setData(MyJobListData data) {
        this.data = data;
    }

    MyJobListData data;

}
