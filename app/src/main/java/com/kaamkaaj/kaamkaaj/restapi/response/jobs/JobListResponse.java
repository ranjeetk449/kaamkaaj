package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class JobListResponse extends BaseResponse {
    public JobListData getData() {
        return data;
    }

    public void setData(JobListData data) {
        this.data = data;
    }

    JobListData data;

}
