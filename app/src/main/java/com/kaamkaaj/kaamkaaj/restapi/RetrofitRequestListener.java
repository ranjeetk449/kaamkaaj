package com.kaamkaaj.kaamkaaj.restapi;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import retrofit2.Call;

/**
 * Created by Bhanushali on 12/21/2016.
 */

public abstract class RetrofitRequestListener<T extends BaseResponse> implements retrofit2.Callback<T> {



    private final Call<T> call;


    public RetrofitRequestListener(Call<T> call) {
        this.call = call;
        onStartApi();
    }



    protected void onStartApi() {
    }



    protected void onEndApi() {

    }
    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onEndApi();
        onError(call, t);
    }

    @Override
    public void onResponse(Call<T> call, retrofit2.Response<T> response) {
        onEndApi();
        if (response.isSuccessful()) {
            try {
                if (response.body().status.response.equalsIgnoreCase("success") || response.body().status.response.equalsIgnoreCase("sucess")) {
                    onSuccess(call, response.body());
                } else {
                    onCustomError(call, response.body());
                }
            } catch (Exception e) {
                onError(call, e);
            }
        } else {
            onError(call, new Exception());
        }

    }

    public abstract void onSuccess(Call<T> call, T response);

    public abstract void onError(Call<T> call, Throwable t);

    public abstract void onCustomError(Call<T> call, T response);

    public void retry() {
        onStartApi();
        call.clone().enqueue(this);
    }
}