package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/17/18.
 */

public class EmployerJobListRequest extends BaseRequest {
    public int page;
}
