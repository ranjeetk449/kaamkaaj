package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/21/18.
 */

public class DeleteJobRequest extends BaseRequest {
    public String id;
}
