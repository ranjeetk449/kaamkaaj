package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 6/19/18.
 */

public class FetchSavedForLaterCandidateRequest extends BaseRequest {
    public String page;
}
