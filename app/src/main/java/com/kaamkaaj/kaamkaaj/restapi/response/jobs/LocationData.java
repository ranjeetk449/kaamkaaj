package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/12/18.
 */

public class LocationData {
    public ArrayList<LocationList> getLocationList() {
        return locations;
    }

    public void setLocationList(ArrayList<LocationList> locationList) {
        this.locations = locationList;
    }

    ArrayList<LocationList> locations;
}
