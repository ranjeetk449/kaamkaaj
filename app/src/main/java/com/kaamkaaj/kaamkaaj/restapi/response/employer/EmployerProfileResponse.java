package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 5/18/18.
 */

public class EmployerProfileResponse extends BaseResponse {
    public EmployerProfileData getData() {
        return data;
    }

    public void setData(EmployerProfileData data) {
        this.data = data;
    }

    EmployerProfileData data;
}
