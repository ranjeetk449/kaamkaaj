package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

/**
 * Created by akshaysingh on 6/21/18.
 */

public class MyChatList {
    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    String fromUser, text, timestamp;
}
