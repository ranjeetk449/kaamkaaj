package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicantsListResponse extends BaseResponse {
    public JobApplicantsListData getData() {
        return data;
    }

    public void setData(JobApplicantsListData data) {
        this.data = data;
    }

    JobApplicantsListData data;
}
