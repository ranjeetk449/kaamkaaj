package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class JobList implements Parcelable {


        String id, selected, created;
    String jobTitle, name, address,jobDescription;
    String minSalary;
    String maxSalary;
    String minExp;

    String maxExp;
    String lng;
    String lat;

    protected JobList(Parcel in) {
        id = in.readString();
        selected = in.readString();
        created = in.readString();
        jobTitle = in.readString();
        name = in.readString();
        address = in.readString();
        jobDescription = in.readString();
        minSalary = in.readString();
        maxSalary = in.readString();
        minExp = in.readString();
        maxExp = in.readString();
        lng = in.readString();
        lat = in.readString();
        custom_map_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(selected);
        dest.writeString(created);
        dest.writeString(jobTitle);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(jobDescription);
        dest.writeString(minSalary);
        dest.writeString(maxSalary);
        dest.writeString(minExp);
        dest.writeString(maxExp);
        dest.writeString(lng);
        dest.writeString(lat);
        dest.writeString(custom_map_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobList> CREATOR = new Creator<JobList>() {
        @Override
        public JobList createFromParcel(Parcel in) {
            return new JobList(in);
        }

        @Override
        public JobList[] newArray(int size) {
            return new JobList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getJob_title() {
        return jobTitle;
    }

    public void setJob_title(String job_title) {
        this.jobTitle = job_title;
    }

    public String getCompany_name() {
        return name;
    }

    public void setCompany_name(String company_name) {
        this.name = company_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob_description() {
        return jobDescription;
    }

    public void setJob_description(String job_description) {
        this.jobDescription = job_description;
    }

    public String getMin_salary() {
        return minSalary;
    }

    public void setMin_salary(String min_salary) {
        this.minSalary = min_salary;
    }

    public String getMax_salary() {
        return maxSalary;
    }

    public void setMax_salary(String max_salary) {
        this.maxSalary = max_salary;
    }

    public String getMin_exp() {
        return minExp;
    }

    public void setMin_exp(String min_exp) {
        this.minExp = min_exp;
    }

    public String getMax_emp() {
        return maxExp;
    }

    public void setMax_emp(String max_emp) {
        this.maxExp = max_emp;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getCustom_map_id() {
        return custom_map_id;
    }

    public void setCustom_map_id(String custom_map_id) {
        this.custom_map_id = custom_map_id;
    }

    String custom_map_id;



    public JobList() {

    }


}
