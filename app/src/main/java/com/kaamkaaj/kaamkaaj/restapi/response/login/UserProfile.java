package com.kaamkaaj.kaamkaaj.restapi.response.login;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 4/20/18.
 */

public class UserProfile implements Parcelable{

    String mobile_no;
    String full_name;
    String fathers_name;
    String gender;
    String dob;
    String current_address;
    String permanent_address;
    String salary;
    String created;
    String status;
    String experience;
    String skill;
    String sub_skill;
    String city_name, lat, lng;
    String job_type;
    String id_proof_doc;

    protected UserProfile(Parcel in) {
        mobile_no = in.readString();
        full_name = in.readString();
        fathers_name = in.readString();
        gender = in.readString();
        dob = in.readString();
        current_address = in.readString();
        permanent_address = in.readString();
        salary = in.readString();
        created = in.readString();
        status = in.readString();
        experience = in.readString();
        skill = in.readString();
        sub_skill = in.readString();
        city_name = in.readString();
        lat = in.readString();
        lng = in.readString();
        job_type = in.readString();
        id_proof_doc = in.readString();
        doc_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mobile_no);
        dest.writeString(full_name);
        dest.writeString(fathers_name);
        dest.writeString(gender);
        dest.writeString(dob);
        dest.writeString(current_address);
        dest.writeString(permanent_address);
        dest.writeString(salary);
        dest.writeString(created);
        dest.writeString(status);
        dest.writeString(experience);
        dest.writeString(skill);
        dest.writeString(sub_skill);
        dest.writeString(city_name);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(job_type);
        dest.writeString(id_proof_doc);
        dest.writeString(doc_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getFathers_name() {
        return fathers_name;
    }

    public void setFathers_name(String fathers_name) {
        this.fathers_name = fathers_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrent_address() {
        return current_address;
    }

    public void setCurrent_address(String current_address) {
        this.current_address = current_address;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getSub_skill() {
        return sub_skill;
    }

    public void setSub_skill(String sub_skill) {
        this.sub_skill = sub_skill;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getJob_type() {
        return job_type;
    }

    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    public String getId_proof_doc() {
        return id_proof_doc;
    }

    public void setId_proof_doc(String id_proof_doc) {
        this.id_proof_doc = id_proof_doc;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    String doc_id;
}
