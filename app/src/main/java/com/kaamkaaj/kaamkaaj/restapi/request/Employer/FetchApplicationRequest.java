package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/22/18.
 */

public class FetchApplicationRequest extends BaseRequest {
    public String job_id;
    public int status;
}
