package com.kaamkaaj.kaamkaaj.restapi.response.chats;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class EmployeeReadChatList {
    String id;
    String js_user_id;
    String cl_user_id;
    String chat_by;
    String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJs_user_id() {
        return js_user_id;
    }

    public void setJs_user_id(String js_user_id) {
        this.js_user_id = js_user_id;
    }

    public String getCl_user_id() {
        return cl_user_id;
    }

    public void setCl_user_id(String cl_user_id) {
        this.cl_user_id = cl_user_id;
    }

    public String getChat_by() {
        return chat_by;
    }

    public void setChat_by(String chat_by) {
        this.chat_by = chat_by;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    String created;
}
