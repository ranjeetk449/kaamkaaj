package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 7/9/18.
 */

public class JobGeoResponse implements Parcelable{
    String lat, lng, name;

    String id, selected, created;
    String job_title, company_name, address,job_description;
    int min_salary;
    int max_salary;

    protected JobGeoResponse(Parcel in) {
        lat = in.readString();
        lng = in.readString();
        name = in.readString();
        id = in.readString();
        selected = in.readString();
        created = in.readString();
        job_title = in.readString();
        company_name = in.readString();
        address = in.readString();
        job_description = in.readString();
        min_salary = in.readInt();
        max_salary = in.readInt();
        min_exp = in.readInt();
    }

    public static final Creator<JobGeoResponse> CREATOR = new Creator<JobGeoResponse>() {
        @Override
        public JobGeoResponse createFromParcel(Parcel in) {
            return new JobGeoResponse(in);
        }

        @Override
        public JobGeoResponse[] newArray(int size) {
            return new JobGeoResponse[size];
        }
    };

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob_description() {
        return job_description;
    }

    public void setJob_description(String job_description) {
        this.job_description = job_description;
    }

    public int getMin_salary() {
        return min_salary;
    }

    public void setMin_salary(int min_salary) {
        this.min_salary = min_salary;
    }

    public int getMax_salary() {
        return max_salary;
    }

    public void setMax_salary(int max_salary) {
        this.max_salary = max_salary;
    }

    public int getMin_exp() {
        return min_exp;
    }

    public void setMin_exp(int min_exp) {
        this.min_exp = min_exp;
    }

    int min_exp;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(lat);
        parcel.writeString(lng);
        parcel.writeString(name);
        parcel.writeString(id);
        parcel.writeString(selected);
        parcel.writeString(created);
        parcel.writeString(job_title);
        parcel.writeString(company_name);
        parcel.writeString(address);
        parcel.writeString(job_description);
        parcel.writeInt(min_salary);
        parcel.writeInt(max_salary);
        parcel.writeInt(min_exp);
    }
}
