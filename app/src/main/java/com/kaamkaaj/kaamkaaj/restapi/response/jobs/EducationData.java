package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/12/18.
 */

public class EducationData {
    ArrayList<EducationList> educations;

    public ArrayList<EducationList> getEducationLists() {
        return educations;
    }

    public void setEducationLists(ArrayList<EducationList> educationLists) {
        this.educations = educationLists;
    }
}
