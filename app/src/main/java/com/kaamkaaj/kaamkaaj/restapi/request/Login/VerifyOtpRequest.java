package com.kaamkaaj.kaamkaaj.restapi.request.Login;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class VerifyOtpRequest extends BaseRequest {
    public String mobileNo, otp;
}
