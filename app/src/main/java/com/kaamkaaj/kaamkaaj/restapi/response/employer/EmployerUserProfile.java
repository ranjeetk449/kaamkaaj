package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 5/21/18.
 */

public class EmployerUserProfile implements Parcelable {


    String address;



    String designation;
    String first_name;
    String last_name;
    String email_id;

    String mobile_no;
    String company_address;
    String company_name;

    protected EmployerUserProfile(Parcel in) {
        address = in.readString();
        designation = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email_id = in.readString();
        mobile_no = in.readString();
        company_address = in.readString();
        company_name = in.readString();
        company_website = in.readString();
    }

    public static final Creator<EmployerUserProfile> CREATOR = new Creator<EmployerUserProfile>() {
        @Override
        public EmployerUserProfile createFromParcel(Parcel in) {
            return new EmployerUserProfile(in);
        }

        @Override
        public EmployerUserProfile[] newArray(int size) {
            return new EmployerUserProfile[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_website() {
        return company_website;
    }

    public void setCompany_website(String company_website) {
        this.company_website = company_website;
    }

    String company_website;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(address);
        parcel.writeString(designation);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeString(email_id);
        parcel.writeString(mobile_no);
        parcel.writeString(company_address);
        parcel.writeString(company_name);
        parcel.writeString(company_website);
    }
}
