package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/7/18.
 */

public class AppliedJobList implements Parcelable {
    String id;
    String jobTitle, name, address,jobDescription;
    String minSalary;
    String maxSalary;
    String minExp, maxExp;

    String education, job_validity, opening, created, posted_date, status, applied_on, application_status;

    protected AppliedJobList(Parcel in) {
        id = in.readString();
        jobTitle = in.readString();
        name = in.readString();
        address = in.readString();
        jobDescription = in.readString();
        minSalary = in.readString();
        maxSalary = in.readString();
        minExp = in.readString();
        maxExp = in.readString();
        education = in.readString();
        job_validity = in.readString();
        opening = in.readString();
        created = in.readString();
        posted_date = in.readString();
        status = in.readString();
        applied_on = in.readString();
        application_status = in.readString();
        skill = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(jobTitle);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(jobDescription);
        dest.writeString(minSalary);
        dest.writeString(maxSalary);
        dest.writeString(minExp);
        dest.writeString(maxExp);
        dest.writeString(education);
        dest.writeString(job_validity);
        dest.writeString(opening);
        dest.writeString(created);
        dest.writeString(posted_date);
        dest.writeString(status);
        dest.writeString(applied_on);
        dest.writeString(application_status);
        dest.writeString(skill);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppliedJobList> CREATOR = new Creator<AppliedJobList>() {
        @Override
        public AppliedJobList createFromParcel(Parcel in) {
            return new AppliedJobList(in);
        }

        @Override
        public AppliedJobList[] newArray(int size) {
            return new AppliedJobList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJob_title() {
        return jobTitle;
    }

    public void setJob_title(String job_title) {
        this.jobTitle = job_title;
    }

    public String getCompany_name() {
        return name;
    }

    public void setCompany_name(String company_name) {
        this.name = company_name;
    }

    public String getAddress() {
        return address;
    }

    public void setLocation(String address) {
        this.address = address;
    }

    public String getJob_description() {
        return jobDescription;
    }

    public void setJob_description(String job_description) {
        this.jobDescription = job_description;
    }

    public String getMin_salary() {
        return minSalary;
    }

    public void setMin_salary(String min_salary) {
        this.minSalary = min_salary;
    }

    public String getMax_salary() {
        return maxSalary;
    }

    public void setMax_salary(String max_salary) {
        this.maxSalary = max_salary;
    }

    public String getMin_exp() {
        return minExp;
    }

    public void setMin_exp(String min_exp) {
        this.minExp = min_exp;
    }

    public String getMax_exp() {
        return maxExp;
    }

    public void setMax_exp(String max_exp) {
        this.maxExp = max_exp;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getJob_validity() {
        return job_validity;
    }

    public void setJob_validity(String job_validity) {
        this.job_validity = job_validity;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getPosted_date() {
        return posted_date;
    }

    public void setPosted_date(String posted_date) {
        this.posted_date = posted_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApplied_on() {
        return applied_on;
    }

    public void setApplied_on(String applied_on) {
        this.applied_on = applied_on;
    }

    public String getApplication_status() {
        return application_status;
    }

    public void setApplication_status(String application_status) {
        this.application_status = application_status;
    }


    public String getSkill() {
        return skill;
    }

    public void setSkills(String skills) {
        this.skill = skill;
    }

    String skill;

}
