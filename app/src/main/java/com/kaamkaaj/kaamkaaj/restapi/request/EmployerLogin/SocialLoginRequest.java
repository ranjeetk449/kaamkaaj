package com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 6/15/18.
 */

public class SocialLoginRequest extends BaseRequest {
   public String firstName, email, source;
}