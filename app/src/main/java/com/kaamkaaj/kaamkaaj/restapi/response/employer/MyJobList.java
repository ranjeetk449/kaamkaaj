package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class MyJobList implements Parcelable {


    public MyJobList() {

    }

    String id;
    String selected;
    String jobTitle;
    String companyName;
    String address;
    String createdAt;
    String lat, lng, name;
    String skill;
    String opening;
    String jobDescription;
    String minSalary;
    String maxSalary;

    public String getContactVisible() {
        return contactVisible;
    }

    public void setContactVisible(String contactVisible) {
        this.contactVisible = contactVisible;
    }

    String contactVisible;

    public String getJobValidity() {
        return jobValidity;
    }

    public void setJobValidity(String jobValidity) {
        this.jobValidity = jobValidity;
    }

    String jobValidity;
    int minExp;
    int maxExp;
    String appliedEmployeeCount;

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    String jobType;


    protected MyJobList(Parcel in) {
        id = in.readString();
        selected = in.readString();
        jobTitle = in.readString();
        companyName = in.readString();
        jobValidity = in.readString();
        address = in.readString();
        createdAt = in.readString();
        lat = in.readString();
        lng = in.readString();
        name = in.readString();
        skill = in.readString();
        opening = in.readString();
        jobDescription = in.readString();
        minSalary = in.readString();
        maxSalary = in.readString();
        minExp = in.readInt();
        maxExp = in.readInt();
        appliedEmployeeCount = in.readString();
        jobType = in.readString();
        contactVisible = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(selected);
        dest.writeString(jobTitle);
        dest.writeString(companyName);
        dest.writeString(jobValidity);
        dest.writeString(address);
        dest.writeString(createdAt);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(name);
        dest.writeString(skill);
        dest.writeString(opening);
        dest.writeString(jobDescription);
        dest.writeString(minSalary);
        dest.writeString(maxSalary);
        dest.writeInt(minExp);
        dest.writeInt(maxExp);
        dest.writeString(appliedEmployeeCount);
        dest.writeString(jobType);
        dest.writeString(contactVisible);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyJobList> CREATOR = new Creator<MyJobList>() {
        @Override
        public MyJobList createFromParcel(Parcel in) {
            return new MyJobList(in);
        }

        @Override
        public MyJobList[] newArray(int size) {
            return new MyJobList[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated() {
        return createdAt;
    }

    public void setCreated(String created) {
        this.createdAt = created;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getOpening() {
        return opening;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(String minSalary) {
        this.minSalary = minSalary;
    }

    public String getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(String maxSalary) {
        this.maxSalary = maxSalary;
    }

    public int getMinExp() {
        return minExp;
    }

    public void setMinExp(int minExp) {
        this.minExp = minExp;
    }

    public int getMaxExp() {
        return maxExp;
    }

    public void setMaxExp(int maxExp) {
        this.maxExp = maxExp;
    }

    public String getNumApplicants() {
        return appliedEmployeeCount;
    }

    public void setNumApplicants(String appliedEmployees) {
        this.appliedEmployeeCount = appliedEmployees;
    }




}
