package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class UserProfileEmployer implements Parcelable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    protected UserProfileEmployer(Parcel in) {
        name = in.readString();
        email = in.readString();
    }

    public static final Creator<UserProfileEmployer> CREATOR = new Creator<UserProfileEmployer>() {
        @Override
        public UserProfileEmployer createFromParcel(Parcel in) {
            return new UserProfileEmployer(in);
        }

        @Override
        public UserProfileEmployer[] newArray(int size) {
            return new UserProfileEmployer[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String email;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(email);
    }
}
