package com.kaamkaaj.kaamkaaj.restapi.response.chats;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class EmployeeChatList {
    String id;
    String mobile_no;
    String chat_by;
    String employeeFirstName;
    String employee_id;
    String employerFirstName;
    String employerMobileNo;
    String employer_id;
    String createdAt;
    String updatedAt;

    public String getChat_by() {
        return chat_by;
    }

    public void setChat_by(String chat_by) {
        this.chat_by = chat_by;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public void setEmployeeFirstName(String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployerFirstName() {
        return employerFirstName;
    }

    public void setEmployerFirstName(String employerFirstName) {
        this.employerFirstName = employerFirstName;
    }

    public String getEmployerMobileNo() {
        return employerMobileNo;
    }

    public void setEmployerMobileNo(String employerMobileNo) {
        this.employerMobileNo = employerMobileNo;
    }

    public String getEmployer_id() {
        return employer_id;
    }

    public void setEmployer_id(String employer_id) {
        this.employer_id = employer_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getFull_name() {
        return firstName;
    }

    public void setFull_name(String firstName) {
        this.firstName = firstName;
    }

    String firstName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String message;
}
