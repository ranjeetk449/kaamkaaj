package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/12/18.
 */

public class LocationResponse extends BaseResponse {
    public LocationData getData() {
        return data;
    }

    public void setData(LocationData data) {
        this.data = data;
    }

    LocationData data;
}
