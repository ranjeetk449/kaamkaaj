package com.kaamkaaj.kaamkaaj.restapi.response.employer;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class EmployerLoginData {
    String token;
    UserProfileEmployer userProfile;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserProfileEmployer getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfileEmployer userProfile) {
        this.userProfile = userProfile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;
}
