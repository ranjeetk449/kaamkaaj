package com.kaamkaaj.kaamkaaj.restapi.request.Job;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicantsListRequest extends BaseRequest {
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    String jobId;
}
