package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/21/18.
 */

public class EmployerProfileUpdateRequest extends BaseRequest {
    public String address;
    public String designation;
    public String first_name;
    public String last_name;
    public String email_id;
    public String mobileNo;
    public String company_address;
    public String company_name;
    public String company_website;
}
