package com.kaamkaaj.kaamkaaj.restapi.request.chats;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 7/3/18.
 */

public class DeleteChatRequest extends BaseRequest {
   // public String js_user_id, cl_user_id,chat_by ;
    public String employer_id, employee_id,chat_by ;
}
