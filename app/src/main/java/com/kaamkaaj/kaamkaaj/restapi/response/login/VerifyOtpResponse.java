package com.kaamkaaj.kaamkaaj.restapi.response.login;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class VerifyOtpResponse extends BaseResponse {
    VerifyOtpData data;

    public VerifyOtpData getData() {
        return data;
    }

    public void setData(VerifyOtpData data) {
        this.data = data;
    }
}
