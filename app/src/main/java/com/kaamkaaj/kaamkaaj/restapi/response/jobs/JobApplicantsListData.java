package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicantsListData {
    public ArrayList<JobApplicant> getApplicants() {
        return appliedEmployees;
    }

    public void setApplicants(ArrayList<JobApplicant> applicants) {
        this.appliedEmployees = applicants;
    }

    ArrayList<JobApplicant> appliedEmployees;
}
