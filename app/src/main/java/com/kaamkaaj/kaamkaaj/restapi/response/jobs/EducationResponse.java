package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/12/18.
 */

public class EducationResponse extends BaseResponse {
    public EducationData getData() {
        return data;
    }

    public void setData(EducationData data) {
        this.data = data;
    }

    EducationData data;
}
