package com.kaamkaaj.kaamkaaj.restapi;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.protocol.HTTP;

public class HttpClient {
    private String boundary = ("SwA" + Long.toString(System.currentTimeMillis()) + "SwA");
    private HttpURLConnection con;
    private String delimiter = "--";
    private OutputStream os;
    private String url;

    public HttpClient(String url) {
        this.url = url;
    }

    public byte[] downloadImage(String imgName) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            System.out.println("URL [" + this.url + "] - Name [" + imgName + "]");
            HttpURLConnection con = (HttpURLConnection) new URL(this.url).openConnection();
            con.setRequestMethod(HttpPost.METHOD_NAME);
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();
            con.getOutputStream().write(("name=" + imgName).getBytes());
            InputStream is = con.getInputStream();
            byte[] b = new byte[1024];
            while (is.read(b) != -1) {
                baos.write(b);
            }
            con.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return baos.toByteArray();
    }

    public void connectForMultipart() throws Exception {
        this.con = (HttpURLConnection) new URL(this.url).openConnection();
        this.con.setRequestMethod(HttpPost.METHOD_NAME);
        this.con.setDoInput(true);
        this.con.setDoOutput(true);
        this.con.setRequestProperty("Connection", HTTP.CONN_KEEP_ALIVE);
        this.con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + this.boundary);
        this.con.setRequestProperty(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
        this.con.connect();
        this.os = this.con.getOutputStream();
    }

    public void addFormPart(String paramName, String value) throws Exception {
        writeParamData(paramName, value);
    }

    public void addFilePart(String paramName, String fileName, byte[] data) throws Exception {
        this.os.write((this.delimiter + this.boundary + "\r\n").getBytes());
        this.os.write(("Content-Disposition: form-data; name=\"" + paramName + "\"; filename=\"" + fileName + "\"\r\n").getBytes());
        this.os.write("Content-Type: application/octet-stream\r\n".getBytes());
        this.os.write("Content-Transfer-Encoding: binary\r\n".getBytes());
        this.os.write("\r\n".getBytes());
        this.os.write(data);
        this.os.write("\r\n".getBytes());
    }

    public void finishMultipart() throws Exception {
        this.os.write((this.delimiter + this.boundary + this.delimiter + "\r\n").getBytes());
    }

    public String getResponse() throws Exception {
        InputStream is = this.con.getInputStream();
        byte[] b1 = new byte[1024];
        StringBuffer buffer = new StringBuffer();
        while (is.read(b1) != -1) {
            buffer.append(new String(b1));
        }
        this.con.disconnect();
        return buffer.toString();
    }

    private void writeParamData(String paramName, String value) throws Exception {
        this.os.write((this.delimiter + this.boundary + "\r\n").getBytes());
        this.os.write("Content-Type: text/plain\r\n".getBytes());
        this.os.write(("Content-Disposition: form-data; name=\"" + paramName + "\"\r\n").getBytes());
        this.os.write(("\r\n" + value + "\r\n").getBytes());
    }
}
