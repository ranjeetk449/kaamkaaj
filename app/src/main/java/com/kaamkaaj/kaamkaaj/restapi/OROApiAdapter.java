package com.kaamkaaj.kaamkaaj.restapi;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.DeleteJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.DeleteSavedCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerJobListRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerJobUpdateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerProfileUpdateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.FetchApplicationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.FetchSavedForLaterCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.JobPostRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.SaveForLaterCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.SearchCandidatesRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.UpdateJobApplicantsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.EmployerLoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.EmployerRegistrationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.ForgetPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.SetNewPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.SocialLoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.VerifyOtpForgetPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.AppliedJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.ApplyJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchEducationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchLocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSkillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.JobGeolocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.JobRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.ChangeNumberRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.FetchProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.LoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.UpdateNewNumberRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.UpdateProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.VerifyOtpRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.ChatListRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.DeleteChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeChatListRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeDeleteChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeNewChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeReadChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeSendChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.NewChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.ReadChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.SendChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ChatListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.EmployeeChatListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.EmployeeReadChatResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ReadChatResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.CandidateSearchResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerLoginResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerProfileResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.MyJobListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.AppliedJobListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.EducationResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobApplicantsListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSubSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.LocationResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.login.FetchProfileResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.login.UpdateNewNumberReponse;
import com.kaamkaaj.kaamkaaj.restapi.response.login.VerifyOtpResponse;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;



public class OROApiAdapter {


    private static OROApiService service;


    public static OROApiService getService() {
        if (service == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            service = retrofit.create(OROApiService.class);
            return service;
        }
        return service;
    }

    public interface OROApiService {

        @POST("employee/login")
        Call<BaseResponse> loginRequest(@Body LoginRequest loginRequest );
        @POST("employee/verifyOtp")
        Call<VerifyOtpResponse> verifyOtpRequest(@Body VerifyOtpRequest verifyOtpRequest);

        @POST("job/jobList")
        Call<JobListResponse> jobRequest(@Body JobRequest jobRequest);

        @POST("employee/fetchProfile")
        Call<FetchProfileResponse> fetchProfileRequest(@Body FetchProfileRequest fetchProfileRequest);
        @POST("employee/updateProfile")
        Call<BaseResponse> updateProfileRequest(@Body UpdateProfileRequest updateProfileRequest);
        @POST("employer/register")
        Call<BaseResponse> employerRegistration(@Body EmployerRegistrationRequest employerRegistrationRequest);
        @POST("employer/login")
        Call<EmployerLoginResponse> employerLogin (@Body EmployerLoginRequest employerLoginRequest);
        @POST("job/postJob")
        Call<BaseResponse> jobJob (@Body JobPostRequest jobPostRequest);
        @POST("job/myJobs")
        Call<MyJobListResponse> myJobListRequest (@Body EmployerJobListRequest employerJobListRequest);
        @POST("job/updateJob")
        Call<BaseResponse> updateJob (@Body EmployerJobUpdateRequest employerJobUpdateRequest);
        @POST("employer/fetchProfile")
        Call<EmployerProfileResponse> fetchProfile (@Body EmployerProfileRequest employerProfileRequest);
        @POST("employer/updateProfile")
        Call<BaseResponse> updateProfileEmployer(@Body EmployerProfileUpdateRequest employerProfileUpdateRequest);
        @POST("employee/search")
        Call<CandidateSearchResponse> searchCandidate(@Body SearchCandidatesRequest searchCandidatesRequest);
        @POST("job/deleteJob")
        Call<BaseResponse> deleteJob(@Body DeleteJobRequest deleteJobRequest);
        @POST("employer/fetchAppliedEmployees")
        Call<JobApplicantsListResponse> fetchApplications(@Body FetchApplicationRequest fetchApplicationRequest);
        @POST("employer/updateAppliedEmployee")
        Call<BaseResponse> updateApplication(@Body UpdateJobApplicantsRequest updateJobApplicantsRequest);
        @POST("employee/applyJob")
        Call<BaseResponse> applyJob(@Body ApplyJobRequest applyJobRequest);

        @POST("employee/fetchAppliedJobs")
        Call<AppliedJobListResponse> fetchAppliedJobs(@Body AppliedJobRequest appliedJobRequest);
        @POST("skill/skills")
        Call<JobSkillsResponse> fetchSkills(@Body FetchSkillsRequest fetchSkillsRequest);
        @POST("master/masterLocations")
        Call<LocationResponse> fetchLocation(@Body FetchLocationRequest fetchLocationRequest);
        @POST("/master/masterEducations")
        Call<EducationResponse> fetchEducation(@Body FetchEducationRequest fetchEducationRequest);

        @POST("subskill/subskills")
        Call<JobSubSkillsResponse> fetchSubSkills(@Body FetchSubskillsRequest fetchSubskillsRequest);
        @POST("employer/loginWithSocial")
        Call<EmployerLoginResponse> loginWithSocial(@Body SocialLoginRequest socialLoginRequest);
        @POST("employer/saveCandidate")
        Call<BaseResponse> saveCandidate(@Body SaveForLaterCandidateRequest saveForLaterCandidateRequest);
       // @POST("employers/:version/searchs/deleteSaveCandidate")
        @POST("employer/deleteSavedCandidate")
        Call<BaseResponse> deleteCandidate (@Body DeleteSavedCandidateRequest deleteSavedCandidateRequest);
        @POST("employer/fetchSavedCandidate")
        Call<CandidateSearchResponse> fetchSaved (@Body FetchSavedForLaterCandidateRequest fetchSavedForLaterCandidateRequest);
        @POST("employers/:version/users/forgetPassword")
        Call<BaseResponse> forgotPassword (@Body ForgetPasswordRequest forgetPasswordRequest);
        @POST("employers/:version/users/verifyOtp")
        Call<BaseResponse> verifyOtpPassword (@Body VerifyOtpForgetPasswordRequest forgetPasswordRequest);
        @POST("employers/:version/users/updatePassword")
        Call<EmployerLoginResponse> updatePassword (@Body SetNewPasswordRequest setNewPasswordRequest);
        @POST(":version/users/changeNumber")
        Call<BaseResponse> changeNumber(@Body ChangeNumberRequest changeNumberRequest);
        @POST(":version/users/updateNewNumber")
        Call<UpdateNewNumberReponse> updateNewNumber(@Body UpdateNewNumberRequest updateNewNumberRequest);


        @POST("chat/start")
        Call<BaseResponse> insertChat (@Body SendChatRequest sendChatRequest);
        @POST("chat/newMessage")
        Call<BaseResponse> insertChatEmployee (@Body SendChatRequest sendChatRequest);
        @POST("chat/newMessage")
        Call<BaseResponse> insertChatEmployer (@Body SendChatRequest sendChatRequest);
        @POST("chat/readChat")
        Call<ReadChatResponse> readChat(@Body ReadChatRequest readChatRequest);
        @POST("chat/newMessage")
        Call<ReadChatResponse>  newChat(@Body NewChatRequest newChatRequest);
        @POST("chat/chatListEmployer")
        Call<ChatListResponse> chatListEmployer(@Body ChatListRequest chatListRequest);
        @POST("chat/deleteChat")
        Call<BaseResponse> deleteChat(@Body DeleteChatRequest deleteChatRequest);



        @POST("chat/start")
        Call<BaseResponse> insertChatEmployee (@Body EmployeeSendChatRequest sendChatRequest);
        @POST("chat/readChat")
        Call<EmployeeReadChatResponse> readChatEmployee(@Body EmployeeReadChatRequest readChatRequest);
        @POST("chat/newMessage")
        Call<EmployeeReadChatResponse>  newChatEmployee(@Body EmployeeNewChatRequest newChatRequest);
        @POST("chat/chatListEmployee")
        Call<EmployeeChatListResponse> chatListEmployee(@Body EmployeeChatListRequest chatListRequest);
        @POST("chat/deleteChat")
        Call<BaseResponse> deleteChatEmployee(@Body EmployeeDeleteChatRequest deleteChatRequest);




        @POST("job/jobListWithRadius")
        Call<JobListResponse> jobListWithRadius(@Body JobGeolocationRequest jobGeolocationRequest);
    }
}
