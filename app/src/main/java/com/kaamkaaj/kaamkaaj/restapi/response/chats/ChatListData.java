package com.kaamkaaj.kaamkaaj.restapi.response.chats;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class ChatListData {
    public ArrayList<ChatList> getChats() {
        return chats;
    }

    public void setChats(ArrayList<ChatList> chats) {
        this.chats = chats;
    }

    ArrayList<ChatList> chats;
}
