package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/8/18.
 */

public class JobSkillsResponse extends BaseResponse {
    public JobSkillsData getData() {
        return data;
    }

    public void setData(JobSkillsData data) {
        this.data = data;
    }

    JobSkillsData data;
}
