package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/8/18.
 */

public class JobSubSkillsResponse extends BaseResponse {
    public JobSubSkillsData getData() {
        return data;
    }

    public void setData(JobSubSkillsData data) {
        this.data = data;
    }

    JobSubSkillsData data;
}
