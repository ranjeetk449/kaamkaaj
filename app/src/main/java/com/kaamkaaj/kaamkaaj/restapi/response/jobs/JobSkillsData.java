package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/8/18.
 */

public class JobSkillsData {
    public ArrayList<JobSkillsList> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<JobSkillsList> skills) {
        this.skills = skills;
    }

    ArrayList<JobSkillsList> skills;
}
