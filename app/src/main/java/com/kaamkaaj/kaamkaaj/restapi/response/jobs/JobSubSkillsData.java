package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import java.util.ArrayList;

/**
 * Created by akshaysingh on 6/8/18.
 */

public class JobSubSkillsData {
    public ArrayList<JobSubSkillsList> getSubSkills() {
        return subSkills;
    }

    public void setSubSkills(ArrayList<JobSubSkillsList> subSkills) {
        this.subSkills = subSkills;
    }

    ArrayList<JobSubSkillsList> subSkills;
}
