package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;


import java.util.ArrayList;

/**
 * Created by akshaysingh on 5/17/18.
 */

public class CandidateSearchData {
    public ArrayList<CandidateSearch> getProfile() {
        return profile;
    }

    public void setProfile(ArrayList<CandidateSearch> candidateSearches) {
        this.profile = candidateSearches;
    }

    ArrayList<CandidateSearch> profile;
}