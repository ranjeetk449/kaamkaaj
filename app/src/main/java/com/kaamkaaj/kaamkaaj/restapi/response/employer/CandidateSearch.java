package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 5/17/18.
 */

public class CandidateSearch implements Parcelable {
    String id, fullName,fathersName , salary, experience, current_address, url,permanentAddress,gender,dob;
    String mobile_no;

    protected CandidateSearch(Parcel in) {
        id = in.readString();
        fullName = in.readString();
        fathersName = in.readString();
        salary = in.readString();
        experience = in.readString();
        current_address = in.readString();
        url = in.readString();
        permanentAddress = in.readString();
        gender = in.readString();
        dob = in.readString();
        mobile_no = in.readString();
        skill = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(fullName);
        dest.writeString(fathersName);
        dest.writeString(salary);
        dest.writeString(experience);
        dest.writeString(current_address);
        dest.writeString(url);
        dest.writeString(permanentAddress);
        dest.writeString(gender);
        dest.writeString(dob);
        dest.writeString(mobile_no);
        dest.writeString(skill);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CandidateSearch> CREATOR = new Creator<CandidateSearch>() {
        @Override
        public CandidateSearch createFromParcel(Parcel in) {
            return new CandidateSearch(in);
        }

        @Override
        public CandidateSearch[] newArray(int size) {
            return new CandidateSearch[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFull_name() {
        return fullName;
    }

    public void setFull_name(String full_name) {
        this.fullName = full_name;
    }

    public String getFathers_name() {
        return fathersName;
    }

    public void setFathers_name(String fathers_name) {
        this.fathersName = fathers_name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getCurrent_address() {
        return current_address;
    }

    public void setCurrent_address(String current_address) {
        this.current_address = current_address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPermanent_address() {
        return permanentAddress;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanentAddress = permanent_address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    String skill;

}
