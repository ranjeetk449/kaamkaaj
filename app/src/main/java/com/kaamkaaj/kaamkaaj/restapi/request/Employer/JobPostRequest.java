package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobPostRequest extends BaseRequest {
        public String jobTitle, jobDescription, minSalary,maxSalary ,minExp, maxExp, address, education, jobValidity, opening, skill, subSkill, lat, lng, name, contactVisible;
    public String jobType;
}
