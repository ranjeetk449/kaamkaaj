package com.kaamkaaj.kaamkaaj.restapi.request.Employer;

import android.os.Parcel;
import android.os.Parcelable;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 5/17/18.
 */

public class SearchCandidatesRequest extends BaseRequest implements Parcelable {
    public String skill, subskill, minSalary, maxSalary, minExp, maxExp, education, location, jobType;
    public int page;

    public SearchCandidatesRequest(Parcel in) {
        skill = in.readString();
        subskill = in.readString();
        minSalary = in.readString();
        maxSalary = in.readString();
        minExp = in.readString();
        maxExp = in.readString();
        education = in.readString();
        location = in.readString();
        jobType = in.readString();
        page = in.readInt();
    }

    public SearchCandidatesRequest() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(skill);
        dest.writeString(subskill);
        dest.writeString(minSalary);
        dest.writeString(maxSalary);
        dest.writeString(minExp);
        dest.writeString(maxExp);
        dest.writeString(education);
        dest.writeString(location);
        dest.writeString(jobType);
        dest.writeInt(page);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchCandidatesRequest> CREATOR = new Creator<SearchCandidatesRequest>() {
        @Override
        public SearchCandidatesRequest createFromParcel(Parcel in) {
            return new SearchCandidatesRequest(in);
        }

        @Override
        public SearchCandidatesRequest[] newArray(int size) {
            return new SearchCandidatesRequest[size];
        }
    };
}
