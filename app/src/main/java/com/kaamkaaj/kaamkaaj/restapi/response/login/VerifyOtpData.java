package com.kaamkaaj.kaamkaaj.restapi.response.login;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class VerifyOtpData {
    String token;
    UserProfile userProfile;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
