package com.kaamkaaj.kaamkaaj.restapi.request.Job;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 4/25/18.
 */

public class JobRequest extends BaseRequest {
    public int page;
    public String minSalary;
    public String maxSalary;
    public String minExp;
    public String maxExp;

    public String skill;
    public String location;
    public String education;

}
