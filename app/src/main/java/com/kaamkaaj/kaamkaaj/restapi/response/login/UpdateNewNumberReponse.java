package com.kaamkaaj.kaamkaaj.restapi.response.login;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/20/18.
 */

public class UpdateNewNumberReponse extends BaseResponse {
    public UpdateNewNumberData getData() {
        return data;
    }

    public void setData(UpdateNewNumberData data) {
        this.data = data;
    }

    UpdateNewNumberData data;
}
