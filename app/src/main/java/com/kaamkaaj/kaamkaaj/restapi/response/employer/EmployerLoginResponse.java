package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class EmployerLoginResponse extends BaseResponse {

    EmployerLoginData data;

    public EmployerLoginData getData() {
        return data;
    }

    public void setData(EmployerLoginData data) {
        this.data = data;
    }
}
