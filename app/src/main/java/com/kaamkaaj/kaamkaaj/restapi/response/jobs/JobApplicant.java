package com.kaamkaaj.kaamkaaj.restapi.response.jobs;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicant implements Parcelable {
 String app_id;
 String id;
    String mobile_no;
    String fullName;
    String fathersName;
    String gender;
    String dob;
    String current_address;
    String permanentAddress;
    String photo;
    String salary;
    String created;
    String createdAt;
    String experience;
    String skill;

    protected JobApplicant(Parcel in) {
        app_id = in.readString();
        id = in.readString();
        mobile_no = in.readString();
        fullName = in.readString();
        fathersName = in.readString();
        gender = in.readString();
        dob = in.readString();
        current_address = in.readString();
        permanentAddress = in.readString();
        photo = in.readString();
        salary = in.readString();
        created = in.readString();
        createdAt = in.readString();
        experience = in.readString();
        skill = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(app_id);
        dest.writeString(id);
        dest.writeString(mobile_no);
        dest.writeString(fullName);
        dest.writeString(fathersName);
        dest.writeString(gender);
        dest.writeString(dob);
        dest.writeString(current_address);
        dest.writeString(permanentAddress);
        dest.writeString(photo);
        dest.writeString(salary);
        dest.writeString(created);
        dest.writeString(createdAt);
        dest.writeString(experience);
        dest.writeString(skill);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<JobApplicant> CREATOR = new Creator<JobApplicant>() {
        @Override
        public JobApplicant createFromParcel(Parcel in) {
            return new JobApplicant(in);
        }

        @Override
        public JobApplicant[] newArray(int size) {
            return new JobApplicant[size];
        }
    };

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getFull_name() {
        return fullName;
    }

    public void setFull_name(String full_name) {
        this.fullName = full_name;
    }

    public String getFathers_name() {
        return fathersName;
    }

    public void setFathers_name(String fathers_name) {
        this.fathersName = fathers_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrent_address() {
        return current_address;
    }

    public void setCurrent_address(String current_address) {
        this.current_address = current_address;
    }

    public String getPermanent_address() {
        return permanentAddress;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanentAddress = permanent_address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getApplied_on() {
        return createdAt;
    }

    public void setApplied_on(String applied_on) {
        this.createdAt = applied_on;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;
}


