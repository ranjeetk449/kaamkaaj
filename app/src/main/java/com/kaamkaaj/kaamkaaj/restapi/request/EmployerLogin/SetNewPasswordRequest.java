package com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin;

import com.kaamkaaj.kaamkaaj.restapi.request.BaseRequest;

/**
 * Created by akshaysingh on 6/20/18.
 */

public class SetNewPasswordRequest extends BaseRequest {
    public String emailId, password;
}

