package com.kaamkaaj.kaamkaaj.restapi.response.employer;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 5/17/18.
 */

public class CandidateSearchResponse extends BaseResponse {
    public CandidateSearchData getData() {
        return data;
    }

    public void setData(CandidateSearchData data) {
        this.data = data;
    }

    CandidateSearchData data;
}
