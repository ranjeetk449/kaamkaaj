package com.kaamkaaj.kaamkaaj.restapi.response.login;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 4/26/18.
 */

public class FetchProfileResponse extends BaseResponse {
    FetchProfileData data;

    public FetchProfileData getData() {
        return data;
    }

    public void setData(FetchProfileData data) {
        this.data = data;
    }
}
