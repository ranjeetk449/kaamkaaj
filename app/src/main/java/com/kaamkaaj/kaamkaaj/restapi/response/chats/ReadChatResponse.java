package com.kaamkaaj.kaamkaaj.restapi.response.chats;

import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;

/**
 * Created by akshaysingh on 6/22/18.
 */

public class ReadChatResponse extends BaseResponse {
    public ReadChatData getData() {
        return data;
    }

    public void setData(ReadChatData data) {
        this.data = data;
    }

    ReadChatData data;
}
