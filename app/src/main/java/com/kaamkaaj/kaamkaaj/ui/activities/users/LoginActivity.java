package com.kaamkaaj.kaamkaaj.ui.activities.users;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.LoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.VerifyOtpRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.restapi.response.login.VerifyOtpResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.JobDescriptionActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class LoginActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.bt_generate_otp)
    Button bt_generate_otp;
    @BindView(R.id.ll_login_view)
    LinearLayout ll_login_view;
    @BindView(R.id.ll_otp_view)
    LinearLayout ll_otp_view;
    @BindView(R.id.bt_continue)
    Button bt_continue;
    @BindView(R.id.bt_back)
    Button bt_back;
    @BindView(R.id.et_otp)
            EditText et_otp;
    @BindView(R.id.tv_resend)
    TextView tv_resend;

    JobList jobList = null;
    String from = "temp";
    Intent intent;
    TinyDB tinyDB;
    boolean otpScreen = false;
    String mobileNum = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.login_text);
        setSupportActionBar(toolbar);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tinyDB = new TinyDB(this);
        if(getIntent().getStringExtra("from") != null) {
            jobList = getIntent().getParcelableExtra(AppConstants.KEY_CLIENT);
            from = getIntent().getStringExtra("from");

        }

        System.out.println("respjobList"+jobList+from);
    }

    @OnClick({R.id.bt_generate_otp, R.id.bt_back, R.id.bt_continue, R.id.tv_resend})
    public void proceed(View view) {
        switch (view.getId()) {
            case R.id.bt_generate_otp:
                generateOtp();
                break;
            case R.id.bt_back:
                onBackPressed();
                break;
            case R.id.bt_continue:
                verifyOtp();
                break;
            case R.id.tv_resend:
                generateOtp();
        }


    }

    public void generateOtp() {

        String mobile = et_mobile_number.getText().toString();
        if (mobile .equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_mobile_number_first),Toast.LENGTH_LONG).show();
            return;
        }
        mobileNum = mobile;
        LoginRequest request = new LoginRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.randomId = SharedPrefUtils.getAnonymousUserId(this);
        request.mobileNo = mobile;

        //request.token = SharedPrefUtils.getApiKey(this);


        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.loginRequest(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                    showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                System.out.println("responseasdeve " + response );
                Toast.makeText(LoginActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                toolbar.setTitle(getResources().getString(R.string.enter_your_otp_text));
                ll_login_view.setVisibility(View.GONE);
                ll_otp_view.setVisibility(View.VISIBLE);
                otpScreen = true;
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {

                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(LoginActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(LoginActivity.this, EmployerTypeActivity.class);
                    LoginActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }

            }

        });
    }

    public void verifyOtp() {
        String otp = et_otp.getText().toString();
        if (otp .equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_otp_to_continue),Toast.LENGTH_LONG).show();
            return;
        }

        VerifyOtpRequest request = new VerifyOtpRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.randomId = SharedPrefUtils.getAnonymousUserId(this);
        request.mobileNo = mobileNum;
        request.otp = otp;

        //request.token = SharedPrefUtils.getApiKey(this);


        Call<VerifyOtpResponse> apiResponseCall;
        apiResponseCall =
                service.verifyOtpRequest(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<VerifyOtpResponse>(apiResponseCall) {
            @Override
            public void onSuccess(Call<VerifyOtpResponse> call, VerifyOtpResponse response) {
                tinyDB.putString("mobile", mobileNum);
                SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);

                SharedPrefUtils.setApiKeyEmployee(LoginActivity.this, response.getData().getToken());
                System.out.println("respemptoken" + response.getData().getToken() );
                String newUser = SharedPrefUtils.getNewApiKey(LoginActivity.this);
                System.out.println("respemptokennewUser" + newUser);


                if (newUser!=null) {
                    if (from .equals("temp")) {
                        //Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        System.out.println("resphii");
                        Intent  i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                        LoginActivity.this.startActivity(i);
                    } else {
                        // Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        System.out.println("respbyee");
                        Intent i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        startActivity(i);
                    }

                } else {
                    SharedPrefUtils.setNewKeyEmployee(LoginActivity.this, "New");
                    if (from .equals("temp")) {
                        System.out.println("respif" + newUser);
                        //Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        Intent  i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        i.putExtra("fromClass", "Login");
                        LoginActivity.this.startActivity(i);
                       // LoginActivity.this.finish();
                    } else {
                        System.out.println("respelse" + newUser);
                        Intent i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        i.putExtra("from", "description");
                        i.putExtra("fromClass", "Login");
                        startActivity(i);

                    }

                }

                /*
                if (response.getData().getUserProfile().getStatus() .equals("new")) {
                    if (from .equals("temp")) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        Intent  i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        LoginActivity.this.startActivity(i);
                    } else {
                        Intent i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        i.putExtra("from", "description");
                        startActivity(i);
                    }
                } else {
                    if (from .equals("temp")) {
                       //Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        System.out.println("resphii");
                        Intent  i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                        LoginActivity.this.startActivity(i);
                    } else {
                       // Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        System.out.println("respbyee");
                        Intent i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        startActivity(i);
                    }
                }*/

            }

            @Override
            public void onError(Call<VerifyOtpResponse> call, Throwable t) {

                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<VerifyOtpResponse> call, VerifyOtpResponse response) {

                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(LoginActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(LoginActivity.this, EmployerTypeActivity.class);
                    LoginActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }            }

        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (otpScreen) {
            ll_login_view.setVisibility(View.VISIBLE);
            ll_otp_view.setVisibility(View.GONE);
            otpScreen = false;
        } else {
            super.onBackPressed();
            this.finish();
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

    }
}