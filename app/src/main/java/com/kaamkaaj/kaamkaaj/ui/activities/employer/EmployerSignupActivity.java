package com.kaamkaaj.kaamkaaj.ui.activities.employer;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.EmployerRegistrationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.SocialLoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerLoginResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/11/18.
 */

public class EmployerSignupActivity extends NewBaseActivity implements View.OnClickListener {


    private static final int RC_SIGN_IN = 111;
    public static final String TAG = EmployerSignupActivity.class.getSimpleName();

    @BindView(R.id.tv_goto_login)
    TextView tv_goto_login;
    @BindView(R.id.ll_social_logins)
    LinearLayout ll_social_logins;
    @BindView(R.id.ll_or_signup_with)
    LinearLayout ll_or_signup_with;
    @BindView(R.id.ll_signup)
    LinearLayout ll_signup;
    @BindView(R.id.et_first_name)
    EditText et_first_name;
    @BindView(R.id.et_last_name)
    EditText et_last_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.et_password_confirm)
    EditText et_password_confirm;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_first_name)
    TextView tv_first_name;
    @BindView(R.id.tv_last_name)
    TextView tv_last_name;
    @BindView(R.id.tv_password)
    TextView tv_password;
    @BindView(R.id.tv_password_confirm)
    TextView tv_password_confirm;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.ll_google_button)
            LinearLayout ll_google_button;

    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;

    String initialUsername = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_employer);
        ButterKnife.bind(this);
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_INDI)) {
            ll_or_signup_with.setVisibility(View.VISIBLE);
            ll_social_logins.setVisibility(View.VISIBLE);
        }
        LoginManager.getInstance().logOut();
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_CORPORATE)) {
            tv_email.setText(getResources().getString(R.string.corporate_email_id_text));
            et_email.setHint(getResources().getString(R.string.corporate_email_id_text));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        LoginManager.getInstance().logOut();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();
        loginButton.setReadPermissions("public_profile email");
        callbackManager = CallbackManager.Factory.create();


        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }



        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("onSuccess");
                if(AccessToken.getCurrentAccessToken() != null){
                    RequestData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }

        });

    }

    @OnClick({R.id.tv_goto_login, R.id.ll_signup, R.id.ll_google_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_goto_login:
                Intent i = new Intent(this, EmployerLoginActivity.class);
                this.finish();
                this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                startActivity(i);
                break;
            case R.id.ll_signup:
                singUp();
                break;
            case R.id.ll_google_button:
                signIn();
                break;
        }

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
//for facebook signup
    public void RequestData(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {
                // Toast.makeText(EmployerLoginActivity.this,"email ",Toast.LENGTH_LONG).show();

                JSONObject json = response.getJSONObject();
                try {
                    //    Toast.makeText(EmployerLoginActivity.this,"email 3qwr: " + json.getString("picture"),Toast.LENGTH_LONG).show();
                    System.out.println("email 3qwr: " + json.getString("picture"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    System.out.println("FAF" + json.getString("email"));
                    //      Toast.makeText(EmployerLoginActivity.this,"email 1: " + json.getString("email"),Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if(json != null){
                        callLogin( json.getString("email"), "Facebook", json.getString("name"));
                        et_email.setText(json.getString("email"));
                        et_first_name.setText(json.getString("name"));
                        //
                        // profile.setProfileId(json.getString("id"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }
   /* public void callLogin(String emails, String socialType,String name) {


        SocialLoginRequest request = new SocialLoginRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.email = emails;
        request.firstName = name;
        request.source = socialType;

        //request.token = SharedPrefUtils.getApiKey(this);


        Call<EmployerLoginResponse> apiResponseCall;
        apiResponseCall =
                service.loginWithSocial(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EmployerLoginResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {
                System.out.println(" !!!!!!!!!! asdeve " + response );
                SharedPrefUtils.setAppState(EmployerSignupActivity.this, AppConstants.STATE_REGISTERED);
                SharedPrefUtils.setApiKey(EmployerSignupActivity.this, response.getData().getToken());
                Intent intent = new Intent(EmployerSignupActivity.this, EmployerDashboardActivity.class);
                EmployerSignupActivity.this.finish();
                startActivity(intent);

            }

            @Override
            public void onError(Call<EmployerLoginResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerSignupActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {

                Toast.makeText(EmployerSignupActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();

            }

        });

    }
*/



    public void callLogin(String emails, String socialType,String name) {



        RequestParams params = new RequestParams();
        params.put("email", emails);
        params.put("source", socialType);
        params.put("firstName", name);

        String url = RequestUrl.API_URL + RequestUrl.SOCIAL_URL;
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                super.onSuccess(statusCode, headers, response);
                System.out.println("responsesocialDetail" + response);

               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                    // responseDetail{"status":{"response":"failure","message":"","userMessage":"Record not found"}}
                    if (respsuccess.equals("success")) {
                        JSONObject userProfile=status.getJSONObject("data");
                        String token=userProfile.getString("token");
                        System.out.println("teamToken"+token);
                        SharedPrefUtils.setAppState(EmployerSignupActivity.this, AppConstants.STATE_REGISTERED);
                        SharedPrefUtils.setApiKey(EmployerSignupActivity.this, token);
                        SharedPrefUtils.setUserLoginType(EmployerSignupActivity.this,socialType);
                        Intent intent = new Intent(EmployerSignupActivity.this, EmployerDashboardActivity.class);
                        EmployerSignupActivity.this.finish();
                        startActivity(intent);
                    }else{
                        CommonMethod.showAlert(userMessage, EmployerSignupActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable
                    throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CommonMethod.showAlert("Unable to connect the server,please try again", EmployerSignupActivity.this);
                hideProgressBar();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgressBar();
            }
        });

    }



    @OnFocusChange({R.id.et_password,R.id.et_email, R.id.et_first_name,R.id.et_last_name,R.id.et_password_confirm})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_email:
                usernameFocus(view, hasFocus);
                break;
            case R.id.et_password:
                passwordFocus(view, hasFocus);
                break;
            case R.id.et_first_name:
                firstNameFocus(view, hasFocus);
                break;
            case R.id.et_last_name:
                lastNameFocus(view, hasFocus);
                break;
            case R.id.et_password_confirm:
                confirmPasswordFocus(view, hasFocus);
                return;
        }
    }

    public void firstNameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_first_name.setVisibility(View.VISIBLE);
            et_first_name.setHint("");
        } else {
            if (et_first_name.getText().toString() .equals(initialUsername)) {
                tv_first_name.setVisibility(View.INVISIBLE);
            } else {
                tv_first_name.setVisibility(View.VISIBLE);

            }
            et_first_name.setHint(getResources().getString(R.string.first_name_text));
        }
    }
    public void lastNameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_last_name.setVisibility(View.VISIBLE);
            et_last_name.setHint("");
        } else {
            if (et_last_name.getText().toString() .equals(initialUsername)) {
                tv_last_name.setVisibility(View.INVISIBLE);
            } else {
                tv_last_name.setVisibility(View.VISIBLE);

            }
            et_last_name.setHint(getResources().getString(R.string.last_name_text));
        }
    }
    public void confirmPasswordFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_password_confirm.setVisibility(View.VISIBLE);
            et_password_confirm.setHint("");
        } else {
            if (et_password_confirm.getText().toString() .equals(initialUsername)) {
                tv_password_confirm.setVisibility(View.INVISIBLE);
            } else {
                tv_password_confirm.setVisibility(View.VISIBLE);

            }
            et_password_confirm.setHint(getResources().getString(R.string.confirm_password_text));
        }
    }

    public void usernameFocus(View view, boolean hasFocus) {
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_CORPORATE)) {

            if (hasFocus) {
                tv_email.setVisibility(View.VISIBLE);
                et_email.setHint("");
            } else {
                if (et_email.getText().toString() .equals(initialUsername)) {
                    tv_email.setVisibility(View.INVISIBLE);
                } else {
                    tv_email.setVisibility(View.VISIBLE);

                }
                et_email.setHint(getResources().getString(R.string.corporate_email_id_text));
            }
        } else {
            if (hasFocus) {
                tv_email.setVisibility(View.VISIBLE);
                et_email.setHint("");
            } else {
                if (et_email.getText().toString() .equals(initialUsername)) {
                    tv_email.setVisibility(View.INVISIBLE);
                } else {
                    tv_email.setVisibility(View.VISIBLE);

                }
                et_email.setHint(getResources().getString(R.string.email_text));
            }
        }

    }
    public void passwordFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_password.setVisibility(View.VISIBLE);
            et_password.setHint("");
        } else {
            if (et_password.getText().toString() .equals(initialUsername)) {
                tv_password.setVisibility(View.INVISIBLE);
            } else {
                tv_password.setVisibility(View.VISIBLE);

            }
            et_password.setHint(getResources().getString(R.string.password_text));
        }
    }

//for google signup
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            System.out.println("@@$@$@R@! : " + account.getEmail());

            et_email.setText(account.getEmail());
            et_first_name.setText(account.getDisplayName());
            callLogin(account.getEmail(),"Google",account.getDisplayName());
            // Signed in successfully, show authenticated UI.
            // updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //  updateUI(null);
        }
    }

    public void singUp() {
        if (et_first_name.getText().length() == 0) {
            et_first_name.setError(getResources().getString(R.string.enter_first_name_text));
            return;
        }
        if (et_last_name.getText().length() == 0) {
            et_last_name.setError(getResources().getString(R.string.enter_last_name_text));
            return;
        }
        if (et_email.getText().length() == 0) {
            et_email.setError(getResources().getString(R.string.enter_email_text));
            return;
        }
        if (et_password.getText().length() == 0) {
            et_password.setError(getResources().getString(R.string.enter_password_text));
            return;
        }
        if (et_password_confirm.getText().length() == 0) {
            et_password_confirm.setError(getResources().getString(R.string.enter_confirm_password_text));
            return;
        }
        if (!et_password_confirm.getText().toString() .equals(et_password.getText().toString())) {
            et_password_confirm.setError(getResources().getString(R.string.password_not_match_text));
            return;
        }
        EmployerRegistrationRequest request = new EmployerRegistrationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setFirstName(et_first_name.getText().toString());
        request.setLastName(et_last_name.getText().toString());
         request.setMobileNo("");
        request.setEmailId(et_email.getText().toString());
        request.setPassword(et_password.getText().toString());
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_INDI)) {
            request.setUserType("individual");
        } else {
            request.setUserType("corporate");
        }


        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.employerRegistration(request);
        System.out.println("resrequest12SIG" + apiResponseCall.toString() );
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                    showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                System.out.println("resrequest12SIGresponse" + response.toString() );
                System.out.println(" !!!!!!!!!! asdeve " + response.toString() );
                Toast.makeText(EmployerSignupActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                Intent i = new Intent(EmployerSignupActivity.this, EmployerLoginActivity.class);
                startActivity(i);
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerSignupActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                Toast.makeText(EmployerSignupActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}