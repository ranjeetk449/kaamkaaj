package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerJobListRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.MyJobList;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.MyJobListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.EmployerMyJobsAdapter;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/16/18.
 */

public class MyJobsActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    ArrayList<MyJobList> jobLists = new ArrayList<MyJobList>();;

    EmployerMyJobsAdapter mAdapter;
    Context mContext;

    private boolean mInFlight;
    int mCurrentPage = 1;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_listing);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.my_jobs_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new EmployerMyJobsAdapter(mContext, jobLists);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);
        fab.setVisibility(View.GONE);
        tinyDB = new TinyDB(this);
       // addCategoryData();

        recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public void onLoadMore(int currentPage, int count) {
                //     Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                  //  mAdapter.addProgressBarView();
                    fetchMyJobs(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });

        fetchMyJobs(mCurrentPage, true);

    }







/*
    public void fetchMyJobs(int mPage, final boolean showProgress) {


        EmployerJobListRequest request = new EmployerJobListRequest();
        request.page = mPage;
        request.token = SharedPrefUtils.getApiKey(this);
        System.out.println("resptokenN"+request.token);
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("page", request.page);
            String url = RequestUrl.API_URL + RequestUrl.JOB_MYJOBS;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseFetchDetails" + response);







               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/

/*
                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    if (respsuccess.equals("success")) {
                        //  System.out.println("_____ : " + response.data);
                      //  for (int i =0;i < response.getData().getJobs().size(); i++) {
                        //    jobLists.add(response.getData().getJobs().get(i));
                       // }
                        mCurrentPage++;
                        mAdapter.notifyDataSetChanged();
                        mInFlight = false;


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", MyJobsActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });


    }
*/







    public void fetchMyJobs(int mPage, final boolean showProgress) {
        EmployerJobListRequest request = new EmployerJobListRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        request.page = mPage;

        request.token = SharedPrefUtils.getApiKey(this);


        Call<MyJobListResponse> apiResponseCall;
        apiResponseCall =
                service.myJobListRequest(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<MyJobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }

            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<MyJobListResponse> call, MyJobListResponse response) {

                  System.out.println("respJobList : " + response);
                  if(response.getData().getJobs().size()>0) {
                      for (int i = 0; i < response.getData().getJobs().size(); i++) {
                          jobLists.add(response.getData().getJobs().get(i));
                      }
                      mCurrentPage++;
                      mAdapter.notifyDataSetChanged();
                      mInFlight = false;
                  }else{
                     // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                      Toast toast = Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                      toast.setGravity(Gravity.CENTER, 0, 0);
                      toast.show();
                  }

                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<MyJobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
               // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                Toast toast = Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<MyJobListResponse> call, MyJobListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(MyJobsActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(MyJobsActivity.this, EmployerTypeActivity2.class);
                    MyJobsActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    Toast.makeText(MyJobsActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }


            }

        });
    }


    public void deleteJob(String id) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, EmployerDashboardActivity.class);
        startActivity(intent);

        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
