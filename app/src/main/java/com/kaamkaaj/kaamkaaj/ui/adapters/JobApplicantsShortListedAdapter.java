package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.UpdateJobApplicantsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobApplicant;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.DemoProfileViewActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.EachChatActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicantsShortListedAdapter  extends RecyclerView.Adapter<JobApplicantsShortListedAdapter.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "CollectionListAdapter";
    private ArrayList<JobApplicant> mDataset;
    String type = "pending";
    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;
    String jobid = "";

    public JobApplicantsShortListedAdapter(Context mContext, ArrayList<JobApplicant> myDataset, String type,String jobId) {
        this.type  = type;
        this.jobid  = jobId;
        System.out.println("@#$@R#R@#$R@!% : " + this.type);
        System.out.println("resmyDataset : " + myDataset);

        this.mContext = mContext;
        mDataset = myDataset;
        mHandler = new Handler() {
            @Override
            public void close() {

            }

            @Override
            public void flush() {

            }

            @Override
            public void publish(LogRecord record) {

            }
        };
    }

    @Override
    public void onItemClick(int position, View view) {
    /*    Intent i = new Intent(mContext, JobsListActivity.class);
        i.putExtra("job_info",mDataset.get(position) );
        mContext.startActivity(i);*/


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;

        @BindView(R.id.tv_experience)
        TextView tv_experience;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_location)
        TextView tv_location;
        @BindView(R.id.tv_posted)
        TextView tv_posted;
        @BindView(R.id.ll_show_profile)
        LinearLayout ll_show_profile;
        @BindView(R.id.iv_icon)
        ImageView iv_icon;
        @BindView(R.id.ll_initial)
        LinearLayout ll_initiall;
        @BindView(R.id.ll_pending)
        LinearLayout ll_pending;
        @BindView(R.id.ll_action)
        LinearLayout ll_action;
        @BindView(R.id.ll_reject_action)
        LinearLayout ll_reject_action;
        @BindView(R.id.ll_shortlist_action)
        LinearLayout ll_shortlist_action;
        @BindView(R.id.ll_line)
        LinearLayout ll_line;
        @BindView(R.id.ll_chat)
        LinearLayout ll_chat;
        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_shortlisted_candidates, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.tv_name.setText(mDataset.get(position).getFull_name());
        holder.tv_experience.setText(mDataset.get(position).getExperience() );
        holder.tv_location.setText(mDataset.get(position).getCurrent_address());
        holder.tv_posted.setText(mDataset.get(position).getApplied_on());
        if (type .equals("pending")) {
            holder.ll_pending.setVisibility(View.VISIBLE);
            holder.ll_initiall.setVisibility(View.GONE);
        } else {
            holder.ll_initiall.setVisibility(View.VISIBLE);
            holder.ll_pending.setVisibility(View.GONE);
        }
        if (type.equalsIgnoreCase("rejected")) {
            holder.ll_chat.setVisibility(View.GONE);
            holder.ll_line.setVisibility(View.GONE);
            Resources res = mContext.getResources();
            String mDrawableName = "ic_cancel_red";
            int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
            Drawable drawable = res.getDrawable(resID );
        /*if (position % 2 == 0) {
            setMargins(holder.ll_each_block, 16, 16,16,0);
        } else {
            setMargins(holder.ll_each_block,16,16,0,0);
        }*/
            holder.iv_icon.setImageDrawable(drawable );

        }
        System.out.println("@#$@R#R@#$R@!111% : " + type);

        holder.ll_shortlist_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModifyApplicantApi(mDataset.get(position).getId(),jobid, "shortlisted");
            }
        });
        holder.ll_reject_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModifyApplicantApi(mDataset.get(position).getId(),jobid, "rejected");
            }
        });
        holder.ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, EachChatActivity.class);
                i.putExtra("fromClass","fromApplicantList");
                i.putExtra("toEmployeeName", mDataset.get(position).getFull_name());
                i.putExtra("employeeMobileNo", mDataset.get(position).getMobile_no());
                i.putExtra("employee_id", mDataset.get(position).getId());
                mContext.startActivity(i);
            }
        });
     //   holder.tv_posted.setText(mDataset.get(position).getAppliedOn());
        /*holder.iv_job_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, JobsListActivity.class);
                i.putExtra("job_info",mDataset.get(position) );
                mContext.startActivity(i);
            }
        });*/

        holder.ll_show_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DemoProfileViewActivity.class);
                i.putExtra("from_class", "shortlist");

                i.putExtra("job_info", mDataset.get(position));

                mContext.startActivity(i);
            }
        });

    }

    public void ModifyApplicantApi(String employeeId,String jobId, String typeStr) {
        UpdateJobApplicantsRequest request = new UpdateJobApplicantsRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        System.out.println("@jobId : " + jobId);
        System.out.println("@employeeId : " + employeeId);
        System.out.println("@typeStr : " + typeStr);

        request.token = SharedPrefUtils.getApiKey(mContext);
        request.job_id = jobId;
        request.employee_id = employeeId;
        System.out.println("!!!!! : " + typeStr);
        if (typeStr .equals("rejected")) {
            request.status = "2";
        } else {
            request.status = "1";
        }
        System.out.println("@jobId : " + jobId);
        System.out.println("@employeeId : " + employeeId);
        System.out.println("@typeStr : " + typeStr);
        System.out.println("@request.status : " + request.status);
        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.updateApplication(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                //  showProgressBar();


            }

            @Override
            protected void onEndApi() {
                // hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                Intent i = new Intent(mContext, MyJobsActivity.class);
                mContext.startActivity(i);


                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(mContext, mContext.getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                // mInFlight = false;
            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {

                Toast.makeText(mContext, response.status.userMessage,Toast.LENGTH_LONG).show();
                //  mInFlight = false;
            }
        });
    }

    public void addItem(JobApplicant dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

}
