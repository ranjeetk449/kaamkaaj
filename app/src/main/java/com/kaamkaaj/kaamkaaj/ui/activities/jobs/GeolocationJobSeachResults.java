package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.JobGeolocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by akshaysingh on 7/9/18.
 */

public class GeolocationJobSeachResults extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    int pageNum = 1;
    String lat, lng, radius;

    ArrayList<JobList> jobLists;
    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;
    private SupportMapFragment mapFragment;
    public CircleOptions circleOptions;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_results_geolocation);
        ButterKnife.bind(this);

        top_bar.setTitle(this.getResources().getString(R.string.job_description_text));
        setSupportActionBar(top_bar);
        tinyDB = new TinyDB(this);
        top_bar.setTitle(R.string.recommended_jobs_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lat = getIntent().getStringExtra("lat");
        lng = getIntent().getStringExtra("lng");
        lat="28.5779";
        lng="77.3181";
        radius = getIntent().getStringExtra("radius");
        pageNum = getIntent().getIntExtra("pageNum",1);
        jobLists = getIntent().getParcelableArrayListExtra("jobs");
        System.out.println("@#$$$$$$$$$$$$@$!@$jobLists" + " " + jobLists);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Double lats = Double.valueOf(lat);
        Double lngs = Double.valueOf(lng);
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lats, lngs))
                .title(getResources().getString(R.string.selected_location_text))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE )));

        for (int i =0; i < jobLists.size(); i++ ) {
            jobLists.get(i).setCustom_map_id(googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.valueOf(jobLists.get(i).getLat()), Double.valueOf(jobLists.get(i).getLng())))
                    .title(jobLists.get(i).getJob_title())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            ).getId());

        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lats, lngs), 10));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle().toString() .equalsIgnoreCase(getResources().getString(R.string.select_location_text))) {
                        System.out.println("@#$$$$$$$$$$$$@$!@$" + " " + marker.getId());
                } else {
                    for (int j=0;j < jobLists.size(); j++) {
                        if (jobLists.get(j).getCustom_map_id() .equalsIgnoreCase(marker.getId())) {
                            gotoDetails(jobLists.get(j));
                            break;
                        }
                    }
                    System.out.println("!!!!!!!!!!!! : " + marker.getTitle().toString() + " " + marker.getId());
                }
                return false;
            }
        });
    }

    public void  gotoDetails(JobList jobList) {
        Intent i = new Intent(this, JobDescriptionActivity.class);
        i.putExtra(AppConstants.FROM_CLASS, "listing");
        i.putExtra(AppConstants.KEY_CLIENT,jobList );
        startActivity(i);
    }

    @OnClick({R.id.ll_action_button})
    public void gotoAction(View view) {
        pageNum = pageNum + 1;
        JobGeolocationRequest request = new JobGeolocationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.token = SharedPrefUtils.getApiKeyEmployee(this);
        request.setPage(pageNum);
        request.setLat(lat);
        request.setLng(lng);
        request.setRadius(radius);

        Call<JobListResponse> apiResponseCall;
        apiResponseCall =
                service.jobListWithRadius(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobListResponse> call, JobListResponse response) {
                Intent i = new Intent(GeolocationJobSeachResults.this, GeolocationJobSeachResults.class);
                i.putExtra("lat", lat);
                i.putExtra("lng", lng);
                i.putExtra("radius", radius);
                i.putParcelableArrayListExtra("jobs",  response.getData().jobs);
                i.putExtra("pageNum",pageNum);
                startActivity(i);
            }
            @Override
            public void onError(Call<JobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
               // Toast.makeText(GeolocationJobSeachResults.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<JobListResponse> call, JobListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(GeolocationJobSeachResults.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(GeolocationJobSeachResults.this, EmployerTypeActivity.class);
                    GeolocationJobSeachResults.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(GeolocationJobSeachResults.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(this, GeolocationJobsSearchFormActivity.class);
        startActivity(intent);

        this.finish();
    }
}
