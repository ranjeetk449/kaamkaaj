package com.kaamkaaj.kaamkaaj.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.FetchApplicationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobApplicant;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobApplicantsListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity2;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.JobPostActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.SearchCandidateResultActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.JobApplicantsShortListedAdapter;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.kaamkaaj.kaamkaaj.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class JobApplicantsSuccessfulFragment extends NewBaseFragment {

    View convertView;
    private static final String TAG = "LumpsumFragment";

    String type = "";
    JobApplicantsShortListedAdapter mAdapter;
    ArrayList<JobApplicant> transactionArrayList;
    Context mContext;
    @BindView(R.id.rv_view)
    RecyclerView rv_view;

    private boolean mInFlight;
    private int mCurrentPage = 1;
    String mClientId;
    String jobId;
    String status = "0";
    TinyDB tinyDB;

    public void JobApplicantsSuccessfulFragment(String type) {
        this.type = type;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View convertView = inflater.inflate(R.layout.fragment_basic_list    , getRootView(), false);
        setContentView(convertView);
        ButterKnife.bind(this, convertView);
        mContext = getActivity();
        System.out.println("!!!!!!!!!!!!!!!!!! : " + type);

        if (getArguments() != null) {
            type = getArguments().getString("type");
            status = getArguments().getString("typeNum");
            jobId = getArguments().getString("id");
        } else {
            type = "pending";
            status = "0";
        }
        tinyDB = new TinyDB(mContext);
        System.out.println("!!!!!!!!!!!!!!!!!! : " + type);

        transactionArrayList = new ArrayList<>();

        mAdapter = new JobApplicantsShortListedAdapter(mContext, transactionArrayList, type,jobId);
        System.out.println("transactionArrayList!!!!!!!!!!!!!!!!!! : " + transactionArrayList);
        rv_view.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        rv_view.setLayoutManager(manager);
        rv_view.setAdapter(mAdapter);
        /*mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                startActivity(new Intent(getContext(), CustomerDashboardActivity.class));
            }
        });*/

        rv_view.addOnScrollListener(new RecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int currentPage, int count) {
                Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                  //  mAdapter.addProgressBarView();
                    getLumpsumTransaction(mCurrentPage, false);
                    mInFlight = true;
                }
            }
        });

        getLumpsumTransaction(mCurrentPage, true);

    }

    /*public  void getTransactionTemp(int mCurrentPage, boolean mInFlight) {
        if (mCurrentPage < 5) {
            for (int i= 0 ; i < 15; i++) {
                JobApplicant jobApplicant = new JobApplicant();
                jobApplicant.setName("Name " + i);
                jobApplicant.setExperience("1 -5 years");
                jobApplicant.setLocation("Delhi, Noida");
                jobApplicant.setAppliedOn("Applied on 4th April, 2018");
                jobApplicant.setStatus("shortlisted");
                jobApplicant.setUserId("24b");
                transactionArrayList.add(jobApplicant);

            }
            mCurrentPage++;
            mAdapter.notifyDataSetChanged();
        } else {
            return;
        }*/

   // }







/*
    public void getLumpsumTransaction(int mPage, final boolean showProgress) {


        FetchApplicationRequest request = new FetchApplicationRequest();
        request.setToken(SharedPrefUtils.getApiKey(mContext));
        request.job_id = jobId;
        request.status = Integer.parseInt(status);



            //FetchApplicationRequest request = new FetchApplicationRequest();
            //request.token = SharedPrefUtils.getApiKey(mContext);
           // request.job_id = jobId;
            RequestParams params = new RequestParams();
            params.put("token", SharedPrefUtils.getApiKey(mContext));
            params.put("job_id", jobId);
            params.put("status", status);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_APPLIEDEMP;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseFETCH_APPLIEDEMP" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}




                  //  {"status":{"response":"success","message":"","userMessage":""},"data":{"appliedEmployees":[]}}


                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        String respsuccess=status.getString("response");
                        if (respsuccess.equals("success")) {
                              //System.out.println("_____ : " + response.data);
                            JSONObject data=response.getJSONObject("data");
                            System.out.println("respdata : " + data);
                            JSONArray appliedEmployees=data.getJSONArray("appliedEmployees");
                            System.out.println("respappliedEmployees : " + appliedEmployees);
                            for (int i =0;i < appliedEmployees.length(); i++) {
                                JSONObject single_user = appliedEmployees.getJSONObject(i);
                               // String name = single_user.getString("name");
                               // System.out.println("name" +name);
                               // subSkillsAdapter.add(name);
                               // subSkillsAdapter.notifyDataSetChanged();

                                mInFlight = false;
                               // transactionArrayList.addAll(appliedEmployees);
                                mCurrentPage++;
                                mAdapter.notifyDataSetChanged();

                            }



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


               }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", getActivity());
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });


    }*/



    public void getLumpsumTransaction(int mPage, final boolean showProgress) {
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        FetchApplicationRequest request = new FetchApplicationRequest();
        request.setToken(SharedPrefUtils.getApiKey(mContext));
        request.job_id = jobId;
        request.status = Integer.parseInt(status);

        System.out.println("lumsum"+SharedPrefUtils.getApiKey(mContext)+"  "+jobId+"  "+status+" "+request);

        Call<JobApplicantsListResponse> apiResponseCall;
        apiResponseCall =
                service.fetchApplications(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobApplicantsListResponse>(apiResponseCall) {

            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobApplicantsListResponse> call, JobApplicantsListResponse response) {
                hideProgressBar();
                mInFlight = false;
                transactionArrayList.addAll(response.getData().getApplicants());
                mCurrentPage++;
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Call<JobApplicantsListResponse> call, Throwable t) {


                mInFlight = false;
                System.out.println(t.toString());
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
               // Utils.showSnackBar(getRootView(), getResources().getString(R.string.error_msg), Snackbar.LENGTH_SHORT);
            }

            @Override
            public void onCustomError(Call<JobApplicantsListResponse> call, JobApplicantsListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(mContext);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(mContext, EmployerTypeActivity2.class);
                    startActivity(intent);
                } else {

                    hideProgressBar();
                    mInFlight = false;
                    Utils.showSnackBar(getRootView(), response.status.userMessage, Snackbar.LENGTH_SHORT);
                }
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
      //  ButterKnife.unbind(this);
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 333 && resultCode == Activity.RESULT_OK) {
/*

            type = data.getStringExtra("type");
            startDate = data.getStringExtra("startDate");
            endDate = data.getStringExtra("endDate");
            codeAMC = data.getStringExtra("codeAMC");
            transactionStatus = data.getStringExtra("transactionStatus");
            paymentStatus = data.getStringExtra("paymentStatus");

            transactionArrayList.clear();
            mAdapter.notifyDataSetChanged();
            mCurrentPage=1;
            getLumpsumTransaction(mCurrentPage, true);*/

        }
    }
}
