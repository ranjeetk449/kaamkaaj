package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.JobsListActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;
import com.kaamkaaj.kaamkaaj.utils.models.JobCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 4/30/18.
 */

public class SubskillsAdapters extends RecyclerView.Adapter<SubskillsAdapters.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "JobListAdapter";
    private ArrayList<String> mDataset;
    OnItemClickListener listener;
    private JobCategory jobCategory;
    Context mContext;
    Handler mHandler;

    public SubskillsAdapters(Context mContext, ArrayList<String> myDataset, JobCategory jobCategory) {
        this.mContext = mContext;
        mDataset = myDataset;
        this.jobCategory = jobCategory;
        mHandler = new Handler();
    }

    @Override
    public void onItemClick(int position, View view) {


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.each_block)
        LinearLayout each_block;

        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_subskills_item, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        holder.tv_name.setText(mDataset.get(position));
        holder.each_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, JobsListActivity.class);
                i.putExtra("job_info",jobCategory );
                i.putExtra("subcategory", mDataset.get(position));

                mContext.startActivity(i);
            }
        });



    }


    public void addItem(String dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);

    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
    public void addProgressBarView() {
        //we will add a dummy item at the end of current list
        String jobList = AppConstants.VIEW_TYPE_PROGRESSBAR;
        mDataset.add(jobList);

        mHandler.post( new Runnable() {
            public void run() {
                //change adapter contents
                //notifyDataSetChanged();
                notifyItemInserted(mDataset.size());

            }
        });

    }

    public void removeProgressBarView() {
        //remove the dummy item we added to the end of list
        if (mDataset.size() > 0) {
            mDataset.remove(mDataset.size() - 1);
            //notifyDataSetChanged();
            notifyItemRemoved(mDataset.size());
        }
    }
}