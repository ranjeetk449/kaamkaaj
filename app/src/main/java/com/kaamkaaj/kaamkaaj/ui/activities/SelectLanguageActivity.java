package com.kaamkaaj.kaamkaaj.ui.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.SiteViewActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.PromoActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akshaysingh on 4/5/18.
 */

public class SelectLanguageActivity extends AppCompatActivity {

    @BindView(R.id.ll_english)
    LinearLayout ll_english;
    @BindView(R.id.ll_hindi)
    LinearLayout ll_hindi;
    @BindView(R.id.iv_animation)
    ImageView iv_animation;
    Intent intent;
    String fromText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_change_pre_onboarding);
        ButterKnife.bind(this);
        fromText = getIntent().getStringExtra(AppConstants.FROM_STRING);
        iv_animation.setBackgroundResource(R.drawable.test_gif);
        iv_animation.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation =
                        (AnimationDrawable) iv_animation.getBackground();
                frameAnimation.start();
            }
        });

    }

    @OnClick({R.id.ll_hindi, R.id.ll_english})
    public void changeLanguage(View view) {

        switch (view.getId()) {
            case R.id.ll_english:
                setLocale("en");
                break;
            case R.id.ll_hindi:
                setLocale("hi");

        }
    }

    public void setLocale(String lang) {
        fromText = getIntent().getStringExtra(AppConstants.FROM_STRING);
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        if (fromText .equals("splash")) {
            System.out.println("New User deted");

            Intent refresh = new Intent(this, PromoActivity.class);
            startActivity(refresh);
            finish();
        } else {
            if (fromText .equals("emp")) {
                Intent refresh = new Intent(this, EmployerDashboardActivity.class);
                startActivity(refresh);
                finish();
            } else {
                if (SharedPrefUtils.getAppState(SelectLanguageActivity.this) == (AppConstants.STATE_REGISTERED)) {
                    Intent refresh = new Intent(this, CustomerDashboardActivity.class);
                    startActivity(refresh);
                    finish();
                } else {
                    Intent refresh = new Intent(this, MainActivity.class);
                    startActivity(refresh);
                    finish();
                }

            }

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


       // this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
