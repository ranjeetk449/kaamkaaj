package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.ForgetPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.SetNewPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.VerifyOtpForgetPasswordRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerLoginResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by akshaysingh on 4/11/18.
 */

public class EmployerForgotPassword  extends NewBaseActivity {


    @BindView(R.id.ll_email) LinearLayout ll_email;
    @BindView(R.id.ll_otp) LinearLayout ll_otp;
    @BindView(R.id.ll_password) LinearLayout ll_password;
    @BindView(R.id.ll_submit_button) LinearLayout ll_submit_button;
    @BindView(R.id.ll_verify) LinearLayout ll_verify;
    @BindView(R.id.iv_im1) CircleImageView iv_im1;
    @BindView(R.id.et_otp_first) EditText et_otp_first;
    @BindView(R.id.et_otp_second) EditText et_otp_second;
    @BindView(R.id.et_otp_third) EditText et_otp_third;
    @BindView(R.id.et_otp_fourth) EditText et_otp_fourth;
    @BindView(R.id.et_email_id) EditText et_email_id;
    @BindView(R.id.et_password) EditText et_password;
    @BindView(R.id.tv_password) TextView tv_password;
    @BindView(R.id.tv_password_confirm)
    TextView tv_password_confirm;
    @BindView(R.id.ll_set_password)
    LinearLayout ll_set_password;
    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.et_password_confirm) EditText et_password_confirm;

    String initialUsername = "";
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(getResources().getString(R.string.forgot_password_text1));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        tinyDB = new TinyDB(this);

        et_otp_first.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(et_otp_first.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp_second.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        et_otp_second.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(et_otp_second.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp_third.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        et_otp_third.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(et_otp_third.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp_fourth.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
init();
    }

    void init() {

    }
    @OnClick({R.id.ll_submit_button,R.id.ll_verify, R.id.ll_set_password})
    public void doStuff(View view) {
        switch (view.getId()) {
            case R.id.ll_submit_button:
                callOtp();
                break;
            case R.id.ll_verify:
                veriyOtp();
                break;
            case R.id.ll_set_password:
                setPassword();
                break;


        }
    }

    public void setPassword() {

        SetNewPasswordRequest request = new SetNewPasswordRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.emailId = et_email_id.getText().toString();
        request.password = et_password.getText().toString();
        Call<EmployerLoginResponse> apiResponseCall;
        apiResponseCall =
                service.updatePassword(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EmployerLoginResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {
                SharedPrefUtils.setAppState(EmployerForgotPassword.this, AppConstants.STATE_REGISTERED);
                SharedPrefUtils.setApiKey(EmployerForgotPassword.this, response.getData().getToken());
                Intent intent = new Intent(EmployerForgotPassword.this, EmployerDashboardActivity.class);
                EmployerForgotPassword.this.finish();
                startActivity(intent);
            }

            @Override
            public void onError(Call<EmployerLoginResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerForgotPassword.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EmployerForgotPassword.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EmployerForgotPassword.this, EmployerTypeActivity2.class);
                    EmployerForgotPassword.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(EmployerForgotPassword.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void callOtp() {
        if (et_email_id.getText().toString().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.enter_email_text),Toast.LENGTH_LONG).show();
            return;
        }


        ForgetPasswordRequest request = new ForgetPasswordRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        request.emailId = et_email_id.getText().toString();

        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.forgotPassword(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                    showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                ll_email.setVisibility(View.GONE);
                ll_otp.setVisibility(View.VISIBLE);
                ll_password.setVisibility(View.GONE);
                iv_im1.setImageResource(R.mipmap.otp_email_bg);
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerForgotPassword.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EmployerForgotPassword.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EmployerForgotPassword.this, EmployerTypeActivity2.class);
                    EmployerForgotPassword.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    Toast.makeText(EmployerForgotPassword.this, response.status.userMessage,Toast.LENGTH_LONG).show();

                }


            }

        });
    }


    public void veriyOtp() {
        if (et_otp_first.getText().toString().length() ==0 || et_otp_second.getText().toString().length() ==0 || et_otp_third.getText().toString().length() ==0 || et_otp_fourth.getText().toString().length() ==0 ) {
            Toast.makeText(this, getResources().getString(R.string.enter_otp_to_continue),Toast.LENGTH_LONG).show();
            return;
        }

        VerifyOtpForgetPasswordRequest request = new VerifyOtpForgetPasswordRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.emailId = et_email_id.getText().toString();
        request.otp  = "" + et_otp_first.getText().toString() + et_otp_second.getText().toString() + et_otp_third.getText().toString() + et_otp_fourth.getText().toString();

        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.verifyOtpPassword(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                ll_email.setVisibility(View.GONE);
                ll_otp.setVisibility(View.GONE);
                iv_im1.setImageResource(R.mipmap.key_bg);
                ll_password.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerForgotPassword.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EmployerForgotPassword.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EmployerForgotPassword.this, EmployerTypeActivity2.class);
                    EmployerForgotPassword.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(EmployerForgotPassword.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @OnFocusChange({R.id.et_password, R.id.et_password_confirm})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_password:
                passwordFocus(view, hasFocus);
                break;
            case R.id.et_password_confirm:
                confirmPasswordFocus(view, hasFocus);
                return;
        }
    }

    public void confirmPasswordFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_password_confirm.setVisibility(View.VISIBLE);
            et_password_confirm.setHint("");
        } else {
            if (et_password_confirm.getText().toString() .equals(initialUsername)) {
                tv_password_confirm.setVisibility(View.INVISIBLE);
            } else {
                tv_password_confirm.setVisibility(View.VISIBLE);

            }
            et_password_confirm.setHint(getResources().getString(R.string.confirm_password_text));
        }
    }

    public void passwordFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_password.setVisibility(View.VISIBLE);
            et_password.setHint("");
        } else {
            if (et_password.getText().toString() .equals(initialUsername)) {
                tv_password.setVisibility(View.INVISIBLE);
            } else {
                tv_password.setVisibility(View.VISIBLE);

            }
            et_password.setHint(getResources().getString(R.string.password_text));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}