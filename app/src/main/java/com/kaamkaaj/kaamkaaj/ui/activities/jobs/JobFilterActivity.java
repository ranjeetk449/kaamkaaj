package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchEducationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchLocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSkillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.EducationResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.LocationResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.FilterAdapters;
import com.kaamkaaj.kaamkaaj.utils.Geofencing;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.kaamkaaj.kaamkaaj.utils.models.FilterObject;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by akshaysingh on 4/24/18.
 */

public class JobFilterActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.rv_location)
    RecyclerView rv_location;
    @BindView(R.id.rv_salary)
    RecyclerView rv_salary;
    @BindView(R.id.rv_skills)
    RecyclerView rv_skills;
    @BindView(R.id.rv_experience)
    RecyclerView rv_experience;
    @BindView(R.id.rv_education)
    RecyclerView rv_education;
    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.tv_location_val)
    TextView tv_location_val;
    @BindView(R.id.ll_location) LinearLayout ll_location;
    @BindView(R.id.ll_skills) LinearLayout ll_skills;
    @BindView(R.id.ll_salary) LinearLayout ll_salary;
    @BindView(R.id.ll_experience) LinearLayout ll_experience;
    @BindView(R.id.ll_education) LinearLayout ll_education;

    @BindView(R.id.ll_location_box)
    LinearLayout ll_location_box;
    @BindView(R.id.ll_salary_box)
            LinearLayout ll_salary_box;
    @BindView(R.id.ll_skills_box)
            LinearLayout ll_skills_box;
    @BindView(R.id.ll_experience_box) LinearLayout ll_experience_box;
    @BindView(R.id.ll_education_box) LinearLayout ll_education_box;
    @BindView(R.id.iv_location)
    ImageView iv_location;
    @BindView(R.id.iv_salary) ImageView iv_salary;
    @BindView(R.id.iv_skills) ImageView iv_skills;
    @BindView(R.id.iv_experience) ImageView iv_experience;
    @BindView(R.id.iv_education) ImageView iv_education;
    @BindView(R.id.tv_location) TextView tv_location;
    @BindView(R.id.tv_salary) TextView tv_salary;
    @BindView(R.id.tv_skills) TextView tv_skills;
    @BindView(R.id.tv_experience) TextView tv_experience;
    @BindView(R.id.tv_education) TextView tv_education;
    @BindView(R.id.ll_action_button) LinearLayout ll_action_button;
    @BindView(R.id.et_education_val) EditText et_education_val;
    @BindView(R.id.tv_education_val) TextView tv_education_val;
    @BindView(R.id.tv_min_salary) TextView tv_min_salary;
    @BindView(R.id.et_min_salary) EditText et_min_salary;
    @BindView(R.id.tv_max_salary) TextView tv_max_salary;
    @BindView(R.id.et_max_salary) EditText et_max_salary;
    @BindView(R.id.ss_min_exp) SearchableSpinner ss_min_exp;
    @BindView(R.id.ss_max_exp) SearchableSpinner ss_max_exp;
    @BindView(R.id.iv_skills_select_all) ImageView iv_skills_select_all;
    @BindView(R.id.ll_selectall_skill) LinearLayout ll_selectall_skill;
    @BindView(R.id.iv_location_select_all) ImageView iv_location_select_all;
    @BindView(R.id.ll_selectall_location) LinearLayout ll_selectall_location;
    @BindView(R.id.iv_education_select_all) ImageView iv_education_select_all;
    @BindView(R.id.ll_selectall_education) LinearLayout ll_selectall_education;

    ArrayList<FilterObject> locationList = new ArrayList<FilterObject>();
    ArrayList<FilterObject> salaryList = new ArrayList<FilterObject>();;
    ArrayList<FilterObject> skillsList = new ArrayList<FilterObject>();;
    ArrayList<FilterObject> experienceList = new ArrayList<FilterObject>();;
    ArrayList<FilterObject> educationList = new ArrayList<FilterObject>();;

    boolean skillSelectAll = false, locationSelectAll = false, educationSelectAll;

    FilterAdapters locationAdapter, salaryAdapter, skillsAdapter, experienceAdapter, educationAdapter;
    Context mContext;

    public static final String TAG = JobFilterActivity.class.getSimpleName();

    String initialUsername = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    private Geofencing mGeofencing;
    private GoogleApiClient mClient;
    String lat = "", lon = "",name = "";
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_jobs);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.search_jobs_text);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        GridLayoutManager lLayout1 = new GridLayoutManager(mContext, 1);
        GridLayoutManager lLayout2 = new GridLayoutManager(mContext, 1);
        GridLayoutManager lLayout3 = new GridLayoutManager(mContext, 1);
        GridLayoutManager lLayout4 = new GridLayoutManager(mContext, 1);

        locationAdapter = new FilterAdapters(mContext, locationList,"location");
        salaryAdapter = new FilterAdapters(mContext, salaryList, "salary");
        skillsAdapter = new FilterAdapters(mContext, skillsList,"skills");
        experienceAdapter = new FilterAdapters(mContext, experienceList,"exp");
        educationAdapter = new FilterAdapters(mContext, educationList,"education");

        rv_location.setLayoutManager(lLayout);
        rv_salary.setLayoutManager(lLayout1);
        rv_skills.setLayoutManager(lLayout2);
        rv_experience.setLayoutManager(lLayout3);
        rv_education.setLayoutManager(lLayout4);

        rv_location.setAdapter(locationAdapter);
        rv_skills.setAdapter(skillsAdapter);
        rv_salary.setAdapter(salaryAdapter);
        rv_experience.setAdapter(experienceAdapter);
        rv_education.setAdapter(educationAdapter);

        et_education_val.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_min_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_max_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        addLocationData();
        addSalaryData();
        addSkillsData();
        addExperienceData();
        addEducationData();
        EnableRuntimePermission();

        mGeofencing = new Geofencing(this, mClient);

        mGeofencing.registerAllGeofences();
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(JobFilterActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();
        if (SharedPrefUtils.getFilterMinSalary(this).length() > 0) {
            et_min_salary.setText(SharedPrefUtils.getFilterMinSalary(this));
            tv_min_salary.setVisibility(View.VISIBLE);
        }
        if (SharedPrefUtils.getFilterMaxSalary(this).length() > 0) {
            et_min_salary.setText(SharedPrefUtils.getFilterMaxSalary(this));
            tv_max_salary.setVisibility(View.VISIBLE);
        }
        et_min_salary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //System.out.println("BEFORE : " + et_set_min.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                SharedPrefUtils.setFilterMinSalary(JobFilterActivity.this,et_min_salary.getText().toString());

            }
        });

        et_max_salary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //System.out.println("BEFORE : " + et_set_min.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // setMin();
                SharedPrefUtils.setFilterMaxSalary(JobFilterActivity.this,et_max_salary.getText().toString());
            }
        });
        final String[] values = getResources().getStringArray(R.array.max_exp);


        ss_max_exp.setSelection(values.length-1);
    }


    @OnItemSelected(R.id.ss_min_exp)
    public void skillSelected(Spinner spinner, int position) {
        SharedPrefUtils.setFilterMinExp(this,ss_min_exp.getSelectedItem().toString());
    }

    @OnItemSelected(R.id.ss_max_exp)
    public void maxExp(Spinner spinner, int position) {
        SharedPrefUtils.setFilterMaxExp(this, ss_max_exp.getSelectedItem().toString());
    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {

            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);

        }


    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    @OnClick({R.id.ll_location_box, R.id.ll_salary_box,R.id.ll_skills_box, R.id.ll_experience_box, R.id.ll_education_box, R.id.ll_action_button, R.id.tv_location_val, R.id.tv_education_val,R.id.ll_selectall_skill,R.id.ll_selectall_location, R.id.ll_selectall_education})
    public void clickThem(View view) {


        Resources res = mContext.getResources();
        String selectedName = "ic_check_blue";
        String unSelectedName = "ic_uncheck";
        int resIDSelected = res.getIdentifier(selectedName , "drawable", mContext.getPackageName());
        int resIDunselected = res.getIdentifier(unSelectedName , "drawable", mContext.getPackageName());

        Drawable drawable1 = res.getDrawable(resIDSelected );
        Drawable drawable2 = res.getDrawable(resIDunselected );
        switch (view.getId()) {
            case R.id.ll_location_box:
                clearAll();
                ll_location_box.setBackgroundColor(getResources().getColor(R.color.white));
                tv_location.setTextColor(getResources().getColor(R.color.highlightedColor));
                iv_location.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_location_blue));
                ll_location.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_salary_box:
                clearAll();
                ll_salary_box.setBackgroundColor(getResources().getColor(R.color.white));
                tv_salary.setTextColor(getResources().getColor(R.color.highlightedColor));
                iv_salary.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_rupee_blue));
                ll_salary.setVisibility(View.VISIBLE);

                break;
            case R.id.ll_skills_box:
                clearAll();
                ll_skills_box.setBackgroundColor(getResources().getColor(R.color.white));
                tv_skills.setTextColor(getResources().getColor(R.color.highlightedColor));
                iv_skills.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_skills_blue));
                ll_skills.setVisibility(View.VISIBLE);

                break;
            case R.id.ll_experience_box:
                clearAll();
                ll_experience_box.setBackgroundColor(getResources().getColor(R.color.white));
                tv_experience.setTextColor(getResources().getColor(R.color.highlightedColor));
                iv_experience.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_experience_blue));
                ll_experience.setVisibility(View.VISIBLE);

                break;
            case R.id.ll_education_box:
                clearAll();
                ll_education_box.setBackgroundColor(getResources().getColor(R.color.white));
                tv_education.setTextColor(getResources().getColor(R.color.highlightedColor));
                iv_education.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_education_blue));
                ll_education.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_action_button:
                String skills = SharedPrefUtils.getFilterSkills(this);
                System.out.println("AFAGGGAGG : " + skills);
                /*if (skills != null && skills.length() < 2) {
                    Toast.makeText(this, getResources().getString(R.string.skills_compulsary_text),Toast.LENGTH_LONG).show();
                    return;
                }*/
                Intent i = new Intent(this, JobsListActivity.class);
                this.startActivity(i);
                break;
            case R.id.tv_location_val:
                onAddPlaceButtonClicked(tv_location_val);
                break;
            case R.id.tv_education_val:
                et_education_val.setVisibility(View.VISIBLE);
                tv_education_val.setVisibility(View.GONE);
                break;
            case R.id.ll_selectall_skill:
                //ArrayList<FilterObject> skillsListNew = new ArrayList<FilterObject>();;
                if (skillSelectAll) {
                    iv_skills_select_all.setImageDrawable(drawable2 );
                    for (int j=0; j < skillsList.size();j++){
                        skillsList.set(j, new FilterObject(skillsList.get(j).getElement(),false));
                    }
                    SharedPrefUtils.setFilterSkills(this,"");
                    skillsAdapter.notifyDataSetChanged();
                    skillSelectAll = false;
                } else {
                    iv_skills_select_all.setImageDrawable(drawable1 );
                    String vals = "";
                    for (int j=0; j < skillsList.size();j++){
                        skillsList.set(j, new FilterObject(skillsList.get(j).getElement(),true));
                        vals = vals + skillsList.get(j).getElement()+",";
                    }
                    SharedPrefUtils.setFilterSkills(this, vals);
                    skillsAdapter.notifyDataSetChanged();
                    skillSelectAll = true;

                }
                break;
            case R.id.ll_selectall_location:
                if (locationSelectAll) {
                    iv_location_select_all.setImageDrawable(drawable2 );
                    for (int j=0; j < locationList.size();j++){
                        locationList.set(j, new FilterObject(locationList.get(j).getElement(),false));
                    }
                    SharedPrefUtils.setFilterLocation(this,"");
                    locationAdapter.notifyDataSetChanged();
                    locationSelectAll = false;
                } else {
                    iv_location_select_all.setImageDrawable(drawable1 );
                    String vals = "";
                    for (int j=0; j < locationList.size();j++){
                        locationList.set(j, new FilterObject(locationList.get(j).getElement(),true));
                        vals = vals + locationList.get(j).getElement()+",";
                    }
                    SharedPrefUtils.setFilterLocation(this, vals);
                    locationAdapter.notifyDataSetChanged();
                    locationSelectAll = true;

                }
                break;
            case R.id.ll_selectall_education:
                if (educationSelectAll) {
                    iv_education_select_all.setImageDrawable(drawable2 );
                    for (int j=0; j < educationList.size();j++){
                        educationList.set(j, new FilterObject(educationList.get(j).getElement(),false));
                    }
                    SharedPrefUtils.setFilterEducation(this,"");
                    educationAdapter.notifyDataSetChanged();
                    educationSelectAll = false;
                } else {
                    iv_education_select_all.setImageDrawable(drawable1 );
                    String vals = "";
                    for (int j=0; j < educationList.size();j++){
                        educationList.set(j, new FilterObject(educationList.get(j).getElement(),true));
                        vals = vals + educationList.get(j).getElement()+",";
                    }
                    SharedPrefUtils.setFilterEducation(this, vals);
                    educationAdapter.notifyDataSetChanged();
                    educationSelectAll = true;

                }
                break;
        }
    }

    public void onAddPlaceButtonClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            return;
        }
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }


    public void clearAll() {
        ll_location_box.setBackgroundColor(getResources().getColor(R.color.grey_200));
        ll_salary_box.setBackgroundColor(getResources().getColor(R.color.grey_200));
        ll_skills_box.setBackgroundColor(getResources().getColor(R.color.grey_200));
        ll_experience_box.setBackgroundColor(getResources().getColor(R.color.grey_200));
        ll_education_box.setBackgroundColor(getResources().getColor(R.color.grey_200));

        ll_location.setVisibility(View.GONE);
        ll_salary.setVisibility(View.GONE);
        ll_skills.setVisibility(View.GONE);
        ll_education.setVisibility(View.GONE);
        ll_experience.setVisibility(View.GONE);

        tv_location.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        tv_salary.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        tv_skills.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        tv_experience.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        tv_education.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        iv_location.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_location_gray));
        iv_skills.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_skills_gray));
        iv_salary.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_rupee_gray));
        iv_experience.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_experience_gray));
        iv_education.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_education_gray));


    }

    @OnFocusChange({ R.id.et_min_salary, R.id.et_max_salary})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {

            case R.id.et_min_salary:
                minSalaryFocus(view, hasFocus);
                break;
            case R.id.et_max_salary:
                maxSalaryFocus(view, hasFocus);
                break;

        }
    }
    public void minSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_min_salary.setVisibility(View.VISIBLE);
            et_min_salary.setHint("");
        } else {
            if (et_min_salary.getText().toString() .equals(initialUsername)) {
                tv_min_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_min_salary.setVisibility(View.VISIBLE);

            }
            et_min_salary.setHint(getResources().getString(R.string.min_salary_text));
        }
    }
    public void maxSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_max_salary.setVisibility(View.VISIBLE);
            et_max_salary.setHint("");
        } else {
            if (et_max_salary.getText().toString() .equals(initialUsername)) {
                tv_max_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_max_salary.setVisibility(View.VISIBLE);

            }
            et_max_salary.setHint(getResources().getString(R.string.max_salary_text));
        }
    }
    public void addLocationData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();

        FetchLocationRequest request = new FetchLocationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKeyEmployee(this));
        Call<LocationResponse> apiResponseCall;
        apiResponseCall =
                service.fetchLocation(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<LocationResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<LocationResponse> call, LocationResponse response) {
                String locations = SharedPrefUtils.getFilterLocation(JobFilterActivity.this);
                for (int i = 0; i < response.getData().getLocationList().size(); i++) {
                    jobCategories.add(response.getData().getLocationList().get(i).getName());
                    FilterObject filterObject;
                    if (locations.indexOf(jobCategories.get(i)) >=0) {
                        filterObject = new FilterObject(jobCategories.get(i),true);
                    } else {
                        filterObject = new FilterObject(jobCategories.get(i),false);

                    }
                    locationList.add(filterObject);
                    locationAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Call<LocationResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast.makeText(JobFilterActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<LocationResponse> call, LocationResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(JobFilterActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(JobFilterActivity.this, EmployerTypeActivity.class);
                    JobFilterActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobFilterActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }

        });


        /*String[] location = getResources().getStringArray(R.array.location);
        for (int i =0; i < location.length; i++) {
            FilterObject filterObject = new FilterObject(location[i],false);
            locationList.add(filterObject);
        }
        locationAdapter.notifyDataSetChanged();*/
    }

    public void addSalaryData() {
        String[] salary = getResources().getStringArray(R.array.salary);
        for (int i =0; i < salary.length; i++) {
            FilterObject filterObject = new FilterObject(salary[i],false);
            salaryList.add(filterObject);
        }
        salaryAdapter.notifyDataSetChanged();
    }
    public void addSkillsData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();

        FetchSkillsRequest request = new FetchSkillsRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKey(this));
        Call<JobSkillsResponse> apiResponseCall;
        apiResponseCall =
                service.fetchSkills(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobSkillsResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                String skillss = SharedPrefUtils.getFilterSkills(JobFilterActivity.this);
                for (int i = 0; i < response.getData().getSkills().size(); i++) {
                    jobCategories.add(response.getData().getSkills().get(i).getName());
                    FilterObject filterObject;
                    if (skillss.indexOf(jobCategories.get(i)) >=0) {
                        filterObject = new FilterObject(jobCategories.get(i),true);
                    } else {
                        filterObject = new FilterObject(jobCategories.get(i),false);

                    }
                    skillsList.add(filterObject);
                    skillsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Call<JobSkillsResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast.makeText(JobFilterActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(JobFilterActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(JobFilterActivity.this, EmployerTypeActivity.class);
                    JobFilterActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobFilterActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }

        });/*
        System.out.println("IN SKILLS");
        jobCategories.add( getResources().getString(R.string.name_sweeper));
        jobCategories.add (getResources().getString(R.string.name_waiter));
        jobCategories.add ( getResources().getString(R.string.name_painter));
        jobCategories.add (getResources().getString(R.string.name_carpenter));
        jobCategories.add (getResources().getString(R.string.name_cook));
        jobCategories.add (getResources().getString(R.string.name_driver));
        jobCategories.add (getResources().getString(R.string.name_electrician));
        jobCategories.add (getResources().getString(R.string.name_helper));
        jobCategories.add (getResources().getString(R.string.name_labour));
        jobCategories.add (getResources().getString(R.string.name_plumber));
        jobCategories.add ( getResources().getString(R.string.name_security_guard));
        jobCategories.add (getResources().getString(R.string.name_welder));
        jobCategories.add (getResources().getString(R.string.name_nurse));
        jobCategories.add (getResources().getString(R.string.name_office_boy));
        jobCategories.add ( getResources().getString(R.string.name_pest_control));
        jobCategories.add (getResources().getString(R.string.name_tank_cleaner));
        jobCategories.add (getResources().getString(R.string.name_photographer));
        jobCategories.add (getResources().getString(R.string.name_fire_fighters));
        jobCategories.add (getResources().getString(R.string.name_dentist));
        jobCategories.add (getResources().getString(R.string.name_delivery_boy));
        jobCategories.add (getResources().getString(R.string.name_tailor));
        jobCategories.add (getResources().getString(R.string.name_other));
        for (int i =0; i < jobCategories.size(); i++) {
            FilterObject filterObject = new FilterObject(jobCategories.get(i),false);
            skillsList.add(filterObject);
        }*/
    }
    public void addExperienceData() {
        String[] experinence = getResources().getStringArray(R.array.experience);
        for (int i =0; i < experinence.length; i++) {
            FilterObject filterObject = new FilterObject(experinence[i],false);
            experienceList.add(filterObject);
        }
        experienceAdapter.notifyDataSetChanged();
    }
    public void addEducationData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();
        FetchEducationRequest request = new FetchEducationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKey(this));
        Call<EducationResponse> apiResponseCall;
        apiResponseCall =
                service.fetchEducation(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EducationResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EducationResponse> call, EducationResponse response) {
                String edus = SharedPrefUtils.getFilterEducation(JobFilterActivity.this);
                for (int i = 0; i < response.getData().getEducationLists().size(); i++) {
                    jobCategories.add(response.getData().getEducationLists().get(i).getName());
                    FilterObject filterObject;
                    if (edus.indexOf(jobCategories.get(i)) >=0) {
                        filterObject = new FilterObject(jobCategories.get(i),true);
                    } else {
                        filterObject = new FilterObject(jobCategories.get(i),false);

                    }
                    educationList.add(filterObject);
                    educationAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Call<EducationResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast.makeText(JobFilterActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<EducationResponse> call, EducationResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(JobFilterActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(JobFilterActivity.this, EmployerTypeActivity.class);
                    JobFilterActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobFilterActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
                Toast.makeText(JobFilterActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
            }

        });


        /*String[] education = getResources().getStringArray(R.array.education);
        for (int i =0; i < education.length; i++) {
            FilterObject filterObject = new FilterObject(education[i],false);
            educationList.add(filterObject);
        }
        educationAdapter.notifyDataSetChanged();*/
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }
            String placeID = place.getId();
            // Get live data information
            refreshPlacesData(place);
        }
    }

    public void refreshPlacesData(Place place) {
        lat = String.valueOf(place.getLatLng().latitude);
        lon = String.valueOf(place.getLatLng().longitude);
        tv_location_val.setText(place.getAddress());
        name = String.valueOf(place.getName());
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SharedPrefUtils.getAppState(JobFilterActivity.this) == AppConstants.STATE_REGISTERED) {

            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}

