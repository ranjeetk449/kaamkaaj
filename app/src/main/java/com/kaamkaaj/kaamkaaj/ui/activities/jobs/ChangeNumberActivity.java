package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.Share;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.ChangeNumberRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.UpdateNewNumberRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.login.UpdateNewNumberReponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by akshaysingh on 6/20/18.
 */

public class ChangeNumberActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.bt_generate_otp)
    Button bt_generate_otp;
    @BindView(R.id.ll_login_view)
    LinearLayout ll_login_view;
    @BindView(R.id.ll_otp_view)
    LinearLayout ll_otp_view;
    @BindView(R.id.bt_continue)
    Button bt_continue;
    @BindView(R.id.bt_back)
    Button bt_back;
    @BindView(R.id.et_otp)
    EditText et_otp;
    @BindView(R.id.tv_resend)
    TextView tv_resend;

    String from = "temp";
    Intent intent;
    TinyDB tinyDB;
    boolean otpScreen = false;
    String mobileNum = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.change_number_text);
        setSupportActionBar(toolbar);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tinyDB = new TinyDB(this);

       // et_mobile_number.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
       // et_otp.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

    }

    @OnClick({R.id.bt_generate_otp, R.id.bt_back, R.id.bt_continue, R.id.tv_resend})
    public void proceed(View view) {
        switch (view.getId()) {
            case R.id.bt_generate_otp:
                generateOtp();
                break;
            case R.id.bt_back:
                onBackPressed();
                break;
            case R.id.bt_continue:
                verifyOtp();
                break;
            case R.id.tv_resend:
                generateOtp();
        }


    }

    public void generateOtp() {

        String mobile = et_mobile_number.getText().toString();
        if (mobile .equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_mobile_number_first),Toast.LENGTH_LONG).show();
            return;
        }
        mobileNum = mobile;
        ChangeNumberRequest request = new ChangeNumberRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.token = SharedPrefUtils.getNewApiKey(this);
        request.mobileNo = mobile;
        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.changeNumber(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }
            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                Toast.makeText(ChangeNumberActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                toolbar.setTitle(getResources().getString(R.string.enter_your_otp_text));
                ll_login_view.setVisibility(View.GONE);
                ll_otp_view.setVisibility(View.VISIBLE);
                otpScreen = true;
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {

                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(ChangeNumberActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(ChangeNumberActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(ChangeNumberActivity.this, EmployerTypeActivity.class);
                    ChangeNumberActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(ChangeNumberActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }

            }

        });
    }

    public void verifyOtp() {
        String otp = et_otp.getText().toString();
        if (otp .equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_otp_to_continue),Toast.LENGTH_LONG).show();
            return;
        }

        UpdateNewNumberRequest request = new UpdateNewNumberRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.mobileNo = mobileNum;
        request.otp = otp;

        request.token = SharedPrefUtils.getApiKey(this);


        Call<UpdateNewNumberReponse> apiResponseCall;
        apiResponseCall =
                service.updateNewNumber(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<UpdateNewNumberReponse>(apiResponseCall) {
            @Override
            public void onSuccess(Call<UpdateNewNumberReponse> call, UpdateNewNumberReponse response) {
                tinyDB.putString("mobile", mobileNum);
                SharedPrefUtils.setApiKey(ChangeNumberActivity.this, response.getData().getToken());
                Intent  i = new Intent(ChangeNumberActivity.this, CustomerDashboardActivity.class);
                ChangeNumberActivity.this.startActivity(i);

                /*SharedPrefUtils.setAppState(ChangeNumberActivity.this, AppConstants.STATE_REGISTERED);
                SharedPrefUtils.setApiKey(LoginActivity.this, response.getData().getToken());
                if (response.getData().getStatus() .equals("new")) {
                    if (from .equals("temp")) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        Intent  i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        LoginActivity.this.startActivity(i);
                    } else {
                        Intent i = new Intent(LoginActivity.this, EditProfileActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        i.putExtra("from", "description");
                        startActivity(i);
                    }
                } else {
                    if (from .equals("temp")) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.complete_profile_text),Toast.LENGTH_LONG).show();
                        Intent  i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                        LoginActivity.this.startActivity(i);
                    } else {
                        Intent i = new Intent(LoginActivity.this, JobDescriptionActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        startActivity(i);
                    }
                }*/

            }

            @Override
            public void onError(Call<UpdateNewNumberReponse> call, Throwable t) {

                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(ChangeNumberActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<UpdateNewNumberReponse> call, UpdateNewNumberReponse response) {

                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(ChangeNumberActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(ChangeNumberActivity.this, EmployerTypeActivity.class);
                    ChangeNumberActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(ChangeNumberActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }            }

        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (otpScreen) {
            ll_login_view.setVisibility(View.VISIBLE);
            ll_otp_view.setVisibility(View.GONE);
            otpScreen = false;
        } else {
            super.onBackPressed();
            this.finish();
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

    }
}