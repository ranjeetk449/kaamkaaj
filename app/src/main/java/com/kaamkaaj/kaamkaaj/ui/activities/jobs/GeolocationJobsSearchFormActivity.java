package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.JobGeolocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.JobPostActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.utils.Geofencing;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar.CrystalSeekbar;
import com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar.OnSeekbarChangeListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by akshaysingh on 7/6/18.
 */

public class GeolocationJobsSearchFormActivity extends NewBaseActivity
         {
    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.tv_min_value)
    TextView tv_min_value;
    @BindView(R.id.tv_max_value)
    TextView tv_max_value;
    @BindView(R.id.rangeSeekbar3)
    CrystalSeekbar rangeSeekbar3;
    @BindView(R.id.tv_distance)
    TextView tv_distance;
    @BindView(R.id.tv_location_val)
    TextView tv_location_val;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;

    private Geofencing mGeofencing;
    private GoogleApiClient mClient;
    String lat = "", lon = "",name = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    public static final String TAG = GeolocationJobsSearchFormActivity.class.getSimpleName();

    int distanceloc = 20 * 1000;
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
    };
    TinyDB tinyDB;

    static int flag=0;
    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;











    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_job_geolocation);

        ButterKnife.bind(this);
        top_bar.setTitle(R.string.search_jobs_text);
        setSupportActionBar(top_bar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int vals = SharedPrefUtils.getRadius(this);

        float perc = ((float) (vals - AppConstants.MIN_RANGE) / (float) (AppConstants.MAX_RANGE - AppConstants.MIN_RANGE)) * 100;

        rangeSeekbar3.setMinValueFromData(perc);


        tv_min_value.setText(getResources().getString(R.string.min_text) + " : "  +AppConstants.MIN_RANGE + " " + getResources().getString(R.string.in_km_text) );
        tv_max_value.setText(getResources().getString(R.string.max_text) + " : "  + AppConstants.MAX_RANGE + " " + getResources().getString(R.string.in_km_text) );
        tv_distance.setText(getResources().getString(R.string.distance_text) + " " + getResources().getString(R.string.in_km_text));
//        Utils.changeToolbarTitleFont(this, toolbar);
        rangeSeekbar3.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                distanceloc = Integer.parseInt(rangeSeekbar3.getSelectedMinValue().toString()) * 1000;

                tv_distance.setText(getResources().getString(R.string.distance_text) + " " + rangeSeekbar3.getSelectedMinValue() + " " + getResources().getString(R.string.in_km_text));
            }
        });
       /* EnableRuntimePermission();

        //EnableRuntimePermission();
        mGeofencing = new Geofencing(this, mClient);
        mGeofencing.registerAllGeofences();
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(GeolocationJobsSearchFormActivity.this)
                .addOnConnectionFailedListener(GeolocationJobsSearchFormActivity.this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();*/
        initLocation();

        // fetchJobs(mCurrentPage, true);
    }


    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(GeolocationJobsSearchFormActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                               // Toast.makeText(GeolocationJobsSearchFormActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        System.out.println("addresssNewOne");
        //startLocationUpdates();
        EnableRuntimePermission();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                // mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                System.out.println("addresssNewTwo");


                updateLocationUI();
            }
        };

        //  mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }





    private void updateLocationUI() {
        if (mCurrentLocation != null) {


            // location last updated time
            // txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);


            Geocoder geocoder =
                    new Geocoder(getApplication(), Locale.getDefault());
            // Get the current location from the input parameter list
            //Location loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses;
            try {
                lat= String.valueOf(mCurrentLocation.getLatitude());
                lon= String.valueOf(mCurrentLocation.getLongitude());
                addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(), 1);
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */
                System.out.println("addresssNew" + address.getAddressLine(0));

               /* et_location_val.setText(
                        "Lat: " + mCurrentLocation.getLatitude() + ", " +
                                "Lng: " + mCurrentLocation.getLongitude()
                );*/
                //  et_location_val.setText(address.getAddressLine(0));
                // giving a blink animation on TextView
                //  et_location_val.setAlpha(0);
                // et_location_val.animate().alpha(1).setDuration(300);

                tv_location_val.setText(address.getAddressLine(0));
                tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));

                String addressStr=address.getAddressLine(0);
                System.out.println("addaddressStr" + addressStr);
                if(addressStr.length()>0) {
                    mFusedLocationClient
                            .removeLocationUpdates(mLocationCallback)
                            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                   // Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                                    // toggleButtons();
                                }
                            });
                }

                //System.out.println("addresss"+address.getLocality()+address.getSubLocality()+address.getPostalCode()+address.getCountryName()+address.getFeatureName()+address.getThoroughfare()+address.getSubAdminArea()+address.getAdminArea());
            } catch (IOException e1) {
                Log.e("LocationSampleActivity",
                        "IO Exception in getFromLocation()");
                e1.printStackTrace();
                //return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(mCurrentLocation.getLatitude()) +
                        " , " +
                        Double.toString(mCurrentLocation.getLongitude()) +
                        " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                // return errorString;
            }
            // If the reverse geocode returned an address
           /* if (addresses.size() > 0) {
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */

            // System.out.println("addresss"+address.getLocality());

            //return address.getLocality();
            //  } else {
            // return "No address found";
            //  }


        }

        // toggleButtons();
    }






/*
    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {

            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);

        }


    }*/





    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {
            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();


        } else {
            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }

    // @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    // mRequestingLocationUpdates = true;
                    startLocationUpdates();
                    //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                } else {
                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }









    @OnClick({R.id.tv_location_val, R.id.ll_action_button})
    public void doStuffs(View view) {
        switch (view.getId()) {
            case R.id.tv_location_val:
               // onAddPlaceButtonClicked(view);
                EnableRuntimePermission();
                break;
            case R.id.ll_action_button:
                getJobs();
        }
    }

    public void getJobs() {
        String location = tv_location_val.getText().toString();

        if (location .equalsIgnoreCase(getResources().getString(R.string.select_location_text))) {
            Toast.makeText(this, getResources().getString(R.string.enter_location_text), Toast.LENGTH_LONG).show();
            return;
        }
        JobGeolocationRequest request = new JobGeolocationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.token = SharedPrefUtils.getApiKeyEmployee(this);
        lat="28.5779";
        lon="77.3181";
        request.setPage(1);
        request.setLat(lat);
        request.setLng(lon);
        request.setRadius(String.valueOf(rangeSeekbar3.getSelectedMinValue()));



        Call<JobListResponse> apiResponseCall;
        apiResponseCall =
                service.jobListWithRadius(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                    showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobListResponse> call, JobListResponse response) {
                System.out.println("resp"+response.getData().jobs.size());

                if(response.getData().jobs.size()>0) {
                    Intent i = new Intent(GeolocationJobsSearchFormActivity.this, GeolocationJobSeachResults.class);
                    i.putExtra("lat", lat);
                    i.putExtra("lng", lon);
                    i.putExtra("radius", rangeSeekbar3.getSelectedMinValue().toString());
                    i.putParcelableArrayListExtra("jobs", response.getData().jobs);
                    i.putExtra("pageNum", 1);
                    startActivity(i);
                }
                else{
                    Toast toast = Toast.makeText(GeolocationJobsSearchFormActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }



            @Override
            public void onError(Call<JobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                //Toast.makeText(GeolocationJobsSearchFormActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<JobListResponse> call, JobListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(GeolocationJobsSearchFormActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(GeolocationJobsSearchFormActivity.this, EmployerTypeActivity.class);
                    GeolocationJobsSearchFormActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(GeolocationJobsSearchFormActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();

                }
            }

        });


    }

   /* public void onAddPlaceButtonClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            return;
        }
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            String placeID = place.getId();


            // Get live data information
            refreshPlacesData(place);
        }
    }

    public void refreshPlacesData(Place place) {
        lat = String.valueOf(place.getLatLng().latitude);
        lon = String.valueOf(place.getLatLng().longitude);
        tv_location_val.setText(place.getAddress());
        tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));

        name = String.valueOf(place.getName());
        System.out.println("sgEGGGGG : " + place.getLatLng());

        System.out.println("sgEGGGGG : " + lat);
        System.out.println("sgEGGGGG : " + lon);

    }*/

    @Override
    public void onResume() {
        super.onResume();


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);

        this.finish();
    }



}
