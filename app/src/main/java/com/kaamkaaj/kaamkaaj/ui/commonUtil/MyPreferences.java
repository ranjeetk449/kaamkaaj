package com.kaamkaaj.kaamkaaj.ui.commonUtil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class MyPreferences {
	private static MyPreferences preferences = null;
	private Editor editor;
	private String isLogbooking = "booking";
	private String isLogedIn = "isLogedIn";
	private String isLogedInweb = "isLogedInweb";
	private SharedPreferences mPreferences;

	public boolean getIsLoggedIn() {
		return this.mPreferences.getBoolean(this.isLogedIn, false);
	}

	public void setIsLoggedIn(boolean isLoggedin) {
		this.editor = this.mPreferences.edit();
		this.editor.putBoolean(this.isLogedIn, isLoggedin);
		this.editor.commit();
	}

	public boolean getIsLoggedInweb() {
		return this.mPreferences.getBoolean(this.isLogedInweb, false);
	}

	public void setIsLoggedInweb(boolean isLogedInweb) {
		this.editor = this.mPreferences.edit();
		this.editor.putBoolean(this.isLogedInweb, isLogedInweb);
		this.editor.commit();
	}

	public boolean getIsbooking() {
		return this.mPreferences.getBoolean(this.isLogbooking, false);
	}

	public void setIsbooking(boolean isLogbooking) {
		this.editor = this.mPreferences.edit();
		this.editor.putBoolean(this.isLogbooking, isLogbooking);
		this.editor.commit();
	}

	public MyPreferences(Context context) {
		setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
	}

	public static MyPreferences getActiveInstance(Context context) {
		if (preferences == null) {
			preferences = new MyPreferences(context);
		}
		return preferences;
	}

	public SharedPreferences getmPreferences() {
		return this.mPreferences;
	}

	public void setmPreferences(SharedPreferences mPreferences) {
		this.mPreferences = mPreferences;
	}

	public Editor getEditor() {
		return this.editor;
	}

	public void setEditor(Editor editor) {
		this.editor = editor;
	}
}
