package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.ApplyJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.AppliedJobList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.LoginActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class JobDescriptionActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.tv_job_description)
    TextView tv_job_description;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;
    @BindView(R.id.ll_success)
    Button ll_success;
    @BindView(R.id.tv_job_title)
            TextView tv_job_title;
    @BindView(R.id.tv_company_name)
            TextView tv_company_name;
    @BindView(R.id.tv_rupee)
            TextView tv_rupee;
    @BindView(R.id.tv_experience)
            TextView tv_experience;
    @BindView(R.id.tv_location)
            TextView tv_location;
    @BindView(R.id.tv_posted)
            TextView tv_posted;

    JobList jobList;
    AppliedJobList appliedJobList;
    TinyDB tinyDB;
    boolean applied = false;
    boolean fromApplied = false;
    String fromClass = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_description);
        ButterKnife.bind(this);
        toolbar.setTitle(this.getResources().getString(R.string.job_description_text));
        setSupportActionBar(toolbar);
        fromClass = getIntent().getStringExtra(AppConstants.FROM_CLASS);
        if (fromClass .equals("applied")) {
            fromApplied = true;
            appliedJobList = getIntent().getParcelableExtra(AppConstants.KEY_CLIENT);
        } else {
            jobList = getIntent().getParcelableExtra(AppConstants.KEY_CLIENT);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tinyDB = new TinyDB(this);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        fullBasics();
        checkJobApplied();

    }

    public void fullBasics() {
        if (fromClass .equals("applied")) {
            tv_job_description.setText(appliedJobList.getJob_description());
            tv_job_title.setText(appliedJobList.getJob_title());
            tv_company_name.setText(appliedJobList.getCompany_name());
            tv_location.setText(appliedJobList.getAddress());
            tv_posted.setText(appliedJobList.getCreated());
            tv_rupee.setText(appliedJobList.getMin_salary() + getResources().getString(R.string.to_text) + appliedJobList.getMax_salary());
            tv_experience.setText(appliedJobList.getMin_exp() + " - " + appliedJobList.getMax_exp() +  " " +  getResources().getString(R.string.sample_experience));

        } else {
            tv_job_description.setText(jobList.getJob_description());
            tv_job_title.setText(jobList.getJob_title());
            tv_company_name.setText(jobList.getCompany_name());
            tv_rupee.setText(jobList.getMin_salary() + getResources().getString(R.string.to_text) + jobList.getMax_salary());
            tv_experience.setText(jobList.getMin_exp() + " - " + jobList.getMax_emp() +  " " +  getResources().getString(R.string.sample_experience));
            tv_location.setText(jobList.getAddress());
            tv_posted.setText(jobList.getCreated());
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void checkJobApplied() {
        if (fromApplied) {
            ll_action_button.setVisibility(View.GONE);
            ll_success.setVisibility(View.VISIBLE);
        } else {
            ArrayList<String> ids;
            ids = tinyDB.getListString(AppConstants.APPLIED_JOBS);

            if (ids.indexOf(jobList.getId()) < 0) {
                ll_success.setVisibility(View.GONE);
                ll_action_button.setVisibility(View.VISIBLE);
            } else {
                ll_action_button.setVisibility(View.GONE);
                ll_success.setVisibility(View.VISIBLE);
            }
        }

    }


    @OnClick({R.id.ll_action_button, R.id.ll_success})
    public void changeLanguage(View view) {

        switch (view.getId()) {
            case R.id.ll_action_button:

                if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
                    showInfo();
                } else {
                    ApplyJobRequest request = new ApplyJobRequest();
                    OROApiAdapter.OROApiService service = OROApiAdapter.getService();
                   // request.randomId = SharedPrefUtils.getAnonymousUserId(this);
                    request.token = SharedPrefUtils.getApiKeyEmployee(this);
                    System.out.println("!!!!#@R@#R@# : " + jobList.getId()+"token "+request.token);
                    request.id = jobList.getId();


                    Call<BaseResponse> apiResponseCall;
                    apiResponseCall =
                            service.applyJob(request);
                    apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                        @Override
                        protected void onStartApi() {
                            showProgressBar();
                        }

                        @Override
                        protected void onEndApi() {
                            hideProgressBar();
                        }

                        @Override
                        public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                            ll_success.setVisibility(View.VISIBLE);
                            ll_action_button.setVisibility(View.GONE);
                            ArrayList<String> ids;
                            ids = tinyDB.getListString(AppConstants.APPLIED_JOBS);
                            ids.add(jobList.getId());
                            tinyDB.putListString(AppConstants.APPLIED_JOBS,ids);
                            Intent vi = new Intent(JobDescriptionActivity.this, AppliedJobActivity.class);
                            vi.putExtra(AppConstants.KEY_CLIENT,jobList );
                            vi.putExtra("from", "description");
                            startActivity(vi);
                        }

                        @Override
                        public void onError(Call<BaseResponse> call, Throwable t) {
                            System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                            //Toast.makeText(JobDescriptionActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                            if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                                SharedPrefUtils.clearSharedPreference(JobDescriptionActivity.this);
                                tinyDB.putString("username","");
                                tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                                Intent intent = new Intent(JobDescriptionActivity.this, EmployerTypeActivity.class);
                                JobDescriptionActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                startActivity(intent);
                            } else {
                                Toast.makeText(JobDescriptionActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                            }
                            Toast.makeText(JobDescriptionActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                        }
                    });
                }
                break;
            case R.id.ll_success:

        }
    }

    public void showInfo() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getResources().getString(R.string.login_first_text));
        alertDialogBuilder.setMessage(getResources().getString(R.string.please_login_description_text));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.proceed_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent vi = new Intent(JobDescriptionActivity.this, LoginActivity.class);
                vi.putExtra(AppConstants.KEY_CLIENT,jobList );
                vi.putExtra("from", "description");
                startActivity(vi);
            }
        });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.dismiss_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            }
        });
        alertDialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (SharedPrefUtils.getAppState(JobDescriptionActivity.this) ==  AppConstants.STATE_REGISTERED){

        } else {
            getMenuInflater().inflate(R.menu.menu_skip, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_skip:
                Intent vi = new Intent(JobDescriptionActivity.this, LoginActivity.class);
                startActivity(vi);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        if (fromClass .equalsIgnoreCase("profile")) {
            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);
        } else {
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }
}
