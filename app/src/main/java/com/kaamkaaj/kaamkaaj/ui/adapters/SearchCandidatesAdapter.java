package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.CandidateSearch;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.DemoProfileViewActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.BindView;
import butterknife.ButterKnife;


// Created by akshaysingh on 5/17/18.



public class SearchCandidatesAdapter extends RecyclerView.Adapter<SearchCandidatesAdapter.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "CollectionListAdapter";
    private ArrayList<CandidateSearch> mDataset;

    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public SearchCandidatesAdapter(Context mContext, ArrayList<CandidateSearch> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        mHandler = new Handler() {
            @Override
            public void close() {

            }

            @Override
            public void flush() {

            }

            @Override
            public void publish(LogRecord record) {

            }
        };
    }

    @Override
    public void onItemClick(int position, View view) {
    /*Intent i = new Intent(mContext, JobsListActivity.class);
        i.putExtra("job_info",mDataset.get(position) );
        mContext.startActivity(i);*/



    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;

        @BindView(R.id.tv_salary)
        TextView tv_salary;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_experience)
        TextView tv_experience;
        @BindView(R.id.tv_skills)
        TextView tv_skills;
        @BindView(R.id.tv_location)
        TextView tv_location;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.each_block)
        LinearLayout each_block;

        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {

            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_search_applicant, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.tv_name.setText(mDataset.get(position).getFull_name());
        if (mDataset.get(position).getExperience() != null && mDataset.get(position).getExperience() .equalsIgnoreCase("0")) {
            holder.tv_experience.setText( mContext.getResources().getString(R.string.fresher_text));
        } else {
            holder.tv_experience.setText(mDataset.get(position).getExperience() + " " + mContext.getResources().getString(R.string.sample_experience));
        }
        holder.tv_location.setText(mDataset.get(position).getCurrent_address());

        if (mDataset.get(position).getSalary() != null && mDataset.get(position).getSalary() .equalsIgnoreCase("0")) {
            holder.tv_salary.setText(mContext.getResources().getString(R.string.na_text));
        } else {
            holder.tv_salary.setText(mDataset.get(position).getSalary());

        }
        holder.tv_skills.setText(mDataset.get(position).getSkill());
            /*Resources res = mContext.getResources();
            String mDrawableName = "ic_cancel_red";
            int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
            Drawable drawable = res.getDrawable(resID );*/


            holder.tv_skills.setText(mDataset.get(position).getSkill());
            holder.each_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, DemoProfileViewActivity.class);
                    i.putExtra("from_class", "search");
                    i.putExtra("job_info", mDataset.get(position));
                    mContext.startActivity(i);
                }
            });

    }


    public void addItem(CandidateSearch dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

}
