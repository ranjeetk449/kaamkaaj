package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.EmployerLoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.EmployerLogin.SocialLoginRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerLoginResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.Constant;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/10/18.
 */

public class EmployerLoginActivity extends NewBaseActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 111;
    public static final String TAG = EmployerLoginActivity.class.getSimpleName();

    @BindView(R.id.et_user_name)
    EditText et_user_name;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_password)
    TextView tv_password;
    @BindView(R.id.tv_goto_signup)
    TextView tv_goto_signup;
    @BindView(R.id.tv_forgot_pass)
    TextView tv_forgot_pass;
    @BindView(R.id.ll_login_button)
    LinearLayout ll_login_button;
    @BindView(R.id.ll_or_signup_with)
            LinearLayout ll_or_signup_with;
    @BindView(R.id.ll_social_logins)
            LinearLayout ll_social_logins;
    @BindView(R.id.ll_facebook_button)
            LinearLayout ll_facebook_button;
    @BindView(R.id.ll_google_button)
            LinearLayout ll_google_button;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.sign_in_button)
    SignInButton sign_in_button;
    String initialUsername = "";
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_employer);
        ButterKnife.bind(this);
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_INDI)) {
            ll_or_signup_with.setVisibility(View.VISIBLE);
            ll_social_logins.setVisibility(View.VISIBLE);
        }

        LoginManager.getInstance().logOut();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
         mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignInClient.signOut();
        sign_in_button.setOnClickListener(this);
        loginButton.setReadPermissions("public_profile email");
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("onSuccess");
                if(AccessToken.getCurrentAccessToken() != null){
                    RequestData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }

        });

    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void RequestData(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {
               // Toast.makeText(EmployerLoginActivity.this,"email ",Toast.LENGTH_LONG).show();

                JSONObject json = response.getJSONObject();
                try {
                //    Toast.makeText(EmployerLoginActivity.this,"email 3qwr: " + json.getString("picture"),Toast.LENGTH_LONG).show();
                    System.out.println("email 3qwr: " + json.getString("picture"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    System.out.println("FAF" + json.getString("email"));
              //      Toast.makeText(EmployerLoginActivity.this,"email 1: " + json.getString("email"),Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if(json != null){
                        /*String text = "<b>Name :</b> "+json.getString("name")+"<br><br><b>Email :</b> "+json.getString("email")+"<br><br><b>Profile link :</b> "+json.getString("link");
                        details_txt.setText(Html.fromHtml(text));*/
                        //Toast.makeText(EmployerLoginActivity.this,"email : " + json.getString("email"),Toast.LENGTH_LONG).show();
                        callLogin( json.getString("email"), "Facebook", json.getString("name"));
                        et_user_name.setText(json.getString("email"));
                        //et.setText(account.getDisplayName());
                        //
                        // profile.setProfileId(json.getString("id"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }






    public void callLogin(String emails, String socialType,String name) {



        RequestParams params = new RequestParams();
        params.put("email", emails);
        params.put("source", socialType);
        params.put("firstName", name);

        String url = RequestUrl.API_URL + RequestUrl.SOCIAL_URL;
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                super.onSuccess(statusCode, headers, response);
                System.out.println("responsesocialDetail" + response);

               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                    // responseDetail{"status":{"response":"failure","message":"","userMessage":"Record not found"}}
                    if (respsuccess.equals("success")) {
                        JSONObject userProfile=status.getJSONObject("data");
                        String token=userProfile.getString("token");
                        System.out.println("teamToken"+token);
                        SharedPrefUtils.setAppState(EmployerLoginActivity.this, AppConstants.STATE_REGISTERED);
                        SharedPrefUtils.setApiKey(EmployerLoginActivity.this, token);
                        SharedPrefUtils.setUserLoginType(EmployerLoginActivity.this,socialType);
                        Intent intent = new Intent(EmployerLoginActivity.this, EmployerDashboardActivity.class);
                        EmployerLoginActivity.this.finish();
                        startActivity(intent);
                    }else{
                        CommonMethod.showAlert(userMessage, EmployerLoginActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable
                    throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CommonMethod.showAlert("Unable to connect the server,please try again", EmployerLoginActivity.this);
                hideProgressBar();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgressBar();
            }
        });

    }




/*
    public void callLogin(String emails, String socialType,String name) {



        SocialLoginRequest request = new SocialLoginRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.email = emails;
        request.firstName = name;
        request.source = socialType;

        //request.token = SharedPrefUtils.getApiKey(this);


        Call<EmployerLoginResponse> apiResponseCall;
        apiResponseCall =
                service.loginWithSocial(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EmployerLoginResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {
                System.out.println("RESP"+response);
                SharedPrefUtils.setAppState(EmployerLoginActivity.this, AppConstants.STATE_REGISTERED);
                SharedPrefUtils.setApiKey(EmployerLoginActivity.this, response.getData().getToken());
                Intent intent = new Intent(EmployerLoginActivity.this, EmployerDashboardActivity.class);
                EmployerLoginActivity.this.finish();
                startActivity(intent);

            }

            @Override
            public void onError(Call<EmployerLoginResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerLoginActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {

                Toast.makeText(EmployerLoginActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();

            }

        });

    }*/

    @OnClick({R.id.tv_goto_signup, R.id.tv_forgot_pass, R.id.ll_login_button, R.id.ll_google_button})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_goto_signup:
                intent = new Intent(this, EmployerSignupActivity.class);
                this.finish();
                this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(intent);
                break;
            case R.id.tv_forgot_pass:
               /* intent = new Intent(this, EmployerForgotPassword.class);
                this.finish();
                this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(intent);*/
                break;
            case R.id.ll_login_button:
                String email = et_user_name.getText().toString();
                String pass = et_password.getText().toString();
                if (email .equals("")) {
                    // setErrorMsg(getResources().getString(R.string.email_text),et_user_name);
                    et_user_name.setError(getResources().getString(R.string.email_text));
                    return;
                }

                if (pass .equals("")) {
                    et_password.setError(getResources().getString(R.string.enter_password_text));
                    return;
                }

                RequestParams params = new RequestParams();
                params.put("email", email);
                params.put("password", pass);
                if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_INDI)) {
                    params.put("userType", "individual");
                } else {

                    params.put("userType", "corporate");
                }
                if (CommonMethod.isOnline(EmployerLoginActivity.this)) {
                    login(params);
                } else {
                    CommonMethod.showAlert("Network connection appears to be disconnected, please check your internet connection", EmployerLoginActivity.this);
                }
                /*intent = new Intent(this, EmployerDashboardActivity.class);
                this.finish();
                this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(intent);*/
                break;
            case R.id.ll_google_button:
                signIn();
                break;
        }
    }

    public static void setErrorMsg(String msg,EditText viewId)
    {
        //Osama ibrahim 10/5/2013
        int ecolor = Color.RED; // whatever color you want
        String estring = msg;
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
        ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
        viewId.setError(ssbuilder);

    }




    public void login(RequestParams params) {

        String url = RequestUrl.API_URL + RequestUrl.LOGIN_URL;
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                super.onSuccess(statusCode, headers, response);
                System.out.println("responseLogin" + response);

               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                   // responseDetail{"status":{"response":"failure","message":"","userMessage":"Record not found"}}
                    if (respsuccess.equals("success")) {
                        JSONObject userProfile=status.getJSONObject("data");
                        String token=userProfile.getString("token");
                        JSONObject userProfiledet=userProfile.getJSONObject("userProfile");
                        String name=userProfiledet.getString("name");
                        String email=userProfiledet.getString("email");
                        //String mobileNo=userProfiledet.getString("mobileNo");
                        //System.out.println("teamToken"+token+email+mobileNo);
                        SharedPrefUtils.setAppState(EmployerLoginActivity.this, AppConstants.STATE_REGISTERED);
                        SharedPrefUtils.setApiKey(EmployerLoginActivity.this, token);
                        SharedPrefUtils.setEmployerName(EmployerLoginActivity.this, name);
                        SharedPrefUtils.setEmployerEmail(EmployerLoginActivity.this, email);
                       // SharedPrefUtils.setEmployerMobile(EmployerLoginActivity.this, mobileNo);
                        SharedPrefUtils.setUserLoginType(EmployerLoginActivity.this,"Normal");
                        Intent intent = new Intent(EmployerLoginActivity.this, EmployerDashboardActivity.class);
                        EmployerLoginActivity.this.finish();
                        startActivity(intent);

                    }else{
                        CommonMethod.showAlert(userMessage, EmployerLoginActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable
                    throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CommonMethod.showAlert("Unable to connect the server,please try again", EmployerLoginActivity.this);
                hideProgressBar();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgressBar();
            }
        });

    }






/*
    public void login() {
        String email = et_user_name.getText().toString();
        String pass = et_password.getText().toString();
        if (email .equals("")) {
           // setErrorMsg(getResources().getString(R.string.email_text),et_user_name);
            et_user_name.setError(getResources().getString(R.string.email_text));
            return;
        }

        if (pass .equals("")) {
            et_password.setError(getResources().getString(R.string.enter_password_text));
            return;
        }

        EmployerLoginRequest request = new EmployerLoginRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setEmailId(email);
        request.setPassword(pass);
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_INDI)) {
            request.setUserType("individual");
        } else {
            request.setUserType("corporate");
        }
        System.out.println("resrequest" + request.toString() );
        //request.token = SharedPrefUtils.getApiKey(this);


        Call<EmployerLoginResponse> apiResponseCall;
        apiResponseCall =
                service.employerLogin(request);
        System.out.println("resrequest12" + apiResponseCall.toString() );
        apiResponseCall.enqueue(new RetrofitRequestListener<EmployerLoginResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }


            @Override
            public void onSuccess(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {

                System.out.println("reslogin" + response.toString() );
                /*SharedPrefUtils.setAppState(EmployerLoginActivity.this, AppConstants.STATE_REGISTERED);
                SharedPrefUtils.setApiKey(EmployerLoginActivity.this, response.getData().getToken());
                Intent intent = new Intent(EmployerLoginActivity.this, EmployerDashboardActivity.class);
                EmployerLoginActivity.this.finish();
                startActivity(intent);*/

           /* }

            @Override
            public void onError(Call<EmployerLoginResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployerLoginActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCustomError(Call<EmployerLoginResponse> call, EmployerLoginResponse response) {

                Toast.makeText(EmployerLoginActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();

            }

        });
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            System.out.println("@@$@$@R@! : " + account.getEmail());

            et_user_name.setText(account.getEmail());
            callLogin(account.getEmail(), "Google",account.getDisplayName());
            // Signed in successfully, show authenticated UI.
           // updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed cod e=" + e.getStatusCode());
          //  updateUI(null);
        }
    }

    @OnFocusChange({R.id.et_password,R.id.et_user_name, R.id.tv_goto_signup})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_user_name:
                usernameFocus(view, hasFocus);
                break;
            case R.id.et_password:
                passwordFocus(view, hasFocus);

        }
    }

    public void usernameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_user_name.setVisibility(View.VISIBLE);
            et_user_name.setHint("");
        } else {
            if (et_user_name.getText().toString() .equals(initialUsername)) {
                tv_user_name.setVisibility(View.INVISIBLE);
            } else {
                tv_user_name.setVisibility(View.VISIBLE);

            }
            et_user_name.setHint(getResources().getString(R.string.email_text));
        }
    }

    public void passwordFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_password.setVisibility(View.VISIBLE);
            et_password.setHint("");
        } else {
            if (et_password.getText().toString() .equals(initialUsername)) {
                tv_password.setVisibility(View.INVISIBLE);
            } else {
                tv_password.setVisibility(View.VISIBLE);

            }
            et_password.setHint(getResources().getString(R.string.password_text));
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
