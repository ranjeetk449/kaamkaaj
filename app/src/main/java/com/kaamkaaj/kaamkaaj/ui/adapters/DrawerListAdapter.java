package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by akshaysingh on 3/29/18.
 */

public class DrawerListAdapter extends BaseAdapter {
    Activity activity;
    private static LayoutInflater inflater=null;
    ArrayList<String> titles;
    ArrayList<String> nameIcons;
    public DrawerListAdapter(Activity activity, ArrayList<String> titles, ArrayList<String> nameIcons) {
// TODO Auto-generated constructor stub
        this.titles = titles;
        this.activity = activity;
        this.nameIcons = nameIcons;
        inflater = ( LayoutInflater )activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
// TODO Auto-generated method stub
        return titles.size();
    }

    @Override
    public Object getItem(int position) {
// TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
// TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv_title;
        ImageView im_icon;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        Holder holder=new Holder();
        View view;
        view = inflater.inflate(R.layout.list_item_drawer_menu, null);

        holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
        holder.im_icon = (ImageView) view.findViewById(R.id.im_icon);

        holder.tv_title.setText(titles.get(position));

        Resources res = activity.getResources();
        String mDrawableName = nameIcons.get(position);
        int resID = res.getIdentifier(mDrawableName , "drawable", activity.getPackageName());
        Drawable drawable = res.getDrawable(resID );
        /*if (position % 2 == 0) {
            setMargins(holder.ll_each_block, 16, 16,16,0);
        } else {
            setMargins(holder.ll_each_block,16,16,0,0);
        }*/
        holder.im_icon.setImageDrawable(drawable );


        return view;
    }

}