package com.kaamkaaj.kaamkaaj.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.Crashlytics;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.dbs.NewUsersDbs;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import io.fabric.sdk.android.Fabric;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import io.fabric.sdk.android.Fabric;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;

/**
 * Created by akshaysingh on 3/26/18.
 */

public class SplashActivity  extends Activity {

    NewUsersDbs dbs;
    static SecureRandom rnd;
    TinyDB tinyDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        dbs = new NewUsersDbs(this);
        tinyDB = new TinyDB(this);
        rnd  = new SecureRandom();
        init();
    }

    public void init() {
        dbs = new NewUsersDbs(this);
        String unquie_id = "";
        String user_id = dbs.fetch_user_id();
     //   Toast.makeText(this, " USER ID : : " + user_id, Toast.LENGTH_LONG).show();
        if (user_id .equals(AppConstants.FAILED_ID_MSG)) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            unquie_id = getUnique(timestamp.toString());
         //   Toast.makeText(this, " USER ID : : " + unquie_id, To ast.LENGTH_LONG).show();
            dbs.add_new_user("John Doe", unquie_id);
            SharedPrefUtils.setUserAnonymousIds(this,unquie_id);
            SharedPrefUtils.setUserAnonymous(this);
            startMainActivity();

        } else {
            unquie_id = user_id;
            startMainActivity();
        }
    }

    public void startMainActivity() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // this code will be executed after 2 seconds
                splashViewFinish();
            }
        }, 2000
        );
    }

    public void splashViewFinish() {

        Intent intent;

        Log.d(TAG, "apikey: " + SharedPrefUtils.getApiKey(this));

        if (!SharedPrefUtils.getPromoState(this)) {
            intent = new Intent(this, SelectLanguageActivity.class);
            intent.putExtra(AppConstants.FROM_STRING,"splash");
            startActivity(intent);

            //SharedPrefUtils.setPromoState(this, true);
        } else {
            if (tinyDB.getString(AppConstants.USER_TYPE_MAIN) .equals(AppConstants.USER_EMPLOYER)) {

                if (SharedPrefUtils.getAppState(SplashActivity.this) == AppConstants.STATE_REGISTERED) {
                    intent = new Intent(this, EmployerDashboardActivity.class);
                    intent.putExtra(AppConstants.FROM_STRING,"splash");
                    startActivity(intent);

                } else {

                    intent = new Intent(this, SelectLanguageActivity.class);
                    intent.putExtra(AppConstants.FROM_STRING,"splash");
                    startActivity(intent);
                }
            } else {

                if (SharedPrefUtils.getAppState(SplashActivity.this) == AppConstants.STATE_REGISTERED) {
                    intent = new Intent(this, CustomerDashboardActivity.class);
                    intent.putExtra(AppConstants.FROM_STRING,"splash");
                   // System.out.print();
                    startActivity(intent);

                } else {

                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(AppConstants.FROM_STRING,"splash");
                    startActivity(intent);
                }
            }

            /*Intent i = new Intent(this,MainActivity.class);*/
            //Intent i = new Intent(this,EmployerLoginActivity.class);

          //  startActivity(i);
        }

        // finish();
        // this.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);

    }

    public String getUnique(String ids) {
        StringBuilder sb = new StringBuilder( 50 );
        for( int i = 0; i < 50; i++ )
            sb.append( ids.charAt( rnd.nextInt(ids.length()) ) );
        return (ids + sb.toString());
    }

}
