package com.kaamkaaj.kaamkaaj.ui.activities.users;
import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSkillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.UpdateProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSubSkillsList;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.JobDescriptionActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.FilePath;
import com.kaamkaaj.kaamkaaj.utils.Geofencing;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/19/18.
 */

public class EditProfileActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.top_bar) Toolbar toolbar;
    @BindView(R.id.ll_testeng) LinearLayout ll_testeng;
    @BindView(R.id.imageView1) CircleImageView imageView1;
    @BindView(R.id.tv_name) TextView tv_name;
  //  @BindView(R.id.ll_location_val) LinearLayout ll_location_val;
    @BindView(R.id.ll_action_button) LinearLayout ll_action_button;
    @BindView(R.id.ed_father_name) EditText ed_father_name;
    @BindView(R.id.ed_full_name) EditText ed_full_name;
    @BindView(R.id.ed_dob) EditText ed_dob;

    @BindView(R.id.ss_gender) Spinner ss_gender;
    @BindView(R.id.ed_per_add) EditText ed_per_add;
    @BindView(R.id.ed_location_val) EditText ed_location_val;
    @BindView(R.id.ll_subskills) LinearLayout ll_subskills;
    /*@BindView(R.id.ed_profile_pic)
            EditText ed_profile_pic;*/
    @BindView(R.id.ss_proof) Spinner ss_proof;
    @BindView(R.id.ll_numbers) LinearLayout ll_number;
    @BindView(R.id.tv_doc_number) TextView tv_doc_number;
    @BindView(R.id.et_doc_number) EditText et_doc_number;
    @BindView(R.id.ss_experience) Spinner ss_experience;
    @BindView(R.id.ss_skills) Spinner ss_skills;

    @BindView(R.id.ss_bank) Spinner ss_bank;
    @BindView(R.id.ed_salary) EditText ed_salary;
            LinearLayout ll_check_add;
    @BindView(R.id.ll_other_skills)
    LinearLayout ll_other_skills;
    @BindView(R.id.et_other_skills)
    EditText et_other_skills;
    @BindView(R.id.ll_other_subskills)
    LinearLayout ll_other_subskills;
    @BindView(R.id.et_other_subskills)
    EditText et_other_subskills;
    @BindView(R.id.ll_full_time)
    LinearLayout ll_full_time;
    @BindView(R.id.ll_part_time) LinearLayout ll_part_time;
    @BindView(R.id.ll_freelancer) LinearLayout ll_freelancer;
    @BindView(R.id.iv_freelance)
    ImageView iv_freelance;
    @BindView(R.id.iv_full_time) ImageView iv_full_time;
    @BindView(R.id.iv_part_time)
    ImageView iv_part_time;
    @BindView(R.id.check_addressSame)
    CheckBox check_addressSame;
   // @BindView(R.id.tv_location_val) TextView tv_location_val;
    @BindView(R.id.ms_subskills)
    MultiSelectSpinner ms_subskills;
    @BindView(R.id.ms_subskillsnew)
    MultipleSelectionSpinner ms_subskillsnew;
    TinyDB tinyDB;
    ArrayList<JobSkillsList> jobSkillsLists;
    ArrayAdapter<String> skillsAdapter;
    List<String> skillsList;

    ArrayList<JobSubSkillsList> jobSubSkillsLists;
    List<String> subSkillsList;
    ArrayList<String> optionsSubskills;
    Bitmap bitmap;
    JobList jobList = null;
    String from = "temp";
    String selectedCard = "", lat = "", lng = "", cityName = "";
    Boolean add_check = false;
    Intent intent;
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    Boolean part = false, full = false, freelance = false;

    private static final int PLACE_PICKER_REQUEST = 2;
    private Geofencing mGeofencing;
    private GoogleApiClient mClient;
    public static final String TAG = EditProfileActivity.class.getSimpleName();

    private static final int PICK_FILE_REQUEST = 1;
    private ProgressDialog prgDialog1;

    String fromClass = "";

    private String selectedFilePath;
    //private String SERVER_URL = "http://charlie.orowealth.com:3008/uploadCanDocs";
    private String SERVER_URL = "http://139.59.30.24/talific/v1/documents/uploadDocument";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.update_profile_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        EnableRuntimePermission();
     //   setProfilePic();
        tinyDB = new TinyDB(this);
        if(getIntent().getStringExtra("from") != null) {
            jobList = getIntent().getParcelableExtra(AppConstants.KEY_CLIENT);
            from = getIntent().getStringExtra("from");
            fromClass = getIntent().getStringExtra("fromClass");
        }
        System.out.println("respjobListEditProfile"+jobList+from);
        skillsList  = new ArrayList<String>();
        skillsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, skillsList);
        ss_skills.setAdapter(skillsAdapter);

        subSkillsList = new ArrayList<String>();

        optionsSubskills = new ArrayList<>();
        ms_subskills.setTitle(getResources().getString(R.string.subskills_text));

        //ms_subskillsnew.

        selectedCard = getResources().getString(R.string.adhaar_card_number_text);
        jobSkillsLists = new ArrayList<JobSkillsList>();
        jobSubSkillsLists = new ArrayList<JobSubSkillsList>();


        fetchSkills();
        mGeofencing = new Geofencing(this, mClient);
        mGeofencing.registerAllGeofences();
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(EditProfileActivity.this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();

       // init();
    }

    void init() {
        ed_father_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_full_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_dob.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_per_add.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_doc_number.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_other_skills.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_other_subskills.setFilters(new InputFilter[] {new InputFilter.AllCaps()});


    }















    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            System.out.println("responseDetailIF");
            return;
        } else {
            System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKeyEmployee(this);
            System.out.println("responseDetailELSE"+token);
            RequestParams params = new RequestParams();
            params.put("token", token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_PROFILE_EMPLOYEE;
            System.out.println("resProfileUrl"+url);
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("reseditUserProfile" + response);

               /*



                 responseDetail{"status":{"response":"success","message":"","userMessage":""},
                 "data":{"userProfile":{"email":"rpathaksoftware@gmail.com","firstName":"ravi","lastName":"pathak",
                 "mobileNo":"121212","password":"123456","userType":"corporate",
                 "createdAt":"2019-07-21T11:11:20.521Z","updatedAt":"2019-07-21T11:11:20.521Z",
                 "id":"5d344858c462bf110673cdc1"}}}



*/




                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        //String userProfile=status.getString("data");
                        // System.out.println("responseuserProfile" + userProfile);
                        String respsuccess=status.getString("response");
                        System.out.println("responseDetaiddddl" + respsuccess);
                        if (respsuccess.equals("success")) {
                            System.out.println("responseuserProfilescsd");
                            JSONObject userProfile=response.getJSONObject("data");
                            System.out.println("responseuserProfile" + userProfile);
                            JSONObject userProfileList=userProfile.getJSONObject("userProfile");
                            //  System.out.println("respppuserProfilee" + userProfilee);
                           /* String mobile_no=userProfileList.getString("mobile_no");
                            String full_name=userProfileList.getString("full_name");
                            String gender=userProfileList.getString("gender");
                            String dob=userProfileList.getString("dob");
                            String current_address=userProfileList.getString("current_address");
                            String permanent_address=userProfileList.getString("permanent_address");
                            String salary=userProfileList.getString("salary");
                            String created=userProfileList.getString("created");
                            String statusUser=userProfileList.getString("status");
                            String fathersName=userProfileList.getString("fathersName");
                            String idProofDoc=userProfileList.getString("idProofDoc");
                            String subSkill=userProfileList.getString("subSkill");
                            String skill=userProfileList.getString("skill");
                            String experience=userProfileList.getString("experience");
                            String city_name=userProfileList.getString("city_name");
*/




/*
                            tv_name.setText(response.getData().getUserProfile().getFull_name());
                            ed_full_name.setText(response.getData().getUserProfile().getFull_name());
                            ed_father_name.setText(response.getData().getUserProfile().getFathers_name());
                            ed_dob.setText(response.getData().getUserProfile().getDob());
                            ed_per_add.setText(response.getData().getUserProfile().getPermanent_address());
                            ed_salary.setText(response.getData().getUserProfile().getSalary());
                            cityName = String.valueOf(response.getData().getUserProfile().getCity_name());

                           */


                            if(userProfileList.has("fullName"))
                            {
                                String full_namee=userProfileList.getString("fullName");
                               // tinyDB.putString("username" , full_namee);
                                ed_full_name.setText(full_namee);
                            }


                            if(userProfileList.has("fathersName"))
                            {
                                String fathersName=userProfileList.getString("fathersName");
                                ed_father_name.setText(fathersName);
                            }
                           /* if(userProfileList.has("gender"))
                            {
                                String gender=userProfileList.getString("gender");
                                ed_gender.setText(gender);
                            }*/
                            if(userProfileList.has("dob"))
                            {
                                String dob=userProfileList.getString("dob");
                                ed_dob.setText(dob);
                            }
                            if(userProfileList.has("current_address"))
                            {
                                String current_address=userProfileList.getString("current_address");
                                ed_location_val.setText(current_address);
                                //ed_location_val.setText(response.getData().getUserProfile().getCurrent_address());
                                ed_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));

                            }

                            if(userProfileList.has("permanentAddress"))
                            {
                                String permanent_address=userProfileList.getString("permanentAddress");
                                ed_per_add.setText(permanent_address);
                            }


                           // et_doc_number.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
                           // ed_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

                            String docIdd=userProfileList.getString("docId");
                            System.out.println("respdocIdd"+docIdd);
                            if(userProfileList.has("docId"))
                            {
                                String docId=userProfileList.getString("docId");
                                et_doc_number.setText(docId);
                               // et_doc_number.setText(userProfileList.getString("docId"));
                            }



                            if(userProfileList.has("salary"))
                            {
                                String salary=userProfileList.getString("salary");
                                ed_salary.setText(salary);
                            }


/*
                            if(userProfileList.has("experience"))
                            {
                                String experience=userProfileList.getString("experience");
                                ed_experience.setText(experience);
                            }
                            if(userProfileList.has("skill"))
                            {
                                String skill=userProfileList.getString("skill");
                                ed_skills.setText(skill);
                            }
                            if(userProfileList.has("subSkill"))
                            {
                                String subSkill=userProfileList.getString("subSkill");
                                ed_subskills.setText(subSkill);
                            }*/



                            String[] id_proof = getResources().getStringArray(R.array.id_proof);
                            String[] genders = getResources().getStringArray(R.array.gender);
                            String[] yesNo = getResources().getStringArray(R.array.yes_no);

                            System.out.println("respid_proof" + id_proof);
                            System.out.println("respgenders" + genders);

                            int proofPos = 0, gen = 0,yesno = 0;

                            for (int i =0; i < yesNo.length; i++) {
                                if (yesNo[i] .equalsIgnoreCase(userProfileList.getString("bankAccount"))) {
                                    yesno = i;
                                }
                            }
                            ss_bank.setSelection(yesno);
                            for (int i =0; i < id_proof.length; i++) {
                                if (id_proof[i] .equalsIgnoreCase(userProfileList.getString("idProofDoc"))) {
                                    proofPos = i;
                                }
                            }

                            for (int i =0; i < genders.length; i++) {
                                System.out.println("respgenders.length" +genders.length);
                                System.out.println("respgenderstest" + genders[i]);
                                if (genders[i] .equalsIgnoreCase(userProfileList.getString("gender"))) {
                                    gen = i;
                                }
                            }
                            ss_gender.setSelection(gen);

                            String[] exps = getResources().getStringArray(R.array.experience);
                            String tmpExp = "";
                            if (userProfileList.getString("experience") .contains("10") && !userProfileList.getString("experience") .contains("9")) {
                                tmpExp = userProfileList.getString("experience") + getResources().getString(R.string.years_or_more_replace_text);
                            } else {
                                tmpExp = userProfileList.getString("experience") + getResources().getString(R.string.replace_year_text);

                            }
                            int expId = 0;
                            for (int i =0; i < exps.length; i++) {
                                /*if (exps[i] .equalsIgnoreCase(tmpExp)) {
                                    expId = i;
                                }*/
                                 if (exps[i] .equals(userProfileList.getString("experience"))) {
                                    expId = i;
                                }
                            }
                            System.out.println("respexp: " +tmpExp+expId);
                            int skillsId = 0;
                            for (int i=0; i < jobSkillsLists.size(); i++) {
                                System.out.println("@@@@@@@@@@@@@@@@@ : " + jobSkillsLists.get(i).getName());
                                if (jobSkillsLists.get(i).getName() .equals(userProfileList.getString("skill"))) {
                                    skillsId = i;
                                }
                            }

                            int subSkillsId = 0;
                            for (int i=0; i < optionsSubskills.size(); i++) {
                                System.out.println("respsubskilldata : " + optionsSubskills.get(i));
                                if (optionsSubskills.get(i) .equals(userProfileList.getString("subSkill"))) {
                                    subSkillsId = i;
                                }
                            }

                            System.out.println("respsubskilldata : " + subSkillsId);
                            String tempType = userProfileList.getString("jobType");
                            if (tempType .contains(getResources().getString(R.string.fulltime_text))) {
                                Resources res = getResources();
                                String mDrawableName = "ic_check_blue";
                                int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                                Drawable drawable = res.getDrawable(resID );
                                iv_full_time.setImageDrawable(drawable );
                                full = true;
                            }
                            if (tempType .contains(getResources().getString(R.string.parttime_text))) {
                                Resources res = getResources();
                                String mDrawableName = "ic_check_blue";
                                int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                                Drawable drawable = res.getDrawable(resID );
                                iv_part_time.setImageDrawable(drawable );
                                part  =true;
                            }
                            if (tempType .contains(getResources().getString(R.string.freelancer_text))) {
                                Resources res = getResources();
                                String mDrawableName = "ic_check_blue";
                                int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                                Drawable drawable = res.getDrawable(resID );
                                iv_freelance.setImageDrawable(drawable );
                                freelance  =true;
                            }
                            ss_skills.setSelection(skillsId);
                            ms_subskills.setSelection(subSkillsId);
                            ss_proof.setSelection(proofPos);

                            ss_experience.setSelection(expId);

                            lat = String.valueOf(userProfileList.getString("lat"));
                            lng = String.valueOf(userProfileList.getString("lng"));
                           // ed_location_val.setText(response.getData().getUserProfile().getCurrent_address());
                           // ed_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));


                        }else{
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", EditProfileActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }










/*
    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            System.out.println("responseDetailIF");
            return;
        } else {
            System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKey(this);
            System.out.println("responseDetailELSE"+token);
            RequestParams params = new RequestParams();
            params.put("token", token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_PROFILE_EMPLOYEE;
            System.out.println("resProfileUrl"+url);
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseEditProfile" + response);

               /*



                 responseDetail{"status":{"response":"success","message":"","userMessage":""},
                 "data":{"userProfile":{"email":"rpathaksoftware@gmail.com","firstName":"ravi","lastName":"pathak",
                 "mobileNo":"121212","password":"123456","userType":"corporate",
                 "createdAt":"2019-07-21T11:11:20.521Z","updatedAt":"2019-07-21T11:11:20.521Z",
                 "id":"5d344858c462bf110673cdc1"}}}



*/



/*
                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        //String userProfile=status.getString("data");
                        // System.out.println("responseuserProfile" + userProfile);
                        String respsuccess=status.getString("response");
                        System.out.println("responseDetaiddddl" + respsuccess);
                        if (respsuccess.equals("success")) {
                            System.out.println("responseuserProfilescsd");
                            JSONObject userProfile=response.getJSONObject("data");
                            System.out.println("responseuserProfile" + userProfile);
                            JSONObject userProfileList=userProfile.getJSONObject("userProfile");
                            //  System.out.println("respppuserProfilee" + userProfilee);
                            String email=userProfileList.getString("email");
                            String firstName=userProfileList.getString("firstName");
                            String lastName=userProfileList.getString("lastName");
                            String mobileNo=userProfileList.getString("mobileNo");
                            String password=userProfileList.getString("password");
                            String userType=userProfileList.getString("userType");

                            if(userProfileList.has("company_name"))
                            {
                                String company_name=userProfileList.getString("company_name");
                                et_company_name.setText(company_name);
                            }
                            if(userProfileList.has("designation"))
                            {
                                String designation=userProfileList.getString("designation");
                                et_designation.setText(designation);
                            }
                            if(userProfileList.has("company_website"))
                            {
                                String company_website=userProfileList.getString("company_website");
                                et_company_website.setText(company_website);
                            }
                            if(userProfileList.has("company_address"))
                            {
                                String company_address=userProfileList.getString("company_address");
                                et_company_address.setText(company_address);
                            }


                            // String token=userProfile.getString("token");
                            // System.out.println("teamToken"+token);



                            tinyDB.putString("username" ,firstName + " " + lastName);
                            // et_company_name.setText(response.getData().userProfile.getCompany_name());

                            et_your_name.setText(firstName + " " + lastName);

                            // et_designation.setText(response.getData().userProfile.getDesignation());
                            et_email.setText(email);
                            // et_company_website.setText(response.getData().userProfile.getCompany_website());
                            et_mobile_number.setText(mobileNo);
                       /* if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                            et_company_address.setText(response.getData().userProfile.getCompany_address());
                        } else {
                            et_company_address.setText(response.getData().userProfile.getAddress());

                        }*/



/*
                        }else{
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", EditProfileActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }










/*

    public void fetchProfile() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchProfileRequest request = new FetchProfileRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));


            Call<FetchProfileResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchProfileRequest(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<FetchProfileResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {

                    showProgressBar();


                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<FetchProfileResponse> call, FetchProfileResponse response) {
                    tv_name.setText(response.getData().getUserProfile().getFull_name());
                    ed_full_name.setText(response.getData().getUserProfile().getFull_name());
                    ed_father_name.setText(response.getData().getUserProfile().getFathers_name());
                    ed_dob.setText(response.getData().getUserProfile().getDob());
                    ed_per_add.setText(response.getData().getUserProfile().getPermanent_address());
                    ed_salary.setText(response.getData().getUserProfile().getSalary());
                    cityName = String.valueOf(response.getData().getUserProfile().getCity_name());
                    String[] id_proof = getResources().getStringArray(R.array.id_proof);
                    String[] genders = getResources().getStringArray(R.array.gender);

                    int proofPos = 0, gen = 0;
                    for (int i =0; i < id_proof.length; i++) {
                        if (id_proof[i] .equalsIgnoreCase(response.getData().getUserProfile().getId_proof_doc())) {
                            proofPos = i;
                        }
                    }

                    for (int i =0; i < genders.length; i++) {
                        if (genders[i] .equalsIgnoreCase(response.getData().getUserProfile().getGender())) {
                            gen = i;
                        }
                    }
                    String[] exps = getResources().getStringArray(R.array.experience);
                    String tmpExp = "";
                    if (response.getData().getUserProfile().getExperience() .contains("10") && !response.getData().getUserProfile().getExperience() .contains("9")) {
                        tmpExp = response.getData().getUserProfile().getExperience() + getResources().getString(R.string.years_or_more_replace_text);
                    } else {
                        tmpExp = response.getData().getUserProfile().getExperience() + getResources().getString(R.string.replace_year_text);

                    }
                    int expId = 0;
                    for (int i =0; i < exps.length; i++) {
                        if (exps[i] .equalsIgnoreCase(tmpExp)) {
                            expId = i;
                        }
                    }
                    int skillsId = 0;
                    for (int i=0; i < jobSkillsLists.size(); i++) {
                        System.out.println("@@@@@@@@@@@@@@@@@ : " + jobSkillsLists.get(i).getName());
                        if (jobSkillsLists.get(i).getName() .equals(response.getData().getUserProfile().getSkill())) {
                            skillsId = i;
                        }
                    }
                    String tempType = response.getData().getUserProfile().getJob_type();
                    if (tempType .contains(getResources().getString(R.string.fulltime_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_full_time.setImageDrawable(drawable );
                        full = true;
                    }
                    if (tempType .contains(getResources().getString(R.string.parttime_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_part_time.setImageDrawable(drawable );
                        part  =true;
                    }
                    if (tempType .contains(getResources().getString(R.string.freelancer_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_freelance.setImageDrawable(drawable );
                        freelance  =true;
                    }
                    ss_skills.setSelection(skillsId);
                    ss_proof.setSelection(proofPos);
                    ss_gender.setSelection(gen);
                    ss_experience.setSelection(expId);
                    et_doc_number.setText(response.getData().getUserProfile().getDoc_id());
                    lat = String.valueOf(response.getData().getUserProfile().getLat());
                    lng = String.valueOf(response.getData().getUserProfile().getLng());
                    tv_location_val.setText(response.getData().getUserProfile().getCurrent_address());
                    tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));


                }

                @Override
                public void onError(Call<FetchProfileResponse> call, Throwable t) {

                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );

                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<FetchProfileResponse> call, FetchProfileResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(EditProfileActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(EditProfileActivity.this, EmployerTypeActivity.class);
                        EditProfileActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }


                }

            });
        }
    }
*/
    public void fetchSkills() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSkillsRequest request = new FetchSkillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            Call<JobSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    jobSkillsLists = response.getData().getSkills();
                    for (int i = 0; i < response.getData().getSkills().size(); i++) {
                        System.out.println("|||| : " + response.getData().getSkills().get(i).getName() );
                        skillsAdapter.add(response.getData().getSkills().get(i).getName());
                        skillsAdapter.notifyDataSetChanged();
                    }
                    fetchProfile();

                }

                @Override
                public void onError(Call<JobSkillsResponse> call, Throwable t) {

                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                }

                @Override
                public void onCustomError(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(EditProfileActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(EditProfileActivity.this, EmployerTypeActivity.class);
                        EditProfileActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }

            });
        }
    }


    @OnClick({R.id.ll_freelancer, R.id.ll_part_time, R.id.ll_full_time,R.id.imageView1, R.id.tv_name,R.id.ll_action_button, R.id.ed_dob,R.id.check_addressSame})
    public void openCam(View view) {


        switch (view.getId()) {
           // case R.id.ll_location_val:
               // onAddPlaceButtonClicked(tv_location_val);
              //  break;

            case R.id.ll_freelancer:
                if (freelance) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance  =true;
                }
                break;
            case R.id.ll_part_time:
                if (part) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part  =true;
                }
                break;
            case R.id.ll_full_time:
                if (full) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = true;
                }
                break;
            case R.id.imageView1:
                searchJobsAlert();
                break;
            case R.id.tv_name:
                break;
            case R.id.ed_dob:
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);

                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(this, myDateListener, year, month, day).show();
                break;
            case R.id.ll_action_button:
                updateprofile();
                break;
            case R.id.check_addressSame:
                copyPermanantAddToCurrent(check_addressSame);
                break;
        }
    }

    public void copyPermanantAddToCurrent(View view) {



        if (((CheckBox) view).isChecked()) {
            Toast.makeText(this,
                    "Bro, cheked Android :)", Toast.LENGTH_LONG).show();
            String peraddress=ed_per_add.getText().toString();
            ed_location_val.setText(peraddress);





        }else{
            Toast.makeText(this,
                    "Bro, uncheked Android :)", Toast.LENGTH_LONG).show();
            ed_location_val.setText(" ");
        }
    }
    public void searchJobsAlert() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_upload_pic);
        dialog.setCancelable(true);
        LinearLayout ll_skills_tab = (LinearLayout) dialog.findViewById(R.id.ll_skills_tab);
       // LinearLayout ll_geo_tab = (LinearLayout) dialog.findViewById(R.id.ll_geo_tab);
        ll_skills_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Intent i = new Intent(CustomerDashboardActivity.this, JobFilterActivity.class);
                //   startActivity(i);
                dialog.dismiss();
                intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 7);
            }
        });
       /* ll_geo_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Intent i = new Intent(CustomerDashboardActivity.this, GeolocationJobsSearchFormActivity.class);
                //   startActivity(i);
                dialog.dismiss();

                uploadFile();
            }
        });*/

        dialog.show();
    }

    public void updateprofile() {
        {
            if (ed_full_name.getText().length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_full_name_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (ed_father_name.getText().length() == 10) {
                Toast.makeText(this, getResources().getString(R.string.enter_father_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (ed_dob.getText().length() == 0 ) {
                Toast.makeText(this, getResources().getString(R.string.enter_dob_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (ed_per_add.getText().length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_per_add_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (ed_location_val.getText().length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_cur_add_text), Toast.LENGTH_LONG).show();
                return;
            }
           /* String location = tv_location_val.getText().toString();
            if (location .equalsIgnoreCase(getResources().getString(R.string.select_current_address_text))) {
                Toast.makeText(EditProfileActivity.this,getResources().getString(R.string.enter_location_text),Toast.LENGTH_LONG).show();
                return;
            }*/

            Toast.makeText(this, ed_location_val.getText().toString(), Toast.LENGTH_LONG).show();

            String subskill = "";
            String skills = ss_skills.getSelectedItem().toString();
            if (skills .equalsIgnoreCase("others")) {
                skills = et_other_skills.getText().toString();
                subskill = et_other_subskills.getText().toString();
                if (skills.length() < 2) {
                    Toast.makeText(this, getResources().getString(R.string.enter_other_skills_text), Toast.LENGTH_LONG).show();
                    return;
                }
                if (subskill.length() < 2) {
                    Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                    return;
                }
            } else {

                if (subSkillsList.size() <= 0) {
                    subskill = ms_subskills.getSelectedItem().toString();;
                } else {
                    subskill = ms_subskills.getSelectedItem().toString();
                }
                if (subskill.length() < 3) {
                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.select_subskills_first_text),Toast.LENGTH_LONG).show();
                    return;
                }
                if (subskill.contains("others") || subskill.contains("Others")) {
                    if (et_other_subskills.getText().toString().length() < 2) {
                        Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                        return;
                    }
                    subskill =  subskill + "," + et_other_subskills.getText().toString();
                }
            }

            if (ed_salary.getText().length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_salary_first), Toast.LENGTH_LONG).show();
                return;
            }
            if (!full && !part && !freelance) {
                Toast.makeText(this, getResources().getString(R.string.one_job_type_text), Toast.LENGTH_LONG).show();
                return;
            }
            String jobType = "";
            if (full) {
                jobType = jobType + getResources().getString(R.string.fulltime_text) +",";
            }
            if (part) {
                jobType = jobType + getResources().getString(R.string.parttime_text) + ",";
            }
            if (freelance) {
                jobType = jobType + getResources().getString(R.string.freelancer_text);
            }

            UpdateProfileRequest request = new UpdateProfileRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKeyEmployee(this));
            request.setFullName(ed_full_name.getText().toString());
            request.setFathersName(ed_father_name.getText().toString());
            request.setGender(ss_gender.getSelectedItem().toString());
            request.setCurrent_address(ed_location_val.getText().toString());
            request.setLat(lat);
            request.setLng(lng);
            request.setCityName(cityName);
            request.setPermanentAddress(ed_per_add.getText().toString());
            request.setSalary(ed_salary.getText().toString());
            request.setIdProofDoc(ss_proof.getSelectedItem().toString());
            request.setBankAccount(ss_bank.getSelectedItem().toString());
            request.setDocId(et_doc_number.getText().toString());
            String exp = ss_experience.getSelectedItem().toString();
            request.setSkill(skills);
            request.setSubSkill(subskill);
            request.jobType = jobType;
            exp = exp.replace(getResources().getString(R.string.years_or_more_replace_text),"");
            exp = exp.replace(getResources().getString(R.string.replace_year_text),"");

            request.setExperience(exp);
            request.setDob(ed_dob.getText().toString());
           // byte[] imageBytes = null;

           // ByteArrayOutputStream baos = new ByteArrayOutputStream();
           // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            // EditProfileActivity.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
          //  imageBytes = baos.toByteArray();
            //String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            //params.put("profile_photo", new ByteArrayInputStream(imageBytes), "image.png");


       /* request.mobileNo = mobileNum;
        request.otp = otp;
*/
            //request.token = SharedPrefUtils.getApiKey(this);


            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.updateProfileRequest(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                    tinyDB.putString("username" , ed_full_name.getText().toString());

                    Toast.makeText(EditProfileActivity.this, EditProfileActivity.this.getResources().getString(R.string.success_profile_updated_text),Toast.LENGTH_LONG).show();
                    if (from .equals("temp")) {
                       // onBackPressed();

                        Intent intent = new Intent(EditProfileActivity.this, UserProfileActivity.class);
                        startActivity(intent);
                        EditProfileActivity.this.finish();
                        EditProfileActivity.this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

                    } else {
                        Intent i = new Intent(EditProfileActivity.this, JobDescriptionActivity.class);
                        i.putExtra(AppConstants.KEY_CLIENT,jobList );
                        i.putExtra(AppConstants.FROM_CLASS,"profile");
                        startActivity(i);
                    }
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {

                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(EditProfileActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(EditProfileActivity.this, EmployerTypeActivity.class);
                        EditProfileActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                    Toast.makeText(EditProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();


                }

            });
        }
    }






   /* public void updateProfileWS(RequestParams params) {
        //String Url = "http://www.dreampartner.net/webservice/updateUserData.php";
        String Url = WebserviceUrlClass.UpdateUserData;
        prgDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Url, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                prgDialog.hide();
                try {
                    Log.d("Json_con", response.toString());
                    String responseCode = response.getString("responseCode");
                    String responseMessage = response.getString("responseMessage");

                    if (responseCode.equals("200")) {
                        JSONObject resposeProfile = response.getJSONObject("userProfile");
                        String profilecompleteness = response.getString("profilecompleteness");
                        String UserId = resposeProfile.getString("user_id");
                        String UserName = resposeProfile.getString("username");
                        String Dob = resposeProfile.getString("date_of_birth");
                        String CountryId = resposeProfile.getString("country");
                        String StateId = resposeProfile.getString("state");
                        String CityId = resposeProfile.getString("city");
                        String IamId = resposeProfile.getString("i_mId");
                        String LookinForId = resposeProfile.getString("lookingId");
                        String ZipCode = resposeProfile.getString("zip_code");
                        String userProfileUrl = resposeProfile.getString("profile_photo_url");
                        String Iam = resposeProfile.getString("I_m");
                        String LookingFor = resposeProfile.getString("lookingFor");
                        String City = resposeProfile.getString("cityName");
                        String State = resposeProfile.getString("stateName");
                        String Country = resposeProfile.getString("countryName");

                        String AgeToo = resposeProfile.getString("age_to");
                        String AgeFromm = resposeProfile.getString("age_from");
                        String FullName = resposeProfile.getString("name");
                        String NickName = resposeProfile.getString("nick_name");
                        Log.e("profilecompleteness", profilecompleteness);

                        if (responseMessage.equals("Data updated successfully.")) {

                            MainActivity.tvUserName.setText(mUserNickName);
                            Picasso.with(getActivity()).load(userProfileUrl).into(MainActivity.mUserCircularImage);

                            SharedPreferences.Editor editor = mPref.edit();
                            editor.putString("userId", UserId);
                            editor.putString("userName", UserName);
                            editor.putString("userDob", Dob);
                            editor.putString("userCountryId", CountryId);
                            editor.putString("stateId", StateId);
                            editor.putString("cityId", CityId);
                            editor.putString("iMId", IamId);
                            editor.putString("userLookingId", LookinForId);
                            editor.putString("zipCode", ZipCode);
                            editor.putString("userProfileUrl", userProfileUrl);
                            editor.putString("iM", Iam);
                            editor.putString("userLookingFor", LookingFor);
                            editor.putString("userCountry", Country);
                            editor.putString("userCity", City);
                            editor.putString("userState", State);

                            editor.putString("userNickName", NickName);
                            editor.putString("userFullName", FullName);
                            editor.putString("ageTo", AgeToo);
                            editor.putString("ageFrom", AgeFromm);
                            editor.putString("profilecompleteness", profilecompleteness);

                            editor.apply();
                            Toast.makeText(getActivity(), "Data updated successfully", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Log.d("allenj", e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }
*/









    public void onAddPlaceButtonClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            return;
        }
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }


    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // arg1 = year
            // arg2 = month
            // arg3 = da3
            String dd = String.valueOf(arg3);
            if (arg3 < 10) {
                dd = "0" + dd;
            }
            int m2 = arg2 + 1;
            if (m2 < 10) {
                ed_dob.setText(dd + "-0" + m2 + "-" + arg1);
            } else {
                ed_dob.setText(dd + "-" + m2 + "-" + arg1);

            }
        }
    };


        public void EnableRuntimePermission(){

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION))
            {

             //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

            } else {

                ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);

            }


        }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

            //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

              //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

    private static void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/kaamkaajjobseeker");
        myDir.mkdirs();

        String fname = "profile_pic1.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*public void setProfilePic() {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/kaamkaaj");
        myDir.mkdirs();

        String fname = "profile_pic1.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) {
            try {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                imageView1.setImageBitmap(b);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

    }*/

    @OnItemSelected(R.id.ss_proof)
    public void spinnerItemSelected(Spinner spinner, int position) {
        ll_number.setVisibility(View.VISIBLE);
        switch (position) {
            case 0:
                tv_doc_number.setText(getResources().getString(R.string.adhaar_card_number_text));
                selectedCard = getResources().getString(R.string.adhaar_card_number_text);
                break;
            case 1:
                tv_doc_number.setText(getResources().getString(R.string.passport_num_Text));
                selectedCard = getResources().getString(R.string.passport_num_Text);
                break;
            case 2:
                tv_doc_number.setText(getResources().getString(R.string.driving_license_number));
                selectedCard = getResources().getString(R.string.driving_license_number);
                break;
            case 3:
                tv_doc_number.setText(getResources().getString(R.string.voter_card_num));
                getResources().getString(R.string.voter_card_num);
                break;
            case 4:
                tv_doc_number.setText(getResources().getString(R.string.pan_number_text));
                getResources().getString(R.string.pan_number_text);
                return;
        }
    }

    @OnItemSelected(R.id.ss_skills)
    public void skillSelected(Spinner spinner, int position) {
        if (ss_skills.getSelectedItem().toString() .equalsIgnoreCase("others")) {
            ll_other_skills.setVisibility(View.VISIBLE);
            ll_subskills.setVisibility(View.GONE);
            ll_other_subskills.setVisibility(View.VISIBLE);
        } else {
            ll_other_skills.setVisibility(View.GONE);
            ll_subskills.setVisibility(View.VISIBLE);
            ll_other_subskills.setVisibility(View.GONE);
        }
        fetchSubSkills2(jobSkillsLists.get(position).getId());

    }
    @OnItemSelected(R.id.ms_subskills)
    public void subskillSelected(Spinner spinner, int position) {
        if (ms_subskills.getSelectedItem().toString() .contains("others") || ms_subskills.getSelectedItem().toString() .contains("Others") ) {
            ll_other_subskills.setVisibility(View.VISIBLE);
        } else {
            ll_other_subskills.setVisibility(View.GONE);
        }

    }

    public void refreshPlacesData(Place place) {
        lat = String.valueOf(place.getLatLng().latitude);
        lng = String.valueOf(place.getLatLng().longitude);
      //  tv_location_val.setText(place.getAddress());
       // tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));
        cityName = String.valueOf(place.getName());
    }






    public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {

            FetchSubskillsRequest request = new FetchSubskillsRequest();
            request.token = SharedPrefUtils.getApiKey(this);
            request.master_skill_id = id;
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("master_skill_id", id);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SUBSKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseSUBFetchDetails" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        String respsuccess=status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject=response.getJSONObject("data");
                            JSONArray subSkillArr=subSkillObject.getJSONArray("subskills");
                            optionsSubskills.clear();
                            for (int i =0;i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("namesub" +name);
                                optionsSubskills.add(name);
                                ms_subskillsnew.setItems(optionsSubskills);

                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter <String>(EditProfileActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                            ms_subskills.setListAdapter(adapter);



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", EditProfileActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }







/*

    public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSubskillsRequest request = new FetchSubskillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            request.master_skill_id = id;
            Call<JobSubSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSubSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSubSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    jobSubSkillsLists = response.getData().getSubSkills();
                    optionsSubskills.clear();

                    for (int i = 0; i < response.getData().getSubSkills().size(); i++) {
                        optionsSubskills.add(response.getData().getSubSkills().get(i).getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter <String>(EditProfileActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                    ms_subskills.setListAdapter(adapter);
                }

                @Override
                public void onError(Call<JobSubSkillsResponse> call, Throwable t) {

                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(EditProfileActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(EditProfileActivity.this, EmployerTypeActivity.class);
                        EditProfileActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                        if (response.status.message .equals("There are no more Subskills")) {
                            optionsSubskills.clear();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter <String>(EditProfileActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                        ms_subskills.setListAdapter(adapter);                    }

                }

            });
        }
    }
*/


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(fromClass.equals("Login")) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            this.finish();
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }else{
            Intent intent = new Intent(this, UserProfileActivity.class);
            startActivity(intent);
            this.finish();
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @OnClick({R.id.ll_testeng})
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.ll_testeng:
                uploadFile();
                break;

        }
    }

    public void uploadFile() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent,"Choose File to Upload.."),PICK_FILE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG,"Selected File Path:1111111" );

        if (requestCode == 7 && resultCode == RESULT_OK) {

            bitmap = (Bitmap) data.getExtras().get("data");

            imageView1.setImageBitmap(bitmap);
            SaveImage(bitmap);
        }



        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                Log.i(TAG,"Selected File Path:11111112222" );

                if(data == null){
                    //no data present
                    return;
                }
                Log.i(TAG,"Selected File Path:1111113124231" );


                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this,selectedFileUri);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    proceed(selectedFilePath);
                }else{
                    Toast.makeText(this,"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public void proceed(final String filePath) {

            uploadFile(selectedFilePath, "WFWGWB","ergeragrg");

    }

    public int uploadFile(final String selectedFilePath,final String password,final String email){

        showProgressDialog1("Please Wait...");
        new Thread(new Runnable() {
            public void run() {
                int serverResponseCode = 0;

                HttpURLConnection connection;
                DataOutputStream dataOutputStream;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";


                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File selectedFile = new File(selectedFilePath);


                String[] parts = selectedFilePath.split("/");
                final String fileName = parts[parts.length - 1];


                try {
                    FileInputStream fileInputStream = new FileInputStream(selectedFile);
                    URL url = new URL(SERVER_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);//Allow Inputs
                    connection.setDoOutput(true);//Allow Outputs
                    connection.setUseCaches(false);//Don't use a cached Copy
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");

                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    connection.setRequestProperty("tenth_marksheet:10cc069d98957550702c18015dc24e3c", selectedFilePath);
                    connection.setRequestProperty("password", password);
                    connection.setRequestProperty("token","10cc069d98957550702c18015dc24e3c");


                    //creating new dataoutputstream
                    dataOutputStream = new DataOutputStream(connection.getOutputStream());

                    int sdf = selectedFilePath.lastIndexOf(".");
                    String gvhjn = selectedFilePath.substring(sdf);
                    String newName = "10cc069d98957550702c18015dc24e3c" + gvhjn;
                    //writing bytes to data outputstream
                    dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"tenth_marksheet:10cc069d98957550702c18015dc24e3c\"; token=10cc069d98957550702c18015dc24e3c; filename=\""
                            + newName  + "\"" + lineEnd);

                    dataOutputStream.writeBytes(lineEnd);

                    //returns no. of bytes present in fileInputStream
                    bytesAvailable = fileInputStream.available();
                    //selecting the buffer size as minimum of available bytes or 1 MB
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    //setting the buffer as byte array of size of bufferSize
                    buffer = new byte[bufferSize];

                    //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                    while (bytesRead > 0) {
                        //write the bytes read from inputstream
                        dataOutputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    dataOutputStream.writeBytes(lineEnd);
                    dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("token", "10cc069d98957550702c18015dc24e3c");
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();

                    Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                    //response code of 200 indicates the server status OK
                    if (serverResponseCode == 200) {
                        hideProgressDialog1();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditProfileActivity.this, "DONEEE!", Toast.LENGTH_SHORT).show();

                                System.out.println("Successfully uploaded docs !!!!!!!!!!!!!!");
                            }
                        });



                    } else {
                        hideProgressBar();
                    }

                    //closing the input and output streams
                    fileInputStream.close();
                    dataOutputStream.flush();
                    dataOutputStream.close();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EditProfileActivity.this, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Toast.makeText(EditProfileActivity.this, "URL error!", Toast.LENGTH_SHORT).show();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(EditProfileActivity.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
                }
            }
        }).start();
        return 0;


    }
    protected void showProgressDialog1(String msg) {
        try {
            if (prgDialog1 == null) {
                prgDialog1 = ProgressDialog.show(this, "", msg);
            } else {
                prgDialog1.show();
            }
        } catch (Exception e) {

        }
    }

    protected void hideProgressDialog1() {
        try {
            if (prgDialog1 != null && prgDialog1.isShowing())
                prgDialog1.dismiss();
        } catch (Exception e) {

        }
    }


}
