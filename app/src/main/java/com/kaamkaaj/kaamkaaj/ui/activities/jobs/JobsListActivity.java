package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.JobRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.LoginActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.JobListAdapter;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class JobsListActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
   /* @BindView(R.id.sv)
    NestedScrollView sv;*/
    ArrayList<JobList> jobLists = new ArrayList<JobList>();;
    JobListAdapter mAdapter;
    private boolean mInFlight;
    Context mContext;
    TinyDB tinyDB;
    int mCurrentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_listing);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.recommended_jobs_text);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new JobListAdapter(mContext, jobLists);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public void onLoadMore(int currentPage, int count) {
           //     Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                 //   mAdapter.addProgressBarView();
                    fetchJobs(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });
        /*recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(lLayout) {
            @Override
            public void onScrolledToEnd() {
                if (!mInFlight) {

                    mAdapter.addProgressBarView();
                    fetchJobs(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });*/
        /*sv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {
                    //code to fetch more data for endless scrolling
                    if (!mInFlight) {

                        //mAdapter.addProgressBarView();
                        fetchJobs(mCurrentPage, true);
                        mInFlight = true;
                    }
                }
            }
        });*/

        fetchJobs(mCurrentPage, true);
    }

    public void fetchJobs(int mPage, final boolean showProgress) {
        JobRequest request = new JobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.randomId = SharedPrefUtils.getAnonymousUserId(this);
        request.page = mPage;
        String skillsFilter = SharedPrefUtils.getFilterSkills(this);
        if (skillsFilter.length() > 1 && skillsFilter.subSequence(skillsFilter.length()-1, skillsFilter.length()) .equals(",")) {
            skillsFilter  = skillsFilter.substring(0,skillsFilter.length()-1);
        }
        String locationFilter = SharedPrefUtils.getFilterLocation(this);
        String minSal = SharedPrefUtils.getFilterMinSalary(this);
        String maxsal = SharedPrefUtils.getFilterMaxSalary(this);
        String minExp = SharedPrefUtils.getFilterMinExp(this);
        String maxExp = SharedPrefUtils.getFilterMaxExp(this);
        String education = SharedPrefUtils.getFilterEducation(this);




        if (locationFilter.length() > 1 &&locationFilter.subSequence(locationFilter.length()-1, locationFilter.length()) .equals(",")) {
            locationFilter  = locationFilter.substring(0,locationFilter.length()-1);
        }
        if (education.length() > 1 && education.subSequence(education.length()-1, education.length()) .equals(",")) {
            education  = education.substring(0,education.length()-1);
        }
        System.out.println("respjobsfilter"+skillsFilter+locationFilter+minSal+minExp+maxExp+minExp+maxExp+education);
        request.skill = skillsFilter;
        request.location=locationFilter;
        request.minSalary = minSal;
        request.maxSalary = maxsal;
        request.minExp = minExp;
        request.maxExp = maxExp;
        request.education = education;
       /* request.mobileNo = mobileNum;
        request.otp = otp;
*/
        //request.token = SharedPrefUtils.getApiKey(this);

        System.out.println("skillsFilter_____ : " + skillsFilter+""+minSal+""+""+maxsal+""+minExp+""+maxExp+""+education);
        Call<JobListResponse> apiResponseCall;
        apiResponseCall =
                service.jobRequest(request);

        apiResponseCall =
                service.jobRequest(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }

            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobListResponse> call, JobListResponse response) {

              //  System.out.println("_____ : " + response.data);
                if(response.getData().jobs.size()>0) {
                    for (int i = 0; i < response.getData().jobs.size(); i++) {
                        response.getData().jobs.get(i).setSelected("no");
                        jobLists.add(response.getData().jobs.get(i));
                    }
                    mCurrentPage++;
                    mAdapter.notifyDataSetChanged();
                    mInFlight = false;
                }else{
                    // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                    Toast toast = Toast.makeText(JobsListActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
              //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }



            @Override
            public void onError(Call<JobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
               // Toast.makeText(JobsListActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                Toast toast = Toast.makeText(JobsListActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<JobListResponse> call, JobListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(JobsListActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(JobsListActivity.this, EmployerTypeActivity.class);
                    JobsListActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobsListActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }
            }

        });
    }

    @OnClick(R.id.fab)
    public void changeLanguage(View view) {
        Intent i = new Intent(mContext, JobFilterActivity.class);
        mContext.startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        return true;*/

        //  getMenuInflater().inflate(R.menu.menu_radius, menu);
        if (SharedPrefUtils.getAppState(JobsListActivity.this) ==  AppConstants.STATE_REGISTERED){

        } else {
            getMenuInflater().inflate(R.menu.menu_skip, menu);

        }


        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_skip:
                Intent vi = new Intent(JobsListActivity.this, LoginActivity.class);
                startActivity(vi);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SharedPrefUtils.getAppState(JobsListActivity.this) == AppConstants.STATE_REGISTERED) {

            Intent intent = new Intent(this, JobFilterActivity.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
