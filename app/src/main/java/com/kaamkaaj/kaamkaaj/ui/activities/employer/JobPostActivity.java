package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.BuildConfig;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchEducationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSkillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.EducationResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSubSkillsList;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.Geofencing;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerJobUpdateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.JobPostRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.MyJobList;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import cz.msebera.android.httpclient.Header;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;

/**
 * Created by akshaysingh on 5/4/18.
 */

public class JobPostActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.ll_full_time)
    LinearLayout ll_full_time;
    @BindView(R.id.ll_part_time)
    LinearLayout ll_part_time;
    @BindView(R.id.ll_freelancer)
    LinearLayout ll_freelancer;
    @BindView(R.id.iv_freelance)
    ImageView iv_freelance;
    @BindView(R.id.iv_full_time)
    ImageView iv_full_time;
    @BindView(R.id.iv_part_time)
    ImageView iv_part_time;
    @BindView(R.id.et_job_title)
    EditText et_job_title;
    @BindView(R.id.ss_min_exp)
    SearchableSpinner ss_min_exp;
    @BindView(R.id.ss_max_exp)
    SearchableSpinner ss_max_exp;

   // TextView tv_location;
    @BindView(R.id.tv_location_val)
    TextView tv_location_val;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.ll_subskills)
    LinearLayout ll_subskills;
    @BindView(R.id.ss_qualification)
    SearchableSpinner ss_qualification;
    @BindView(R.id.ss_validity)
    SearchableSpinner ss_validity;
    @BindView(R.id.tv_job_title)
    TextView tv_job_title;
    @BindView(R.id.tv_job_description)
    TextView tv_job_description;
    @BindView(R.id.et_job_description)
    EditText et_job_description;
    //@BindView(R.id.et_location_val)
   // EditText et_location_val;
    @BindView(R.id.tv_number_opening)
    TextView tv_number_opening;
    @BindView(R.id.et_number_opening)
    EditText et_number_opening;
    @BindView(R.id.tv_min_value)
    TextView tv_min_value;/*
    @BindView(R.id.rangeSeekbar3)
    CrystalRangeSeekbar rangeSeekbar3;*/
    @BindView(R.id.tv_max_value)
    TextView tv_max_value;
    @BindView(R.id.ll_min)
    LinearLayout ll_min;
    @BindView(R.id.ll_max)
    LinearLayout ll_max;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;
    @BindView(R.id.ss_skills)
    SearchableSpinner ss_skills;
    @BindView(R.id.ss_subskills)
    SearchableSpinner ss_subskills;
    @BindView(R.id.et_set_min)
    EditText et_set_min;
    @BindView(R.id.tv_set_min)
    TextView tv_set_min;
    @BindView(R.id.ll_min1)
    LinearLayout ll_min1;
    @BindView(R.id.et_set_max)
    EditText et_set_max;
    @BindView(R.id.tv_set_max)
    TextView tv_set_max;
    @BindView(R.id.ll_max1)
    LinearLayout ll_max1;
    @BindView(R.id.et_min_salary)
    EditText et_min_salary;
    @BindView(R.id.et_max_salary)
    EditText et_max_salary;
    @BindView(R.id.tv_min_salary)
    TextView tv_min_salary;
    @BindView(R.id.tv_max_salary)
    TextView tv_max_salary;
    @BindView(R.id.ll_no_radio)
    LinearLayout ll_no_radio;
    @BindView(R.id.ll_yes_radio)
    LinearLayout ll_yes_radio;
    @BindView(R.id.iv_no_radio)
    ImageView iv_no_radio;
    @BindView(R.id.iv_yes_radio)
    ImageView iv_yes_radio;
    @BindView(R.id.ms_subskills)
    MultiSelectSpinner ms_subskills;
    @BindView(R.id.ms_education)
    MultiSelectSpinner ms_education;
    @BindView(R.id.ll_other_skills)
    LinearLayout ll_other_skills;
    @BindView(R.id.et_other_skills)
    EditText et_other_skills;
    @BindView(R.id.ll_other_subskills)
    LinearLayout ll_other_subskills;
    @BindView(R.id.et_other_subskills)
    EditText et_other_subskills;

    Boolean part = false, full = false, freelance = false;

    public static final String TAG = JobPostActivity.class.getSimpleName();

    String initialUsername = "", showContact = "yes";
    boolean fromMyJobs = false;
    boolean minTextVisible = false, maxTextVisible = false;
    MyJobList myJobList;
    private static final int PLACE_PICKER_REQUEST = 1;
    private Geofencing mGeofencing;
    private GoogleApiClient mClient;
    String lat = "", lon = "", name = "";
    public static final int RequestPermissionCode = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };



    ArrayList<JobSkillsList> jobSkillsLists;
    ArrayAdapter<String> skillsAdapter;
    List<String> skillsList;
    ArrayList<String> optionsSubskills;


    ArrayList<JobSubSkillsList> jobSubSkillsLists;
    ArrayAdapter<String> subSkillsAdapter;
    List<String> subSkillsList;
    TinyDB tinyDB;


    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ButterKnife.bind(this);
        top_bar.setTitle(R.string.post_job_text);
        setSupportActionBar(top_bar);
        myJobList = new MyJobList();
        tv_min_value.setText(getResources().getString(R.string.Rs) + " " + AppConstants.MIN_SALARY);
        tv_max_value.setText(getResources().getString(R.string.Rs) + " " + AppConstants.MAX_SALARY);
        ms_subskills.setTitle(getResources().getString(R.string.subskills_text));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ms_education.setTitle(getResources().getString(R.string.education_text));
// set listener
        /*rangeSeekbar3.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tv_min_value.setText(getResources().getString(R.string.Rs) + " "  + String.valueOf(minValue));
                if (Integer.parseInt(minValue.toString()) > 0) {
                    et_set_min.setText(String.valueOf(minValue));
                }
                tv_max_value.setText(getResources().getString(R.string.Rs) + " "  + String.valueOf(maxValue));
                et_set_max.setText(String.valueOf(maxValue));

            }
        });*/

        optionsSubskills = new ArrayList<>();
        ss_skills.setTitle(getResources().getString(R.string.profile_skills_text));
        ss_subskills.setTitle(getResources().getString(R.string.subskills_text));
        addEducationData();
        tinyDB = new TinyDB(this);
        if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("myjobs")) {
            fromMyJobs = true;
            top_bar.setTitle(R.string.edit_job_text);
        }

        if (fromMyJobs) {
            myJobList = getIntent().getParcelableExtra("data");
            //System.out.println("myJobList : " + myJobList.getc);
            et_job_title.setText(myJobList.getJobTitle());
            et_number_opening.setText(myJobList.getOpening());
            et_job_description.setText(myJobList.getJobDescription());
            et_min_salary.setText(myJobList.getMinSalary());
            et_max_salary.setText(myJobList.getMaxSalary());
            tv_location_val.setText(myJobList.getAddress());
        }


        /*et_set_min.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("BEFORE : " + et_set_min.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("on change : " + et_set_min.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // setMin();

            }
        });

        et_set_max.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("BEFORE : " + et_set_max.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("on change : " + et_set_max.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setMax();

            }
        });*/

        ss_min_exp.setTitle(getResources().getString(R.string.min_exp_text));
        ss_max_exp.setTitle(getResources().getString(R.string.max_exp_text));
        ss_qualification.setTitle(getResources().getString(R.string.education_text));
        ss_validity.setTitle(getResources().getString(R.string.job_validity_text));
        EnableRuntimePermission();
        mGeofencing = new Geofencing(this, mClient);
        mGeofencing.registerAllGeofences();
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(JobPostActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();
        skillsList = new ArrayList<String>();
        skillsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, skillsList);
        ss_skills.setAdapter(skillsAdapter);
        subSkillsList = new ArrayList<String>();
        subSkillsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subSkillsList);
        ss_subskills.setAdapter(subSkillsAdapter);
        jobSkillsLists = new ArrayList<JobSkillsList>();
        jobSubSkillsLists = new ArrayList<JobSubSkillsList>();
        // init();

       // initLocation();
        //startLocationUpdates();

        fetchSkills();




    }


    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        System.out.println("addresssNewOne");
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                System.out.println("addresssNewTwo");
                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /*public void EnableRuntimePermission() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }*/




    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                      //  Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(JobPostActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                              //  Toast.makeText(JobPostActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                         updateLocationUI();
                    }
                });
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {


            // location last updated time
            // txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);


            Geocoder geocoder =
                    new Geocoder(getApplication(), Locale.getDefault());
            // Get the current location from the input parameter list
            //Location loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses;
            try {
                lat= String.valueOf(mCurrentLocation.getLatitude());
                lon= String.valueOf(mCurrentLocation.getLongitude());
                addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(), 1);
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */
                System.out.println("addresssNew" + address.getAddressLine(0));

               /* et_location_val.setText(
                        "Lat: " + mCurrentLocation.getLatitude() + ", " +
                                "Lng: " + mCurrentLocation.getLongitude()
                );*/
              //  et_location_val.setText(address.getAddressLine(0));
                 tv_location_val.setText(address.getAddressLine(0));
                 tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));
                // giving a blink animation on TextView
              //  et_location_val.setAlpha(0);
               // et_location_val.animate().alpha(1).setDuration(300);
                String addressStr=address.getAddressLine(0);
                System.out.println("addaddressStr" + addressStr);
               if(addressStr.length()>0) {
                   mFusedLocationClient
                           .removeLocationUpdates(mLocationCallback)
                           .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                               @Override
                               public void onComplete(@NonNull Task<Void> task) {
                                  // Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                                   // toggleButtons();
                               }
                           });
               }

                //System.out.println("addresss"+address.getLocality()+address.getSubLocality()+address.getPostalCode()+address.getCountryName()+address.getFeatureName()+address.getThoroughfare()+address.getSubAdminArea()+address.getAdminArea());
            } catch (IOException e1) {
                Log.e("LocationSampleActivity",
                        "IO Exception in getFromLocation()");
                e1.printStackTrace();
                //return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(mCurrentLocation.getLatitude()) +
                        " , " +
                        Double.toString(mCurrentLocation.getLongitude()) +
                        " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                // return errorString;
            }
            // If the reverse geocode returned an address
           /* if (addresses.size() > 0) {
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */

            // System.out.println("addresss"+address.getLocality());

            //return address.getLocality();
            //  } else {
            // return "No address found";
            //  }


        }

        // toggleButtons();
    }


    void init() {
        et_job_title.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_job_description.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // et_location_val.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_number_opening.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_set_min.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_set_max.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_max_salary.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_min_salary.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_other_skills.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_other_subskills.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

    }


/*
    public void fetchSkills() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {


            FetchSkillsRequest request = new FetchSkillsRequest();
        request.token = SharedPrefUtils.getApiKey(this);
        System.out.println("resptokenN"+request.token);
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseFetchDetails" + response);







               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


               /* try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    if (respsuccess.equals("success")) {
                        //  System.out.println("_____ : " + response.data);
                      //  for (int i =0;i < response.getData().getJobs().size(); i++) {
                        //    jobLists.add(response.getData().getJobs().get(i));
                       // }
                        mCurrentPage++;
                        mAdapter.notifyDataSetChanged();
                        mInFlight = false;


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);

/*
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", JobPostActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
     }

    }
*/

    public void fetchSkills() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSkillsRequest request = new FetchSkillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            Call<JobSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSkillsResponse> call, JobSkillsResponse response) {


                    jobSkillsLists = response.getData().getSkills();
                    for (int i = 0; i < response.getData().getSkills().size(); i++) {
                        System.out.println("|||| : " + response.getData().getSkills().get(i).getName());
                        skillsAdapter.add(response.getData().getSkills().get(i).getName());
                        skillsAdapter.notifyDataSetChanged();

                    }


                    int skillsId = 0;
                    System.out.println("respjobskill@@@@@@@@@@@@@@@@@ : " + jobSkillsLists.size());
                    for (int i=0; i < jobSkillsLists.size(); i++) {
                        System.out.println("resp@@@@@@@@@@@@@@@@@ : " + jobSkillsLists.get(i).getName());
                        if (jobSkillsLists.get(i).getName() .equals(myJobList.getSkill())) {
                            skillsId = i;
                        }
                    }
                    ss_skills.setSelection(skillsId);
                    String[] minexps = getResources().getStringArray(R.array.min_exp);
                    String[] max_exp = getResources().getStringArray(R.array.max_exp);
                    String[] job_val = getResources().getStringArray(R.array.job_validity);

                    int expIdMin = 0;
                    System.out.println("resplength@@@@@@@@@@@@@@@@@ : " +minexps.length);
                   // System.out.println("respdata@@@@@@@@@@@@@@@@@ : " +);
                    //String min= String.valueOf(myJobList.getMinExp());
                    for (int i =0; i < minexps.length; i++) {
                                /*if (exps[i] .equalsIgnoreCase(tmpExp)) {
                                    expId = i;
                                }*/
                        if (minexps[i].equals(String.valueOf(myJobList.getMinExp()))) {
                            expIdMin = i;
                        }
                    }
                    System.out.println("respexpIdMin@@@@@@@@@@@@@@@@@ : " +expIdMin);

                    int expIdMax = 0;
                    for (int i =0; i < max_exp.length; i++) {
                                /*if (exps[i] .equalsIgnoreCase(tmpExp)) {
                                    expId = i;
                                }*/
                        if (max_exp[i].equals(String.valueOf(myJobList.getMaxExp()))) {
                            expIdMax = i;
                        }
                    }


                    int jobVal = 0;
                    for (int i =0; i < job_val.length; i++) {
                                /*if (exps[i] .equalsIgnoreCase(tmpExp)) {
                                    expId = i;
                                }*/
                        if (job_val[i].equals(myJobList.getJobValidity())) {
                            jobVal = i;
                        }
                    }

                    String showContacts = myJobList.getContactVisible();
                    if (showContacts.equals("yes")) {
                        iv_yes_radio.setImageResource(R.mipmap.radio_chk);
                        iv_no_radio.setImageResource(R.mipmap.radio_unchk);
                        showContact = "yes";
                    }

                    if (showContacts.equals("no")) {
                        iv_no_radio.setImageResource(R.mipmap.radio_chk);
                        iv_yes_radio.setImageResource(R.mipmap.radio_unchk);
                        showContact = "no";
                    }
                    String tempType = myJobList.getJobType();
                    System.out.println("respgetJobType " +showContacts);
                    if (tempType .contains(getResources().getString(R.string.fulltime_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_full_time.setImageDrawable(drawable );
                        full = true;
                    }
                    if (tempType .contains(getResources().getString(R.string.parttime_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_part_time.setImageDrawable(drawable );
                        part  =true;
                    }
                    if (tempType .contains(getResources().getString(R.string.freelancer_text))) {
                        Resources res = getResources();
                        String mDrawableName = "ic_check_blue";
                        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
                        Drawable drawable = res.getDrawable(resID );
                        iv_freelance.setImageDrawable(drawable );
                        freelance  =true;
                    }

                    System.out.println("respjobvali@@@@@@@@@@@@@@@@@ : " +myJobList.getJobValidity());
                    ss_min_exp.setSelection(expIdMin);
                    ss_max_exp.setSelection(expIdMax);
                    ss_validity.setSelection(jobVal);
                   // String minExp = ss_min_exp.getSelectedItem().toString();
                   // String maxExp = ss_max_exp.getSelectedItem().toString();

                }

                @Override
                public void onError(Call<JobSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    if (response.status.message.equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username", "");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                    }
                }

            });
        }
    }


    public void fetchSubSkills(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {

            FetchSubskillsRequest request = new FetchSubskillsRequest();
            request.token = SharedPrefUtils.getApiKey(this);
            request.master_skill_id = id;
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("master_skill_id", id);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SUBSKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseSUBFetchDetails" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                    try {
                        hideProgressBar();
                        JSONObject status = response.getJSONObject("status");
                        String respsuccess = status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject = response.getJSONObject("data");
                            JSONArray subSkillArr = subSkillObject.getJSONArray("subskills");
                            for (int i = 0; i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("name" + name);
                                subSkillsAdapter.add(name);
                                subSkillsAdapter.notifyDataSetChanged();

                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", JobPostActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }


    public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {

            FetchSubskillsRequest request = new FetchSubskillsRequest();
            request.token = SharedPrefUtils.getApiKey(this);
            request.master_skill_id = id;
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("master_skill_id", id);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SUBSKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseSUBFetchDetails" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                    try {
                        hideProgressBar();
                        JSONObject status = response.getJSONObject("status");
                        String respsuccess = status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject = response.getJSONObject("data");
                            JSONArray subSkillArr = subSkillObject.getJSONArray("subskills");
                            optionsSubskills.clear();
                            for (int i = 0; i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("name" + name);
                                optionsSubskills.add(name);


                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                            ms_subskills.setListAdapter(adapter);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", JobPostActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }




/*
    public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSubskillsRequest request = new FetchSubskillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            request.master_skill_id = id;
            Call<JobSubSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSubSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSubSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    jobSubSkillsLists = response.getData().getSubSkills();
                    optionsSubskills.clear();

                    for (int i = 0; i < response.getData().getSubSkills().size(); i++) {

                        optionsSubskills.add(response.getData().getSubSkills().get(i).getName());

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter <String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                    ms_subskills.setListAdapter(adapter);
                }

                @Override
                public void onError(Call<JobSubSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                        if (response.status.message .equals("There are no more Subskills")) {
                            optionsSubskills.clear();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter <String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                        ms_subskills.setListAdapter(adapter);
                    }

                }

            });
        }
    }
*/


    /*

    public void fetchSubSkills(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSubskillsRequest request = new FetchSubskillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            request.master_skill_id = id;
            Call<JobSubSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSubSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSubSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    jobSubSkillsLists = response.getData().getSubSkills();
                    for (int i = 0; i < response.getData().getSubSkills().size(); i++) {
                        System.out.println("|||| : " + response.getData().getSubSkills().get(i).getName() );
                        subSkillsAdapter.add(response.getData().getSubSkills().get(i).getName());
                        subSkillsAdapter.notifyDataSetChanged();

                    }
                }

                @Override
                public void onError(Call<JobSubSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }

            });
        }
    }*/
    public void addEducationData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();
        FetchEducationRequest request = new FetchEducationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKey(this));
        Call<EducationResponse> apiResponseCall;
        apiResponseCall =
                service.fetchEducation(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EducationResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EducationResponse> call, EducationResponse response) {
                /*for (int i = 0; i < response.getData().getEducationLists().size(); i++) {
                    jobCategories.add(response.getData().getEducationLists().get(i).getName());
                    FilterObject filterObject = new FilterObject(jobCategories.get(i),false);
                    educationList.add(filterObject);
                    educationAdapter.notifyDataSetChanged();
                }*/
                ArrayList<String> options = new ArrayList<>();

                for (int i = 0; i < response.getData().getEducationLists().size(); i++) {

                    options.add(response.getData().getEducationLists().get(i).getName());

                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, options);
                ms_education.setListAdapter(adapter);
                ms_education.setSelectAll(true);
            }

            @Override
            public void onError(Call<EducationResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<EducationResponse> call, EducationResponse response) {
                if (response.status.message.equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                    tinyDB.putString("username", "");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                    JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobPostActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                }
            }

        });


        /*String[] education = getResources().getStringArray(R.array.education);
        for (int i =0; i < education.length; i++) {
            FilterObject filterObject = new FilterObject(education[i],false);
            educationList.add(filterObject);
        }
        educationAdapter.notifyDataSetChanged();*/
    }

   /* public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSubskillsRequest request = new FetchSubskillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            request.master_skill_id = id;
            Call<JobSubSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSubSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSubSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    jobSubSkillsLists = response.getData().getSubSkills();
                    optionsSubskills.clear();

                    for (int i = 0; i < response.getData().getSubSkills().size(); i++) {

                        optionsSubskills.add(response.getData().getSubSkills().get(i).getName());

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter <String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                    ms_subskills.setListAdapter(adapter);
                }

                @Override
                public void onError(Call<JobSubSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                        if (response.status.message .equals("There are no more Subskills")) {
                            optionsSubskills.clear();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter <String>(JobPostActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                        ms_subskills.setListAdapter(adapter);
                    }

                }

            });
        }
    }*/









    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {
            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                } else {
                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @OnClick({R.id.tv_location_val,R.id.ll_freelancer, R.id.ll_part_time, R.id.ll_full_time, R.id.ll_min, R.id.ll_max, R.id.ll_action_button, R.id.tv_skills,  R.id.ll_yes_radio,R.id.ll_no_radio})
    public void clickMethods(View view) {
        switch (view.getId()) {
            case R.id.ll_freelancer:
                if (freelance) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance  =true;
                }
                break;
            case R.id.ll_part_time:
                if (part) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part  =true;
                }
                break;
            case R.id.ll_full_time:
                if (full) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = true;
                }
                break;
            case R.id.ll_min:
              //  rangeSeekbar3.setNor(10);
                ll_min.setVisibility(View.GONE);
                ll_min1.setVisibility(View.VISIBLE);
                minTextVisible = true;
               // tv_min_value.setText(getResources().getString(R.string.Rs) +  );

                /*rangeSeekbar3.setLeft(0);
                tv_min_value.setText(getResources().getString(R.string.Rs) + " 0");*/
                break;
            case R.id.tv_location_val:
               // EnableRuntimePermission();
                //initLocation();
                onAddPlaceButtonClicked(tv_location_val);
                break;
            case R.id.ll_max:
                ll_max.setVisibility(View.GONE);
                ll_max1.setVisibility(View.VISIBLE);
                maxTextVisible = true;
                //rangeSeekbar3.setMaxStartValue();
               /* rangeSeekbar3.setRight(30000);
                tv_max_value.setText(getResources().getString(R.string.Rs) + " 30000");*/
               break;
            case R.id.ll_action_button:
                postJob();
                break;
            case R.id.ll_yes_radio:
                iv_yes_radio.setImageResource(R.mipmap.radio_chk);
                iv_no_radio.setImageResource(R.mipmap.radio_unchk);
                showContact = "yes";
                break;
            case R.id.ll_no_radio:
                iv_no_radio.setImageResource(R.mipmap.radio_chk);
                iv_yes_radio.setImageResource(R.mipmap.radio_unchk);
                showContact = "no";

        }
    }

    public void onAddPlaceButtonClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            return;
        }
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }







    public void postJob() {

        String title = et_job_title.getText().toString();
        String des = et_job_description.getText().toString();
        String minSal = et_min_salary.getText().toString();
        String maxSal = et_max_salary.getText().toString();
        String minExp = ss_min_exp.getSelectedItem().toString();
        String maxExp = ss_max_exp.getSelectedItem().toString();
        String education = ms_education.getSelectedItem().toString();
        String location = tv_location_val.getText().toString();
        String validity = ss_validity.getSelectedItem().toString();
        String opening = et_number_opening.getText().toString();
        System.out.println("resplatlon"+lat+lon);
        if (title.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_job_title_text), Toast.LENGTH_LONG).show();

            return;
        }

        if (des.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_job_des_text), Toast.LENGTH_LONG).show();
            return;
        }
      /*  if (location.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_location_text), Toast.LENGTH_LONG).show();
            return;
        }*/

       /* if (location .equalsIgnoreCase(getResources().getString(R.string.select_location_text))) {
            Toast.makeText(this, getResources().getString(R.string.enter_location_text), Toast.LENGTH_LONG).show();
            return;
        }*/
        String subskill = "";

        String skills = ss_skills.getSelectedItem().toString();
        if (skills .equalsIgnoreCase("others")) {
            skills = et_other_skills.getText().toString();
            subskill = et_other_subskills.getText().toString();
            if (skills.length() < 2) {
                Toast.makeText(this, getResources().getString(R.string.enter_other_skills_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (subskill.length() < 2) {
                Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                return;
            }
        } else {

            if (subSkillsList.size() <= 0) {
                subskill = skills;
            } else {
                subskill = ms_subskills.getSelectedItem().toString();

            }

            if (subskill.length() < 3) {
                Toast.makeText(JobPostActivity.this, getResources().getString(R.string.select_subskills_first_text),Toast.LENGTH_LONG).show();
                return;
            }
            if (subskill.contains("others") || subskill.contains("Others")) {
                if (et_other_subskills.getText().toString().length() < 2) {
                    Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                    return;
                }
                subskill =  subskill + "," + et_other_subskills.getText().toString();

            }
        }
        if (minSal.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.select_min_salary_text), Toast.LENGTH_LONG).show();
            return;
        }
        if (maxSal.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.select_max_salary_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (Integer.parseInt(minExp) >= Integer.parseInt(maxExp)) {
            Toast.makeText(this, getResources().getString(R.string.min_max_exp),Toast.LENGTH_LONG).show();
            return;
        }
        if (education.length() <2) {
            Toast.makeText(JobPostActivity.this, getResources().getString(R.string.select_education_first),Toast.LENGTH_LONG).show();
            return;
        }
        if (opening.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_num_opening_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (!full && !part && !freelance) {
            Toast.makeText(this, getResources().getString(R.string.one_job_type_text), Toast.LENGTH_LONG).show();
            return;
        }
            System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKey(this);
            System.out.println("responseDetailELSE"+token);

        System.out.println("resfromMyJobs"+fromMyJobs);
        RequestParams params = new RequestParams();

        if (fromMyJobs) {
            EmployerJobUpdateRequest request = new EmployerJobUpdateRequest();
           // OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(JobPostActivity.this);
            request.id = myJobList.getId();
            request.jobTitle = title;
            request.jobDescription = des;
            request.minSalary = String.valueOf(minSal);
            request.maxSalary = String.valueOf(maxSal);
            request.minExp = minExp;
            request.maxExp = maxExp;
            request.address = location;
            request.education = education;
            request.jobValidity = validity;
            request.opening = opening;
            request.skill = skills;
            request.subSkill = subskill;
            request.contactVisible = showContact;
            if (lat.equals("")) {
                request.lat = myJobList.getLat();
                request.lng = myJobList.getLng();
                request.name = myJobList.getName();
            } else {
                request.lat = lat;
                request.lng = lon;
                request.name = name;
            }

            String jobType = "";
            if (full) {
                jobType = jobType + getResources().getString(R.string.fulltime_text) + ",";
            }
            if (part) {
                jobType = jobType + getResources().getString(R.string.parttime_text) + ",";
            }
            if (freelance) {
                jobType = jobType + getResources().getString(R.string.freelancer_text);
            }
            request.jobType = jobType;




          /*  request.token = SharedPrefUtils.getApiKey(JobPostActivity.this);
            request.id = myJobList.getId();
            request.jobTitle = title;
            request.jobDescription = des;
            request.minSalary = String.valueOf(minSal);
            request.maxSalary = String.valueOf(maxSal);
            request.minExp = minExp;
            request.maxExp = maxExp;
            request.address = location;
            request.education = education;
            request.jobValidity = validity;
            request.opening = opening;
            request.skill = skills;
            request.subSkill = subskill;
            request.contactVisible = showContact;*/


            String tokenN=SharedPrefUtils.getApiKey(JobPostActivity.this);
            System.out.println("resptokenNEDIT"+tokenN+title+request.id+showContact);
            params.put("id", request.id);
            params.put("token", tokenN);
            params.put("jobTitle", title);
            params.put("jobDescription", des);
            params.put("minSalary", String.valueOf(minSal));
            params.put("maxSalary",  String.valueOf(maxSal));
            params.put("minExp",  minExp);
            params.put("maxExp", maxExp);
            params.put("address", location);
            params.put("education", education);
            params.put("jobValidity", validity);
            params.put("opening", opening);
            params.put("skill", skills);
            params.put("subSkill", subskill);
            params.put("lat",  lat);
            params.put("lng",  lon);
            params.put("name",  name);
            params.put("contactVisible", showContact);
            params.put("jobType", request.jobType);
            String url = RequestUrl.API_URL + RequestUrl.JOB_UPDATE;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responsePOSTIF" + response+url);


                    // Toast.makeText(JobPostActivity.this, response.userMessage, Toast.LENGTH_LONG).show();
                  //  Intent i = new Intent(JobPostActivity.this, EmployerDashboardActivity.class);
                   // startActivity(i);

               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


               try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                    if (respsuccess.equals("success")) {

                        Toast.makeText(JobPostActivity.this, "Job Edited Sucessfully", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(JobPostActivity.this, MyJobsActivity.class);
                         startActivity(i);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                   // CommonMethod.showAlert("Unable to connect the server,please try again", JobPostActivity.this);

                    Toast toast = Toast.makeText(JobPostActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });



        }else{



            JobPostRequest request = new JobPostRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(JobPostActivity.this);
            request.jobTitle = title;
            request.jobDescription = des;
            request.minSalary = String.valueOf(minSal);
            request.maxSalary = String.valueOf(maxSal);
            request.minExp = minExp;
            request.maxExp = maxExp;
           // request.address = location;
            request.education = education;
            request.jobValidity = validity;
            request.opening = opening;
            request.skill = skills;
            request.subSkill = subskill;
            request.contactVisible = showContact;


            if (lat.equals("")) {
                request.lat = myJobList.getLat();
                request.lng = myJobList.getLng();
                request.name = myJobList.getName();
            } else {
                request.lat = lat;
                request.lng = lon;
                request.name = name;
            }

            String jobType = "";
            if (full) {
                jobType = jobType + getResources().getString(R.string.fulltime_text) + ",";
            }
            if (part) {
                jobType = jobType + getResources().getString(R.string.parttime_text) + ",";
            }
            if (freelance) {
                jobType = jobType + getResources().getString(R.string.freelancer_text);
            }
            request.jobType = jobType;

           /* params.put("token", request.token);
            params.put("jobTitle", request.jobTitle);
            params.put("jobDescription", request.jobDescription);
            params.put("minSalary", request.minSalary);
            params.put("maxSalary",  request.maxSalary);
            params.put("minExp",  request.minExp);
            params.put("maxExp", request.maxExp);
            params.put("address", request.address);
            params.put("education", request.education);
            params.put("jobValidity", request.jobValidity);
            params.put("opening", request.opening);
            params.put("skill", request.skill);
            params.put("subSkill", request.subSkill);
            params.put("lat",   request.lat);
            params.put("lng",  request.lng);
            params.put("name",  request.name);
            params.put("contactVisible", request.contactVisible);
            params.put("jobType", request.jobType);*/



            params.put("token", token);
            params.put("jobTitle", title);
            params.put("jobDescription", des);
            params.put("minSalary", String.valueOf(minSal));
            params.put("maxSalary",  String.valueOf(maxSal));
            params.put("minExp",  minExp);
            params.put("maxExp", maxExp);
            params.put("address", location);
            params.put("education", education);
            params.put("jobValidity", validity);
            params.put("opening", opening);
            params.put("skill", skills);
            params.put("subSkill", subskill);
            params.put("lat",  lat);
            params.put("lng",  lon);
            params.put("name",  name);
            params.put("contactVisible", showContact);
            params.put("jobType", request.jobType);



            String url = RequestUrl.API_URL + RequestUrl.JOB_POST;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responsePOSTELSE" + response);

                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                    if (respsuccess.equals("success")) {
                        Toast.makeText(JobPostActivity.this, "Job Posted Sucessfully", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(JobPostActivity.this, EmployerDashboardActivity.class);
                        startActivity(i);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                   // CommonMethod.showAlert("Unable to connect the server,please try again", JobPostActivity.this);

                    Toast toast = Toast.makeText(JobPostActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });

        }

    }




/*

    public void postJob() {
        //Toast.makeText(JobPostActivity.this, "  @ : " + ms_subskills.getSelectedItem(),Toast.LENGTH_LONG).show();
        String title = et_job_title.getText().toString();
        String des = et_job_description.getText().toString();
        String minSal = et_min_salary.getText().toString();
        String maxSal = et_max_salary.getText().toString();
        String minExp = ss_min_exp.getSelectedItem().toString();
        String maxExp = ss_max_exp.getSelectedItem().toString();

        String education = ms_education.getSelectedItem().toString();
        String location = tv_location_val.getText().toString();
        String validity = ss_validity.getSelectedItem().toString();
        String opening = et_number_opening.getText().toString();
        if (title.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_job_title_text), Toast.LENGTH_LONG).show();

            return;
        }

        if (des.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_job_des_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (location .equalsIgnoreCase(getResources().getString(R.string.select_location_text))) {
            Toast.makeText(this, getResources().getString(R.string.enter_location_text), Toast.LENGTH_LONG).show();
            return;
        }
        String subskill = "";

        String skills = ss_skills.getSelectedItem().toString();
        if (skills .equalsIgnoreCase("others")) {
            skills = et_other_skills.getText().toString();
            subskill = et_other_subskills.getText().toString();
            if (skills.length() < 2) {
                Toast.makeText(this, getResources().getString(R.string.enter_other_skills_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (subskill.length() < 2) {
                Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                return;
            }
        } else {

            if (subSkillsList.size() <= 0) {
                subskill = skills;
            } else {
                subskill = ms_subskills.getSelectedItem().toString();

            }

            if (subskill.length() < 3) {
                Toast.makeText(JobPostActivity.this, getResources().getString(R.string.select_subskills_first_text),Toast.LENGTH_LONG).show();
                return;
            }
            if (subskill.contains("others") || subskill.contains("Others")) {
                if (et_other_subskills.getText().toString().length() < 2) {
                    Toast.makeText(this, getResources().getString(R.string.enter_other_subskills_text), Toast.LENGTH_LONG).show();
                    return;
                }
                subskill =  subskill + "," + et_other_subskills.getText().toString();

            }
        }
        if (minSal.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.select_min_salary_text), Toast.LENGTH_LONG).show();
            return;
        }
        if (maxSal.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.select_max_salary_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (Integer.parseInt(minExp) >= Integer.parseInt(maxExp)) {
            Toast.makeText(this, getResources().getString(R.string.min_max_exp),Toast.LENGTH_LONG).show();
            return;
        }
        if (education.length() <2) {
            Toast.makeText(JobPostActivity.this, getResources().getString(R.string.select_education_first),Toast.LENGTH_LONG).show();
            return;
        }
        if (opening.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.enter_num_opening_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (!full && !part && !freelance) {
            Toast.makeText(this, getResources().getString(R.string.one_job_type_text), Toast.LENGTH_LONG).show();
            return;
        }

        if (fromMyJobs) {
            EmployerJobUpdateRequest request = new EmployerJobUpdateRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(JobPostActivity.this);
            request.id = myJobList.getId();
            request.jobTitle = title;
            request.jobDescription = des;
            request.minSalary = String.valueOf(minSal);
            request.maxSalary = String.valueOf(maxSal);
            request.minExp = minExp;
            request.maxExp = maxExp;
            request.address = location;
            request.education = education;
            request.jobValidity = validity;
            request.opening = opening;
            request.skill = skills;
            request.subSkill = subskill;
            request.contactVisible = showContact;
            if (lat .equals("")) {
                request.lat = myJobList.getLat();
                request.lng = myJobList.getLng();
                request.name = myJobList.getName();
            } else {
                request.lat = lat;
                request.lng = lon;
                request.name = name;
            }

            String jobType = "";
            if (full) {
                jobType = jobType + getResources().getString(R.string.fulltime_text) + ",";
            }
            if (part) {
                jobType = jobType + getResources().getString(R.string.parttime_text)+",";
            }
            if (freelance) {
                jobType = jobType + getResources().getString(R.string.freelancer_text);
            }
            request.jobType = jobType;

            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.updateJob(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                    Toast.makeText(JobPostActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(JobPostActivity.this, EmployerDashboardActivity.class);
                    startActivity(i);

                    //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                    //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }

                }

            });
        } else {
            JobPostRequest request = new JobPostRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(JobPostActivity.this);
            request.jobTitle = title;
            request.jobDescription = des;
            request.minSalary = String.valueOf(minSal);
            request.maxSalary = String.valueOf(maxSal);
            request.minExp = minExp;
            request.maxExp = maxExp;
            request.address = location;
            request.education = education;
            request.jobValidity = validity;
            request.opening = opening;
            request.skill = skills;
            request.subSkill = subskill;
            request.lat = lat;
            request.lng = lon;
            request.name = name;
            request.contactVisible = showContact;


            String jobType = "";
            if (full) {
                jobType = jobType + getResources().getString(R.string.fulltime_text) +",";
            }
            if (part) {
                jobType = jobType + getResources().getString(R.string.parttime_text)+",";
            }
            if (freelance) {
                jobType = jobType + getResources().getString(R.string.freelancer_text);
            }
            request.jobType = jobType;

            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.jobJob(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();


                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                    Toast.makeText(JobPostActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(JobPostActivity.this, EmployerDashboardActivity.class);
                    startActivity(i);
                    //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                    //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(JobPostActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(JobPostActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(JobPostActivity.this, EmployerTypeActivity2.class);
                        JobPostActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(JobPostActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }

            });
        }

    }*/

    @OnItemSelected(R.id.ss_skills)
    public void skillSelected(Spinner spinner, int position) {
        subSkillsAdapter.clear();
        if (ss_skills.getSelectedItem().toString() .equalsIgnoreCase("others")) {
            ll_other_skills.setVisibility(View.VISIBLE);
            ll_subskills.setVisibility(View.GONE);
            ll_other_subskills.setVisibility(View.VISIBLE);
        } else {
            ll_other_skills.setVisibility(View.GONE);
            ll_subskills.setVisibility(View.VISIBLE);
            ll_other_subskills.setVisibility(View.GONE);
        }
        fetchSubSkills(jobSkillsLists.get(position).getId());
        fetchSubSkills2(jobSkillsLists.get(position).getId());
    }

    @OnItemSelected(R.id.ms_subskills)
    public void minExpSelected(Spinner spinner, int position) {
        if (ms_subskills.getSelectedItem().toString() .contains("others") || ms_subskills.getSelectedItem().toString() .contains("Others") ) {
            ll_other_subskills.setVisibility(View.VISIBLE);
        } else {
            ll_other_subskills.setVisibility(View.GONE);
        }
    }

    @OnFocusChange({R.id.et_job_title,R.id.et_job_description, R.id.et_number_opening, R.id.et_min_salary, R.id.et_max_salary})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_job_description:
                descriptionFocus(view, hasFocus);
                break;
            case R.id.et_job_title:
                titleFocus(view, hasFocus);
                break;
            case R.id.et_number_opening:
                numberOpeningFocus(view, hasFocus);
                break;
            case R.id.et_min_salary:
                minSalaryFocus(view, hasFocus);
                break;
            case R.id.et_max_salary:
                maxSalaryFocus(view, hasFocus);
                break;

        }
    }
    public void minSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_min_salary.setVisibility(View.VISIBLE);
            et_min_salary.setHint("");
        } else {
            if (et_min_salary.getText().toString() .equals(initialUsername)) {
                tv_min_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_min_salary.setVisibility(View.VISIBLE);

            }
            et_min_salary.setHint(getResources().getString(R.string.min_salary_text));
        }
    }
    public void maxSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_max_salary.setVisibility(View.VISIBLE);
            et_max_salary.setHint("");
        } else {
            if (et_max_salary.getText().toString() .equals(initialUsername)) {
                tv_max_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_max_salary.setVisibility(View.VISIBLE);

            }
            et_max_salary.setHint(getResources().getString(R.string.max_salary_text));
        }
    }
    public void titleFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_job_title.setVisibility(View.VISIBLE);
            et_job_title.setHint("");
        } else {
            if (et_job_title.getText().toString() .equals(initialUsername)) {
                tv_job_title.setVisibility(View.INVISIBLE);
            } else {
                tv_job_title.setVisibility(View.VISIBLE);

            }
            et_job_title.setHint(getResources().getString(R.string.job_title_text));
        }
    }
    public void numberOpeningFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_number_opening.setVisibility(View.VISIBLE);
            et_number_opening.setHint("");
        } else {
            if (et_number_opening.getText().toString() .equals(initialUsername)) {
                tv_number_opening.setVisibility(View.INVISIBLE);
            } else {
                tv_number_opening.setVisibility(View.VISIBLE);

            }
            et_number_opening.setHint(getResources().getString(R.string.number_opening_text));
        }
    }

    public void descriptionFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_job_description.setVisibility(View.VISIBLE);
            et_job_description.setHint("");
        } else {
            if (et_job_description.getText().toString() .equals(initialUsername)) {
                tv_job_description.setVisibility(View.INVISIBLE);
            } else {
                tv_job_description.setVisibility(View.VISIBLE);

            }
            et_job_description.setHint(getResources().getString(R.string.job_title_text));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("newplace"+requestCode+resultCode);
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            String placeID = place.getId();


            // Get live data information
            refreshPlacesData(place);
        }
    }

    public void refreshPlacesData(Place place) {
        lat = String.valueOf(place.getLatLng().latitude);
        lon = String.valueOf(place.getLatLng().longitude);
       // tv_location_val.setText(place.getAddress());
        //tv_location_val.setTextColor(getResources().getColor(R.color.basicTextColor));

        name = String.valueOf(place.getName());
        System.out.println("sgEGGGGG : " + place.getLatLng());

        System.out.println("sgEGGGGG : " + lat);
        System.out.println("sgEGGGGG : " + lon);

    }

    @Override
    public void onResume() {
        super.onResume();


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from") .equals("myjobs")) {
            Intent i = new Intent(this, MyJobsActivity.class);
            this.startActivity(i);
        } else {
            Intent intent = new Intent(this, EmployerDashboardActivity.class);
            startActivity(intent);
        }
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
