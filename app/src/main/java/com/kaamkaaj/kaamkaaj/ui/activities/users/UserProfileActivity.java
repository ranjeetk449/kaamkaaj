package com.kaamkaaj.kaamkaaj.ui.activities.users;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Login.FetchProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.login.FetchProfileResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.login.UserProfile;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

/**
 * Created by akshaysingh on 3/28/18.
 */

public class UserProfileActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.imageView1)
    CircleImageView imageView1;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.ed_father_name)
    EditText ed_father_name;
    @BindView(R.id.ed_full_name)
    EditText ed_full_name;
    @BindView(R.id.ed_dob)
    EditText ed_dob;
    @BindView(R.id.ed_cur_add)
    EditText ed_cur_add;
    @BindView(R.id.ed_gender)
    EditText ed_gender;
    @BindView(R.id.ed_per_add)
    EditText ed_per_add;
    /*@BindView(R.id.profile_pic)
    EditText profile_pic;*/
    @BindView(R.id.et_id)
    EditText et_id;
    @BindView(R.id.ed_experience) EditText ed_experience;
    @BindView(R.id.ed_skills) EditText ed_skills;
    @BindView(R.id.ed_subskills) EditText ed_subskills;
    @BindView(R.id.ed_salary) EditText ed_salary;
    TinyDB tinyDB;

    UserProfile userProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.my_profile_text);
        setSupportActionBar(toolbar);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
          setProfilePic();
          fetchProfile();
          tinyDB = new TinyDB(this);
         // init();
    }
    void init() {
        ed_father_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_full_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_dob.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_cur_add.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_gender.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_per_add.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_id.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_experience.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        ed_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

    }

    @OnClick({ R.id.fab})
    public void openCam(View view) {


        switch (view.getId()) {
            case R.id.fab:
                Intent i = new Intent(this, EditProfileActivity.class);
                startActivity(i);
        }
    }



    public void setProfilePic() {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/kaamkaajjobseeker");
        myDir.mkdirs();

        String fname = "profile_pic1.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) {
            try {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                String token = SharedPrefUtils.getApiKeyEmployee(this);
                if(token.length()>0)
                imageView1.setImageBitmap(b);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

    }





    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            System.out.println("responseDetailIF");
            return;
        } else {
            System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKeyEmployee(this);
            System.out.println("responseDetailELSE"+token);
            RequestParams params = new RequestParams();
            params.put("token", token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_PROFILE_EMPLOYEE;
            System.out.println("resProfileUrl"+url);
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseEmployeeMyProfile" + response);

               /*



                 responseDetail{"status":{"response":"success","message":"","userMessage":""},
                 "data":{"userProfile":{"email":"rpathaksoftware@gmail.com","firstName":"ravi","lastName":"pathak",
                 "mobileNo":"121212","password":"123456","userType":"corporate",
                 "createdAt":"2019-07-21T11:11:20.521Z","updatedAt":"2019-07-21T11:11:20.521Z",
                 "id":"5d344858c462bf110673cdc1"}}}



*/




                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        //String userProfile=status.getString("data");
                        // System.out.println("responseuserProfile" + userProfile);
                        String respsuccess=status.getString("response");
                        System.out.println("responseDetaiddddl" + respsuccess);
                        if (respsuccess.equals("success")) {
                            System.out.println("responseuserProfilescsd");
                            JSONObject userProfile=response.getJSONObject("data");
                            System.out.println("responseuserProfile" + userProfile);
                            JSONObject userProfileList=userProfile.getJSONObject("userProfile");
                            //  System.out.println("respppuserProfilee" + userProfilee);
                           /* String mobile_no=userProfileList.getString("mobile_no");
                            String full_name=userProfileList.getString("full_name");
                            String gender=userProfileList.getString("gender");
                            String dob=userProfileList.getString("dob");
                            String current_address=userProfileList.getString("current_address");
                            String permanent_address=userProfileList.getString("permanent_address");
                            String salary=userProfileList.getString("salary");
                            String created=userProfileList.getString("created");
                            String statusUser=userProfileList.getString("status");
                            String fathersName=userProfileList.getString("fathersName");
                            String idProofDoc=userProfileList.getString("idProofDoc");
                            String subSkill=userProfileList.getString("subSkill");
                            String skill=userProfileList.getString("skill");
                            String experience=userProfileList.getString("experience");
                            String city_name=userProfileList.getString("city_name");
*/




                            if(userProfileList.has("fullName"))
                            {
                                String full_namee=userProfileList.getString("fullName");
                                tinyDB.putEmployeeNameString("username" , full_namee);
                            }



                             if(userProfileList.has("fathersName"))
                            {
                                String fathersName=userProfileList.getString("fathersName");
                                ed_father_name.setText(fathersName);
                            }
                            if(userProfileList.has("gender"))
                            {
                                String gender=userProfileList.getString("gender");
                                ed_gender.setText(gender);
                            }
                            if(userProfileList.has("dob"))
                            {
                                String dob=userProfileList.getString("dob");
                                ed_dob.setText(dob);
                            }
                            if(userProfileList.has("current_address"))
                            {
                                String current_address=userProfileList.getString("current_address");
                                ed_cur_add.setText(current_address);
                            }

                            if(userProfileList.has("permanentAddress"))
                            {
                                String permanent_address=userProfileList.getString("permanentAddress");
                                ed_per_add.setText(permanent_address);
                            }

                             if(userProfileList.has("idProofDoc"))
                            {
                                String idProofDoc=userProfileList.getString("idProofDoc");
                                et_id.setText(idProofDoc);
                            }

                             if(userProfileList.has("experience"))
                            {
                                String experience=userProfileList.getString("experience");
                                ed_experience.setText(experience);
                            }
                             if(userProfileList.has("skill"))
                            {
                                String skill=userProfileList.getString("skill");
                                ed_skills.setText(skill);
                            }
                             if(userProfileList.has("subSkill"))
                            {
                                String subSkill=userProfileList.getString("subSkill");
                                ed_subskills.setText(subSkill);
                            }
                            if(userProfileList.has("salary"))
                            {
                                String salary=userProfileList.getString("salary");
                                ed_salary.setText(salary);
                            }


                        }else{
                           // Toast.makeText(UserProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                            Toast toast = Toast.makeText(UserProfileActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                   // CommonMethod.showAlert("Unable to connect the server,please try again", UserProfileActivity.this);

                    Toast toast = Toast.makeText(UserProfileActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }










/*
    public void fetchProfile() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchProfileRequest request = new FetchProfileRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
       /* request.mobileNo = mobileNum;
        request.otp = otp;
*/
            //request.token = SharedPrefUtils.getApiKey(this);

/*
            Call<FetchProfileResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchProfileRequest(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<FetchProfileResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }
                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }
                @Override
                public void onSuccess(Call<FetchProfileResponse> call, FetchProfileResponse response) {

                    System.out.println("responsenew"+response.getData().getUserProfile());
                    tinyDB.putString("username" , response.getData().getUserProfile().getFull_name());

                    tv_name.setText(response.getData().getUserProfile().getFull_name());
                    ed_full_name.setText(response.getData().getUserProfile().getFull_name());
                    ed_father_name.setText(response.getData().getUserProfile().getFathers_name());
                    ed_gender.setText(response.getData().getUserProfile().getGender());
                    ed_dob.setText(response.getData().getUserProfile().getDob());
                    ed_cur_add.setText(response.getData().getUserProfile().getCurrent_address());
                    ed_per_add.setText(response.getData().getUserProfile().getPermanent_address());
                    ed_salary.setText(response.getData().getUserProfile().getSalary());
                    ed_skills.setText(response.getData().getUserProfile().getSkill());
                    ed_subskills.setText(response.getData().getUserProfile().getSub_skill());
                    et_id.setText(response.getData().getUserProfile().getId_proof_doc());
                    if (response.getData().getUserProfile().getExperience() .contains("10") && !response.getData().getUserProfile().getExperience() .contains("9")) {
                        ed_experience.setText(response.getData().getUserProfile().getExperience() + getResources().getString(R.string.years_or_more_replace_text));
                    } else {
                        ed_experience.setText(response.getData().getUserProfile().getExperience() + getResources().getString(R.string.replace_year_text));

                    }
                }

                @Override
                public void onError(Call<FetchProfileResponse> call, Throwable t) {

                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(UserProfileActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<FetchProfileResponse> call, FetchProfileResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(UserProfileActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(UserProfileActivity.this, EmployerTypeActivity.class);
                        UserProfileActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(UserProfileActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }


            });
        }
    }*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SharedPrefUtils.getAppState(UserProfileActivity.this) == AppConstants.STATE_REGISTERED) {

            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);
        } else {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
