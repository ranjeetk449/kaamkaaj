package com.kaamkaaj.kaamkaaj.ui.activities;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;


public abstract class NewBaseActivity extends AppCompatActivity {

    private ProgressDialog prgDialog;
    private ViewGroup mProgressBarView;
    private ViewGroup mRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showProgressDialog(String msg) {
        try {
            if (prgDialog == null) {
                prgDialog = ProgressDialog.show(this, "", msg);
            } else {
                prgDialog.show();
            }
        } catch (Exception e) {

        }
    }

    protected void hideProgressDialog() {
        try {
            if (prgDialog != null && prgDialog.isShowing())
                prgDialog.dismiss();
        } catch (Exception e) {

        }
    }

    protected ViewGroup getRootView() {
        if (mRootView != null) {
            return mRootView;
        }

        mRootView =  (ViewGroup) this.findViewById(android.R.id.content);

        return mRootView;
    }


    public void showProgressBar() {
        if (mProgressBarView == null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mProgressBarView = (ViewGroup) inflater.inflate(R.layout.progressbar_frame, null);


            int color = getResources().getColor(R.color.colorPrimaryDark);
            ((ProgressBar)mProgressBarView.findViewById(R.id.progressbar)).getIndeterminateDrawable().setColorFilter(
                    Color.parseColor("#c87137"),
                    android.graphics.PorterDuff.Mode.SRC_ATOP);

            getRootView().addView(mProgressBarView, -1);
        }

            if (mProgressBarView.getVisibility() == View.GONE) {
                mProgressBarView.setVisibility(View.VISIBLE);

            }

    }


    public void hideProgressBar() {
        if (mProgressBarView != null) {
            mProgressBarView.setVisibility(View.GONE);
        }

    }


    protected void retryAlertDialog(String msg , final RetrofitRequestListener listener) {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(msg);
        builder.setPositiveButton("RETRY", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.retry();
            }
        });
        builder.setNegativeButton("CANCEL", null);
        builder.show();
    }
}


