package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerProfileUpdateRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerProfileResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.EditProfileActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.FilePath;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class ActivityEmployerProfileUpdate extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.tv_company_name)
    TextView tv_company_name;
    @BindView(R.id.et_company_name)
    EditText et_company_name;
    @BindView(R.id.tv_designation)
    TextView tv_designation;
    @BindView(R.id.et_designation)
    EditText et_designation;
    @BindView(R.id.tv_company_website)
    TextView tv_company_website;
    @BindView(R.id.et_company_website)
    EditText et_company_website;
    @BindView(R.id.tv_company_address)
    TextView tv_company_address;
    @BindView(R.id.et_company_address)
    EditText et_company_address;
    @BindView(R.id.iv_im1)
    CircleImageView iv_im1;
    @BindView(R.id.tv_first_name)
            TextView tv_first_name;
    @BindView(R.id.et_first_name)
            EditText et_first_name;
    @BindView(R.id.tv_last_name)
    TextView tv_last_name;
    @BindView(R.id.et_last_name)
    EditText et_last_name;
    @BindView(R.id.tv_email)
            TextView tv_email;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.tv_mobile_number)
            TextView tv_mobile_number;
    @BindView(R.id.et_mobile_number)
            EditText et_mobile_number;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;

    TinyDB tinyDB;
    Intent intent;
    String employerType = "";
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    String initialUsername = "";
    public static final String TAG = ActivityEmployerProfileUpdate.class.getSimpleName();

    private static final int PICK_FILE_REQUEST = 1;
    private ProgressDialog prgDialog1;



    private String selectedFilePath;
    //private String SERVER_URL = "http://charlie.orowealth.com:3008/uploadCanDocs";
    private String SERVER_URL = "http://139.59.30.24/talific/v1/documents/uploadDocument";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_profile_update);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.update_profile_text);
        setSupportActionBar(toolbar);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();
        employerType = SharedPrefUtils.getEmployerType(this);
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        EnableRuntimePermission();
        tinyDB = new TinyDB(this);
       /* fetchProfile();*/
       hideUnImp();
       fetchProfile();
    }
    void init() {
        et_first_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_designation.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_website.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_address.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_mobile_number.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_email.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_last_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

    }
    @OnClick({R.id.ll_action_button})
    public void clickFunction(View view) {
        switch (view.getId()) {
            case R.id.ll_action_button:
                updateProfile();
                break;
        }
    }






    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            System.out.println("responseDetailIF");
            return;
        } else {
            System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKey(this);
            System.out.println("responseDetailELSE"+token);
            RequestParams params = new RequestParams();
            params.put("token", token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_PROFILE;
            System.out.println("resProfileUrl"+url);
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseDetail" + response);

               /*



                 responseDetail{"status":{"response":"success","message":"","userMessage":""},
                 "data":{"userProfile":{"email":"rpathaksoftware@gmail.com","firstName":"ravi","lastName":"pathak",
                 "mobileNo":"121212","password":"123456","userType":"corporate",
                 "createdAt":"2019-07-21T11:11:20.521Z","updatedAt":"2019-07-21T11:11:20.521Z",
                 "id":"5d344858c462bf110673cdc1"}}}



*/




                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        //String userProfile=status.getString("data");
                        // System.out.println("responseuserProfile" + userProfile);
                        String respsuccess=status.getString("response");
                        System.out.println("responseDetaiddddl" + respsuccess);
                        if (respsuccess.equals("success")) {
                            System.out.println("responseuserProfilescsd");
                            JSONObject userProfile=response.getJSONObject("data");
                            System.out.println("responseuserProfile" + userProfile);
                            JSONObject userProfileList=userProfile.getJSONObject("userProfile");
                            //  System.out.println("respppuserProfilee" + userProfilee);
                            String email=userProfileList.getString("email");
                            String firstName=userProfileList.getString("firstName");

                            String mobileNo=userProfileList.getString("mobileNo");
                           // String password=userProfileList.getString("password");
                           // String userType=userProfileList.getString("userType");
                           /* String company_name=userProfileList.getString("company_name");
                            String designation=userProfileList.getString("designation");
                            String company_website=userProfileList.getString("company_website");
                            String company_address=userProfileList.getString("company_address");
                            // String token=userProfile.getString("token");
                            // System.out.println("teamToken"+token);
                            et_company_name.setText(company_name);
                            et_designation.setText(designation);
                            et_company_website.setText(company_website);
                            et_company_address.setText(company_address);*/



                            if(userProfileList.has("company_name"))
                            {
                                String company_name=userProfileList.getString("company_name");
                                et_company_name.setText(company_name);
                            }

                            if(userProfileList.has("last_name"))
                            {
                                String last_name=userProfileList.getString("last_name");
                                et_last_name.setText(last_name);
                            }

                           /* if(userProfileList.has("lastName"))
                            {

                                String lastName=userProfileList.getString("lastName");
                                et_last_name.setText(lastName);
                            }*/

                            if(userProfileList.has("address"))
                            {
                                String address=userProfileList.getString("address");
                                System.out.println("respaddress"+address);
                               // et_company_name.setText(company_name);
                                et_company_address.setText(address);
                            }
                            if(userProfileList.has("designation"))
                            {
                                String designation=userProfileList.getString("designation");
                                et_designation.setText(designation);
                            }
                            if(userProfileList.has("company_website"))
                            {
                                String company_website=userProfileList.getString("company_website");
                                et_company_website.setText(company_website);
                            }
                            if(userProfileList.has("company_address"))
                            {
                                String company_address=userProfileList.getString("company_address");
                                et_company_address.setText(company_address);
                            }




                           /* et_company_name.setText(response.getData().userProfile.getCompany_name());
                            et_last_name.setText(response.getData().userProfile.getLast_name() );
                            et_first_name.setText(response.getData().userProfile.getFirst_name() );
                            et_designation.setText(response.getData().userProfile.getDesignation());
                            et_email.setText(response.getData().userProfile.getEmail_id());
                            et_company_website.setText(response.getData().userProfile.getCompany_website());
                            et_mobile_number.setText(response.getData().userProfile.getMobile_no());
                            et_company_address.setText(response.getData().userProfile.getCompany_address());
                           */
                            // et_company_name.setText(response.getData().userProfile.getCompany_name());

                            et_first_name.setText(firstName);


                            // et_designation.setText(response.getData().userProfile.getDesignation());
                            et_email.setText(email);
                            // et_company_website.setText(response.getData().userProfile.getCompany_website());
                            et_mobile_number.setText(mobileNo);
                       /* if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                            et_company_address.setText(response.getData().userProfile.getCompany_address());
                        } else {
                            et_company_address.setText(response.getData().userProfile.getAddress());

                        }*/




                        }else{

                            Toast toast = Toast.makeText(ActivityEmployerProfileUpdate.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                           // Toast.makeText(ActivityEmployerProfileUpdate.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", ActivityEmployerProfileUpdate.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }





/*
    public void fetchProfile() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            EmployerProfileRequest request = new EmployerProfileRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            Call<EmployerProfileResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchProfile(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<EmployerProfileResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<EmployerProfileResponse> call, EmployerProfileResponse response) {
                    System.out.println("!!!!!!!!! : " + response.getData().userProfile.getCompany_name());
                    et_company_name.setText(response.getData().userProfile.getCompany_name());
                    et_last_name.setText(response.getData().userProfile.getLast_name() );
                    et_first_name.setText(response.getData().userProfile.getFirst_name() );
                    et_designation.setText(response.getData().userProfile.getDesignation());
                    et_email.setText(response.getData().userProfile.getEmail_id());
                    et_company_website.setText(response.getData().userProfile.getCompany_website());
                    et_mobile_number.setText(response.getData().userProfile.getMobile_no());
                    et_company_address.setText(response.getData().userProfile.getCompany_address());
                }

                @Override
                public void onError(Call<EmployerProfileResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                    Toast.makeText(ActivityEmployerProfileUpdate.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<EmployerProfileResponse> call, EmployerProfileResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(ActivityEmployerProfileUpdate.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(ActivityEmployerProfileUpdate.this, EmployerTypeActivity2.class);
                        ActivityEmployerProfileUpdate.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ActivityEmployerProfileUpdate.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }

                }
            });
        }
    }
*/
    public void updateProfile() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            String companyName = et_company_name.getText().toString();
            final String firstName = et_first_name.getText().toString();
            final String lastName = et_last_name.getText().toString();
            String designation = et_designation.getText().toString();
            final String email = et_email.getText().toString();
            String companyWebsite = et_company_website.getText().toString();
            String mobileNum = et_mobile_number.getText().toString();
            String companyAdd = et_company_address.getText().toString();
            if (firstName.length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_first_name_text), Toast.LENGTH_LONG).show();
                return;
            }
            if (lastName.length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_last_name_text), Toast.LENGTH_LONG).show();

                return;
            }
            if (email.length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_email_text), Toast.LENGTH_LONG).show();

                return;
            }
            if (mobileNum.length() == 0) {
                Toast.makeText(this, getResources().getString(R.string.enter_mobile_number_first), Toast.LENGTH_LONG).show();

                return;
            }
            if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                if (companyName.length() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.enter_company_name_text), Toast.LENGTH_LONG).show();
                    return;
                }
                if (designation.length() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.enter_designation_text), Toast.LENGTH_LONG).show();

                    return;
                }
                if (companyAdd.length() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.enter_company_add_text), Toast.LENGTH_LONG).show();

                    return;
                }
                if (companyWebsite.length() == 0) {
                    Toast.makeText(this, getResources().getString(R.string.enter_website_text), Toast.LENGTH_LONG).show();

                    return;
                }
            }
            EmployerProfileUpdateRequest request = new EmployerProfileUpdateRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                request.company_address = companyAdd;
                request.company_name = companyName;
                request.company_website = companyWebsite;
                request.designation = designation;
            } else {
                request.address = companyAdd;
            }
            request.first_name = firstName;
            request.last_name = lastName;
            request.email_id = email;
            request.mobileNo = mobileNum;
            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.updateProfileEmployer(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                    TinyDB tinyDB = new TinyDB(ActivityEmployerProfileUpdate.this);
                    tinyDB.putString("username" , firstName + " " + lastName);
                    tinyDB.putString("mobile", email);
                    Intent i = new Intent(ActivityEmployerProfileUpdate.this, ActivityEmployerProfile.class);
                    startActivity(i);
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );

                    Toast toast = Toast.makeText(ActivityEmployerProfileUpdate.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    //Toast.makeText(ActivityEmployerProfileUpdate.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(ActivityEmployerProfileUpdate.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(ActivityEmployerProfileUpdate.this, EmployerTypeActivity2.class);
                        ActivityEmployerProfileUpdate.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ActivityEmployerProfileUpdate.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    @OnFocusChange({R.id.et_company_name ,R.id.et_first_name, R.id.et_last_name,R.id.et_designation, R.id.et_email, R.id.et_company_website,R.id.et_mobile_number, R.id.et_company_address })
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_company_name:
                companyNameFocus(view, hasFocus);
                break;
            case R.id.et_first_name:
                firstNameFocus(view, hasFocus);
                break;
            case R.id.et_last_name:
                lastNameFocus(view, hasFocus);
            case R.id.et_designation:
                designationFocus(view, hasFocus);
                break;
            case R.id.et_email:
                emailFocus(view, hasFocus);
                break;
            case R.id.et_company_website:
                companyWebsiteFocus(view, hasFocus);
                break;
            case R.id.et_mobile_number:
                mobileFocus(view, hasFocus);
                break;
            case R.id.et_company_address:
                companyAddressFocus(view,hasFocus);
        }
    }

    public void companyAddressFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_company_address.setVisibility(View.VISIBLE);
            et_company_address.setHint("");
        } else {
            if (et_company_address.getText().toString() .equals(initialUsername)) {
                tv_company_address.setVisibility(View.INVISIBLE);
            } else {
                tv_company_address.setVisibility(View.VISIBLE);
            }
            et_company_address.setHint(getResources().getString(R.string.company_address_text));
        }
    }
    public void mobileFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_mobile_number.setVisibility(View.VISIBLE);
            et_mobile_number.setHint("");
        } else {
            if (et_mobile_number.getText().toString() .equals(initialUsername)) {
                tv_mobile_number.setVisibility(View.INVISIBLE);
            } else {
                tv_mobile_number.setVisibility(View.VISIBLE);
            }
            et_mobile_number.setHint(getResources().getString(R.string.mobile_number_text));
        }
    }
    public void companyWebsiteFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_company_website.setVisibility(View.VISIBLE);
            et_company_website.setHint("");
        } else {
            if (et_company_website.getText().toString() .equals(initialUsername)) {
                tv_company_website.setVisibility(View.INVISIBLE);
            } else {
                tv_company_website.setVisibility(View.VISIBLE);
            }
            et_company_website.setHint(getResources().getString(R.string.company_website_text));
        }
    }
    public void emailFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_email.setVisibility(View.VISIBLE);
            et_email.setHint("");
        } else {
            if (et_email.getText().toString() .equals(initialUsername)) {
                tv_email.setVisibility(View.INVISIBLE);
            } else {
                tv_email.setVisibility(View.VISIBLE);
            }
            et_email.setHint(getResources().getString(R.string.email_text));
        }
    }
    public void designationFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_designation.setVisibility(View.VISIBLE);
            et_designation.setHint("");
        } else {
            if (et_designation.getText().toString() .equals(initialUsername)) {
                tv_designation.setVisibility(View.INVISIBLE);
            } else {
                tv_designation.setVisibility(View.VISIBLE);
            }
            et_designation.setHint(getResources().getString(R.string.designation_text));
        }
    }


    public void companyNameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_company_name.setVisibility(View.VISIBLE);
            et_company_name.setHint("");
        } else {
            if (et_company_name.getText().toString() .equals(initialUsername)) {
                tv_company_name.setVisibility(View.INVISIBLE);
            } else {
                tv_company_name.setVisibility(View.VISIBLE);
            }
            et_company_name.setHint(getResources().getString(R.string.company_name_text));
        }
    }

    public void lastNameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_last_name.setVisibility(View.VISIBLE);
            et_last_name.setHint("");
        } else {
            if (et_last_name.getText().toString() .equals(initialUsername)) {
                tv_last_name.setVisibility(View.INVISIBLE);
            } else {
                tv_last_name.setVisibility(View.VISIBLE);
            }
            et_last_name.setHint(getResources().getString(R.string.last_name_text));
        }
    }
    public void firstNameFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_first_name.setVisibility(View.VISIBLE);
            et_first_name.setHint("");
        } else {
            if (et_first_name.getText().toString() .equals(initialUsername)) {
                tv_first_name.setVisibility(View.INVISIBLE);
            } else {
                tv_first_name.setVisibility(View.VISIBLE);
            }
            et_first_name.setHint(getResources().getString(R.string.user_name_text));
        }
    }


  /*  protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 7 && resultCode == RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            System.out.println("respbitmaponactivity"+bitmap);
            iv_im1.setImageBitmap(bitmap);

        }
    }*/

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {
        } else {
            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

    private void SaveImage(Bitmap finalBitmap) {
        System.out.print("respfinalBitmap"+finalBitmap);
        String userType = SharedPrefUtils.getUserLoginType(this);
        System.out.print("respuserType"+userType);
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
       // File myDir = new File(root + "/kaamkaaj");
        System.out.print("respdscdssda"+root);

       // File myDir;
        if(userType.equals("Google")){
            File   myDir1 = new File(root + "/kaamkaajgoogle");
            myDir1.mkdirs();
            String fname = "profile_pic1.jpg";
            File file = new File (myDir1, fname);
            if (file.exists ()) file.delete ();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(userType.equals("Facebook")){
            File  myDir2 = new File(root + "/kaamkaajfb");
            myDir2.mkdirs();
            String fname = "profile_pic1.jpg";
            File file = new File (myDir2, fname);
            if (file.exists ()) file.delete ();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(userType.equals("Normal")){
            System.out.print("resp"+userType);
            File  myDir3 = new File(root + "/kaamkaajj");
            myDir3.mkdirs();
            System.out.print("respuserType"+userType);

            String fname = "profile_pic1.jpg";
            File file = new File (myDir3, fname);
            if (file.exists ()) file.delete ();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public void hideUnImp() {
        System.out.print("!#!@$!@$!@$!@$!@!@$!@$!$$");

        if (employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
            tv_company_name.setVisibility(View.GONE);
            et_company_name.setVisibility(View.GONE);
            tv_designation.setVisibility(View.GONE);
            et_designation.setVisibility(View.GONE);
            tv_company_website.setVisibility(View.GONE);
            et_company_website.setVisibility(View.GONE);
            tv_company_address.setText(getResources().getString(R.string.adress_text));
            et_company_address.setHint(getResources().getString(R.string.adress_text));

        }
    }

    @OnClick({R.id.iv_im1})
    public void openCam(View view) {
        switch (view.getId()) {
            case R.id.iv_im1:
               /* Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                startActivityForResult(intent, 7);
                return;*/
                searchJobsAlert();
        }
    }


    public void searchJobsAlert() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_upload_pic);
        dialog.setCancelable(true);
        LinearLayout ll_skills_tab = (LinearLayout) dialog.findViewById(R.id.ll_skills_tab);
        // LinearLayout ll_geo_tab = (LinearLayout) dialog.findViewById(R.id.ll_geo_tab);
        ll_skills_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Intent i = new Intent(CustomerDashboardActivity.this, JobFilterActivity.class);
                //   startActivity(i);
                dialog.dismiss();
                intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 7);
            }
        });
       /* ll_geo_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Intent i = new Intent(CustomerDashboardActivity.this, GeolocationJobsSearchFormActivity.class);
                //   startActivity(i);
                dialog.dismiss();

                uploadFile();
            }
        });*/

        dialog.show();
    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG,"Selected File Path:1111111" );

        if (requestCode == 7 && resultCode == RESULT_OK) {

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");

            iv_im1.setImageBitmap(bitmap);
            SaveImage(bitmap);
        }



        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                Log.i(TAG,"Selected File Path:11111112222" );

                if(data == null){
                    //no data present
                    return;
                }
                Log.i(TAG,"Selected File Path:1111113124231" );


                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this,selectedFileUri);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){
                    proceed(selectedFilePath);
                }else{
                    Toast.makeText(this,"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }



    public void proceed(final String filePath) {

        uploadFile(selectedFilePath, "WFWGWB","ergeragrg");

    }

    public int uploadFile(final String selectedFilePath,final String password,final String email){

        showProgressDialog1("Please Wait...");
        new Thread(new Runnable() {
            public void run() {
                int serverResponseCode = 0;

                HttpURLConnection connection;
                DataOutputStream dataOutputStream;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";


                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File selectedFile = new File(selectedFilePath);


                String[] parts = selectedFilePath.split("/");
                final String fileName = parts[parts.length - 1];


                try {
                    FileInputStream fileInputStream = new FileInputStream(selectedFile);
                    URL url = new URL(SERVER_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);//Allow Inputs
                    connection.setDoOutput(true);//Allow Outputs
                    connection.setUseCaches(false);//Don't use a cached Copy
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");

                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    connection.setRequestProperty("tenth_marksheet:10cc069d98957550702c18015dc24e3c", selectedFilePath);
                    connection.setRequestProperty("password", password);
                    connection.setRequestProperty("token","10cc069d98957550702c18015dc24e3c");


                    //creating new dataoutputstream
                    dataOutputStream = new DataOutputStream(connection.getOutputStream());

                    int sdf = selectedFilePath.lastIndexOf(".");
                    String gvhjn = selectedFilePath.substring(sdf);
                    String newName = "10cc069d98957550702c18015dc24e3c" + gvhjn;
                    //writing bytes to data outputstream
                    dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"tenth_marksheet:10cc069d98957550702c18015dc24e3c\"; token=10cc069d98957550702c18015dc24e3c; filename=\""
                            + newName  + "\"" + lineEnd);

                    dataOutputStream.writeBytes(lineEnd);

                    //returns no. of bytes present in fileInputStream
                    bytesAvailable = fileInputStream.available();
                    //selecting the buffer size as minimum of available bytes or 1 MB
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    //setting the buffer as byte array of size of bufferSize
                    buffer = new byte[bufferSize];

                    //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                    while (bytesRead > 0) {
                        //write the bytes read from inputstream
                        dataOutputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    dataOutputStream.writeBytes(lineEnd);
                    dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("token", "10cc069d98957550702c18015dc24e3c");
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();

                    Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                    //response code of 200 indicates the server status OK
                    if (serverResponseCode == 200) {
                        hideProgressDialog1();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ActivityEmployerProfileUpdate.this, "DONEEE!", Toast.LENGTH_SHORT).show();

                                System.out.println("Successfully uploaded docs !!!!!!!!!!!!!!");
                            }
                        });



                    } else {
                        hideProgressBar();
                    }

                    //closing the input and output streams
                    fileInputStream.close();
                    dataOutputStream.flush();
                    dataOutputStream.close();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivityEmployerProfileUpdate.this, "File Not Found", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Toast.makeText(ActivityEmployerProfileUpdate.this, "URL error!", Toast.LENGTH_SHORT).show();

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ActivityEmployerProfileUpdate.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
                }
            }
        }).start();
        return 0;


    }

    protected void showProgressDialog1(String msg) {
        try {
            if (prgDialog1 == null) {
                prgDialog1 = ProgressDialog.show(this, "", msg);
            } else {
                prgDialog1.show();
            }
        } catch (Exception e) {

        }
    }

    protected void hideProgressDialog1() {
        try {
            if (prgDialog1 != null && prgDialog1.isShowing())
                prgDialog1.dismiss();
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ActivityEmployerProfile.class);
        startActivity(intent);
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
