package com.kaamkaaj.kaamkaaj.ui.commonUtil;

public class Constant {

    // camara
    public static String APPLICATION_JSON = "application/json";
    public static String FORM_URL_ENCODE = "application/x-www-form-urlencoded";
    public static String ACCESS = "Access";
    public static final String AGENTID = "agent_id";
    public static final String BLANK_TEXT = "";
    public static final String BOOKINGID = "booking_id";
    public static final String BOOKINGNUM = "booking_num";
    public static String CONTENT_TYPE = "Content-type";
    public static String DREAMPARTNER_DATABASE = "DatabaseDreampartner";
    public static final String FALSE = "false";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String NAV_FLAG = "nav_flag";
    public static final int REQUEST_CODE_CROP_IMAGE = 3;
    public static final int REQUEST_CODE_GALLERY = 1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 2;
    public static final String RESPONCE_CODE = "responseCode";
    public static final String RESPONCE_MESSAGE = "responseMessage";
    public static String SERVER_RESPONSE_SUCCESS_CODE = "200";
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final String TOGGLE_FLAG = "toggleFlag";
    public static final String TXT_BLANK = "";
    public static String UTF_8 = "UTF-8";
    public static final String VEHICLE_NUMBER = "vehicle_no";
    public static final String NAME = "name";
    public static final String CURRENT_STATUS = "current_status";
    public static final String NAMEIMAGE = "nameimage";
    public static final String VID = "v_id";
    public static final String RECHARGE_AMOUNT = "recharge_amount";

    public static final String OWNER_NUMBER = "owner_number";
    public static final String OWNER_EMAIL = "owner_email";
    public static final String DRIVER_NAME = "driver_name";
    public static final String DRIVER_NUM = "driver_num";







}
