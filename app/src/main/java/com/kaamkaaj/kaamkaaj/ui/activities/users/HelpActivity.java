package com.kaamkaaj.kaamkaaj.ui.activities.users;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akshaysingh on 4/27/18.
 */

public class HelpActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.tv_email)
    TextView tv_email;

    public Handler handler = null;
    public static Runnable runnable = null;

    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.CALL_PHONE
    };
    boolean blueColor = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        handler = new Handler();

        ButterKnife.bind(this);
        toolbar.setTitle(R.string.help_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        EnableRuntimePermission();
        runnable = new Runnable() {
            public void run() {
                //fetchDetails();
                System.out.println("Service onStartCommand ciunet eusanjv svnasjv sfvuasj nbarwbrwijb arsba");
                checkTextColor();
                // Toast.makeText(context, "Service is still running", Toast.LENGTH_LONG).show();
                handler.postDelayed(runnable, 600);
            }
        };
        handler.postDelayed(runnable, 600);
    }

    public void checkTextColor() {
        if (blueColor) {
            blueColor = false;
            tv_number.setVisibility(View.INVISIBLE);
         //   tv_number.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            blueColor = true;
            tv_number.setVisibility(View.VISIBLE);
            //tv_number.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        }

    }

    @OnClick({R.id.tv_number, R.id.tv_email})
    public void doThings(View view) {
        switch (view.getId()) {
            case R.id.tv_number:

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:9811188688"));

                if (ActivityCompat.checkSelfPermission(HelpActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
                break;
        }
    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CALL_PHONE))
        {
            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {
            ActivityCompat.requestPermissions(this, EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(runnable);

            super.onBackPressed();
            this.finish();
            this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


    }
}