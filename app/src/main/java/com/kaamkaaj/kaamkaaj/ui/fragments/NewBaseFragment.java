package com.kaamkaaj.kaamkaaj.ui.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;


/**
 * Created by workcellsolutions on 24/12/16.
 */

public class NewBaseFragment extends Fragment {

    private ProgressDialog prgDialog;
    private ViewGroup mContentContainer;
    private ViewGroup mProgressBarView;


    protected void showProgressDialog(String msg) {
        try {
            if (prgDialog == null) {
                prgDialog = ProgressDialog.show(getActivity(), "", msg);
            } else {
                prgDialog.show();
            }
        } catch (Exception e) {

        }
    }

    protected void hideProgressDialog() {
        try {
            if (prgDialog != null && prgDialog.isShowing())
                prgDialog.dismiss();
        } catch (Exception e) {

        }
    }

    protected ViewGroup getRootView() {

        return mContentContainer;
    }




    protected void retryAlertDialog(String msg , final RetrofitRequestListener listener) {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setMessage(msg);
        builder.setPositiveButton("RETRY", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.retry();
            }
        });
        builder.setNegativeButton("CANCEL", null);
        builder.show();
    }


    public NewBaseFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.basefragment, container, false);
        mContentContainer = (ViewGroup) view.findViewById(R.id.content_container);
        return view;
    }


    public void setContentView(int layoutResId) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View contentView = layoutInflater.inflate(layoutResId, null);
        setContentView(contentView);
    }


    public void setContentView(View view) {

        if (view == null) {
            throw new IllegalArgumentException("Content view can't be null");
        }
        if (mContentContainer instanceof ViewGroup) {
            ViewGroup contentContainer = (ViewGroup) mContentContainer;
            contentContainer.addView(view);
        } else {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
    }
    public void showProgressBar() {
        if (mProgressBarView == null) {

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            mProgressBarView = (ViewGroup) inflater.inflate(R.layout.progressbar_frame, null);


            int color = getResources().getColor(R.color.colorAccent);
            ((ProgressBar)mProgressBarView.findViewById(R.id.progressbar)).getIndeterminateDrawable().setColorFilter(
                    Color.parseColor("#" + Integer.toHexString(color)),
                    android.graphics.PorterDuff.Mode.SRC_ATOP);

            getRootView().addView(mProgressBarView, -1);
        }

        if (mProgressBarView.getVisibility() == View.GONE) {
            mProgressBarView.setVisibility(View.VISIBLE);

        }

    }


    public void hideProgressBar() {
        if (mProgressBarView != null) {
            mProgressBarView.setVisibility(View.GONE);
        }

    }

}
