package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akshaysingh on 4/12/18.
 */

public class EmployerTypeActivity2 extends AppCompatActivity {

    @BindView(R.id.rl_corporate)
    RelativeLayout rl_corporate;
    @BindView(R.id.rl_individual)
    RelativeLayout rl_individual;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_selection_activity_2);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.rl_individual, R.id.rl_corporate})
    public void gotoClass(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.rl_corporate:
                SharedPrefUtils.setEmployerType(this, AppConstants.EMPLOYER_TYPE_CORPORATE);
                intent = new Intent(this, EmployerSignupActivity.class);
                this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                intent.putExtra(AppConstants.USER_TYPE,AppConstants.EMPLOYER_TYPE_CORPORATE);
                startActivity(intent);
                break;
            case R.id.rl_individual:
                SharedPrefUtils.setEmployerType(this, AppConstants.EMPLOYER_TYPE_INDI);
                intent = new Intent(this, EmployerSignupActivity.class);
                intent.putExtra(AppConstants.USER_TYPE,AppConstants.EMPLOYER_TYPE_INDI);
                this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(intent);
                return;
        }
    }
}
