package com.kaamkaaj.kaamkaaj.ui.commonUtil;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by amit on 17/9/17.
 */

public class CommonMethod {

    /** Called for checking Internet connection */
    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo;
        try {
            netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    public static void showAlert(String message, Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    /** Called For Saving VechileNumber  */

    public static void saveVehileNumber(Context context, String key,
                                        String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getVehicleNumber(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.VEHICLE_NUMBER,"");

        return vehicleNumber;
    }



    public static void saveOwnerNumber(Context context, String key,
                                       String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getOwnerNumber(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.OWNER_NUMBER,"");

        return vehicleNumber;
    }


    public static void saveOwnerEmail(Context context, String key,
                                      String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getOwnerEmail(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.OWNER_EMAIL,"");

        return vehicleNumber;
    }




    public static void saveDriverName(Context context, String key,
                                      String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getDriverName(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.DRIVER_NAME,"");

        return vehicleNumber;
    }


    public static void saveDriveMobNum(Context context, String key,
                                       String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getDriveMobNum(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.DRIVER_NUM,"");

        return vehicleNumber;
    }






    public static void saveName(Context context, String key,
                                String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getName(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.NAME,"");

        return vehicleNumber;
    }


    public static void saveNameAmount(Context context, String key,
                                      String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getNameAmount(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.RECHARGE_AMOUNT,"");

        return vehicleNumber;
    }









    public static void saveNameCurrent(Context context, String key,
                                       String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getNameCurrent(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String vehicleNumber = sharedPreferences.getString(Constant.CURRENT_STATUS,"");

        return vehicleNumber;
    }







    public static void saveUserImage(Context context, String key,
                                     String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static String getUserImage(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String userImage = sharedPreferences.getString(Constant.NAMEIMAGE,"");

        return userImage;
    }









    /** Called For Saving Latitude  */

    public static void saveLatitude(Context context, String key,
                                    String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }


    public static void saveLongitude(Context context, String key,
                                     String value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }



    public static String getLatitude(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String latitude = sharedPreferences.getString(Constant.LATITUDE,"");

        return latitude;
    }
    /** Called For Saving Latitude  */


    public static String getLongitude(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(Constant.LONGITUDE, "");
    }





    /** Called For Saving toogleFlag  */

    public static void saveToggleFlag(Context context, String key,
                                      boolean value) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }
    public static boolean getToggleFlag(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        boolean toggleFlag = sharedPreferences.getBoolean(Constant.TOGGLE_FLAG,false);

        return toggleFlag;
    }

    public static void saveNavigationFlag(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getNavigationFlag(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String longitude = sharedPreferences.getString(Constant.NAV_FLAG,"");
        return longitude;

    }

    public static void saveBookingNumber(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getBookingNumber(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String longitude = sharedPreferences.getString(Constant.BOOKINGNUM,"");
        return longitude;


    }


}
