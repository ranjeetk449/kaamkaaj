package com.kaamkaaj.kaamkaaj.ui.interfaces;

import android.view.View;


public interface OnItemClickListener {

    void onItemClick(int position, View view);
}
