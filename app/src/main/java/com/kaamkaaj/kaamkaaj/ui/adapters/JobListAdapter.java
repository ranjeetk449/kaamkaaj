package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.JobDescriptionActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;

import java.util.ArrayList;
import android.os.Handler;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 3/27/18.
 */

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "JobListAdapter";
    private ArrayList<JobList> mDataset;
    private ArrayList<Boolean> selected;
    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public JobListAdapter(Context mContext, ArrayList<JobList> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        selected = new ArrayList<Boolean>();

        mHandler = new Handler();
    }

    @Override
    public void onItemClick(int position, View view) {


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;
        @BindView(R.id.iv_job_check)
        ImageView iv_job_check;
        @BindView(R.id.tv_job_title)
        TextView tv_job_title;
        @BindView(R.id.tv_company_name)
        TextView tv_company_name;
        @BindView(R.id.tv_salary)
        TextView tv_salary;
        @BindView(R.id.tv_experience)
        TextView tv_experience;
        @BindView(R.id.tv_location)
        TextView tv_location;
        @BindView(R.id.each_block)
        LinearLayout each_block;
        @BindView(R.id.ll_check)
        LinearLayout ll_check;
        @BindView(R.id.ll_company)
        LinearLayout ll_compnay;

        @BindView(R.id.ll_just_to_check)
        LinearLayout ll_just_to_check;

        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_job_item, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        holder.tv_job_title.setText(mDataset.get(position).getJob_title());
        holder.tv_company_name.setText(mDataset.get(position).getCompany_name());
        holder.tv_salary.setText(mDataset.get(position).getMin_salary() + " to " + mDataset.get(position).getMax_salary());
        holder.tv_experience.setText(mDataset.get(position).getMin_exp() + " - " + mDataset.get(position).getMax_emp() +  " " +  mContext.getResources().getString(R.string.sample_experience));
        holder.tv_location.setText(mDataset.get(position).getAddress());
        holder.each_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, JobDescriptionActivity.class);
                i.putExtra(AppConstants.FROM_CLASS, "listing");
                i.putExtra(AppConstants.KEY_CLIENT,mDataset.get(position) );
                mContext.startActivity(i);
            }
        });

        holder.ll_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDataset.get(position).getSelected() .equals("no")) {
                    mDataset.get(position).setSelected("yes");
                    Resources res = mContext.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    holder.iv_job_check.setImageDrawable(drawable );

                } else {
                    mDataset.get(position).setSelected("no");
                    Resources res = mContext.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    holder.iv_job_check.setImageDrawable(drawable );
                }
            }
        });

    }


    public void addItem(JobList dataObj, int index) {
        mDataset.add(dataObj);
        selected.set(index,false);
        notifyItemInserted(index);

    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void removeProgressBarView() {
        //remove the dummy item we added to the end of list
        if (mDataset.size() > 0) {
            mDataset.remove(mDataset.size() - 1);
            //notifyDataSetChanged();
            notifyItemRemoved(mDataset.size());
        }
    }
}