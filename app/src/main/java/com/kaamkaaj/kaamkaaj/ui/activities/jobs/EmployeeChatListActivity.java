package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.claudiodegio.msv.OnSearchViewListener;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.ChatListRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.EmployeeChatListRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ChatList;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ChatListResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.EmployeeChatList;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.EmployeeChatListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerLoginActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.EmployeeChatAdapter;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 6/21/18.
 */

public class EmployeeChatListActivity extends NewBaseActivity implements OnSearchViewListener {

    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.rv_jobs)
    RecyclerView rv_jobs;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    EmployeeChatList chatList;
    ArrayList<EmployeeChatList> chatListArrayList = new ArrayList<>();
    ArrayList<ChatList> chatListArrayListTotal = new ArrayList<ChatList>();;

    EmployeeChatAdapter mAdapter;
    private boolean mInFlight;
    Context mContext;
    TinyDB tinyDB;
    int mCurrentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_listing);
        mContext = this;
        ButterKnife.bind(this);
        top_bar.setTitle(R.string.chat_text);
        setSupportActionBar(top_bar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new EmployeeChatAdapter(mContext, chatListArrayList);
        rv_jobs.setLayoutManager(lLayout);
        rv_jobs.setAdapter(mAdapter);
        fab.setVisibility(View.GONE);
      //  fillData();
        getChatList();

       // fetchJobs(mCurrentPage, true);
    }


    public void getChatList() {
        String employee_id = SharedPrefUtils.getApiKeyEmployee(this);
        System.out.println("respemployee_id"+employee_id);
        RequestParams params = new RequestParams();
        params.put("employee_id", employee_id);

        String url = RequestUrl.API_URL + RequestUrl.CHATLIST_EMPLOYEE;
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                super.onSuccess(statusCode, headers, response);
                System.out.println("responseLogin" + response);

               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                try {
                    hideProgressBar();
                    chatListArrayList.clear();
                    JSONObject status=response.getJSONObject("status");
                    String respsuccess=status.getString("response");
                    String userMessage=status.getString("userMessage");
                    // responseDetail{"status":{"response":"failure","message":"","userMessage":"Record not found"}}
                    if (respsuccess.equals("success")) {
                        JSONObject data=response.getJSONObject("data");
                        //System.out.println("responseuserProfile" + userProfile);
                        JSONArray chats=data.getJSONArray("chats");

                        System.out.println("respchatchats.length()" +chats.length());

                       /* {"chat_by":"employer","employeeFirstName":"jobseaker",
                                "employee_id":"5d65605c14bf550bd293a755","employerFirstName":"ravi pathak","employerMobileNo":"121212",
                                "employer_id":"5d655e9614bf550bd293a753","chats":[],"createdAt":"2019-10-14T16:59:30.895Z",
                                "updatedAt":"2019-10-14T17:09:43.193Z","id":"5da4a972eb8cfa080f213f55"}*/

                        if(chats.length()>0) {
                            for (int i = 0; i < chats.length(); i++) {
                                chatList = new EmployeeChatList();
                                JSONObject chatobject = chats.getJSONObject(i);
                                System.out.println("respchatchatobject" + chatobject);
                                String id = chatobject.getString("id");
                                String chat_by = chatobject.getString("chat_by");
                                String employeeFirstName = chatobject.getString("employeeFirstName");
                                String employee_id = chatobject.getString("employee_id");
                                String employerFirstName = chatobject.getString("employerFirstName");
                               // String employerMobileNo = chatobject.getString("employerMobileNo");
                                String employerMobileNo;
                                if (chatobject.has("employerMobileNo")) {
                                     employerMobileNo = chatobject.getString("employerMobileNo");
                                }
                                else{
                                    employerMobileNo="";
                                }

                                String employer_id = chatobject.getString("employer_id");
                                JSONArray chatarray=chatobject.getJSONArray("chats");
                                String message="";
                                for (int j = 0; j < chatarray.length(); j++) {
                                    JSONObject chatArrayobject = chatarray.getJSONObject(j);

                                    message =chatArrayobject.getString("message");
                                }
                                System.out.println("resID" + id);
                                chatList.setId(id);
                                chatList.setChat_by(chat_by);
                                if(message!="null")
                                    chatList.setMessage(message);
                                else
                                    chatList.setMessage("");
                                chatList.setEmployeeFirstName(employeeFirstName);
                                chatList.setEmployee_id(employee_id);
                                chatList.setEmployerFirstName(employerFirstName);
                                chatList.setEmployerMobileNo(employerMobileNo);
                                chatList.setEmployer_id(employer_id);


                                chatListArrayList.add(chatList);

                            }
                            mAdapter.notifyDataSetChanged();
                        }

                        else{
                            Toast toast = Toast.makeText(EmployeeChatListActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                       // String name=userProfiledet.getString("name");
                       // String email=userProfiledet.getString("email");
                        //String mobileNo=userProfiledet.getString("mobileNo");
                        /*System.out.println("teamToken"+token+email+mobileNo);
                        SharedPrefUtils.setAppState(EmployerLoginActivity.this, AppConstants.STATE_REGISTERED);
                        SharedPrefUtils.setApiKey(EmployerLoginActivity.this, token);
                        SharedPrefUtils.setEmployerName(EmployerLoginActivity.this, name);
                        SharedPrefUtils.setEmployerEmail(EmployerLoginActivity.this, email);
                        SharedPrefUtils.setEmployerMobile(EmployerLoginActivity.this, mobileNo);
                        SharedPrefUtils.setUserLoginType(EmployerLoginActivity.this,"Normal");
                        Intent intent = new Intent(EmployerLoginActivity.this, EmployerDashboardActivity.class);
                        EmployerLoginActivity.this.finish();
                        startActivity(intent);*/

                    }else{
                        CommonMethod.showAlert(userMessage, EmployeeChatListActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable
                    throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CommonMethod.showAlert("Unable to connect the server,please try again", EmployeeChatListActivity.this);
                hideProgressBar();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgressBar();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        chatListArrayList.clear();
        getChatList();

    }


/*/
    public void getChatList() {
        EmployeeChatListRequest request = new EmployeeChatListRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.employer_id = SharedPrefUtils.getApiKey(this);
        Call<EmployeeChatListResponse> apiResponseCall;
        apiResponseCall =
                service.chatListEmployee(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EmployeeChatListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

            }

            @Override
            protected void onEndApi() {

            }

            @Override
            public void onSuccess(Call<EmployeeChatListResponse> call, EmployeeChatListResponse response) {
                chatListArrayList.clear();

                if(response.getData().getChats().size()>0) {
                    for (int i = 0; i < response.getData().getChats().size(); i++) {
                        EmployeeChatList chatList = response.getData().getChats().get(i);
                        chatListArrayList.add(chatList);
                        chatListArrayListTotal.add(chatList);
                    }
                    mAdapter.notifyDataSetChanged();
                }else{
                    // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                    Toast toast = Toast.makeText(EmployeeChatListActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onError(Call<EmployeeChatListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EmployeeChatListActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                mInFlight = false;
            }

            @Override
            public void onCustomError(Call<ChatListResponse> call, ChatListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EmployeeChatListActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EmployeeChatListActivity.this, EmployerTypeActivity.class);
                    EmployeeChatListActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    mInFlight = false;
                }
            }
        });
    }
   */






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem mSearch = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               /* if (query .equals("")) {
                    getChatList();
                } else {

                    chatListArrayList.clear();
                    for (int i=0; i < chatListArrayListTotal.size(); i++) {
                        if (chatListArrayListTotal.get(i).getFull_name().toLowerCase().contains(query.toLowerCase())) {
                            chatListArrayList.add(chatListArrayListTotal.get(i));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }*/

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               /* if (newText .equals("")) {
                    getChatList();
                } else {

                    chatListArrayList.clear();
                    for (int i=0; i < chatListArrayListTotal.size(); i++) {
                        if (chatListArrayListTotal.get(i).getFull_name().toLowerCase().contains(newText.toLowerCase())) {
                            chatListArrayList.add(chatListArrayListTotal.get(i));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }*/
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN) .equals(AppConstants.USER_EMPLOYER)) {
            Intent intent = new Intent(this, EmployerDashboardActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);
        }
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onSearchViewShown() {

    }

    @Override
    public void onSearchViewClosed() {

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public void onQueryTextChange(String s) {

    }
}
