package com.kaamkaaj.kaamkaaj.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.FetchSavedForLaterCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.CandidateSearch;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.CandidateSearchResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity2;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.SearchCandidatesAdapter;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by akshaysingh on 6/19/18.
 */

public class SavedCandidatesActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    ArrayList<CandidateSearch> candidateSearchArrayList = new ArrayList<CandidateSearch>();;
    SearchCandidatesAdapter mAdapter;
    Context mContext;
    private boolean mInFlight;
    int mCurrentPage = 1;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_listing);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.saved_candidates_text);
        tinyDB = new TinyDB(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new SearchCandidatesAdapter(mContext, candidateSearchArrayList);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);
        fab.setVisibility(View.GONE);



        //addCategoryData();
        recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public void onLoadMore(int currentPage, int count) {
                //     Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                    //  mAdapter.addProgressBarView();
                    searchCandidates(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });
        searchCandidates(mCurrentPage, true);
    }


    public void searchCandidates(int mPage, final boolean showProgress) {
        FetchSavedForLaterCandidateRequest request = new FetchSavedForLaterCandidateRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.page = String.valueOf(mPage);
        request.token = SharedPrefUtils.getApiKey(this);
        Call<CandidateSearchResponse> apiResponseCall;
        apiResponseCall = service.fetchSaved(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<CandidateSearchResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<CandidateSearchResponse> call, CandidateSearchResponse response) {
                ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
               // System.out.println("respSave : " + response+response.getData().size());
                if(response.getData().getProfile().size()>0) {
                    for (int i = 0; i < response.getData().getProfile().size(); i++) {
                        candidateSearchArrayList.add(response.getData().getProfile().get(i));
                        list.add(response.getData().getProfile().get(i).getId());

                    }
                    mCurrentPage++;
                    mAdapter.notifyDataSetChanged();
                    mInFlight = false;
                    tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST, list);
                }else{
                    // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                    Toast toast = Toast.makeText(SavedCandidatesActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onError(Call<CandidateSearchResponse> call, Throwable t) {
               // Toast.makeText(SavedCandidatesActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                Toast toast = Toast.makeText(SavedCandidatesActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                mInFlight = false;
            }

            @Override
            public void onCustomError(Call<CandidateSearchResponse> call, CandidateSearchResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(SavedCandidatesActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(SavedCandidatesActivity.this, EmployerTypeActivity2.class);
                    SavedCandidatesActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(SavedCandidatesActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }
            }

        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
