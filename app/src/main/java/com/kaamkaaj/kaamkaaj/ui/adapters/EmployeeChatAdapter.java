package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ChatList;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.EmployeeChatList;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.EachChatActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.EmployeeChatListActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.EmployeeEachChatActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by akshaysingh on 6/21/18.
 */

public class EmployeeChatAdapter extends RecyclerView.Adapter<EmployeeChatAdapter.Holder> implements
        AdapterView.OnItemClickListener, OnItemClickListener {

    private static String LOG_TAG = "JobListAdapter";
    //private ArrayList<String> mDataset;
    private List<EmployeeChatList> mDataset;
    private ArrayList<Boolean> selected;
    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public EmployeeChatAdapter(Context mContext, List<EmployeeChatList> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        selected = new ArrayList<Boolean>();

        mHandler = new Handler();
    }

    @Override
    public void onItemClick(int position, View view) {


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_mobile)
        TextView tv_mobile;
        @BindView(R.id.imageView1)
        CircleImageView imageView1;
        @BindView(R.id.each_block)
        LinearLayout each_block;
        @BindView(R.id.tv_message)
        TextView tv_message;
        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_chat, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        holder.tv_mobile.setText(mDataset.get(position).getEmployerMobileNo());
        holder.tv_message.setText(mDataset.get(position).getMessage());
        holder.tv_name.setText(mDataset.get(position).getEmployerFirstName());
        holder.each_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, EmployeeEachChatActivity.class);
                i.putExtra("fromClass","fromChatList");
                i.putExtra("toEmployeeName", mDataset.get(position).getEmployerFirstName());
                i.putExtra("employeeMobileNo", mDataset.get(position).getMobile_no());
                i.putExtra("employee_id", mDataset.get(position).getEmployee_id());
                i.putExtra("employer_id", mDataset.get(position).getEmployer_id());
                mContext.startActivity(i);
            }
        });
    }


    public void addItem(EmployeeChatList dataObj, int index) {
        mDataset.add(dataObj);
        selected.set(index,false);
        notifyItemInserted(index);

    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void removeProgressBarView() {
        //remove the dummy item we added to the end of list
        if (mDataset.size() > 0) {
            mDataset.remove(mDataset.size() - 1);
            //notifyDataSetChanged();
            notifyItemRemoved(mDataset.size());
        }
    }

}
