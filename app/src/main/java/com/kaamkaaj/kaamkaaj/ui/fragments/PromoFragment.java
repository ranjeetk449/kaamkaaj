package com.kaamkaaj.kaamkaaj.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PromoFragment extends Fragment {
    private static final int[] mImages = new int[]{
            R.mipmap.element_onboarding1,
            R.mipmap.element_onboarding2
    };
    private static final int[] titles = new int[] {
            R.string.promo_title1,
            R.string.promo_title2
    };
    private static final int[] txts = new int[] {
            R.string.promo_text1,
            R.string.promo_text2
    };


    public PromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_promo, container, false);
        ImageView iv = (ImageView) v.findViewById(R.id.fragment_image);
        TextView tv_tile = (TextView) v.findViewById(R.id.tv_title_promo);
        TextView tv_txt = (TextView) v.findViewById(R.id.tv_promo_text);


        int position = getArguments().getInt("position");
        iv.setImageDrawable(getResources().getDrawable(mImages[position]));
        tv_tile.setText(getResources().getString(titles[position]));
        tv_txt.setText(getResources().getString(txts[position]));
        return v;
    }

}
