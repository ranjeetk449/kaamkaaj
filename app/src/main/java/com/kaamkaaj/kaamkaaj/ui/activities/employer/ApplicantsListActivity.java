package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.TabPagerAdapter;
import com.kaamkaaj.kaamkaaj.ui.fragments.JobApplicantsSuccessfulFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class ApplicantsListActivity extends NewBaseActivity {
    @BindView(R.id.top_bar)
    Toolbar topBar;
    @BindView(R.id.sliding_tabs)
    TabLayout slidingTabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    private TabPagerAdapter mPagerAdapter;

    String mClientUniqueId;
    String jobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicants);
        ButterKnife.bind(this);

        setSupportActionBar(topBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // int num = getIntent().getIntExtra("numberApplicants",0);
       // getSupportActionBar().setTitle(num + " " + getResources().getString(R.string.applicants_text));

        // getSupportActionBar().setTitle(getIntent().getStringExtra("numberApplicants") + getResources().getString(R.string.applicants_text));
        jobId = getIntent().getStringExtra("id");
      String  num = getIntent().getStringExtra("numberApplicants");

        System.out.println("responseFetchDetailsjobId" + jobId+"noapplicant"+num);

        mPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
        mPagerAdapter.addFragment(getPending(), getResources().getString(R.string.pending_text));
        mPagerAdapter.addFragment(getShortlisted(), getResources().getString(R.string.shortlisted_text));
        mPagerAdapter.addFragment(getRejected(), getResources().getString(R.string.rejected_text));
        viewpager.setAdapter(mPagerAdapter);

        slidingTabs.setupWithViewPager(viewpager);

    }


    public Fragment getShortlisted() {
        Fragment fragment;
        fragment = new JobApplicantsSuccessfulFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "shortlisted");
        bundle.putString("typeNum", "1");

        bundle.putString("id", jobId);

        fragment.setArguments(bundle);
        return fragment;
    }
    public Fragment getPending() {
        Fragment fragment;
        fragment = new JobApplicantsSuccessfulFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "pending");
        bundle.putString("typeNum", "0");

        bundle.putString("id", jobId);

        fragment.setArguments(bundle);
        return fragment;
    }
    public Fragment getRejected() {

        Fragment fragment;
        fragment = new JobApplicantsSuccessfulFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "rejected");
        bundle.putString("typeNum", "2");

        bundle.putString("id", jobId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
