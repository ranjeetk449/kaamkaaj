package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.claudiodegio.msv.MaterialSearchView;
import com.claudiodegio.msv.OnSearchViewListener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.SelectLanguageActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity2;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.HelpActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.LoginActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.UserProfileActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.DrawerListAdapter;
import com.kaamkaaj.kaamkaaj.ui.adapters.JobCategoryBlockAdapter;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.kaamkaaj.kaamkaaj.utils.models.JobCategory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends NewBaseActivity implements OnSearchViewListener {
    public ArrayList<String> drawer_icons;



    ArrayList<String> navigation_items;
    DrawerListAdapter drawerListAdapter;

    @BindView(R.id.lv_drawer)
    ListView lv_drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.ll_profile)
    LinearLayout ll_profile;
    @BindView(R.id.sv)
    MaterialSearchView materialSearchView;
    @BindView(R.id.tv_name)
            TextView tv_name;
    @BindView(R.id.tv_email)
            TextView tv_email;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    final String TAG= "r3r :" ;

    ArrayList<JobCategory> jobCategories = new ArrayList<JobCategory>();;
    ArrayList<JobCategory> jobCategoriesNew = new ArrayList<JobCategory>();;

    JobCategoryBlockAdapter mAdapter;
    Context mContext;
    ActionBarDrawerToggle toggle;

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    TinyDB tinyDB;


    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    static int flag=0;
    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.job_categories_text);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(this);

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
          //  doMySearch(query);
        }
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 2);
        mAdapter = new JobCategoryBlockAdapter(mContext, jobCategoriesNew);
        recyclerView.setLayoutManager(lLayout);
            recyclerView.setAdapter(mAdapter);
        materialSearchView.setOnSearchViewListener((OnSearchViewListener) this);
        init();
        initLocation();
        addCategoryData();
        SetDrawer();
        setProfilePic();
        ArrayList<String> ids = new ArrayList<>();
        tinyDB.putListString(AppConstants.APPLIED_JOBS,ids);
        fab.setVisibility(View.VISIBLE);
        /*tv_name.setText(tinyDB.getString("username"));
        tv_email.setText(tinyDB.getString("mobile"));*/
    }

    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {
            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();


        } else {
            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                    if(flag==0) {
                        EnableRuntimePermission();
                        flag++;
                    }
                    // GPSTracker getLoc=new GPSTracker(this);

                    //Location lac=getLoc.getLocation();
                    // double lat=lac.getLatitude();
                    // double lon=lac.getLongitude();
                    // if(lat==0.00){
                    // initLocation();
                    //}
                    // System.out.println("latlong"+lat+lon);

                    //     Toast.makeText(this, (int) lat, Toast.LENGTH_LONG).show();
                } else {
                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                       // Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        //  updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                              //  Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        // updateLocationUI();
                    }
                });
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        System.out.println("addresssNewOne");
        //startLocationUpdates();
        EnableRuntimePermission();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                // mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                System.out.println("addresssNewTwo");


                updateLocationUI();
            }
        };

        //  mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }



    private void updateLocationUI() {
        if (mCurrentLocation != null) {


            // location last updated time
            // txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);


            Geocoder geocoder =
                    new Geocoder(getApplication(), Locale.getDefault());
            // Get the current location from the input parameter list
            //Location loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(), 1);
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */
                System.out.println("addresssNew" + address.getAddressLine(0));

               /* et_location_val.setText(
                        "Lat: " + mCurrentLocation.getLatitude() + ", " +
                                "Lng: " + mCurrentLocation.getLongitude()
                );*/
                //  et_location_val.setText(address.getAddressLine(0));
                // giving a blink animation on TextView
                //  et_location_val.setAlpha(0);
                // et_location_val.animate().alpha(1).setDuration(300);
                String addressStr=address.getAddressLine(0);
                System.out.println("addaddressStr" + addressStr);
                if(addressStr.length()>0) {
                    mFusedLocationClient
                            .removeLocationUpdates(mLocationCallback)
                            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                                    // toggleButtons();
                                }
                            });
                }

                //System.out.println("addresss"+address.getLocality()+address.getSubLocality()+address.getPostalCode()+address.getCountryName()+address.getFeatureName()+address.getThoroughfare()+address.getSubAdminArea()+address.getAdminArea());
            } catch (IOException e1) {
                Log.e("LocationSampleActivity",
                        "IO Exception in getFromLocation()");
                e1.printStackTrace();
                //return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(mCurrentLocation.getLatitude()) +
                        " , " +
                        Double.toString(mCurrentLocation.getLongitude()) +
                        " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                // return errorString;
            }
            // If the reverse geocode returned an address
           /* if (addresses.size() > 0) {
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */

            // System.out.println("addresss"+address.getLocality());

            //return address.getLocality();
            //  } else {
            // return "No address found";
            //  }


        }

        // toggleButtons();
    }
    private void init() {


        navigation_items = new ArrayList<>();
        drawer_icons = new ArrayList<>();
        if (SharedPrefUtils.getAppState(MainActivity.this) == AppConstants.STATE_REGISTERED) {
            navigation_items.add(getResources().getString(R.string.my_profile_text));
            drawer_icons.add("ic_myprofile");

        }
//adding menu items for naviations
        navigation_items.add(getResources().getString(R.string.search_jobs_text));
        navigation_items.add(getResources().getString(R.string.help_text));
    //    navigation_items.add(getResources().getString(R.string.change_number_text));
  //      navigation_items.add(getResources().getString(R.string.logout_text));

        navigation_items.add(getResources().getString(R.string.change_language_text));

        navigation_items.add(getResources().getString(R.string.switch_to_employer));

        drawer_icons.add("ic_search");
        drawer_icons.add("ic_help");
    //    drawer_icons.add("ic_change_no");
     //   drawer_icons.add("ic_logout");
        drawer_icons.add("ic_changelang");
        drawer_icons.add("ic_switch");



    }

    public void addCategoryData() {
        jobCategoriesNew.clear();
        jobCategories.clear();
        jobCategories.add(new JobCategory("deliveryboy", getResources().getString(R.string.name_delivery_boy)));
        jobCategories.add(new JobCategory("electrician",getResources().getString(R.string.name_electrician)));
        jobCategories.add(new JobCategory("driver", getResources().getString(R.string.name_driver)));
        jobCategories.add(new JobCategory("plumber",getResources().getString(R.string.name_plumber)));
        jobCategories.add(new JobCategory("tailor", getResources().getString(R.string.name_tailor)));
        jobCategories.add(new JobCategory("helper", getResources().getString(R.string.name_helper)));
        jobCategories.add(new JobCategory("labour",getResources().getString(R.string.name_labour)));
        jobCategories.add(new JobCategory("securityguard", getResources().getString(R.string.name_security_guard)));
        jobCategories.add(new JobCategory("cook",getResources().getString(R.string.name_cook)));
        jobCategories.add(new JobCategory("customer_support_executive",getResources().getString(R.string.customer_support_executive_text)));
        jobCategories.add(new JobCategory("human_resouces",getResources().getString(R.string.human_resources_text)));
        jobCategories.add(new JobCategory("store_executive",getResources().getString(R.string.store_executive_text)));

        jobCategories.add(new JobCategory("officeboy", getResources().getString(R.string.name_office_boy)));


        jobCategories.add(new JobCategory("others", getResources().getString(R.string.name_other)));

        for (int i = 0; i < jobCategories.size();i++) {
            jobCategoriesNew.add(jobCategories.get(i));
        }

     //    Toast.makeText(this,"TAJGOS : " + jobCategoriesNew.size(),Toast.LENGTH_LONG).show();
        mAdapter.notifyDataSetChanged();
    }

    private void SetDrawer() {

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(MainActivity.this, navigation_items, drawer_icons);
        lv_drawer.setAdapter(drawerListAdapter);

        lv_drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.my_profile_text))) {

                Intent i = new Intent(mContext, UserProfileActivity.class);
                mContext.startActivity(i);
               // tv_selected_navigation.setText("Selected Call");

            } else if (navigation_items.get(position).equalsIgnoreCase(getString(R.string.change_language_text))) {
                Intent i = new Intent(mContext, SelectLanguageActivity.class);
                i.putExtra(AppConstants.FROM_STRING,"main");
                mContext.startActivity(i);
              //  tv_selected_navigation.setText("Selected Favorite");

            } else  if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.search_jobs_text))) {
                Intent i = new Intent(mContext, JobFilterActivity.class);
                mContext.startActivity(i);
            } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.help_text))) {
                Intent i = new Intent(mContext, HelpActivity.class);
                mContext.startActivity(i);
            } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.switch_to_employer))) {
                SharedPrefUtils.clearSharedPreference(MainActivity.this);
                tinyDB.putString("username","");
                tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                Intent intent = new Intent(MainActivity.this, EmployerTypeActivity2.class);
                MainActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(intent);
            }
            }
        });
    }

    public void setProfilePic() {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/kaamkaajjobseeker");
        myDir.mkdirs();

        String fname = "profile_pic1.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) {
            try {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                String token = SharedPrefUtils.getApiKeyEmployee(this);
                System.out.println("resptoken"+token);
                if(token!=null)

                    profile_pic.setImageBitmap(b);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

    }

    @OnClick(R.id.fab)
        public void tempFunction(View view) {
            switch (view.getId()) {
                case R.id.fab:
                    Intent vi = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(vi);
            }
        }

/*    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }*/

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            //drawer is open
        } else {
            deleteJob();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        return true;*/

        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearch = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query .equals("")) {
                    addCategoryData();
                } else {
                    jobCategoriesNew.clear();
                    for (int i=0; i < jobCategories.size(); i++) {
                        if (jobCategories.get(i).getImg_name().toLowerCase().contains(query.toLowerCase())) {
                            jobCategoriesNew.add(jobCategories.get(i));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText .equals("")) {
                    addCategoryData();
                } else {
                    jobCategoriesNew.clear();
                    for (int i=0; i < jobCategories.size(); i++) {
                        if (jobCategories.get(i).getImg_name().toLowerCase().contains(newText.toLowerCase())) {
                            jobCategoriesNew.add(jobCategories.get(i));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void doSearch() {
//
    }

    @Override
    public void onSearchViewShown() {

    }

    @Override
    public void onSearchViewClosed() {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // handle text submit and then return true
        return false;
    }

    public void deleteJob() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.exit_app_text));
        alertDialogBuilder.setMessage(mContext.getResources().getString(R.string.sure_exit_text));
        alertDialogBuilder.setPositiveButton(mContext.getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        alertDialogBuilder.setNegativeButton(mContext.getResources().getString(R.string.no_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            }
        });
        alertDialog.show();
    }
    @Override
    public void onQueryTextChange(String newText) {

    }
}