package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.SelectLanguageActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.PromoActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akshaysingh on 4/12/18.
 */

public class EmployerTypeActivity extends AppCompatActivity {

    @BindView(R.id.ll_employer)
    LinearLayout ll_employer;
    @BindView(R.id.ll_seeker)
    LinearLayout ll_seeker;
    @BindView(R.id.iv_jobseeker)
    ImageView iv_jobseeker;
    @BindView(R.id.iv_employer)
            ImageView iv_employer;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_selection_activity);
        ButterKnife.bind(this);
        tinyDB = new TinyDB(this);
    }

    @OnClick({R.id.ll_employer, R.id.ll_seeker, R.id.iv_jobseeker, R.id.iv_employer})
    public void gotoActivity(View view) {
        switch (view.getId()) {
            case R.id.ll_employer:
                callEmployer();
                break;
            case R.id.ll_seeker:
                callJobseeker();
                break;
            case R.id.iv_jobseeker:
                callJobseeker();
                break;
            case R.id.iv_employer:
                callEmployer();
        }
    }

    public void callJobseeker() {
        Intent intent;
        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_JOBSEEKER);
        intent = new Intent(this, MainActivity.class);
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        startActivity(intent);
    }

    public void callEmployer() {
        Intent intent;
        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
        intent = new Intent(this, EmployerTypeActivity2.class);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPrefUtils.setPromoState(this, false);
        // this.finish();
        Intent refresh = new Intent(this, PromoActivity.class);
        //refresh.putExtra(AppConstants.FROM_STRING,"splash");

        this.overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        startActivity(refresh);
        finish();
    }

}
