package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.share.Share;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.JobsListActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.models.JobCategory;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 3/26/18.
 */

public class JobCategoryBlockAdapter extends RecyclerView.Adapter<JobCategoryBlockAdapter.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "CollectionListAdapter";
    private ArrayList<JobCategory> mDataset;

    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public JobCategoryBlockAdapter(Context mContext, ArrayList<JobCategory> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        mHandler = new Handler() {
            @Override
            public void close() {

            }

            @Override
            public void flush() {

            }

            @Override
            public void publish(LogRecord record) {

            }
        };
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent i = new Intent(mContext, JobsListActivity.class);
        i.putExtra("job_info",mDataset.get(position) );
        mContext.startActivity(i);


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;

        @BindView(R.id.iv_job_icon)
        ImageView iv_job_icon;
        @BindView(R.id.tv_job_title)
        TextView tv_job_title;
        @BindView(R.id.ll_each_block)
        LinearLayout ll_each_block;


        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {

            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_job_categoy_block, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        System.out.println("!!!!!!!!!!!!!!   :   " + mDataset.get(position).getDisp_text()
        );

        int id = mContext.getResources().getIdentifier("com.kaamkaaj.kaamkaaj:mipmap/" + mDataset.get(position).getImg_name(), null, null);

        holder.iv_job_icon.setImageResource(id);
        holder.tv_job_title.setText(mDataset.get(position).getDisp_text());

        holder.iv_job_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefUtils.clearFilters(mContext);
                    SharedPrefUtils.setFilterSkills(mContext,mDataset.get(position).getDisp_text() + ",");

                Intent i = new Intent(mContext, JobsListActivity.class);
                i.putExtra("job_info",mDataset.get(position) );
                mContext.startActivity(i);
            }
        });

    }


    public void addItem(JobCategory dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}