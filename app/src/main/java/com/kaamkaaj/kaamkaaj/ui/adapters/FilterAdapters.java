package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.models.FilterObject;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 4/24/18.
 */

public class FilterAdapters  extends RecyclerView.Adapter<FilterAdapters.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "CollectionListAdapter";
    private ArrayList<FilterObject> mDataset;

    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;
    String types = "";

    public FilterAdapters(Context mContext, ArrayList<FilterObject> myDataset, String types) {
        this.types = types;
        this.mContext = mContext;
        mDataset = myDataset;
        mHandler = new Handler() {
            @Override
            public void close() {

            }

            @Override
            public void flush() {

            }

            @Override
            public void publish(LogRecord record) {

            }
        };
    }

    @Override
    public void onItemClick(int position, View view) {
       /* Intent i = new Intent(mContext, JobsListActivity.class);
        i.putExtra("job_info",mDataset.get(position) );
        mContext.startActivity(i);*/


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;

        @BindView(R.id.each_filter)
        LinearLayout each_filter;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.iv_check)
        ImageView iv_check;


        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {

            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_filter_item, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        holder.tv_title.setText(mDataset.get(position).getElement());
        if (mDataset.get(position).isChecked()) {
            Resources res = mContext.getResources();
            String mDrawableName = "ic_check_blue";
            int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
            Drawable drawable = res.getDrawable(resID );
            holder.iv_check.setImageDrawable(drawable );
        } else {
            Resources res = mContext.getResources();
            String mDrawableName = "ic_uncheck";
            int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
            Drawable drawable = res.getDrawable(resID );
            holder.iv_check.setImageDrawable(drawable );
        }
        System.out.println("!!!!!!!!!!!!!!   :   " + mDataset.get(position).getElement());
        holder.each_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDataset.get(position).isChecked()) {
                    mDataset.get(position).setChecked(false);
                    Resources res = mContext.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    holder.iv_check.setImageDrawable(drawable );
                    removeFilters(types, mDataset.get(position).getElement());

                } else {
                    mDataset.get(position).setChecked(true);
                    Resources res = mContext.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    holder.iv_check.setImageDrawable(drawable );
                    addFilters(types, mDataset.get(position).getElement());
                }
            }
        });

    }

    public void addFilters(String typeFilter, String valss) {
        String skills = SharedPrefUtils.getFilterSkills(mContext);
        System.out.println("AFAGGGAGG : " + skills);

        if (typeFilter .equalsIgnoreCase("skills")) {
            String skillsFilter = SharedPrefUtils.getFilterSkills(mContext);
            if (skillsFilter.contains(valss)) {

            } else {
                SharedPrefUtils.setFilterSkills(mContext,skillsFilter + valss+",");
            }
        }
        if (typeFilter.equalsIgnoreCase("location")) {
            String locationFilter = SharedPrefUtils.getFilterLocation(mContext);
            if (locationFilter.contains(valss)) {

            } else {
                SharedPrefUtils.setFilterLocation(mContext,locationFilter + valss+ ",");
            }
        }
        if (typeFilter.equalsIgnoreCase("education")) {
            String educationFilter = SharedPrefUtils.getFilterEducation(mContext);
            if (educationFilter.contains(valss)) {

            } else {
                SharedPrefUtils.setFilterEducation(mContext,educationFilter + valss+",");
            }
        }
    }
    public void removeFilters(String typeFilter, String valss) {
        if (typeFilter .equalsIgnoreCase("skills")) {
            String skillsFilter = SharedPrefUtils.getFilterSkills(mContext);
            String skills = SharedPrefUtils.getFilterSkills(mContext);
            System.out.println("AFAGGGAGG : " + skills);
            if (skillsFilter.contains(valss)) {
                skillsFilter = skillsFilter.replaceAll(valss +",", "");
                SharedPrefUtils.setFilterSkills(mContext,skillsFilter);
                String skills11 = SharedPrefUtils.getFilterSkills(mContext);
                System.out.println("AFAGGGAGG11 : " + skills11);
            } else {

            }
        }
        if (typeFilter.equalsIgnoreCase("location")) {
            String locationFilter = SharedPrefUtils.getFilterLocation(mContext);
            if (locationFilter.contains(valss)) {
                locationFilter = locationFilter.replaceAll(valss +",", "");
                SharedPrefUtils.setFilterLocation(mContext,locationFilter);
            } else {
            }
        }
        if (typeFilter.equalsIgnoreCase("education")) {
            String educationFilter = SharedPrefUtils.getFilterEducation(mContext);
            if (educationFilter.contains(valss)) {

                educationFilter = educationFilter.replaceAll(valss +",", "");
                SharedPrefUtils.setFilterEducation(mContext,educationFilter);
            } else {
            }
        }
    }


    public void addItem(FilterObject dataObj, int index) {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}