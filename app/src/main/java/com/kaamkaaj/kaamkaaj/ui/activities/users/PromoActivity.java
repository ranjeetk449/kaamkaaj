package com.kaamkaaj.kaamkaaj.ui.activities.users;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.SelectLanguageActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.fragments.PromoFragment;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.widgets.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 4/6/18.
 */

public class PromoActivity  extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private static final int NUM_PAGES = 2;
    private static final String TAG = "PromoActivity";
    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.done_promo)
    Button donePromo;
    @BindView(R.id.ib_next)
    Button ibNext;
    @BindView(R.id.btn_skip)
    Button btn_skip;

    private ScreenSlidePagerAdapter mPagerAdapter;
    private int mCurrentPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        ButterKnife.bind(this);

        mPager.setOnPageChangeListener(this);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);


       // Utils.setCustomFontsForButton(this, donePromo, AppConstants.FONTS_LATO_REGULAR_TTF);

        donePromo.setOnClickListener(this);
        ibNext.setOnClickListener(this);
        btn_skip.setOnClickListener(this);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.done_promo:
                handleSkipClick();
                break;
            case R.id.ib_next:
                handleNextPage();
                break;
            case R.id.btn_skip:
                goToNextPage();
                break;
        }

    }

    public void goToNextPage() {
        SharedPrefUtils.setPromoState(this, true);
        Intent i = new Intent(this, EmployerTypeActivity.class);
        startActivity(i);
        this.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }

    public void handleSkipClick() {
        SharedPrefUtils.setPromoState(this, true);
        Intent i = new Intent(this, EmployerTypeActivity.class);
        startActivity(i);
        this.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }


    private void handleNextPage() {
        Log.d(TAG, "next: position:" + mCurrentPosition);
        if (mCurrentPosition == (NUM_PAGES - 1)) {
            SharedPrefUtils.setPromoState(this, true);

            Log.d(TAG, "Starting login activity");

            Intent i = new Intent(this, EmployerTypeActivity.class);
            startActivity(i);
            this.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
            finish();
            return;
        }

        mCurrentPosition++;
        mPager.setCurrentItem(mCurrentPosition);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //no-op
    }

    @Override
    public void onPageSelected(int position) {
        int lastPagePosition = mPagerAdapter.getCount() - 1;
        indicator.setCurrentItem(position);
        ibNext.setVisibility(position == lastPagePosition ? View.GONE : View.VISIBLE);
        donePromo.setVisibility(position == lastPagePosition ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //no-op
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Bundle args = new Bundle();
            args.putInt("position", position);
            Fragment f = new PromoFragment();
            f.setArguments(args);
            return f;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPrefUtils.setPromoState(this, false);
       // this.finish();
        Intent refresh = new Intent(this, SelectLanguageActivity.class);
        refresh.putExtra(AppConstants.FROM_STRING,"splash");
        this.overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        startActivity(refresh);
        finish();

    }
}

