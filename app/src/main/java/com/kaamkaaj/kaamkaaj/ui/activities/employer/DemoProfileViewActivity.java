package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.DeleteSavedCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.SaveForLaterCandidateRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.CandidateSearch;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobApplicant;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

/**
 * Created by akshaysingh on 5/16/18.
 */

public class DemoProfileViewActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar topBar;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_gender_dob_experience)
    TextView tv_gender_dob_experience;
    @BindView(R.id.tv_father_name)
    TextView tv_father_name;
    @BindView(R.id.tv_permanent_address)
    TextView tv_permanent_address;
    @BindView(R.id.tv_skills)
    TextView tv_skills;
    @BindView(R.id.tv_salary)
    TextView tv_salary;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.tv_phone)
            TextView tv_phone;

    JobApplicant jobApplicant;
    CandidateSearch candidateSearch;
    String fromClass = "";
    boolean candidateSaved = false;
    TinyDB tinyDB;
    private Menu menu;
    String id = "0";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_profile_view_jobseeker);
        ButterKnife.bind(this);
        setSupportActionBar(topBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.jobseeker_profile_text));
        fromClass = getIntent().getStringExtra("from_class");
      //  jobApplicant  = getIntent().getParcelableExtra("job_info");
        //id = Integer.parseInt(jobApplicant.getId());
      //  id = jobApplicant.getId();
       // System.out.println("@elseid : " + id);
        if (fromClass .equals("search")) {
            candidateSearch = getIntent().getParcelableExtra("job_info");
            //id = Integer.parseInt(candidateSearch.getId());
            id = candidateSearch.getId();
            System.out.println("@ifid : " + id);
        } else {
            jobApplicant  = getIntent().getParcelableExtra("job_info");
            //id = Integer.parseInt(jobApplicant.getId());
            id = jobApplicant.getId();
            System.out.println("@elseid : " + id);
        }
        tinyDB = new TinyDB(this);
        fillData();
        checkCandidateSaved();
    }

    public void checkCandidateSaved() {

    }
    public void fillData() {
        if (fromClass .equals("search")) {
            tv_name.setText(candidateSearch.getFull_name());
            tv_phone.setText(candidateSearch.getMobile_no());
            tv_skills.setText(candidateSearch.getSkill());
            tv_salary.setText(candidateSearch.getSalary());
            tv_father_name.setText(candidateSearch.getFathers_name());
            tv_permanent_address.setText(candidateSearch.getPermanent_address());
            tv_location.setText(candidateSearch.getCurrent_address());
            tv_gender_dob_experience.setText(candidateSearch.getGender() + " | Born " + candidateSearch.getDob() + " | Experience " + candidateSearch.getExperience() + " years" );

            // tv_gender_dob_experience.setText(candidateSearch.getGender() + " | Born " + candidateSearch.getDob() + " | Experience " + candidateSearch.getExperience() + " years" );
        } else {
            tv_name.setText(jobApplicant.getFull_name());
            tv_phone.setText(jobApplicant.getMobile_no());
            tv_gender_dob_experience.setText(jobApplicant.getGender() + " | Born " + jobApplicant.getDob() + " | Experience " + jobApplicant.getExperience() + " years" );
            tv_location.setText(jobApplicant.getCurrent_address());
            tv_father_name.setText(jobApplicant.getFathers_name());
            tv_permanent_address.setText(jobApplicant.getPermanent_address());
            tv_salary.setText(jobApplicant.getSalary());
            tv_skills.setText(jobApplicant.getSkill());
        }


     //   tv_father_name.setText(candidateSearch.get);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
        getMenuInflater().inflate(R.menu.menu_saved, menu);
        this.menu = menu;
        System.out.println("AFAFAFFAGA    L : " + list.size() );
        System.out.println("AFAFAFFAGA    L : " + list );

        if (list.size() == 0) {
            candidateSaved = false;
        } else {
            System.out.println("AFAFAFFAGA    L : " + list.size() );
            boolean ava = false;
            for (int i=0;i < list.size();i++) {
                if (list.get(i) .equalsIgnoreCase(String.valueOf(id))) {
                    ava = true;
                }
            }
            System.out.println("respAFAFAFFAGA    L1 : " + String.valueOf(id)  + "  :  " + list.indexOf(id) );

            if (ava) {
                System.out.println("respAFAFAFFAGA 12122   L1 : " + id );

                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_saved));
                candidateSaved = true;

            } else {
                candidateSaved = false;
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_saved_line));
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveCandidate();
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }









    public void saveCandidate() {
       /* if (candidateSaved) {
            System.out.println("respcandidateSaved" +  candidateSaved);
            DeleteSavedCandidateRequest request = new DeleteSavedCandidateRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(this);
            request.id = String.valueOf(id);

            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.deleteCandidate(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                    candidateSaved = false;
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved_line));
                    ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                    list.remove(list.indexOf(String.valueOf(id)));
                    tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST, list);
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(DemoProfileViewActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    Toast.makeText(DemoProfileViewActivity.this, response.status.userMessage, Toast.LENGTH_LONG).show();
                }

            });

        }*/





        if(candidateSaved){


            DeleteSavedCandidateRequest request = new DeleteSavedCandidateRequest();
                request.token = SharedPrefUtils.getApiKey(this);
                request.id = String.valueOf(id);
                System.out.println("respSAVECANDDELETE request.idI" +  request.id);
                RequestParams params = new RequestParams();
                params.put("token", request.token);
                params.put("id",  request.id);
                String url = RequestUrl.API_URL + RequestUrl.DELETE_CANDIDATE;
                AsyncHttpClient client = new AsyncHttpClient();
                client.post(url, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        showProgressBar();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        super.onSuccess(statusCode, headers, response);
                        System.out.println("respdELETECANDI" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                        try {
                            hideProgressBar();
                            JSONObject status = response.getJSONObject("status");
                            String respsuccess = status.getString("response");
                            String message = status.getString("message");
                            System.out.println("respdELETEsuccessTRUE : " + respsuccess);
                            if (respsuccess.equals("success")) {
                                //  System.out.println("_____ : " + response.data);
                                /*candidateSaved = true;
                                menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved));
                                ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                                String af = String.valueOf(id);
                                list.add(af);
                                tinyDB.remove(AppConstants.SAVE_FOR_LATER_LIST);
                                tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST, list);
                                Toast.makeText(DemoProfileViewActivity.this, message,Toast.LENGTH_LONG).show();*/
                                candidateSaved = false;
                                menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved_line));
                                ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                                list.remove(list.indexOf(String.valueOf(id)));
                                tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST, list);
                                Toast.makeText(DemoProfileViewActivity.this, message,Toast.LENGTH_LONG).show();




                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable
                            throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        CommonMethod.showAlert("Unable to connect the server,please try again", DemoProfileViewActivity.this);
                        hideProgressBar();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        hideProgressBar();
                    }
                });








        }










      else{
        SaveForLaterCandidateRequest request = new SaveForLaterCandidateRequest();
        request.token = SharedPrefUtils.getApiKey(this);
        request.id = String.valueOf(id);
            System.out.println("respSAVECAND request.idI" +  request.id);
        RequestParams params = new RequestParams();
        params.put("token", request.token);
        params.put("id",  request.id);
        String url = RequestUrl.API_URL + RequestUrl.SAVE_CANDIDATE;
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                super.onSuccess(statusCode, headers, response);
                System.out.println("respSAVECANDI" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                try {
                    hideProgressBar();
                    JSONObject status = response.getJSONObject("status");
                    String respsuccess = status.getString("response");
                    String message = status.getString("message");
                    System.out.println("respsuccessTRUE : " + respsuccess);
                    if (respsuccess.equals("success")) {
                        //  System.out.println("_____ : " + response.data);
                        candidateSaved = true;
                        menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved));
                        ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                        String af = String.valueOf(id);
                        list.add(af);
                        tinyDB.remove(AppConstants.SAVE_FOR_LATER_LIST);
                        tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST, list);
                        Toast.makeText(DemoProfileViewActivity.this, message,Toast.LENGTH_LONG).show();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable
                    throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CommonMethod.showAlert("Unable to connect the server,please try again", DemoProfileViewActivity.this);
                hideProgressBar();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgressBar();
            }
        });
    }


    }






/*
    public void saveCandidate() {
        if (candidateSaved) {
            DeleteSavedCandidateRequest request = new DeleteSavedCandidateRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.token = SharedPrefUtils.getApiKey(this);
            request.id = String.valueOf(id);

            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.deleteCandidate(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                    candidateSaved = false;
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved_line));
                    ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                    list.remove(list.indexOf(String.valueOf(id)));
                    tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST,list);
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                    Toast.makeText(DemoProfileViewActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                    Toast.makeText(DemoProfileViewActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }

            });

        } else {

            SaveForLaterCandidateRequest request = new SaveForLaterCandidateRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();


            request.token = SharedPrefUtils.getApiKey(this);
            request.id = String.valueOf(id);

            Call<BaseResponse> apiResponseCall;
            apiResponseCall =
                    service.saveCandidate(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                        showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                    candidateSaved = true;
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(DemoProfileViewActivity.this, R.drawable.ic_saved));
                    ArrayList<String> list = tinyDB.getListString(AppConstants.SAVE_FOR_LATER_LIST);
                    String af = String.valueOf(id);
                    list.add(af);
                    tinyDB.remove(AppConstants.SAVE_FOR_LATER_LIST);
                    tinyDB.putListString(AppConstants.SAVE_FOR_LATER_LIST,list);
                }

                @Override
                public void onError(Call<BaseResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                    Toast.makeText(DemoProfileViewActivity.this, getResources().getString(R.string.error_msg), Toast.LENGTH_LONG).show();


                }

                @Override
                public void onCustomError(Call<BaseResponse> call, BaseResponse response) {

                        Toast.makeText(DemoProfileViewActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();

                }

            });

        }
    }
*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
