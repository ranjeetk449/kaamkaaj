package com.kaamkaaj.kaamkaaj.ui.activities.users;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar.CrystalSeekbar;
import com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar.OnSeekbarChangeListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by akshaysingh on 5/29/18.
 */

public class SetRadiusActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.tv_min_value)
    TextView tv_min_value;
    @BindView(R.id.tv_max_value)
    TextView tv_max_value;
    @BindView(R.id.rangeSeekbar3)
    CrystalSeekbar rangeSeekbar3;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;
    @BindView(R.id.tv_distance)
    TextView tv_distance;
    int distanceloc = 20 * 1000;
    static final LatLng HAMBURG = new LatLng(53.558, 9.927);
    static final LatLng KIEL = new LatLng(53.551, 9.993);
    private SupportMapFragment mapFragment;
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
    };
    public CircleOptions circleOptions;
    public Circle circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_radius);
        ButterKnife.bind(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        circleOptions = new CircleOptions()
                .center(new LatLng(28.644800, 77.216721))
                .radius(distanceloc)
                .strokeWidth(0f)
                .fillColor(0x550000FF);
        top_bar.setTitle(this.getResources().getString(R.string.job_description_text));
        setSupportActionBar(top_bar);
        top_bar.setTitle(R.string.set_job_radius);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        /*rangeSeekbar3.setMaxValue(AppConstants.MIN_RANGE);
        rangeSeekbar3.setMaxValue(AppConstants.MAX_RANGE);
        rangeSeekbar3.setMinStartValue(AppConstants.MIN_RANGE);*/
       // rangeSeekbar3.setMinStartValue(40);
        int vals = SharedPrefUtils.getRadius(this);

        float perc = ((float) (vals - AppConstants.MIN_RANGE) / (float) (AppConstants.MAX_RANGE - AppConstants.MIN_RANGE)) * 100;

        rangeSeekbar3.setMinValueFromData(perc);


        tv_min_value.setText(getResources().getString(R.string.min_text) + " : "  +AppConstants.MIN_RANGE + " " + getResources().getString(R.string.in_km_text) );
        tv_max_value.setText(getResources().getString(R.string.max_text) + " : "  +AppConstants.MAX_RANGE + " " + getResources().getString(R.string.in_km_text) );
        tv_distance.setText(getResources().getString(R.string.distance_text) + " " + getResources().getString(R.string.in_km_text));
//        Utils.changeToolbarTitleFont(this, toolbar);
        rangeSeekbar3.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                distanceloc = Integer.parseInt(rangeSeekbar3.getSelectedMinValue().toString()) * 1000;
                System.out.println("THIS IS NEW RADIUS: " + distanceloc);
                circleOptions.radius(distanceloc);
                tv_distance.setText(getResources().getString(R.string.distance_text) + " " + rangeSeekbar3.getSelectedMinValue() + " " + getResources().getString(R.string.in_km_text));
            }
        });
        EnableRuntimePermission();

    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {

            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);

        }


    }


    @OnClick({R.id.ll_action_button})
    public void clickFunction(View view) {
        switch (view.getId()) {
            case R.id.ll_action_button:
                int vals = Integer.parseInt(String.valueOf(rangeSeekbar3.getSelectedMinValue()));
                SharedPrefUtils.setRadius(SetRadiusActivity.this, vals);
                super.onBackPressed();
                this.finish();
                this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(28.644800, 77.216721))
                .title("LinkedIn")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


     //   googleMap.addCircle(circleOptions);

        /*  googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4629101,-122.2449094))
                .title("Facebook")
                .snippet("Facebook HQ: Menlo Park"));

            googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.3092293, -122.1136845))
                .title("Apple"));*/

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(28.644800, 77.216721), 10));
    }
}
