package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.SubskillsAdapters;
import com.kaamkaaj.kaamkaaj.utils.models.JobCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 4/30/18.
 */

public class JobSubCategoryListActivity extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    ArrayList<String> subcategories = new ArrayList<String>();;

    SubskillsAdapters mAdapter;
    private boolean mInFlight;
    Context mContext;
    int mCurrentPage = 1;
    JobCategory jobCategory = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_listing);
        mContext = this;
        ButterKnife.bind(this);
        jobCategory = getIntent().getParcelableExtra("job_info");
        if (jobCategory != null) {
            toolbar.setTitle(jobCategory.getDisp_text());
        }
       // toolbar.setTitle(R.string.recommended_jobs_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 2);
        mAdapter = new SubskillsAdapters(mContext, subcategories, jobCategory);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);
        fab.setVisibility(View.GONE);
        addsubcategory();

        /*recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public void onLoadMore(int currentPage, int count) {
                //     Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                    mAdapter.addProgressBarView();
                    fetchJobs(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });*/

        //fetchJobs(mCurrentPage, true);
    }

    public void addsubcategory() {
        for (int i = 0 ; i < 15; i++ ) {
            subcategories.add(jobCategory.getDisp_text() + " subcat "  + i);

        }
        mAdapter.notifyDataSetChanged();
    }
/*
    public void fetchJobs(int mPage, final boolean showProgress) {
        JobRequest request = new JobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.randomId = SharedPrefUtils.getAnonymousUserId(this);
        request.pageNo = mPage;
       *//* request.mobileNo = mobileNum;
        request.otp = otp;
*//*
        //request.token = SharedPrefUtils.getApiKey(this);


        Call<JobListResponse> apiResponseCall;
        apiResponseCall =
                service.jobRequest(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<JobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }

            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobListResponse> call, JobListResponse response) {

                //  System.out.println("_____ : " + response.data);
                for (int i =0;i < response.getData().jobs.size(); i++) {
                    response.getData().jobs.get(i).setSelected("no");
                    jobLists.add(response.getData().jobs.get(i));
                }
                mCurrentPage++;
                mAdapter.notifyDataSetChanged();
                mInFlight = false;


                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<JobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<JobListResponse> call, JobListResponse response) {

                Toast.makeText(JobsListActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                mInFlight = false;


            }

        });
    }
   */

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
