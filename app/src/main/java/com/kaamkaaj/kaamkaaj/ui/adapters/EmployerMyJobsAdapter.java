package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.DeleteJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.MyJobList;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.ApplicantsListActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.JobPostActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.SearchCandidateResultActivity;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/16/18.
 */

public class EmployerMyJobsAdapter  extends RecyclerView.Adapter<EmployerMyJobsAdapter.Holder> implements
        OnItemClickListener {

    private static String LOG_TAG = "JobListAdapter";
    private ArrayList<MyJobList> mDataset;
    private ArrayList<Boolean> selected;
    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public EmployerMyJobsAdapter(Context mContext, ArrayList<MyJobList> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        selected = new ArrayList<Boolean>();

        mHandler = new Handler() {
            @Override
            public void close() {

            }

            @Override
            public void flush() {

            }

            @Override
            public void publish(LogRecord record) {

            }
        };
    }

    @Override
    public void onItemClick(int position, View view) {


    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;
        @BindView(R.id.tv_job_title)
        TextView tv_job_title;
        @BindView(R.id.tv_salary)
        TextView tv_salary;
        @BindView(R.id.tv_experience)
        TextView tv_experience;
        @BindView(R.id.tv_location)
        TextView tv_location;
        @BindView(R.id.tv_posted)
        TextView tv_posted;
        @BindView(R.id.tv_num_applicants)
        TextView tv_num_applicants;
        @BindView(R.id.ll_delete_job)
        LinearLayout ll_delete_job;
        @BindView(R.id.ll_edit)
        LinearLayout ll_edit;
        @BindView(R.id.ll_num_applicants)
        LinearLayout ll_num_applicants;
        @BindView(R.id.ll_search)
        LinearLayout ll_search;


        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_employer_job_item, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        holder.tv_job_title.setText(mDataset.get(position).getJobTitle());
        holder.tv_salary.setText(mDataset.get(position).getMinSalary() + " to " + mDataset.get(position).getMaxSalary());
        holder.tv_experience.setText(mDataset.get(position).getMinExp() + " - " + mDataset.get(position).getMaxExp() +    mContext.getResources().getString(R.string.sample_experience));
        holder.tv_posted.setText(mDataset.get(position).getCreated());
        holder.tv_location.setText(mDataset.get(position).getAddress());
        holder.tv_num_applicants.setText(mDataset.get(position).getNumApplicants() + " Applicants");

        holder.ll_delete_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               deleteJob(mDataset.get(position));
            }
        });
        holder.ll_num_applicants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // System.out.println("responseDATASET" + mDataset.get(position).getNumApplicants());
                if(!mDataset.get(position).getNumApplicants().equals("0")) {
                    Intent i = new Intent(mContext, ApplicantsListActivity.class);
                    i.putExtra("id", mDataset.get(position).getId());
                    i.putExtra("numberApplicants", mDataset.get(position).getNumApplicants());
                    mContext.startActivity(i);
                }
            }
        });
        holder.ll_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, JobPostActivity.class);
                i.putExtra("from", "myjobs");
                i.putExtra("data", mDataset.get(position));
                mContext.startActivity(i);
            }
        });
        holder.ll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, SearchCandidateResultActivity.class);
                i.putExtra("skill", mDataset.get(position).getSkill());

                mContext.startActivity(i);
            }
        });
    }

    public void deleteJob(final MyJobList jobList) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.delete_text));
        alertDialogBuilder.setMessage(mContext.getResources().getString(R.string.delete_msg_text));
        alertDialogBuilder.setPositiveButton(mContext.getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteJobApi(jobList);
            }
        });

        alertDialogBuilder.setNegativeButton(mContext.getResources().getString(R.string.no_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            }
        });
        alertDialog.show();
    }

    public void deleteJobApi(MyJobList jobList) {
        DeleteJobRequest request = new DeleteJobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();


        request.token = SharedPrefUtils.getApiKey(mContext);
        request.id = jobList.getId();

        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.deleteJob(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                  //  showProgressBar();
            }

            @Override
            protected void onEndApi() {
                // hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                Intent i = new Intent(mContext, MyJobsActivity.class);
                mContext.startActivity(i);
                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(mContext, mContext.getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
               // mInFlight = false;
            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {

                Toast.makeText(mContext, response.status.userMessage,Toast.LENGTH_LONG).show();
              //  mInFlight = false;
            }
        });
    }

    public void addItem(MyJobList dataObj, int index) {
        mDataset.add(dataObj);
        selected.set(index,false);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}