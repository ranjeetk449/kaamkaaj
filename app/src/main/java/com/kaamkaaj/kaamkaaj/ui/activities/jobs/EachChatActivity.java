package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.DeleteChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.NewChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.ReadChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.chats.SendChatRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.BaseResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.chats.ReadChatResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.MyChatList;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.MyChatAdapter;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by akshaysingh on 6/21/18.
 */

public class EachChatActivity extends NewBaseActivity  implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.et_text)
    EditText et_text;
    @BindView(R.id.ll_send_chat)
    LinearLayout ll_send_chat;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    /* @BindView(R.id.sv)
     NestedScrollView sv;*/
    ArrayList<MyChatList> myChatListArrayList = new ArrayList<MyChatList>();;
    MyChatAdapter mAdapter;
    private boolean mInFlight;
    Context mContext;
    TinyDB tinyDB;
    String first_id = "", last_id ="";
    int mCurrentPage = 1;
    static Handler handler = new Handler();
    Runnable runnable;
    String fromClass = "",toEmployeeName = "",employee_id ="", chat_by = "",employeeMobileNo ="",newstatus="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.chat_text);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new MyChatAdapter(mContext, myChatListArrayList);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);
        fromClass = getIntent().getStringExtra("fromClass");
        toEmployeeName = getIntent().getStringExtra("toEmployeeName");
        employeeMobileNo = getIntent().getStringExtra("employeeMobileNo");
        employee_id = getIntent().getStringExtra("employee_id");
        toolbar.setTitle(toEmployeeName);

        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN) .equals(AppConstants.USER_EMPLOYER)) {
            chat_by = "employer";
        } else {
            chat_by = "jobseeker";
        }
        System.out.println("respchatlist  :  " + fromClass+ toEmployeeName +employee_id);

        recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(-1)) {
                  //  fetchOlderChat();
                    System.out.println("#Q#@R@#R@#R   ON TOP" +
                            "");
                   // addTopChat();
                } else if (!recyclerView.canScrollVertically(1)) {
                 //   onScrolledToBottom();
                }
                if (dy < 0) {
                 //   onScrolledUp(dy);
                } else if (dy > 0) {
                  //  onScrolledDown(dy);
                }
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {

            }
        });
        //addChats();
        runnable = new Runnable() {
            @Override
            public void run() {
               // fetchNewChats();

                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(runnable, 5000);
        fetchChat();
    }

    public void  fetchOlderChat() {
        ReadChatRequest request = new ReadChatRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        //request.chat_by = chat_by;
        System.out.println("QR#V#QRQV : " + chat_by );
        if (chat_by .equalsIgnoreCase("jobseeker")) {
            request.employee_id = employee_id;
        } else {
            request.employee_id = employee_id;
        }
       // request.last_id = first_id;
        request.employer_id = SharedPrefUtils.getApiKey(this);


        Call<ReadChatResponse> apiResponseCall;
        apiResponseCall =
                service.readChat(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<ReadChatResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<ReadChatResponse> call, ReadChatResponse response) {
                if(response.getData().getChats().size()>0) {
                    first_id = response.getData().getChats().get(0).getId();
                    for (int i = 0; i < response.getData().getChats().size(); i++) {
                        MyChatList chatList = new MyChatList();
                        chatList.setText(response.getData().getChats().get(i).getMessage());
                        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_JOBSEEKER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("jobseeker")) {
                            chatList.setFromUser("me");
                        } else if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_EMPLOYER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("employer")) {
                            chatList.setFromUser("me");
                        } else {
                            chatList.setFromUser("you");

                        }
                        myChatListArrayList.add(0, chatList);
                        mAdapter.notifyItemInserted(0);
                    }
                }else{
                    Toast toast = Toast.makeText(EachChatActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onError(Call<ReadChatResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeveolder chat " + t.toString() );
                Toast.makeText(EachChatActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<ReadChatResponse> call, ReadChatResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EachChatActivity.this);
                    tinyDB.putString("username","");
                    handler.removeCallbacks(runnable);
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EachChatActivity.this, EmployerTypeActivity.class);
                    EachChatActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    mInFlight = false;
                }


            }

        });
    }

    public void  fetchChat() {
        ReadChatRequest request = new ReadChatRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        //request.chat_by = chat_by;
        System.out.println("respQR#V#QRQV : " + chat_by );
        if (chat_by .equalsIgnoreCase("jobseeker")) {
            request.employee_id = employee_id;
        } else {
            request.employee_id = employee_id;
        }
        request.employer_id = SharedPrefUtils.getApiKey(this);





        Call<ReadChatResponse> apiResponseCall;
        apiResponseCall =
                service.readChat(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<ReadChatResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<ReadChatResponse> call, ReadChatResponse response) {
                if(response.getData().getChats().size()>0) {

                   // first_id = response.getData().getChats().get(0).getId();
                   // last_id = response.getData().getChats().get(response.getData().getChats().size() - 1).getId();
                    for (int i = 0; i < response.getData().getChats().size(); i++) {
                        MyChatList chatList = new MyChatList();
                        chatList.setText(response.getData().getChats().get(i).getMessage());
                        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_JOBSEEKER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("jobseeker")) {
                            chatList.setFromUser("me");
                        } else if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_EMPLOYER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("employer")) {
                            chatList.setFromUser("me");
                        } else {
                            chatList.setFromUser("you");

                        }


                        myChatListArrayList.add(chatList);
                    }
                    mAdapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(myChatListArrayList.size() - 1);
                }else{
                    newstatus="YesStart";
                    Toast toast = Toast.makeText(EachChatActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
/*
                MyChatList chatList = new MyChatList();
                chatList.setFromUser("me");
                myChatListArrayList.add(chatList);
                mAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(myChatListArrayList.size() - 1);*/
                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<ReadChatResponse> call, Throwable t) {
                newstatus="YesStart";
                System.out.println("respasdeve fetch chat " + t.toString() );
                Toast toast = Toast.makeText(EachChatActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<ReadChatResponse> call, ReadChatResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EachChatActivity.this);
                    handler.removeCallbacks(runnable);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EachChatActivity.this, EmployerTypeActivity.class);
                    EachChatActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    Toast.makeText(EachChatActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }


            }

        });
    }

    public void fetchNewChats() {
        NewChatRequest request = new NewChatRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        request.chat_by = chat_by;
        System.out.println("QR#V#QRQV : " + chat_by );
        if (chat_by .equalsIgnoreCase("jobseeker")) {
            request.employee_id = employee_id;
        } else {
            request.employee_id = employee_id;
        }
       // request.last_id = last_id;
        request.employer_id = SharedPrefUtils.getApiKey(this);


        Call<ReadChatResponse> apiResponseCall;
        apiResponseCall =
                service.newChat(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<ReadChatResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

            }

            @Override
            protected void onEndApi() {

            }

            @Override
            public void onSuccess(Call<ReadChatResponse> call, ReadChatResponse response) {
                last_id = response.getData().getChats().get(response.getData().getChats().size()-1).getId();
                if(response.getData().getChats().size()>0) {
                    for (int i = 0; i < response.getData().getChats().size(); i++) {
                        MyChatList chatList = new MyChatList();
                        chatList.setText(response.getData().getChats().get(i).getMessage());
                        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_JOBSEEKER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("jobseeker")) {
                            chatList.setFromUser("me");
                        } else if (tinyDB.getString(AppConstants.USER_TYPE_MAIN).equals(AppConstants.USER_EMPLOYER) && response.getData().getChats().get(i).getChat_by().equalsIgnoreCase("employer")) {
                            chatList.setFromUser("me");
                        } else {
                            chatList.setFromUser("you");

                        }
                        myChatListArrayList.add(chatList);
                        mAdapter.notifyDataSetChanged();
                        recyclerView.scrollToPosition(myChatListArrayList.size() - 1);

                    }
                }
                    else{
                        // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                        Toast toast = Toast.makeText(EachChatActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }


            }

            @Override
            public void onError(Call<ReadChatResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
              //  Toast.makeText(EachChatActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<ReadChatResponse> call, ReadChatResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EachChatActivity.this);
                    handler.removeCallbacks(runnable);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EachChatActivity.this, EmployerTypeActivity.class);
                    EachChatActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    mInFlight = false;
                }


            }

        });
    }

    public void pushChat(String msg) {
        if (msg.length() == 0 ) {
            return;
        }

        SendChatRequest request = new SendChatRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();

        request.chat_by = "employer";
        request.employer_id = SharedPrefUtils.getApiKey(this);
        request.employerMobileNo = SharedPrefUtils.getEmployerMobile(this);;
        request.employerFirstName = SharedPrefUtils.getEmployerName(this);;
        request.employee_id = employee_id;
        request.employeeMobileNo = employeeMobileNo;
        request.employeeFirstName = toEmployeeName;
        request.message = msg;
        System.out.println(" resp!!!!!!!!!! abcasdeve " +newstatus+ request.chat_by+request.employer_id+request.employerMobileNo+request.employerFirstName+ request.employee_id+ request.employeeMobileNo+request.employeeFirstName );

        Call<BaseResponse> apiResponseCall;
        if(newstatus.equals("YesStart")) {
            apiResponseCall =
                    service.insertChat(request);
        }else{
            apiResponseCall =
                    service.insertChatEmployer(request);
        }
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                    showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {

                MyChatList chatList = new MyChatList();
                chatList.setFromUser("me");
                chatList.setText(msg);
                myChatListArrayList.add(chatList);
                mAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(myChatListArrayList.size() - 1);
                last_id = response.status.message;
                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EachChatActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EachChatActivity.this);
                    tinyDB.putString("username","");
                    handler.removeCallbacks(runnable);
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EachChatActivity.this, EmployerTypeActivity.class);
                    EachChatActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    Toast.makeText(EachChatActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }
            }
        });
    }

    public void deletChat() {
        DeleteChatRequest request = new DeleteChatRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        if (chat_by .equalsIgnoreCase("jobseeker")) {
            request.employee_id = employee_id;
        } else {
            request.employee_id = employee_id;
        }
        if (tinyDB.getString(AppConstants.USER_TYPE_MAIN) .equals(AppConstants.USER_EMPLOYER)) {
            request.chat_by = "employer";
        } else {
            request.chat_by = "jobseeker";
        }
        request.employer_id = SharedPrefUtils.getApiKey(this);


        Call<BaseResponse> apiResponseCall;
        apiResponseCall =
                service.deleteChat(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<BaseResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {

                showProgressBar();


            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<BaseResponse> call, BaseResponse response) {
                Toast.makeText(EachChatActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                onBackPressed();
            }

            @Override
            public void onError(Call<BaseResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                Toast.makeText(EachChatActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<BaseResponse> call, BaseResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(EachChatActivity.this);
                    handler.removeCallbacks(runnable);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(EachChatActivity.this, EmployerTypeActivity.class);
                    EachChatActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {

                    Toast.makeText(EachChatActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;
                }


            }

        });
    }

    @OnClick({R.id.ll_send_chat})
    public void clickFunction(View view) {
        switch (view.getId()) {
            case R.id.ll_send_chat:
                String msg = et_text.getText().toString();
                et_text.setText("");
                pushChat(msg);
                /*String msg = et_text.getText().toString();
                et_text.setText("");
                addLocalChat(msg, "me");*/
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete_chat, menu);

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
               // deletChat();
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        super.onBackPressed();
        handler.removeCallbacks(runnable);
       // this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
       /* handler.removeCallbacks(runnable);
        this.finish();
        Intent i = new Intent(mContext, MyJobsActivity.class);
        startActivity(i);
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);*/
    }

}
