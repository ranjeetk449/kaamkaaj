package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.ui.activities.SavedCandidatesActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.SelectLanguageActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.ChatListActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.jobs.MainActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.HelpActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.DrawerListAdapter;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.GPSTracker;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by akshaysingh on 4/17/18.
 */

public class EmployerDashboardActivity extends AppCompatActivity {

    @BindView(R.id.lv_drawer)
    ListView lv_drawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.ll_post_job)
    LinearLayout ll_post_job;
    @BindView(R.id.ll_search_candidate)
    LinearLayout ll_search_candidate;
    @BindView(R.id.ll_my_profile) LinearLayout ll_my_profile;
    @BindView(R.id.ll_my_jobs) LinearLayout ll_my_jobs;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.profile_pic)
    CircleImageView profile_pic;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.imageViewplaces)
    ImageView imageViewplaces;

    final String TAG= "r3r :" ;
    public ArrayList<String> drawer_icons;
    ArrayList<String> navigation_items;
    DrawerListAdapter drawerListAdapter;
    ActionBarDrawerToggle toggle;
    Context mContext;
    TinyDB tinyDB;
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
static int flag=0;
    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_employer);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.home_text);
        setSupportActionBar(toolbar);
        mContext = this;
        tinyDB = new TinyDB(this);

        init();

        SetDrawer();
        setProfilePic();
    //    addData();
        tv_name.setText(tinyDB.getString("username"));
        setDasboardPic();
        initLocation();
     //   tv_email.setText(tinyDB.getString("mobile"));
    }






    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {
            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();


        } else {
            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                    if(flag==0) {
                        EnableRuntimePermission();
                        flag++;
                    }
                   // GPSTracker getLoc=new GPSTracker(this);

                    //Location lac=getLoc.getLocation();
                   // double lat=lac.getLatitude();
                   // double lon=lac.getLongitude();
                   // if(lat==0.00){
                       // initLocation();
                    //}
                   // System.out.println("latlong"+lat+lon);

                       //     Toast.makeText(this, (int) lat, Toast.LENGTH_LONG).show();
                } else {
                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                      //  updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(EmployerDashboardActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                               // Toast.makeText(EmployerDashboardActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                       // updateLocationUI();
                    }
                });
    }

    private void initLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        System.out.println("addresssNewOne");
        //startLocationUpdates();
        EnableRuntimePermission();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
               // mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                System.out.println("addresssNewTwo");


                updateLocationUI();
            }
        };

      //  mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }



    private void updateLocationUI() {
        if (mCurrentLocation != null) {


            // location last updated time
            // txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);


            Geocoder geocoder =
                    new Geocoder(getApplication(), Locale.getDefault());
            // Get the current location from the input parameter list
            //Location loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(), 1);
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */
                System.out.println("addresssNew" + address.getAddressLine(0));

               /* et_location_val.setText(
                        "Lat: " + mCurrentLocation.getLatitude() + ", " +
                                "Lng: " + mCurrentLocation.getLongitude()
                );*/
                //  et_location_val.setText(address.getAddressLine(0));
                // giving a blink animation on TextView
                //  et_location_val.setAlpha(0);
                // et_location_val.animate().alpha(1).setDuration(300);
                String addressStr=address.getAddressLine(0);
                System.out.println("addaddressStr" + addressStr);
                if(addressStr.length()>0) {
                    mFusedLocationClient
                            .removeLocationUpdates(mLocationCallback)
                            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                   // Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                                    // toggleButtons();
                                }
                            });
                }

                //System.out.println("addresss"+address.getLocality()+address.getSubLocality()+address.getPostalCode()+address.getCountryName()+address.getFeatureName()+address.getThoroughfare()+address.getSubAdminArea()+address.getAdminArea());
            } catch (IOException e1) {
                Log.e("LocationSampleActivity",
                        "IO Exception in getFromLocation()");
                e1.printStackTrace();
                //return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(mCurrentLocation.getLatitude()) +
                        " , " +
                        Double.toString(mCurrentLocation.getLongitude()) +
                        " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                // return errorString;
            }
            // If the reverse geocode returned an address
           /* if (addresses.size() > 0) {
                Address address = addresses.get(0);
        /*
                    address.getLocality(),
                    address.getCountryName());
        */

            // System.out.println("addresss"+address.getLocality());

            //return address.getLocality();
            //  } else {
            // return "No address found";
            //  }


        }

        // toggleButtons();
    }




    private void init() {


        navigation_items = new ArrayList<>();

//adding menu items for naviations
        navigation_items.add(getResources().getString(R.string.post_job_text));
        navigation_items.add(getResources().getString(R.string.my_jobs_text));
        navigation_items.add(getResources().getString(R.string.my_profile_text));
        navigation_items.add(getResources().getString(R.string.search_candidate_text));
        navigation_items.add(getResources().getString(R.string.saved_candidates_text));
        navigation_items.add(getResources().getString(R.string.chat_text));
        navigation_items.add(getResources().getString(R.string.help_text));
        navigation_items.add(getResources().getString(R.string.change_language_text));
        navigation_items.add(getResources().getString(R.string.switch_to_jobseeker));


        drawer_icons = new ArrayList<>();
        drawer_icons.add("ic_postjob");
        drawer_icons.add("ic_myjobs");
        drawer_icons.add("ic_myprofile");
        drawer_icons.add("ic_search");
        drawer_icons.add("ic_save_candidates");
        drawer_icons.add("ic_chat");
        drawer_icons.add("ic_help");
        //   drawer_icons.add("ic_logout");
        drawer_icons.add("ic_changelang");
        drawer_icons.add("ic_switch");

    }

    private void SetDrawer() {

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(EmployerDashboardActivity.this, navigation_items, drawer_icons);
        lv_drawer.setAdapter(drawerListAdapter);

        lv_drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.my_profile_text))) {

                    Intent i = new Intent(mContext, ActivityEmployerProfile.class);
                    mContext.startActivity(i);
                    // tv_selected_navigation.setText("Selected Call");

                } else if (navigation_items.get(position).equalsIgnoreCase(getString(R.string.change_language_text))) {
                    Intent i = new Intent(mContext, SelectLanguageActivity.class);
                    i.putExtra(AppConstants.FROM_STRING,"emp");

                    mContext.startActivity(i);
                    //  tv_selected_navigation.setText("Selected Favorite");


                } else  if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.search_candidate_text))) {
                    Intent i = new Intent(mContext, SearchCandidateActivity.class);
                    mContext.startActivity(i);
                } else if(navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.saved_candidates_text))) {
                    Intent i = new Intent(mContext, SavedCandidatesActivity.class);
                    mContext.startActivity(i);
                } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.help_text))) {
                    Intent i = new Intent(mContext, HelpActivity.class);
                    mContext.startActivity(i);
                } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.post_job_text))) {
                    Intent i = new Intent(mContext, JobPostActivity.class);
                    mContext.startActivity(i);
                } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.my_jobs_text))) {
                    Intent i = new Intent(mContext, MyJobsActivity.class);
                    mContext.startActivity(i);
                } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.switch_to_jobseeker))) {
                    SharedPrefUtils.clearSharedPreference(EmployerDashboardActivity.this);
                    tinyDB.putString("username","");
                 //   tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                  //  SharedPrefUtils.setEmployerType(EmployerDashboardActivity.this, AppConstants.EMPLOYER_TYPE_INDI);
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_JOBSEEKER);

                    Intent intent = new Intent(EmployerDashboardActivity.this, MainActivity.class);
                    //intent.putExtra(AppConstants.USER_TYPE,AppConstants.EMPLOYER_TYPE_INDI);
                    EmployerDashboardActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);

                } else if (navigation_items.get(position).equalsIgnoreCase(getResources().getString(R.string.chat_text))) {
                    Intent i = new Intent(mContext, ChatListActivity.class);
                    mContext.startActivity(i);
                }
            }
        });
    }/*
    public void addData() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String existingId = tinyDB.getString("existingId");

        if (existingId .equals("")){
            int j = 1;
            Map<String, Object> user = new HashMap<>();

            for (int i =0; i < 100; i++) {
                user.put(String.valueOf(i),"ajfnkjgfjs weui wae : " + i);
            }
            db.collection("logs")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
        }

    }*/


    public void setDasboardPic() {
        if (SharedPrefUtils.getEmployerType(this) .equals(AppConstants.EMPLOYER_TYPE_CORPORATE)) {
            imageViewplaces.setImageResource(R.mipmap.dash_emp_big);

        } else {
            imageViewplaces.setImageResource(R.mipmap.indi_bg);

        }

    }
    public void setProfilePic() {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        String userType = SharedPrefUtils.getUserLoginType(this);


        if(userType.length()>0) {


            File myDir = null;
            if (userType.equals("Google")) {
                myDir = new File(root + "/kaamkaajgoogle");
                myDir.mkdirs();
            } else if (userType.equals("Facebook")) {
                myDir = new File(root + "/kaamkaajfb");
                myDir.mkdirs();
            } else if (userType.equals("Normal")) {
                myDir = new File(root + "/kaamkaajj");
                myDir.mkdirs();
            }

            String fname = "profile_pic1.jpg";
            File file = new File(myDir, fname);
            if (file.exists()) {
                try {
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                    profile_pic.setImageBitmap(b);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @OnClick({R.id.ll_post_job, R.id.ll_search_candidate, R.id.ll_my_profile, R.id.ll_my_jobs})
    public void moveAhead(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.ll_my_jobs:
                i = new Intent(this, MyJobsActivity.class);
                startActivity(i);
                break;
            case R.id.ll_my_profile:
                i = new Intent(this, ActivityEmployerProfile.class);
                startActivity(i);
                break;
            case R.id.ll_post_job:
                i = new Intent(this, JobPostActivity.class);
                startActivity(i);
                break;
            case R.id.ll_search_candidate:
                i = new Intent(this, SearchCandidateActivity.class);
                startActivity(i);
                return;
        }
    }
    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            //drawer is open
        } else {
            deleteJob();

        }
    }



    public void deleteJob() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.exit_app_text));
        alertDialogBuilder.setMessage(mContext.getResources().getString(R.string.sure_exit_text));
        alertDialogBuilder.setPositiveButton(mContext.getResources().getString(R.string.yes_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        alertDialogBuilder.setNegativeButton(mContext.getResources().getString(R.string.no_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            }
        });
        alertDialog.show();
    }
}