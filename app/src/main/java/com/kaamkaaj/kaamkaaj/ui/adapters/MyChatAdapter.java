package com.kaamkaaj.kaamkaaj.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.MyChatList;
import com.kaamkaaj.kaamkaaj.ui.interfaces.OnItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by akshaysingh on 6/21/18.
 */

public class MyChatAdapter extends RecyclerView.Adapter<MyChatAdapter.Holder> implements
        AdapterView.OnItemClickListener, OnItemClickListener {

    private static String LOG_TAG = "JobListAdapter";
    private ArrayList<MyChatList> mDataset;
    private ArrayList<Boolean> selected;
    OnItemClickListener listener;
    Context mContext;
    Handler mHandler;

    public MyChatAdapter(Context mContext, ArrayList<MyChatList> myDataset) {
        this.mContext = mContext;
        mDataset = myDataset;
        selected = new ArrayList<Boolean>();

        mHandler = new Handler();
    }

    @Override
    public void onItemClick(int position, View view) {


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    public class Holder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        OnItemClickListener mListener;
        @BindView(R.id.each_block)
        LinearLayout each_block;

        @BindView(R.id.tv_my_text)
        TextView tv_my_text;
        @BindView(R.id.tv_other_text)
        TextView tv_other_text;
        @BindView(R.id.ll_my_text)
        LinearLayout ll_my_text;
        @BindView(R.id.ll_other_text)
        LinearLayout ll_other_text;


        public Holder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.listener = mItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent,
                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_each_chat, parent, false);

        Holder dataObjectHolder = new Holder(view, this);
        return dataObjectHolder;

    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        if (mDataset.get(position).getFromUser() .equalsIgnoreCase("me")) {
            holder.ll_my_text.setVisibility(View.VISIBLE);
            holder.ll_other_text.setVisibility(View.GONE);
            holder.tv_my_text.setText(mDataset.get(position).getText());
        } else {
            holder.ll_my_text.setVisibility(View.GONE);
            holder.ll_other_text.setVisibility(View.VISIBLE);
            holder.tv_other_text.setText(mDataset.get(position).getText());
        }
        /*holder.tv_job_title.setText(mDataset.get(position).getJob_title());
        holder.tv_company_name.setText(mDataset.get(position).getCompany_name());
        holder.tv_salary.setText(mDataset.get(position).getMin_salary() + " to " + mDataset.get(position).getMax_salary());
        holder.tv_experience.setText(mDataset.get(position).getMin_exp() + " - " + mDataset.get(position).getMax_emp() +  " " +  mContext.getResources().getString(R.string.sample_experience));
        holder.tv_location.setText(mDataset.get(position).getAddress());
        holder.each_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, JobDescriptionActivity.class);
                i.putExtra(AppConstants.FROM_CLASS, "listing");
                i.putExtra(AppConstants.KEY_CLIENT,mDataset.get(position) );
                mContext.startActivity(i);
            }
        });*/


    }


    public void addItem(MyChatList dataObj, int index) {
        mDataset.add(dataObj);
        selected.set(index,false);
        notifyItemInserted(index);

    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void removeProgressBarView() {
        //remove the dummy item we added to the end of list
        if (mDataset.size() > 0) {
            mDataset.remove(mDataset.size() - 1);
            //notifyDataSetChanged();
            notifyItemRemoved(mDataset.size());
        }
    }

}
