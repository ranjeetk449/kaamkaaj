package com.kaamkaaj.kaamkaaj.ui.activities.jobs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.share.Share;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.AppliedJobRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.AppliedJobList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.AppliedJobListResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.EmployerTypeActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.employer.MyJobsActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.CustomerDashboardActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.AppliedJobsAdapter;
import com.kaamkaaj.kaamkaaj.utils.RecyclerOnScrollListener;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/9/18.
 */

public class AppliedJobActivity  extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.rv_jobs)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;

    ArrayList<AppliedJobList> jobLists = new ArrayList<AppliedJobList>();;
    ArrayList<AppliedJobList> jobListsNew = new ArrayList<AppliedJobList>();;



    AppliedJobsAdapter mAdapter;
    Context mContext;
    TinyDB tinyDB;
    int mCurrentPage = 1;

    private boolean mInFlight;
    String kills = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applied_jobs);
        mContext = this;
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.applied_jobs);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GridLayoutManager lLayout = new GridLayoutManager(mContext, 1);
        mAdapter = new AppliedJobsAdapter(mContext, jobLists);
        recyclerView.setLayoutManager(lLayout);
        recyclerView.setAdapter(mAdapter);
        fab.setVisibility(View.GONE);
        ll_action_button.setVisibility(View.VISIBLE);
        ll_action_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefUtils.clearFilters(AppliedJobActivity.this);
                SharedPrefUtils.setFilterSkills(AppliedJobActivity.this, kills);
                Intent i = new Intent(mContext, JobsListActivity.class);
                mContext.startActivity(i);
            }
        });
     //   addCategoryData();
        recyclerView.addOnScrollListener(new RecyclerOnScrollListener(lLayout) {
            @Override
            public void onLoadMore(int currentPage, int count) {
                //     Log.d(TAG, "onLoadMore: " + mInFlight + "---- Count" + count);
                if (!mInFlight) {

                 //   mAdapter.addProgressBarView();
                    fetchJobs(mCurrentPage, true);
                    mInFlight = true;
                }
            }
        });

        fetchJobs(mCurrentPage, true);
        /*sv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {
                    //code to fetch more data for endless scrolling
                    if (!mInFlight) {

                        //   mAdapter.addProgressBarView();
                        fetchJobs(mCurrentPage, true);
                        mInFlight = true;
                    }
                }
            }
        });

        fetchJobs(mCurrentPage, true);*/
    }

    ///// /// Will have to save objects, instead od ids

    /*public void addCategoryData() {
        ArrayList<String> ids = new ArrayList<String>();
        ids = tinyDB.getListString(AppConstants.APPLIED_JOBS);
     //   Toast.makeText(this,"Number of saved : " + ids.size(), Toast.LENGTH_LONG  ).show();
        JobRequest request = new JobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.randomId = SharedPrefUtils.getAnonymousUserId(this);
        request.pageNo = 1;

        Call<JobListResponse> apiResponseCall;
        apiResponseCall =
                service.jobRequest(request);
        final ArrayList<String> finalIds = ids;
        apiResponseCall.enqueue(new RetrofitRequestListener<JobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                    showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<JobListResponse> call, JobListResponse response) {
                //  System.out.println("_____ : " + response.data);
                for (int i =0;i < response.getData().jobs.size(); i++) {
                    if(finalIds.indexOf(response.getData().jobs.get(i).getId()) >= 0) {
                        jobLists.add(response.getData().jobs.get(i));
                    }
                    response.getData().jobs.get(i).setSelected("no");
                    jobLists.add(response.getData().jobs.get(i));
                }
                mAdapter.notifyDataSetChanged();
                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Call<JobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );

            }

            @Override
            public void onCustomError(Call<JobListResponse> call, JobListResponse response) {
                Toast.makeText(AppliedJobActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
            }

        });
    }*/







      /*

    public void fetchJobs(int mPage, final boolean showProgress) {    


        AppliedJobRequest request = new AppliedJobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.token = SharedPrefUtils.getApiKey(this);
      

            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("page", request.page);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_APPLIEDJOBS;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseAPPLIEDJOBS" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/

                  /*
                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        String respsuccess=status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject=response.getJSONObject("data");
                            JSONArray jobs=subSkillObject.getJSONArray("jobs");
                           /* for (int i =0;i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("name" +name);
                                subSkillsAdapter.add(name);
                                subSkillsAdapter.notifyDataSetChanged();

                            }*/

                           








                    /*    }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", AppliedJobActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });


    }
       */







    public void fetchJobs(int mPage, final boolean showProgress) {
        AppliedJobRequest request = new AppliedJobRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.token = SharedPrefUtils.getApiKeyEmployee(this);
        request.page = mPage;
       /* request.mobileNo = mobileNum;
        request.otp = otp;
*/
        //request.token = SharedPrefUtils.getApiKey(this);


        Call<AppliedJobListResponse> apiResponseCall;
        apiResponseCall =
                service.fetchAppliedJobs(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<AppliedJobListResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                if (showProgress) {
                    showProgressBar();
                }

            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<AppliedJobListResponse> call, AppliedJobListResponse response) {

                  System.out.println("respApplied: " + response.getData().jobs.size());
                  if(response.getData().jobs.size()>0) {
                      for (int i = 0; i < response.getData().jobs.size(); i++) {
                          jobLists.add(response.getData().jobs.get(i));
                          jobListsNew.add(response.getData().jobs.get(i));
                          kills = kills + response.getData().jobs.get(i).getSkill() + ",";
                      }
                      mCurrentPage++;
                      mAdapter.notifyDataSetChanged();
                      mInFlight = false;

                  }else{
                      // Toast.makeText(MyJobsActivity.this, getResources().getString(R.string.record_not_found),Toast.LENGTH_LONG).show();

                      Toast toast = Toast.makeText(AppliedJobActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                      toast.setGravity(Gravity.CENTER, 0, 0);
                      toast.show();
                  }
                //   SharedPrefUtils.setAppState(LoginActivity.this, AppConstants.STATE_REGISTERED);
                //  Toast.makeText(LoginActivity.this, "TOKEN IS : " + response.getData().getToken().toString(),Toast.LENGTH_LONG).show();
            }



            @Override
            public void onError(Call<AppliedJobListResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                //Toast.makeText(AppliedJobActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                Toast toast = Toast.makeText(AppliedJobActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();


                mInFlight = false;

            }

            @Override
            public void onCustomError(Call<AppliedJobListResponse> call, AppliedJobListResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(AppliedJobActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(AppliedJobActivity.this, EmployerTypeActivity.class);
                    AppliedJobActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(AppliedJobActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    mInFlight = false;

                }
            }

        });
    }
      

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        return true;*/

        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearch = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query .equals("")) {
                    jobLists.clear();
                    for (int i=0; i < jobListsNew.size(); i++) {
                            jobLists.add(jobListsNew.get(i));

                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    jobLists.clear();
                    for (int i=0; i < jobListsNew.size(); i++) {
                         if(jobListsNew.get(i).getJob_title()!=null) {
                             if (jobListsNew.get(i).getJob_title().toLowerCase().contains(query.toLowerCase())) {
                                 jobLists.add(jobListsNew.get(i));
                             }
                         }
                    }
                    mAdapter.notifyDataSetChanged();
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                     System.out.println("respnewTextasdeve " + newText );

                if (newText.equals("")) {
                    jobLists.clear();
                    for (int i=0; i < jobListsNew.size(); i++) {
                        jobLists.add(jobListsNew.get(i));

                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    jobLists.clear();

                     System.out.println("respnewTextasdevehhh " + jobListsNew.size() );
                    for (int i=0; i < jobListsNew.size(); i++) {
                       // System.out.println("respnewTextasdevehhhNew " + jobListsNew.get(i).getJob_title());
                        if(jobListsNew.get(i).getJob_title()!=null) {
                            if (jobListsNew.get(i).getJob_title().toLowerCase().contains(newText.toLowerCase())) {
                                jobLists.add(jobListsNew.get(i));
                            }
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (SharedPrefUtils.getAppState(AppliedJobActivity.this) == AppConstants.STATE_REGISTERED) {
            Intent intent = new Intent(this, CustomerDashboardActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }


        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


}
