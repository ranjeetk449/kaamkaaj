package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.AppConstants;

import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Employer.EmployerProfileRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.employer.EmployerProfileResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

/**
 * Created by akshaysingh on 4/18/18.
 */

public class ActivityEmployerProfile extends NewBaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.top_bar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.et_your_name)
    EditText et_your_name;
    @BindView(R.id.tv_company_name)
    TextView tv_company_name;
    @BindView(R.id.et_company_name)
    EditText et_company_name;
    @BindView(R.id.tv_designation)
    TextView tv_designation;
    @BindView(R.id.et_designation)
    EditText et_designation;
    @BindView(R.id.tv_company_website)
    TextView tv_company_website;
    @BindView(R.id.et_company_website)
    EditText et_company_website;
    @BindView(R.id.tv_company_address)
    TextView tv_company_address;
    @BindView(R.id.et_company_address)
    EditText et_company_address;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.iv_im1)
    CircleImageView iv_im1;
    String employerType = "";
    TinyDB tinyDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employer_profile);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.my_profile_text);
        setSupportActionBar(toolbar);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        employerType = SharedPrefUtils.getEmployerType(this);
        hideUnImp();
        tinyDB = new TinyDB(this);
        setProfilePic();
        fetchProfile();
       /* setProfilePic();
        fetchProfile();*/
      // init();
    }
    void init() {
        et_your_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_name.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_designation.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_website.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_company_address.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_mobile_number.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_email.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

    }

    public void setProfilePic() {
        System.out.println("respProfilePicEmployer");
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        System.out.println("respProfilePicEmploER1"+root);
        String userType = SharedPrefUtils.getUserLoginType(this);
        System.out.println("respProfilePicEmploER2"+userType);
       // File myDir=null;
        if(userType.equals("Google")){
            System.out.println("respProfilePicEmploER3"+userType);
            File   myDir = new File(root + "/kaamkaajgoogle");
            myDir.mkdirs();
            String fname = "profile_pic1.jpg";
            File file = new File (myDir, fname);
            System.out.print("respbitmapviewfilexist"+file.exists ());
            if (file.exists ()) {
                try {
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                    System.out.print("respbitmapviewbitmap"+b);
                    iv_im1.setImageBitmap(b);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }else if(userType.equals("Facebook")){
            System.out.println("respProfilePicEmploER4"+userType);
            File  myDir = new File(root + "/kaamkaajfb");
            myDir.mkdirs();
            String fname = "profile_pic1.jpg";
            File file = new File (myDir, fname);
            System.out.print("respbitmapviewfilexist"+file.exists ());
            if (file.exists ()) {
                try {
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                    System.out.print("respbitmapviewbitmap"+b);
                    iv_im1.setImageBitmap(b);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }else if(userType.equals("Normal")){
            System.out.println("respProfilePicEmploER5"+userType);
            File  myDir = new File(root + "/kaamkaajj");
            myDir.mkdirs();
            String fname = "profile_pic1.jpg";
            File file = new File (myDir, fname);
            System.out.print("respbitmapviewfilexist"+file.exists ());
            if (file.exists ()) {
                try {
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                    System.out.print("respbitmapviewbitmap"+b);
                    iv_im1.setImageBitmap(b);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.print("respbitmapviewuserType"+userType);


    }
    @OnClick(R.id.fab)
    public void gotoFunction() {
        Intent i = new Intent(this, ActivityEmployerProfileUpdate.class);
        startActivity(i);
    }

    public void hideUnImp() {
        System.out.print("!#!@$!@$!@$!@$!@!@$!@$!$$");
        if (employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
            tv_company_name.setVisibility(View.GONE);
            et_company_name.setVisibility(View.GONE);
            tv_designation.setVisibility(View.GONE);
            et_designation.setVisibility(View.GONE);
            tv_company_website.setVisibility(View.GONE);
            et_company_website.setVisibility(View.GONE);
            tv_company_address.setText(getResources().getString(R.string.adress_text));
            et_company_address.setHint(getResources().getString(R.string.adress_text));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }








    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            System.out.println("responseDetailIF");
            return;
        } else {
             System.out.println("responseDetailELSE");
            String token = SharedPrefUtils.getApiKey(this);
            System.out.println("responseDetailELSE"+token);
            RequestParams params = new RequestParams();
            params.put("token", token);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_PROFILE;
            System.out.println("resProfileUrl"+url);
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseDetail" + response);

               /*



                 responseDetail{"status":{"response":"success","message":"","userMessage":""},
                 "data":{"userProfile":{"email":"rpathaksoftware@gmail.com","firstName":"ravi","lastName":"pathak",
                 "mobileNo":"121212","password":"123456","userType":"corporate",
                 "createdAt":"2019-07-21T11:11:20.521Z","updatedAt":"2019-07-21T11:11:20.521Z",
                 "id":"5d344858c462bf110673cdc1"}}}



*/




                try {
                    hideProgressBar();
                    JSONObject status=response.getJSONObject("status");
                    //String userProfile=status.getString("data");
                   // System.out.println("responseuserProfile" + userProfile);
                    String respsuccess=status.getString("response");
                    System.out.println("responseDetaiddddl" + respsuccess);
                    if (respsuccess.equals("success")) {
                        System.out.println("responseuserProfilescsd");
                        JSONObject userProfile=response.getJSONObject("data");
                        System.out.println("responseuserProfile" + userProfile);
                        JSONObject userProfileList=userProfile.getJSONObject("userProfile");
                      //  System.out.println("respppuserProfilee" + userProfilee);
                        String email=userProfileList.getString("email");
                        String firstName=userProfileList.getString("firstName");
                        String lastName=userProfileList.getString("lastName");
                        String mobileNo=userProfileList.getString("mobileNo");
                       // String password=userProfileList.getString("password");
                       // String userType=userProfileList.getString("userType");

                       if(userProfileList.has("company_name"))
                       {
                           String company_name=userProfileList.getString("company_name");
                           et_company_name.setText(company_name);
                       }
                        if(userProfileList.has("designation"))
                        {
                            String designation=userProfileList.getString("designation");
                            et_designation.setText(designation);
                        }
                        if(userProfileList.has("company_website"))
                        {
                            String company_website=userProfileList.getString("company_website");
                            et_company_website.setText(company_website);
                        }
                        if(userProfileList.has("company_address"))
                        {
                            String company_address=userProfileList.getString("company_address");
                            et_company_address.setText(company_address);
                        }

                        if(userProfileList.has("address"))
                        {
                            String company_address=userProfileList.getString("address");
                            et_company_address.setText(company_address);
                        }

                       // String token=userProfile.getString("token");
                       // System.out.println("teamToken"+token);

                       /* System.out.println("responsefirstName" + firstName);
                        System.out.println("responselastName" + lastName);
                        System.out.println("responseemail" + email);
                        System.out.println("responsemobileNo" + mobileNo);*/
                      //  System.out.println("responseuserProfile" + firstName);

                        tinyDB.putString("username" ,firstName + " " + lastName);
                       // et_company_name.setText(response.getData().userProfile.getCompany_name());

                        et_your_name.setText(firstName + " " + lastName);

                       // et_designation.setText(response.getData().userProfile.getDesignation());
                        et_email.setText(email);
                       // et_company_website.setText(response.getData().userProfile.getCompany_website());
                        et_mobile_number.setText(mobileNo);
                       /* if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                            et_company_address.setText(response.getData().userProfile.getCompany_address());
                        } else {
                            et_company_address.setText(response.getData().userProfile.getAddress());

                        }*/




                    }else{
                        Toast toast = Toast.makeText(ActivityEmployerProfile.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        //Toast.makeText(ActivityEmployerProfile.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", ActivityEmployerProfile.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }







/*

    public void fetchProfile() {

        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            EmployerProfileRequest request = new EmployerProfileRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            Call<EmployerProfileResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchProfile(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<EmployerProfileResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<EmployerProfileResponse> call, EmployerProfileResponse response) {
                    tinyDB.putString("username" ,response.getData().userProfile.getFirst_name() + " " + response.getData().userProfile.getLast_name());
                    et_company_name.setText(response.getData().userProfile.getCompany_name());

                    et_your_name.setText(response.getData().userProfile.getFirst_name() + " " + response.getData().userProfile.getLast_name());

                    et_designation.setText(response.getData().userProfile.getDesignation());
                    et_email.setText(response.getData().userProfile.getEmail_id());
                    et_company_website.setText(response.getData().userProfile.getCompany_website());
                    et_mobile_number.setText(response.getData().userProfile.getMobile_no());
                    if (!employerType .equalsIgnoreCase(AppConstants.EMPLOYER_TYPE_INDI)) {
                        et_company_address.setText(response.getData().userProfile.getCompany_address());
                    } else {
                        et_company_address.setText(response.getData().userProfile.getAddress());

                    }
                }

                @Override
                public void onError(Call<EmployerProfileResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString() );
                    Toast.makeText(ActivityEmployerProfile.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<EmployerProfileResponse> call, EmployerProfileResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(ActivityEmployerProfile.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(ActivityEmployerProfile.this, EmployerTypeActivity2.class);
                        ActivityEmployerProfile.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ActivityEmployerProfile.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

*/
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, EmployerDashboardActivity.class);
        startActivity(intent);
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
