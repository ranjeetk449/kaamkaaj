package com.kaamkaaj.kaamkaaj.ui.activities.employer;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.restapi.OROApiAdapter;
import com.kaamkaaj.kaamkaaj.restapi.RequestUrl;
import com.kaamkaaj.kaamkaaj.restapi.RetrofitRequestListener;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchEducationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchLocationRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSkillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.request.Job.FetchSubskillsRequest;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.EducationResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSkillsResponse;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobSubSkillsList;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.LocationResponse;
import com.kaamkaaj.kaamkaaj.ui.activities.NewBaseActivity;
import com.kaamkaaj.kaamkaaj.ui.activities.users.UserProfileActivity;
import com.kaamkaaj.kaamkaaj.ui.adapters.FilterAdapters;
import com.kaamkaaj.kaamkaaj.ui.commonUtil.CommonMethod;
import com.kaamkaaj.kaamkaaj.utils.Geofencing;
import com.kaamkaaj.kaamkaaj.utils.SharedPrefUtils;
import com.kaamkaaj.kaamkaaj.utils.TinyDB;
import com.kaamkaaj.kaamkaaj.utils.models.FilterObject;
import com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar.CrystalRangeSeekbar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import cz.msebera.android.httpclient.Header;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;

/**
 * Created by akshaysingh on 5/14/18.
 */

public class SearchCandidateActivity extends NewBaseActivity implements  NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.top_bar)
    Toolbar top_bar;
    @BindView(R.id.ll_action_button)
    LinearLayout ll_action_button;
    @BindView(R.id.ll_full_time)
    LinearLayout ll_full_time;
    @BindView(R.id.rangeSeekbar3)
    CrystalRangeSeekbar rangeSeekbar3;
    @BindView(R.id.ll_part_time) LinearLayout ll_part_time;
    @BindView(R.id.ll_freelancer) LinearLayout ll_freelancer;
    @BindView(R.id.iv_freelance) ImageView iv_freelance;
    @BindView(R.id.iv_full_time) ImageView iv_full_time;
    @BindView(R.id.iv_part_time)
    ImageView iv_part_time;
    @BindView(R.id.tv_min_value)
    TextView tv_min_value;
    @BindView(R.id.et_set_min) EditText et_set_min;
    @BindView(R.id.tv_set_min) TextView tv_set_min;
    @BindView(R.id.ll_min) LinearLayout ll_min;
    @BindView(R.id.ll_max) LinearLayout ll_max;
    @BindView(R.id.ll_min1) LinearLayout ll_min1;
    @BindView(R.id.et_set_max) EditText et_set_max;
    @BindView(R.id.tv_set_max) TextView tv_set_max;
    @BindView(R.id.ll_max1) LinearLayout ll_max1;
    @BindView(R.id.tv_max_value) TextView tv_max_value;
    @BindView(R.id.tv_location_val)
    TextView tv_location_val;
    @BindView(R.id.tv_location)
    TextView tv_location;
    @BindView(R.id.ss_skills)
    SearchableSpinner ss_skills;
    @BindView(R.id.ss_subskills)
    SearchableSpinner ss_subskills;
    @BindView(R.id.ss_min_exp) SearchableSpinner ss_min_exp;
    @BindView(R.id.ss_max_emp) SearchableSpinner ss_max_exp;
    @BindView(R.id.ss_education) SearchableSpinner ss_education;
    @BindView(R.id.et_min_salary) EditText et_min_salary;
    @BindView(R.id.et_max_salary) EditText et_max_salary;
    @BindView(R.id.tv_min_salary) TextView tv_min_salary;
    @BindView(R.id.tv_max_salary) TextView tv_max_salary;
    @BindView(R.id.ms_location)
    MultiSelectSpinner ms_location;
    @BindView(R.id.ms_subskills)
    MultiSelectSpinner ms_subskills;
    @BindView(R.id.ms_education)
    MultiSelectSpinner ms_education;
    @BindView(R.id.ll_other_skills)
    LinearLayout ll_other_skills;
    @BindView(R.id.et_other_skills)
    EditText et_other_skills;
    /*@BindView(R.id.ss_education1)
    MultiSpinner ss_education1;
*/
    public static final String TAG = JobPostActivity.class.getSimpleName();


    boolean minTextVisible = false, maxTextVisible = false;

    Boolean part = false, full = false, freelance = false;

    private static final int PLACE_PICKER_REQUEST = 1;
    private Geofencing mGeofencing;
    private GoogleApiClient mClient;
    String lat = "", lon = "",name = "", initialUsername = "";
    public  static final int RequestPermissionCode  = 1;
    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };


    ArrayList<JobSkillsList> jobSkillsLists;
    ArrayAdapter<String> skillsAdapter;
    List<String> skillsList;

    ArrayList<JobSubSkillsList> jobSubSkillsLists;
    ArrayAdapter<String> subSkillsAdapter;
    List<String> subSkillsList;
    ArrayList<String> optionsSubskills;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_candidate);
        ButterKnife.bind(this);
        top_bar.setTitle(R.string.search_candidate_text);
        setSupportActionBar(top_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_min_value.setText(getResources().getString(R.string.Rs) + " "  + AppConstants.MIN_SALARY);
        tv_max_value.setText(getResources().getString(R.string.Rs) + " "  + AppConstants.MAX_SALARY);
        tinyDB = new TinyDB(this);
        ss_skills.setTitle(getResources().getString(R.string.subskills_text));
        ss_subskills.setTitle(getResources().getString(R.string.subskills_text));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ss_min_exp.setTitle(getResources().getString(R.string.min_exp_text));
        ss_max_exp.setTitle(getResources().getString(R.string.max_exp_text));
        ss_education.setTitle(getResources().getString(R.string.education_text));
        optionsSubskills  = new ArrayList<>();
        final List<String> list = Arrays.asList(getResources().getStringArray(R.array.education));

        EnableRuntimePermission();
        skillsList  = new ArrayList<String>();
        skillsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, skillsList);
        ss_skills.setAdapter(skillsAdapter);

        subSkillsList = new ArrayList<String>();
        subSkillsAdapter = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, subSkillsList);
        ss_subskills.setAdapter(subSkillsAdapter);

        jobSkillsLists = new ArrayList<JobSkillsList>();
        jobSubSkillsLists = new ArrayList<JobSubSkillsList>();

        fetchSubSkills2();
        addLocationData();
        addEducationData();
        ms_subskills.setTitle(getResources().getString(R.string.profile_skills_text));
        ms_location.setTitle(getResources().getString(R.string.location_text));
        ms_education.setTitle(getResources().getString(R.string.education_text));
        mGeofencing = new Geofencing(this, mClient);

        mGeofencing.registerAllGeofences();
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();
       // init();
    }




    void init() {
        et_set_min.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_set_max.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_min_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_max_salary.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        et_other_skills.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

    }

    public void fetchSkills() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSkillsRequest request = new FetchSkillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            Call<JobSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    jobSkillsLists = response.getData().getSkills();
                    for (int i = 0; i < response.getData().getSkills().size(); i++) {
                        System.out.println("respCand|||| : " + response.getData().getSkills().get(i).getName());
                        skillsAdapter.add(response.getData().getSkills().get(i).getName().trim());
                        skillsAdapter.notifyDataSetChanged();
                    }
                   // subSkillsAdapter.clear();
                  //  fetchSubSkills(response.getData().getSkills().get(0).getId(),response.getData().getSkills().get(0).getName());
                }

                @Override
                public void onError(Call<JobSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast toast = Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                   // Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCustomError(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(SearchCandidateActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(SearchCandidateActivity.this, EmployerTypeActivity2.class);
                        SearchCandidateActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(SearchCandidateActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    public void addEducationData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();
        FetchEducationRequest request = new FetchEducationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKey(this));
        Call<EducationResponse> apiResponseCall;
        apiResponseCall =
                service.fetchEducation(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<EducationResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<EducationResponse> call, EducationResponse response) {
                /*for (int i = 0; i < response.getData().getEducationLists().size(); i++) {
                    jobCategories.add(response.getData().getEducationLists().get(i).getName());
                    FilterObject filterObject = new FilterObject(jobCategories.get(i),false);
                    educationList.add(filterObject);
                    educationAdapter.notifyDataSetChanged();
                }*/
                ArrayList<String> options = new ArrayList<>();

                for (int i = 0; i < response.getData().getEducationLists().size(); i++) {

                    options.add(response.getData().getEducationLists().get(i).getName().trim());

                }
                ArrayAdapter<String> adapter = new ArrayAdapter <String>(SearchCandidateActivity.this, android.R.layout.simple_list_item_multiple_choice, options);
                ms_education.setListAdapter(adapter);
            }

            @Override
            public void onError(Call<EducationResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast toast = Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                //Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<EducationResponse> call, EducationResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(SearchCandidateActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(SearchCandidateActivity.this, EmployerTypeActivity2.class);
                    SearchCandidateActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(SearchCandidateActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }

        });


        /*String[] education = getResources().getStringArray(R.array.education);
        for (int i =0; i < education.length; i++) {
            FilterObject filterObject = new FilterObject(education[i],false);
            educationList.add(filterObject);
        }
        educationAdapter.notifyDataSetChanged();*/
    }


    @OnFocusChange({ R.id.et_min_salary, R.id.et_max_salary})
    public void clickOnPassword(View view, boolean hasFocus) {
        switch (view.getId()) {

            case R.id.et_min_salary:
                minSalaryFocus(view, hasFocus);
                break;
            case R.id.et_max_salary:
                maxSalaryFocus(view, hasFocus);
                break;

        }
    }
    public void minSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_min_salary.setVisibility(View.VISIBLE);
            et_min_salary.setHint("");
        } else {
            if (et_min_salary.getText().toString() .equals(initialUsername)) {
                tv_min_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_min_salary.setVisibility(View.VISIBLE);

            }
            et_min_salary.setHint(getResources().getString(R.string.min_salary_text));
        }
    }
    public void maxSalaryFocus(View view, boolean hasFocus) {
        if (hasFocus) {
            tv_max_salary.setVisibility(View.VISIBLE);
            et_max_salary.setHint("");
        } else {
            if (et_max_salary.getText().toString() .equals(initialUsername)) {
                tv_max_salary.setVisibility(View.INVISIBLE);
            } else {
                tv_max_salary.setVisibility(View.VISIBLE);

            }
            et_max_salary.setHint(getResources().getString(R.string.max_salary_text));
        }
    }






    public void fetchSubSkills(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {

            FetchSubskillsRequest request = new FetchSubskillsRequest();
            request.token = SharedPrefUtils.getApiKey(this);
            request.master_skill_id = id;
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("master_skill_id", id);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SUBSKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseSUBFetchDetails" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/


                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        String respsuccess=status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject=response.getJSONObject("data");
                            JSONArray subSkillArr=subSkillObject.getJSONArray("subskills");
                            for (int i =0;i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("name" +name);
                                subSkillsAdapter.add(name);
                                subSkillsAdapter.notifyDataSetChanged();

                            }



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", SearchCandidateActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }










/*
    public void fetchSubSkills(String id, final String name) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
           // Toast.makeText(this, name, Toast.LENGTH_LONG).show();
            FetchSubskillsRequest request = new FetchSubskillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));
            request.master_skill_id = id;
            Call<JobSubSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSubSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSubSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    jobSubSkillsLists = response.getData().getSubSkills();
                    for (int i = 0; i < response.getData().getSubSkills().size(); i++) {
                        System.out.println("|||| : " + response.getData().getSubSkills().get(i).getName() );
                        subSkillsAdapter.add(response.getData().getSubSkills().get(i).getName());
                        subSkillsAdapter.notifyDataSetChanged();
                    }
                    if (response.getData().getSubSkills().size() == 0) {
                        subSkillsAdapter.add(name);
                        subSkillsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onError(Call<JobSubSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSubSkillsResponse> call, JobSubSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(SearchCandidateActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(SearchCandidateActivity.this, EmployerTypeActivity2.class);
                        SearchCandidateActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {
                        Toast.makeText(SearchCandidateActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
*/

    public void setMin() {
        if (minTextVisible) {
            String tempMin = et_set_min.getText().toString();
            int tempMinInt = 0;
            if (tempMin.length() > 0) {
                tempMinInt = Integer.parseInt(tempMin);
            }

            if (tempMinInt <= AppConstants.MAX_SALARY) {
                float na = ((float) AppConstants.MAX_SALARY / (float) tempMinInt);
                float perc = ((float) tempMinInt / (float) AppConstants.MAX_SALARY) * 100;
                rangeSeekbar3.setMinValueFromData(perc);
                tv_min_value.setText(getResources().getString(R.string.Rs) + " " + tempMinInt);
            } else {
                int valStr = Integer.parseInt(String.valueOf(rangeSeekbar3.getSelectedMaxValue()));

                float perc = (float) valStr / (float) AppConstants.MAX_SALARY * 100;
                System.out.println("PERCE : " + perc);

                rangeSeekbar3.setMinValueFromData(perc);
                tv_min_value.setText(getResources().getString(R.string.Rs) + " " + rangeSeekbar3.getSelectedMaxValue());

            }
        }
    }

    public void setMax() {
        if (maxTextVisible) {
            String tempMax = et_set_max.getText().toString();
            int tempMaxInt = AppConstants.MAX_SALARY;
            int selectedMin = Integer.parseInt(rangeSeekbar3.getSelectedMinValue().toString());
            if (tempMax.length() > 0) {
                tempMaxInt = Integer.parseInt(tempMax);
            }
            if (tempMaxInt > AppConstants.MAX_SALARY) {
                rangeSeekbar3.setMaxValueFromData(AppConstants.MAX_SALARY);
                tv_max_value.setText(getResources().getString(R.string.Rs) + " " + AppConstants.MAX_SALARY);

            } else if (tempMaxInt < selectedMin){
                float perc = ((float) selectedMin / (float) AppConstants.MAX_SALARY) * 100;
                rangeSeekbar3.setMaxValueFromData(perc);
                tv_max_value.setText(getResources().getString(R.string.Rs) + " " + selectedMin);

            } else {
                float perc = ((float) tempMaxInt / (float) AppConstants.MAX_SALARY) * 100;
                rangeSeekbar3.setMaxValueFromData(perc);
                tv_max_value.setText(getResources().getString(R.string.Rs) + " " + tempMaxInt);
            }
        }
    }
    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION))
        {

            //   Toast.makeText(this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,EXTERNAL_STORAGE_PERMISSIONS, RequestPermissionCode);

        }


    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //        Toast.makeText(this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    //      Toast.makeText(this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    @OnItemSelected(R.id.ms_subskills)
    public void subskillSelected(Spinner spinner, int position) {
        if (ms_subskills.getSelectedItem().toString() .contains("others") || ms_subskills.getSelectedItem().toString() .contains("Others") ) {
            ll_other_skills.setVisibility(View.VISIBLE);
        } else {
            ll_other_skills.setVisibility(View.GONE);
        }

    }



/*
    public void fetchSubSkills2(String id) {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {

            FetchSubskillsRequest request = new FetchSubskillsRequest();
            request.token = SharedPrefUtils.getApiKey(this);
            request.master_skill_id = id;
            RequestParams params = new RequestParams();
            params.put("token", request.token);
            params.put("master_skill_id", id);
            String url = RequestUrl.API_URL + RequestUrl.FETCH_SUBSKILL;
            AsyncHttpClient client = new AsyncHttpClient();
            client.post(url, params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    showProgressBar();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    System.out.println("responseSUBFetchDetails" + response);



                  /*  responseSUBFetchDetails{"status":{"response":"success","message":"","userMessage":""},
                  "data":{"subskills":[{"name":"ITI Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:23:51.014Z",
                  "updatedAt":"2019-07-24T13:23:51.014Z","id":"5d385be7eca7bf2b662e61ef"},
                  {"name":"Auto Electrician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:19.989Z","updatedAt":"2019-07-24T13:24:19.989Z",
                  "id":"5d385c03eca7bf2b662e61f0"},{"name":"Maintenance Electrician","status":"1",
                  "master_skill_id":"5d385ab8eca7bf2b662e61eb","createdAt":"2019-07-24T13:24:35.784Z",
                  "updatedAt":"2019-07-24T13:24:35.784Z","id":"5d385c13eca7bf2b662e61f1"},
                  {"name":"Electrician Technician","status":"1","master_skill_id":"5d385ab8eca7bf2b662e61eb",
                  "createdAt":"2019-07-24T13:24:53.174Z","updatedAt":"2019-07-24T13:24:53.174Z",
                  "id":"5d385c25eca7bf2b662e61f2"}]}}
                   */




               /* {"status":{"response":"success","message":"","userMessage":"","data":{"userProfile":{"name":"ravi pathak","email":"rpathaksoftware@gmail.com"},
                        "token":"5d1a4b006842de2e07803daa","status":""}}}*/

/*
                    try {
                        hideProgressBar();
                        JSONObject status=response.getJSONObject("status");
                        String respsuccess=status.getString("response");
                        if (respsuccess.equals("success")) {
                            //  System.out.println("_____ : " + response.data);
                            JSONObject subSkillObject=response.getJSONObject("data");
                            JSONArray subSkillArr=subSkillObject.getJSONArray("subskills");
                            optionsSubskills.clear();
                            for (int i =0;i < subSkillArr.length(); i++) {
                                JSONObject single_user = subSkillArr.getJSONObject(i);
                                String name = single_user.getString("name");
                                System.out.println("name" +name);
                                optionsSubskills.add(name);


                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter <String>(SearchCandidateActivity.this, android.R.layout.simple_list_item_multiple_choice, optionsSubskills);
                            ms_subskills.setListAdapter(adapter);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //CommonMethod.showAlert("No Detail Data Found ", CricketFixtureDetailActivity.this);


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable
                        throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    CommonMethod.showAlert("Unable to connect the server,please try again", SearchCandidateActivity.this);
                    hideProgressBar();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    hideProgressBar();
                }
            });
        }

    }
*/


    public void fetchSubSkills2() {
        if (SharedPrefUtils.getAppState(this) != AppConstants.STATE_REGISTERED) {
            return;
        } else {
            FetchSkillsRequest request = new FetchSkillsRequest();
            OROApiAdapter.OROApiService service = OROApiAdapter.getService();
            request.setToken(SharedPrefUtils.getApiKey(this));

            Call<JobSkillsResponse> apiResponseCall;
            apiResponseCall =
                    service.fetchSkills(request);
            apiResponseCall.enqueue(new RetrofitRequestListener<JobSkillsResponse>(apiResponseCall) {
                @Override
                protected void onStartApi() {
                    showProgressBar();
                }

                @Override
                protected void onEndApi() {
                    hideProgressBar();
                }

                @Override
                public void onSuccess(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    jobSkillsLists = response.getData().getSkills();
                    optionsSubskills.clear();

                    for (int i = 0; i < response.getData().getSkills().size(); i++) {
                        System.out.println("respCandidate|||| : " + response.getData().getSkills().get(i).getName());
                        optionsSubskills.add(response.getData().getSkills().get(i).getName().trim());

                    }
                   // optionsSubskills.get(optionsSubskills-1);
                   // optionsSubskills.remove(optionsSubskills.size());
                    ArrayAdapter<String> adapter = new ArrayAdapter <String>(SearchCandidateActivity.this, android.R.layout.simple_list_item_multiple_choice,  optionsSubskills);
                    ms_subskills.setListAdapter(adapter);
                }

                @Override
                public void onError(Call<JobSkillsResponse> call, Throwable t) {
                    System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                    Toast toast = Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    //Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onCustomError(Call<JobSkillsResponse> call, JobSkillsResponse response) {
                    if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                        SharedPrefUtils.clearSharedPreference(SearchCandidateActivity.this);
                        tinyDB.putString("username","");
                        tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                        Intent intent = new Intent(SearchCandidateActivity.this, EmployerTypeActivity2.class);
                        SearchCandidateActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(intent);
                    } else {

                    }
                }

            });
        }
    }

    @OnClick({R.id.ll_action_button,R.id.ll_freelancer, R.id.ll_part_time, R.id.ll_full_time, R.id.ll_min, R.id.ll_max,R.id.tv_location_val, R.id.tv_min_salary})
    public void gotoFunction(View view) {
        switch (view.getId()) {
            case R.id.ll_action_button:
                searchAcutualCandidate();

                break;
            case R.id.ll_freelancer:
                if (freelance) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_freelance.setImageDrawable(drawable );
                    freelance  =true;
                }
                break;
            case R.id.ll_part_time:
                if (part) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_part_time.setImageDrawable(drawable );
                    part  =true;
                }
                break;
            case R.id.ll_full_time:
                if (full) {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_uncheck";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = false;

                } else {
                    Resources res = this.getResources();
                    String mDrawableName = "ic_check_blue";
                    int resID = res.getIdentifier(mDrawableName , "drawable", this.getPackageName());
                    Drawable drawable = res.getDrawable(resID );
                    iv_full_time.setImageDrawable(drawable );
                    full = true;
                }
                break;
            case R.id.ll_min:
                //  rangeSeekbar3.setNor(10);
                ll_min.setVisibility(View.GONE);
                ll_min1.setVisibility(View.VISIBLE);
                minTextVisible = true;
                // tv_min_value.setText(getResources().getString(R.string.Rs) +  );

                /*rangeSeekbar3.setLeft(0);
                tv_min_value.setText(getResources().getString(R.string.Rs) + " 0");*/
                break;
            case R.id.ll_max:
                ll_max.setVisibility(View.GONE);
                ll_max1.setVisibility(View.VISIBLE);
                maxTextVisible = true;
                //rangeSeekbar3.setMaxStartValue();
               /* rangeSeekbar3.setRight(30000);
                tv_max_value.setText(getResources().getString(R.string.Rs) + " 30000");*/
                break;
            case R.id.tv_location_val:
                onAddPlaceButtonClicked(tv_location_val);
                break;
            case R.id.tv_min_salary:
                openPopup();


        }
    }
    public void openPopup() {
        ArrayList<FilterObject> educationList = new ArrayList<FilterObject>();
        FilterAdapters educationAdapter = new FilterAdapters(this, educationList, "education");;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = (this).getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the
        // dialog layout
        builder.setTitle("DELETE");
        builder.setCancelable(false);
        builder.setIcon(R.drawable.ic_location);
        builder.setView(inflater.inflate(R.layout.fragment_basic_list, null))
                // Add action buttons
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                }
    });
    builder.create();
    builder.show();
    }
    public void onAddPlaceButtonClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.error_msg), Toast.LENGTH_LONG).show();
            return;
        }
        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void addLocationData() {
        final ArrayList<String> jobCategories = new ArrayList<String>();

        FetchLocationRequest request = new FetchLocationRequest();
        OROApiAdapter.OROApiService service = OROApiAdapter.getService();
        request.setToken(SharedPrefUtils.getApiKey(this));
        Call<LocationResponse> apiResponseCall;
        apiResponseCall =
                service.fetchLocation(request);
        apiResponseCall.enqueue(new RetrofitRequestListener<LocationResponse>(apiResponseCall) {
            @Override
            protected void onStartApi() {
                showProgressBar();
            }

            @Override
            protected void onEndApi() {
                hideProgressBar();
            }

            @Override
            public void onSuccess(Call<LocationResponse> call, LocationResponse response) {
                /*for (int i = 0; i < response.getData().getLocationList().size(); i++) {
                    jobCategories.add(response.getData().getLocationList().get(i).getName());
                    FilterObject filterObject = new FilterObject(jobCategories.get(i),false);
                    locationList.add(filterObject);
                    locationAdapter.notifyDataSetChanged();
                }*/

                ArrayList<String> options = new ArrayList<>();

                for (int i = 0; i < response.getData().getLocationList().size(); i++) {

                    options.add(response.getData().getLocationList().get(i).getName().trim());

                }
                ArrayAdapter<String> adapter = new ArrayAdapter <String>(SearchCandidateActivity.this, android.R.layout.simple_list_item_multiple_choice, options);
                ms_location.setListAdapter(adapter);
            }

            @Override
            public void onError(Call<LocationResponse> call, Throwable t) {
                System.out.println(" !!!!!!!!!! asdeve " + t.toString());
                Toast toast = Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.record_not_found), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                //Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.error_msg),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCustomError(Call<LocationResponse> call, LocationResponse response) {
                if (response.status.message .equalsIgnoreCase(AppConstants.LOGOUT_MSG)) {
                    SharedPrefUtils.clearSharedPreference(SearchCandidateActivity.this);
                    tinyDB.putString("username","");
                    tinyDB.putString(AppConstants.USER_TYPE_MAIN, AppConstants.USER_EMPLOYER);
                    Intent intent = new Intent(SearchCandidateActivity.this, EmployerTypeActivity2.class);
                    SearchCandidateActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    startActivity(intent);
                } else {
                    Toast.makeText(SearchCandidateActivity.this, response.status.userMessage,Toast.LENGTH_LONG).show();
                }
            }

        });


        /*String[] location = getResources().getStringArray(R.array.location);
        for (int i =0; i < location.length; i++) {
            FilterObject filterObject = new FilterObject(location[i],false);
            locationList.add(filterObject);
        }
        locationAdapter.notifyDataSetChanged();*/
    }

    public void searchAcutualCandidate() {
        String subskill = "";

       /* String skills = ms_subskills.getSelectedItem().toString();
        if (skills.contains("others") || skills.contains("Others")) {
            if (et_other_skills.getText().toString().length() < 2) {
                Toast.makeText(this, getResources().getString(R.string.enter_other_skills_text), Toast.LENGTH_LONG).show();
                return;
            }
            skills =  skills + "," + et_other_skills.getText().toString();
        }*/

        String skills="";
        if(ms_subskills.getSelectedItem().toString().length()>0) {
            skills = ms_subskills.getSelectedItem().toString();
        }

        String location="";
        if(ms_location.getSelectedItem().toString().length()>0) {
            location = ms_location.getSelectedItem().toString();
        }
        String minSalary = et_min_salary.getText().toString();
        String maxSalary = et_max_salary.getText().toString();
        String minExp = ss_min_exp.getSelectedItem().toString();
        String maxExp = ss_max_exp.getSelectedItem().toString();
       // Toast.makeText(SearchCandidateActivity.this, "size : " + location.length(), Toast.LENGTH_LONG).show();


        if (subskill.length() < 3) {
            /*Toast.makeText(SearchCandidateActivity.this, getResources().getString(R.string.select_subskills_first_text),Toast.LENGTH_LONG).show();
            return;*/
        }

        String jobType = "";
        if (full) {
            jobType = jobType + getResources().getString(R.string.fulltime_text) + ",";
        }
        if (part) {
            jobType = jobType + getResources().getString(R.string.parttime_text) + ",";
        }
        if (freelance) {
            jobType = jobType + getResources().getString(R.string.freelancer_text);
        }
        System.out.println("respskill"+skills+location+jobType);
//        String education = ms_education.getSelectedItem().toString();
        String education="";
         //education = ms_education.getSelectedItem().toString();
        if(ms_education.getSelectedItem().toString().length()>0) {
            education = ms_education.getSelectedItem().toString();
        }
        System.out.println("respskill"+skills+location+education+minSalary+maxSalary+minExp+maxExp+jobType);
        Intent i = new Intent(this, SearchCandidateResultActivity.class);
        i.putExtra("skill", skills);
        i.putExtra("subskills", subskill);
        i.putExtra("location", location);
        i.putExtra("minSalary", minSalary);
        i.putExtra("maxSalary", maxSalary);
        i.putExtra("minExp",minExp);
        i.putExtra("maxExp",maxExp);
        i.putExtra("education",education);
        i.putExtra("jobType", jobType);
        startActivity(i);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, EmployerDashboardActivity.class);
        startActivity(intent);
        this.finish();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            String placeID = place.getId();


            // Get live data information
            refreshPlacesData(place);
        }
    }

    public void refreshPlacesData(Place place) {
        lat = String.valueOf(place.getLatLng().latitude);
        lon = String.valueOf(place.getLatLng().longitude);
        tv_location_val.setText(place.getAddress());
        name = String.valueOf(place.getName());
        System.out.println("sgEGGGGG : " + place.getLatLng());

        System.out.println("sgEGGGGG : " + lat);
        System.out.println("sgEGGGGG : " + lon);

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
