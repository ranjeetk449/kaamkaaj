package com.kaamkaaj.kaamkaaj.dbs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kaamkaaj.kaamkaaj.AppConstants;


/**
 * Created by personal on 28/1/17.
 */

public class NewUsersDbs extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "kaamkaaj";
    private static final String PLAYLIST_TABLE_NAME = "new_users";
    private static final String PLAYLIST_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS new_users(user_name TEXT, unique_id TEXT)";

    private String unique_id, user_name;
    public NewUsersDbs(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*db.execSQL(PLAYLIST_TABLE_QUERY);*/
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void add_new_user(String user_name, String unique_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(PLAYLIST_TABLE_QUERY);
        db.execSQL("INSERT INTO new_users VALUES('" + user_name + "','" + unique_id + "');");
    }

    public String fetch_user_id() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(PLAYLIST_TABLE_QUERY);
        String result= "";
        String query = "SELECT DISTINCT unique_id FROM new_users";
        Cursor listNames = db.rawQuery(query, null);
        if (listNames.getCount() == 0) {
            return AppConstants.FAILED_ID_MSG;
        }
        for (int i = 0; i < listNames.getCount(); i++) {
            listNames.moveToPosition(i);
            result = listNames.getString(0);
        }
        return result;
    }

}