package com.kaamkaaj.kaamkaaj.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.kaamkaaj.kaamkaaj.R;

import org.greenrobot.eventbus.EventBus;



public class AlertDialogManager {
    private static AlertDialogManager mInstance = new AlertDialogManager();
    private static AlertDialog mDialog;
    private static final String TAG = "AlertDialogManager";

    private AlertDialogManager() {
    }

    public static AlertDialogManager getInstance() {
        return mInstance;
    }

    public synchronized void show(final Context ctx, int title, int message,
                                  DialogInterface.OnClickListener retryListener,
                                  DialogInterface.OnClickListener cancelListener) {

        if (mDialog != null) {
            Log.d(TAG, "Dialog already visible");
            return;
        }

        if (mDialog == null) {
            Log.d(TAG, "Creating new dialog");
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

            builder.setMessage(ctx.getResources().getString(message))
                    .setTitle(ctx.getResources().getString(title))
                    .setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            hide();
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.EVENT_RETRY));
                        }
                    })
                    .setNegativeButton("CANCEL", cancelListener)
                    .setCancelable(false);

            mDialog = builder.create();
            //mDialog.getButton(AlertDialog.BUTTON_POSITIVE).
            //      setTextColor(ContextCompat.getColor(ctx, R.color.colorAccent));
            mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    mDialog.getButton(AlertDialog.BUTTON_POSITIVE).
                            setTextColor(ctx.getResources().getColor(R.color.colorAccent));
                }
            });
        }

        if (!((Activity) ctx).isFinishing()) {
            //show dialog
            Log.d(TAG, "Showing dialog");
            mDialog.show();
        }

    }

    public synchronized void hide() {

        if (mDialog != null) {
            Log.d(TAG, "Deleting dialog");
            mDialog.dismiss();
        }

        mDialog = null;
    }
}
