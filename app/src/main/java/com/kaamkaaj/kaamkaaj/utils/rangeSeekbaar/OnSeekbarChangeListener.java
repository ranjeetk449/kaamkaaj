package com.kaamkaaj.kaamkaaj.utils.rangeSeekbaar;

/**
 * Created by akshaysingh on 5/25/18.
 */

public interface OnSeekbarChangeListener {
    void valueChanged(Number value);
}
