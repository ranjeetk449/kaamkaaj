package com.kaamkaaj.kaamkaaj.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.R;
import com.kaamkaaj.kaamkaaj.widgets.RangeTimePickerDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


//import akshay.com.cdsmnew.ui.activities.user.LoginActivity;


public class Utils {
    private static final String TAG = "Utils";
    private static final int MIN_ACCURACY = 100; //meters

    private static final String EXTERNAL_STORAGE_PERMISSIONS[] = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static void showSnackBar() {

    }


    public static void openDialer(Context ctx, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        ctx.startActivity(intent);
    }


    public static float convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static void sendEmail(Context ctx, String to, String subject, String message, String prompt)
            throws android.content.ActivityNotFoundException {

        Intent send = new Intent(Intent.ACTION_SENDTO);
        String uriText = "mailto:" + Uri.encode(to) +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(message);
        Uri uri = Uri.parse(uriText);

        send.setData(uri);
        ctx.startActivity(Intent.createChooser(send, prompt));
    }

    public static String getFormattedDate(String date) {
        if (TextUtils.isEmpty(date)) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

        Date datetime = null;
        try {
            datetime = sdf.parse(date);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
            return "";
        }

        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(datetime);
    }


    public static void hideSoftKeyboard(Context mContext, EditText input) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Context mContext, EditText input) {
        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE))
                .toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void setCustomFontsForTextView(Context mContext, TextView mTextView, String fonts) {
        Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        mTextView.setTypeface(typeFace);
    }

    public static void setCustomFontsForEditText(Context mContext, EditText mEditText, String fonts) {
        Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        mEditText.setTypeface(typeFace);
    }

    public static void setCustomFontsForButton(Context mContext, Button mButton, String fonts) {
        Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        mButton.setTypeface(typeFace);
    }

    public static void setCustomFontsForRadioButton(Context mContext, RadioButton mRadioButton, String fonts) {
        Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        mRadioButton.setTypeface(typeFace);
    }

    public static void setCustomFontsForCheckBox(Context mContext, CheckBox mCheckBox, String fonts) {
        Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        mCheckBox.setTypeface(typeFace);
    }

    public static void changeToolbarTitleFont(Context mContext, Toolbar mToolbar) {
        TextView toolbarTitle = null;
        for (int i = 0; i < mToolbar.getChildCount(); ++i) {
            View child = mToolbar.getChildAt(i);

            // assuming that the title is the first instance of TextView
            // you can also check if the title string matches
            if (child instanceof TextView) {
                toolbarTitle = (TextView) child;
                setCustomFontsForTextView(mContext, toolbarTitle, AppConstants.FONTS_LATO_HEAVY_TTF);
                break;
            }
        }
    }

    public static void changeTabsFont(Context mContext, TabLayout mTabLayout, String fonts) {

        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Typeface tFace = Typeface.createFromAsset(mContext.getAssets(), fonts);
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tFace, Typeface.NORMAL);
                }
            }
        }
    }


    public static ProgressDialog showProgressDialog(
            Context ctx, boolean indeterminate, boolean cancelable, int style, String msg) {
        ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setIndeterminate(indeterminate);
        progressDialog.setCancelable(cancelable);
        progressDialog.setProgressStyle(style);
        progressDialog.setMessage(msg);
        progressDialog.show();

        return progressDialog;
    }

    public static Snackbar showSnackBar(View root, String message, int length) {
        Snackbar snackbar = Snackbar.make(root, message, length);
        snackbar.show();
        return snackbar;

    }

    public static String translateError(String err) {
        String error = null;
        //translate error msg into user friendly one
        if (err.equals("invalid-code")) {
            error = "Invalid code";
        } else if (err.equals("invalid-number")) {
            error = "Invalid number";
        } else if (err.equals("invalid-referral-code")) {
            error = "Invalid Referral Code";
        } else {
            error = err;
        }

        return error;
    }

    public static void validateTextField(
            String field,
            String alias,
            boolean isNumeric,
            boolean isAlpha,
            int length,
            int minLength,
            int maxLength
    ) {
        if (TextUtils.isEmpty(field)) {
            if (alias.equalsIgnoreCase("pincode")) {
                throw new IllegalArgumentException("Please enter a 6 digit pincode");
            } else if (alias.equalsIgnoreCase("Distributor City")) {
                throw new IllegalArgumentException("Distributor City should not be empty");
            } else if (alias.equalsIgnoreCase("Product Company Name")) {
                throw new IllegalArgumentException("Product Company Name should not be empty");
            } else {
                throw new IllegalArgumentException("Please enter a valid " + alias.toLowerCase() +
                        " (Minimum " + minLength + " characters)");
            }
        }

        if (length != -1) {
            if (field.length() != length) {
                if (alias.equalsIgnoreCase("pincode")) {
                    throw new IllegalArgumentException("Please enter a 6 digit pincode");
                } else if (alias.equalsIgnoreCase("Distributor City")) {
                    throw new IllegalArgumentException("Distributor City should not be empty");
                } else if (alias.equalsIgnoreCase("Product Company Name")) {
                    throw new IllegalArgumentException("Product Company Name should not be empty");
                } else {
                    throw new IllegalArgumentException("Please enter a valid " + alias.toLowerCase() +
                            " (Minimum " + minLength + " characters)");
                }
            }
        }

        if (minLength != -1) {
            if (field.length() < minLength) {
                if (alias.equalsIgnoreCase("pincode")) {
                    throw new IllegalArgumentException("Please enter a 6 digit pincode");
                } else if (alias.equalsIgnoreCase("Distributor City")) {
                    throw new IllegalArgumentException("Distributor City should not be empty");
                } else if (alias.equalsIgnoreCase("Product Company Name")) {
                    throw new IllegalArgumentException("Product Company Name should not be empty");
                } else {
                    throw new IllegalArgumentException("Please enter a valid " + alias.toLowerCase() +
                            " (Minimum " + minLength + " characters)");
                }
            }
        }

        if (maxLength != -1) {
            if (field.length() > maxLength) {
                throw new IllegalArgumentException("Please enter a valid " +
                        alias.toLowerCase() + " (Minimum " + minLength + " characters)");
            }
        }
    }

    public static void emailValidator(String email) {

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            throw new IllegalArgumentException("Please enter a valid email");
        }
    }

    public static String getDeviceId(Context ctx) {
        return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String replaceNull(String input) {
        return input == null ? "" : input;
    }

    public static String getCurrentTime() {

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());

        return formattedDate;
    }

    public static Date getCurrentDateFromString(String strDate) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.getDefault());
        Date date = null;
        try {
            date = df.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getFirstDateFromString(String strDate) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = df.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getNotificationFormattedDate(String date) {
        if (TextUtils.isEmpty(date)) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", Locale.getDefault());

        Date datetime = null;
        try {
            datetime = sdf.parse(date);
        } catch (ParseException e) {
            //Log.e(TAG, e.getMessage());

            return "";
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        return sdf1.format(datetime);
    }

    public static File storeScreenShot(Bitmap bm, String fileName) {
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static void shareScreenShotUrl(Context context, View mRootView, String fileName, String subject, String url) {

        if (PermissionUtils.hasSelfPermission((Activity) context, EXTERNAL_STORAGE_PERMISSIONS)) {
            Bitmap bm = getScreenShot(mRootView);
            File file = storeScreenShot(bm, fileName);
            Log.i("chase", "filepath: " + file.getAbsolutePath());
            Uri uri = Uri.fromFile(new File(file.getAbsolutePath()));
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, "Download Reap App for Chemist - " + url);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            context.startActivity(Intent.createChooser(intent, "Share"));
        } else {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, "Download Reap App for Chemist - " + url);
            context.startActivity(Intent.createChooser(i, "Share"));
        }
    }


    public static float dp2px(Context context, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }

    public static float sp2px(Context context, float sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                context.getResources().getDisplayMetrics());
    }

    public static void openShareDialog(Context mCtx) {
        String uriShare = "http://play.google.com/store/apps/details?id="
                + mCtx.getPackageName();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "KidzPlus Teacher");
        i.putExtra(Intent.EXTRA_TEXT, "Download KidzPlus Teacher App " +
                "now - " + uriShare);
        mCtx.startActivity(Intent.createChooser(i, "Share via"));
    }

    /*public static String getGoogleEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account = null;
        try {
            if (accounts.length > 0) {
                account = accounts[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }*/


    private static Calendar calendar;
    private static int mYear, mMonth, mDay;

    @SuppressWarnings("deprecation")
    public static void select_date(final TextView tv_date, Context context) {
        // TODO Auto-generated method stub
        // Process to get Current Date
        // final Calendar c = Calendar.getInstance();
        int mYear, mMonth, mDay;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        // c.add(Calendar.DAY_OF_MONTH, -6);

        try {

            String date = tv_date.getText().toString();
            Date showdate = getDate(date);
            mYear = showdate.getYear() + 1900;
            mMonth = showdate.getMonth();
            mDay = showdate.getDate();

            System.out.println(date);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox


                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tv_date.setText(dateFormatter.format(newDate.getTime()));

                        }

                    }, mYear, mMonth, mDay);
            // dpd.getDatePicker().setMaxDate(new Date().getTime());

            // Date mindate = getDate(date);

            // edt_date.setText(str_date);

            //dpd.getDatePicker().setMaxDate(new Date().getTime());
            dpd.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);


            dpd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void select_future_date(Context context, final TextView tv_date) {
        // TODO Auto-generated method stub
        // Process to get Current Date
        int mYear, mMonth, mDay;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        try {

            String date = tv_date.getText().toString();
            Date showdate = getDate(date);
            mYear = showdate.getYear() + 1900;
            mMonth = showdate.getMonth();
            mDay = showdate.getDate();

            System.out.println(date);

            // Launch Date Picker Dialog
            DatePickerDialog dpd = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            /*String temp_date = year + "-"
                                    + (monthOfYear + 1) + "-" + dayOfMonth;


                            tv_date.setText(temp_date);*/

                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tv_date.setText(dateFormatter.format(newDate.getTime()));

                        }

                    }, mYear, mMonth, mDay);
            // dpd.getDatePicker().setMaxDate(new Date().getTime());

            // Date mindate = getDate(date);

            // edt_date.setText(str_date);

            dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


            dpd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static Date getDate(String date) {
        Date date1 = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date1 = dateFormat.parse(date);
            // System.out.println(date1);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date1;

    }


    public static String getDOBDate(String date) {

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date da = null;
        try {
            da = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        String strDateTime = outputFormatter.format(da);

        return strDateTime;
    }

    public static String getDMYDate(String date) {

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date da = null;
        try {
            da = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        DateFormat outputFormatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDateTime = outputFormatter.format(da);

        return strDateTime;
    }

    public static String getYMDDate(String date) {

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd-MM-yyyy");
        Date da = null;
        try {
            da = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        DateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDateTime = outputFormatter.format(da);

        return strDateTime;
    }

    @SuppressLint("SimpleDateFormat")
    public static Date getTime(String date) {
        Date date1 = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
        try {
            date1 = dateFormat.parse(date);
            // System.out.println(date1);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date1;

    }

    public static void select_time(Context context, final TextView tv_time, boolean is24HrFormat) {

        int mStartHr, mStartMin;
        boolean is24HourFormat = is24HrFormat;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm aa", Locale.US);
        // Get Current Time
        String time = tv_time.getText().toString();
        Date showTime = getTime(time);

        mStartHr = showTime.getHours();
        mStartMin = showTime.getMinutes();
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                updateTime(hourOfDay, minute, tv_time);

            }
        }, mStartHr, mStartMin, false);

        timePickerDialog.show();
    }

    public static void select_range_time(Context context, final TextView tv_time, String limit, boolean is24HrFormat) {

        int mStartHr, mStartMin, mRangeHr, mRangeMin;
        boolean is24HourFormat = is24HrFormat;
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm aa", Locale.US);
        // Get Current Time
        String time = tv_time.getText().toString();
        Date showTime = getTime(time);

        mStartHr = showTime.getHours();
        mStartMin = showTime.getMinutes();

        Date rangeTime = getTime(limit);
        mRangeHr = rangeTime.getHours();
        mRangeMin = rangeTime.getMinutes();

        RangeTimePickerDialog timePickerDialog = new RangeTimePickerDialog(
                context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                updateTime(hourOfDay, minute, tv_time);

            }
        }, mStartHr, mStartMin, false);
        timePickerDialog.setMin(mRangeHr, mRangeMin);
        timePickerDialog.show();
    }

    static void updateTime(int hours, int mins, TextView tv_time) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        tv_time.setText(aTime);
    }

    public static String getCurrentDate(String format) {
        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        final SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.US);

        String str_date = dateFormatter.format(calendar.getTime());
        // edt_date.setText(str_date);
        return str_date;
    }

    public static String getCurrentTimeInHrsMin() {
        int hours, mins;
        calendar = Calendar.getInstance();
        hours = calendar.get(Calendar.HOUR_OF_DAY);
        mins = calendar.get(Calendar.MINUTE);

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        return aTime;
    }


    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static void invalidTokenError(Context mContext) {
  /*      SharedPrefUtils.clearSharedPreference(mContext);
        Intent i = new Intent(mContext, LoginActivity.class);
        i.setFlags(i.FLAG_ACTIVITY_CLEAR_TOP | i.FLAG_ACTIVITY_SINGLE_TOP | i.FLAG_ACTIVITY_CLEAR_TASK | i.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
        ((Activity) mContext).finish();*/
    }

    public static void onSessionExpireAlert(final Context mContext, String message) {

        AlertDialog.Builder del_builder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
        del_builder.setTitle(mContext.getResources().getString(R.string.app_name));
        del_builder.setMessage(message);
        del_builder.setCancelable(false);
        del_builder.setNegativeButton("dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                invalidTokenError(mContext);
            }
        });
        final AlertDialog dialog = del_builder.create();
        dialog.show();
    }

    public static void onLogoutAlert(final Context mContext, String message) {

        AlertDialog.Builder del_builder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
        del_builder.setTitle(mContext.getResources().getString(R.string.app_name));
        del_builder.setMessage(message);
        del_builder.setCancelable(false);
        del_builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                invalidTokenError(mContext);
            }
        });
        del_builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        final AlertDialog dialog = del_builder.create();
        dialog.show();
    }


    public static String getTransactionDate(String date) {
        //02-Aug-2016 12:25:34
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        Date da = null;
        try {
            da = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        DateFormat outputFormatter = new SimpleDateFormat("dd MMM yyyy");
        String strDateTime = outputFormatter.format(da);

        return strDateTime;
    }

    public static String getTransactionDetailsDate(String date) {
        //02-Aug-2016 12:25:34
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        Date da = null;
        try {
            da = inputFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        DateFormat outputFormatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDateTime = outputFormatter.format(da);

        return strDateTime;
    }

    //Snippet for checkPlayServices method below
    public static boolean checkPlayServices(Activity activityContext) {
        int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activityContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activityContext, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                activityContext.finish();
            }
            return false;
        }
        return true;
    }

    public static String getCurrentTime(String format) {

        SimpleDateFormat df = new SimpleDateFormat(format, Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());

        return formattedDate;
    }

}
