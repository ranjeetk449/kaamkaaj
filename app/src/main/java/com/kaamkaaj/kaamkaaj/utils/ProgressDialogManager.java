package com.kaamkaaj.kaamkaaj.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;


public class ProgressDialogManager {
    private static ProgressDialogManager mInstance = new ProgressDialogManager();

    private static ProgressDialog mDialog;
    private static boolean mIsShowing = false;
    private static final String TAG = "ProgressDialogManager";

    private ProgressDialogManager() {
    }

    public static ProgressDialogManager getInstance() {
        return mInstance;
    }

    public synchronized void show(Context ctx) {
        if (mDialog != null) {
            return;
        }

        Log.d(TAG, "Showing progress");
        mDialog = Utils.showProgressDialog(ctx, true, false, ProgressDialog.STYLE_SPINNER, "Please wait");
    }

    public synchronized void hide() {
        Log.d(TAG, "Hiding progress");
        if ((mDialog != null) && mDialog.isShowing()) {
            mDialog.dismiss();
        }

        mDialog = null;
    }
}
