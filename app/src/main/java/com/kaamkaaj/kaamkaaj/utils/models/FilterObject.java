package com.kaamkaaj.kaamkaaj.utils.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 4/24/18.
 */

public class FilterObject implements Parcelable {
    String element;
    boolean checked;

    public FilterObject(Parcel in) {
        element = in.readString();
        checked = in.readByte() != 0;
    }

    public static final Creator<FilterObject> CREATOR = new Creator<FilterObject>() {
        @Override
        public FilterObject createFromParcel(Parcel in) {
            return new FilterObject(in);
        }

        @Override
        public FilterObject[] newArray(int size) {
            return new FilterObject[size];
        }
    };

    public FilterObject(String element, boolean checked) {
        this.element = element;
        this.checked = checked;

    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(element);
        parcel.writeByte((byte) (checked ? 1 : 0));
    }
}
