package com.kaamkaaj.kaamkaaj.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.kaamkaaj.kaamkaaj.AppConstants;
import com.kaamkaaj.kaamkaaj.restapi.response.jobs.JobList;


public class SharedPrefUtils {

    public static void clearSharedPreference(Context ctx) {
        setPhoneNumber(ctx, "");
        setAppState(ctx, AppConstants.STATE_INITIAL);
        setApiKey(ctx, null);
        clearFilters(ctx);

    }

    public static void saveAppliedJob(Context ctx,  JobList job) {
        /// FLOW : store list in "MyJobListData" not "JobList"
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
    }

    /*public static MyJobListData jobListData(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        String alreadySaved = prefs.getString(AppConstants.APPLIED_JOBS_LIST,"");
    }*/

    public static void setFilterSkills(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_SKILLS,skills);
        editor.commit();
    }
    public static void setFilterLocation(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_LOCATION,skills);
        editor.commit();
    }
    public static void setFilterMinSalary(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_MIN_SALARY,skills);
        editor.commit();
    }
    public static void setFilterMaxSalary(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_MAX_SALARY,skills);
        editor.commit();
    }
    public static void setFilterMinExp(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_MIN_EXP,skills);
        editor.commit();
    }
    public static void setFilterMaxExp(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_MAX_EXP,skills);
        editor.commit();
    }
    public static void setFilterEducation(Context ctx, String skills) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.FILTER_EDUCATION,skills);
        editor.commit();
    }
    public static String getFilterSkills(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_SKILLS,"");
    }
    public static String getFilterLocation(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_LOCATION,"");
    }
    public static String getFilterMinSalary(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_MIN_SALARY,"");
    }
    public static String getFilterMaxSalary(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_MAX_SALARY,"");
    }
    public static String getFilterMinExp(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_MIN_EXP,"");
    }
    public static String getFilterMaxExp(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_MAX_EXP,"");
    }
    public static String getFilterEducation(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.FILTER_EDUCATION,"");
    }

    public static void clearFilters(Context context) {
        setFilterSkills(context, "");
        setFilterLocation(context,"");
        setFilterMaxSalary(context,"");
        setFilterMinSalary(context,"");
        setFilterMinExp(context,"");
        setFilterMaxExp(context,"");
        setFilterEducation(context,"");
    }

    public static void setUserAnonymousIds(Context ctx, String ids) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.JOHN_DOE_RANDOM_ID,ids);
        editor.commit();
    }
    public static String getAnonymousUserId(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.JOHN_DOE_RANDOM_ID,"");
    }

    public static void setEmployerType(Context ctx, String type) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.USER_TYPE, type);
        editor.commit();
    }

    public static String getEmployerType(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.USER_TYPE,"");
    }

    public static String getUserName(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.USER_NAME, "");
    }

    public static void setUserAnonymous(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(AppConstants.STATE, AppConstants.STATE_ANONYMOUS);
        editor.putString(AppConstants.USER_NAME,AppConstants.ANONYMOUS_USER_NAME);
        editor.commit();
    }
    public static void setPhoneNumber(Context ctx, String num) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.PHONE_NUMBER, num);
        editor.commit();
    }

    public static String getPhoneNumber(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.PHONE_NUMBER, "");
    }

    public static int getAppState(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getInt(AppConstants.STATE, AppConstants.STATE_INITIAL);
    }


    public static void setAppState(Context ctx, int state) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(AppConstants.STATE, state);
        editor.commit();
    }


    public static String getCan(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.CAN,"");
    }


    public static void setCan(Context ctx, String state) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.CAN, state);
        editor.commit();
    }

    public static void removeCan(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(AppConstants.CAN);
        editor.commit();
    }

    public static void setCANSelectedPosition(Context ctx, int classPosition) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(AppConstants.CAN_POSITION, classPosition);
        editor.commit();
    }

    public static int getCANSelectedPosition(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getInt(AppConstants.CAN_POSITION, 0);
    }

    public static void removeCanPosition(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(AppConstants.CAN_POSITION);
        editor.commit();
    }

    public static boolean getPromoState(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(AppConstants.PROMO_STATE, false);
    }

    public static void setPromoState(Context ctx, boolean state) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AppConstants.PROMO_STATE, state);
        editor.commit();
    }

    public static void setUserLoginType(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.USERlOGIN_TYPE, apiKey);
        editor.commit();
    }

    public static String getUserLoginType(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.USERlOGIN_TYPE, null);
    }




    public static void setEmployerMobile(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.EMPLOYER_MOBILE, apiKey);
        editor.commit();
    }


    public static String getEmployerMobile(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.EMPLOYER_MOBILE, null);
    }



    public static void setEmployerName(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.EMPLOYER_NAME, apiKey);
        editor.commit();
    }


    public static String getEmployerName(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.EMPLOYER_NAME, null);
    }




    public static void setEmployerEmail(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.EMPLOYER_EMAIL, apiKey);
        editor.commit();
    }


    public static String getEmployerEmail(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.EMPLOYER_EMAIL, null);
    }






    public static void setApiKey(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.API_TOKEN, apiKey);
        editor.commit();
    }


    public static String getApiKey(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.API_TOKEN, null);
    }


    public static void setApiKeyEmployee(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.API_TOKEN_EMPLOYEE, apiKey);
        editor.commit();
    }

    public static String getNewApiKey(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.NEW_TOKEN_EMPLOYEE, null);
    }

    public static void setNewKeyEmployee(Context ctx, String apiKey) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.NEW_TOKEN_EMPLOYEE, apiKey);
        editor.commit();
    }


    public static String getApiKeyEmployee(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.API_TOKEN_EMPLOYEE, null);
    }


    public static void setUserStatus(Context ctx, String status) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.USER_STATUS, status);
        editor.commit();
    }

    public static void setTotalInvestment(Context ctx, String totalInvestment, String cur_value) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        /*editor.putString(AppConstants.TOTAL_INVESTMENT, totalInvestment);
        editor.putString(AppConstants.CUR_VALUE, cur_value);*/
        editor.commit();
    }
    public static String getTotalInvestment(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.API_TOKEN, null);
    }

    public static void setRadius(Context ctx, int radius) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("radius", radius);
        editor.commit();
    }

    public static int getRadius(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getInt("radius", 20);
    }


    public static String getStatus(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.USER_STATUS, null);
    }

    public static void setSkippedVersion(Context ctx, boolean skippedVersionAvailable) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AppConstants.KEY_SKIPPED_VERSION_AVAILABLE, skippedVersionAvailable);
        editor.commit();
    }

    public static boolean getSkippedVersionAvailable(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(AppConstants.KEY_SKIPPED_VERSION_AVAILABLE, false);
    }

    public static void setLatestVersionAvailable(Context ctx, boolean latestVersionAvailable) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(AppConstants.KEY_LATEST_VERSION_AVAILABLE, latestVersionAvailable);
        editor.commit();
    }

    public static boolean getLatestVersionAvailable(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getBoolean(AppConstants.KEY_LATEST_VERSION_AVAILABLE, false);
    }

    public static void setLatestVersionType(Context ctx, String type) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.KEY_LATEST_VERSION_TYPE, type);
        editor.commit();
    }

    public static String getLatestVersionType(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.KEY_LATEST_VERSION_TYPE, AppConstants.UPDATE_NON_CRITICAL);
    }

    public static void setAboutUs(Context ctx, String type) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.AGREEMENT_TYPE_ABOUT_US, type);
        editor.commit();
    }

    public static String getAboutUs(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.AGREEMENT_TYPE_ABOUT_US, "");
    }

    public static void setTnC(Context ctx, String type) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.AGREEMENT_TYPE_TERMS, type);
        editor.commit();
    }

    public static String getTnC(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.AGREEMENT_TYPE_TERMS, "");
    }

    public static void setPP(Context ctx, String type) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AppConstants.AGREEMENT_TYPE_POLICY, type);
        editor.commit();
    }

    public static String getPP(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(AppConstants.PREF, Context.MODE_PRIVATE);
        return prefs.getString(AppConstants.AGREEMENT_TYPE_POLICY, "");
    }

}
