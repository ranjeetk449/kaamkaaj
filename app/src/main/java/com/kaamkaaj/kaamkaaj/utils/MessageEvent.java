package com.kaamkaaj.kaamkaaj.utils;


public class MessageEvent {


    public final int type;
    public static final int EVENT_SMS_RECEIVED = 0;
    public static final int EVENT_RETRY = 1;
    public static final int EVENT_DASHBOARD_STUDENT_REFRESHED = 2;
    public static final int EVENT_CHECKIN_STUDENT_REFRESHED = 3;
    public static final int EVENT_CHECKOUT_STUDENT_REFRESHED = 4;

    public Object data = null;

    public MessageEvent(int type) {
        this.type = type;
    }

    public MessageEvent(int type, Object data) {
        this.type = type;
        this.data = data;
    }
}
