package com.kaamkaaj.kaamkaaj.utils.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by akshaysingh on 3/26/18.
 */

public class JobCategory implements Parcelable {
    String img_name, disp_text;

    public JobCategory(Parcel in) {
        img_name = in.readString();
        disp_text = in.readString();
    }

    public static final Creator<JobCategory> CREATOR = new Creator<JobCategory>() {
        @Override
        public JobCategory createFromParcel(Parcel in) {
            return new JobCategory(in);
        }

        @Override
        public JobCategory[] newArray(int size) {
            return new JobCategory[size];
        }
    };

    public JobCategory(String img_name, String disp_text) {
        this.setDisp_text(disp_text);
        this.setImg_name(img_name);
    }

    public String getImg_name() {
        return img_name;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }

    public String getDisp_text() {
        return disp_text;
    }

    public void setDisp_text(String disp_text) {
        this.disp_text = disp_text;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(img_name);
        parcel.writeString(disp_text);
    }
}
